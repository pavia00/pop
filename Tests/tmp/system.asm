;?debug	macro    ;//ненастоящий макрос
;        endm

INCLUDELIB  IMPORT32    
	extern  GetStdHandle 	: proc
    extern  WriteFile 		: proc
    extern  ReadFile 		: proc
	extern  WriteConsoleW	: proc
	extern  ReadConsoleW	: proc
	extern  SetConsoleOutputCP	: proc	
    extern  ExitProcess 	: proc
;    extern  SwitchToThread 	: proc
    extern  SleepEx 		: proc
	
Code	SEGMENT	PARA PUBLIC "code" USE32


AssignString proc
        ARG _OutStr, _InStr:DWord
		PUSH EBP
		MOV  ebp, esp
		SUB  esp, 0
		MOV  EAX, [_InStr]
		MOV  EAX, [EAX] 
		MOV  EBX, [_OutStr]
		MOV [EBX], EAX
		MOV ESP, EBP
		POP EBP
		RET 8		
endp

CompareString proc
        ARG _OutStr, _InStr:DWord
		PUSH EBP
		MOV  ebp, esp
		SUB  esp, 0
		MOV  EAX, [_InStr]
		MOV  EAX, [EAX]
		MOV  EBX, [_OutStr]
		CMP [EBX], EAX
		MOV ESP, EBP
		POP EBP
		RET 8		
endp

;ConcatenateStringString proc
;        ARG _OutStr, _InStr:DWord
;		PUSH EBP
;		MOV  ebp, esp
;		SUB  esp, 0
;		MOV  EAX, [_InStr]
;		MOV  EAX, [EAX]
;		MOV  EBX, [_OutStr]
;		CMP [EBX], EAX
;		MOV ESP, EBP
;		POP EBP
;		RET 8		
;endp

;WriteStr proc
;        ARG Text:DWord
;wrCount	EQU (DWord PTR [EBP - 4])
;		PUSH EBP
;		mov     ebp, esp
;		sub     esp, 4
;											; WriteFile( hstdOut, message, length(message), &wrCount, 0);
;		push    0
;		lea     eax, wrCount
;		push    eax
;		MOV     eax, [Text]
;		push    DWORD PTR [eax-4]
;		push    eax
;		push    [hstdOut]
;		call    WriteConsoleW
;		MOV ESP, EBP
;		POP EBP
;		RET 4		
;endp

;WriteLnStr proc
;        ARG Text:DWord
;		PUSH EBP
;		mov  ebp, esp
;		MOV  EAX, TEXT
;		PUSh EAX
;		CALL WriteStr
;		PUSH NL
;		CALL WriteStr		
;		MOV ESP, EBP
;		POP EBP
;		RET 4		
;endp

;WriteLnInt proc
;        ARG Text:DWord
;		PUSH EBP
;		mov  ebp, esp
;		MOV  EAX, TEXT
;		PUSh EAX
;		CALL WriteStr
;		PUSH NL
;		CALL WriteStr		
;		MOV ESP, EBP
;		POP EBP
;		RET 4		
;endp

;ReadInt proc
;        ARG Value:DWord
;		PUSH EBP
;		mov  ebp, esp
;		MOV EAX, DWord PTR [Value]
;		MOV DWord PTR [EAX], 1234d
;		MOV ESP, EBP
;		POP EBP
;		RET 4			
;endp

;ReadLnInt proc
;        ARG Value:DWord
;		PUSH EBP
;		mov  ebp, esp
;		MOV EAX, DWord PTR [Value]
;		MOV DWord PTR [EAX], 1234d
;		MOV ESP, EBP
;		POP EBP
;		RET 4			
;endp

;ReadStr proc
;        ARG Value:DWord
;		PUSH EBP
;		mov  ebp, esp
;		MOV EAX, DWord PTR [Value]
;		MOV DWord PTR [EAX], 0
;		MOV ESP, EBP
;		POP EBP
;		RET 4			
;endp

;ReadLnStr proc
;        ARG Value:DWord
;		PUSH EBP
;		mov  ebp, esp
;		MOV EAX, DWord PTR [Value]
;		MOV DWord PTR [EAX], 0
;		MOV ESP, EBP
;		POP EBP
;		RET 4			
;endp

_start:
		ASSUME CS:Code
									;hStdOut = GetstdHandle( STD_OUTPUT_HANDLE)	
        push    STD_INPUT_HANDLE
        call    GetStdHandle
        mov     [hStdIn], eax
									;hStdOut = GetstdHandle( STD_OUTPUT_HANDLE)	
        push    STD_OUTPUT_HANDLE
        call    GetStdHandle
        mov     [hStdOut], eax
											;hStdErr = GetstdHandle( STD_ERROR_HANDLE)	
        push    STD_ERROR_HANDLE
        call    GetStdHandle
        mov     [hStdErr], eax
		push    CP_UTF16
		call    SetConsoleOutputCP
		FINIT  
		ret
_halt:
		; выход из программы
											; ExitProcess(0)
		push    0
		call    ExitProcess

		; никогда не выполняется
		hlt
	    ret
Code	ENDS
Data0	SEGMENT	WORD PUBLIC "data" 
true 	dd 1
false 	dd 0
hStdIn  dd 0
hStdOut dd 0
hStdErr dd 0
STD_INPUT_HANDLE  = -10
STD_OUTPUT_HANDLE = -11
STD_ERROR_HANDLE  = -12
CP_1251 	= 1251
CP_UTF16 	= 1200
CP_UTF8 	= 65001
NL		DD NL_IMM 
		DD 2
NL_IMM	DW 13,10
		DW 0        	; [Const]
Data0 ends