{� 2017 Pavia. All Rights Reserved, by Pavia.}
program Project1;

{$APPTYPE CONSOLE}

{$R 'main.res' 'main.rc'}     // ��������� ������ � ������
{ $R *.res}

{����:}
// �������� ���� ��������
// ������� ���� �����
// ������� ���� �������
// ������� ���� ����������.
// - ������� ���� ����������� ������������� �������� ��� ����������.
// - array code - ���� �������������� ������ ��� �������� �������.
// - ���������� ������� (� ����������� ������ ����������� �������� ������� ����� ��� ����������)
// -- ��� ���������� ����� var ��� �������� ���������� �� ������ �� ��� ���� ������� 2 ���������?
// -- ����� � ���������� ������� ��� ����������
//== ck:=+ck;
// ������� ���� �������������� �����
// �������� �������� ����������
// �������� ����� ��� ��������� ����������
// �������� ����������� �������
// ��������� �����. ��������� ������� � ��������� �����������. IsDigit
// ������ ��� ����������� ��������������� �������!!!
// ������ �������� � �������������� ����� � ��������� ����
// ���������� �������
// ��������� ���������� ��������� ����������
// ��������� ���������� ��������� ����� �������
// for i:=0 to count-1 do ��� �����
// Write('imm'); - ������ ������� � imm.
// ������ else �� PrintVarAssignSimpleSignature
// ��������� ABI � ���������������.
// �������������� ��������/�������� ��������� �����
// ���������� ����� ����� �����������.
// ������� � ������ � ������� ��������� ���������
// ����������� PrintExpression ������ char
// DestroyFuncCall � DestroyVarAssign ������� ����� ����� ���������.
// 'exit' � Breck ������� ������� push � �� ������ ��.
// ������������ TTypeParser.Parse: ������ ����� � ParseAlias �������� �� �����������
// PCHar, �������� PDword - �������� ���� �� ���������
// procedure TSemanter.PVarAssignVar_String(  ----- �������� ConcatinatedStringChar �� Assign
//[!]
// ������ �� ��������� ImmTypeChar
// ����� ����� ����� ����� �� ���������� ����� ���������������, ������, ������� � ��.
// ������� ����� ��� ����� � �������.
// ����� ������ � ��� ������ FactorType(VarAssignAST.Expression.Factor2)
// PrintCmdRegAssignIndent2 �������� EDX �� 'E'+REG
// �������� ������ � FuncDecl �� �������������� ����������.
//  AssignStringString � �� ���������� ������� �������� � AssignStringChar
// ������ ����� �� ������ ��� �������?
// ��������� �������� �������� ������� � ������. ����� �� �� ���� ������ char?
// ��������� �������� � ������� 0 ������� � �������. �������������� �������.
// �������� ���������� � ����� 0 ������� � ������� ������������.
// ������������ StringPaddingSize.
// ��������� � ����� ��������� ������� CharSize ����������
// PrintRegAssignAddr dkDeSignatureArray �������� �� ������ ���������
// ������� ����� GenCompaundStatement � injectionCompaundStatement
// ��� PByte ������ �� simpletype  �� ������� UToASM UPrint � ��.
// StringsCompare ���������� �� char
// ��������� �����, �������� ��� ������� � ������������� �������
// TCompilerWoker � TCodePrint ������ ���� �������� ����� �������� AOBj<>nil
// GetSizeOfString - ��������� ���������� 0 ������� �� ������� �������.
// PrintVarAssignVar ����������� �������� �� SignatureTypeFamily
// ������ ������� �������� �������� �� ��������.
// ��������� �����, ������� � ������������� ������.
// ��������� ��������� ������������� ��� � �������.
// ������� ���������� ��� ��� ������ �� ���������.
// --------


// �����������
// - ������ ������ ������ ������, ����������� �� ������
// - ������� ������ ��������������� ��������� ParseDesignator
// ��������� ������������� ������ � ��������������
// - �������� � ParseFuncCall ������� TFactor
// - ������ ������������� ��� �� PrintRegAssignFunc � ������ factor
// ������ �������� ����� � ���� ������������� ������
// �������� ����������� ������ ���������
// ��������� ������� ���������
// ��������
// ������� ������� ��� �������� ������ ����������
// ������� ���� ��� � ���� ������
// ���������� ��������� ��������� ����������� ������ � �����.


uses      
  Windows,
  SysUtils,
  classes,
  USimvler,
  UGramAnaliser,
  UParserUnit,
  UParserDeclarations,
  UParserType,
  UParserExpression,
  UParserStatement,
  UParserImplementation,
  UParserFactor,
  UCompiler,
  UScaner,
  UAST,
  UASTPrint in 'src\UnitsTests\UASTPrint.pas',
  IAST,
  LICompiler,
  UASTFabric,
  LParser,
  LIParser,
  UCompilerError,
  USemantic,
  USemanticCheking,
  USystemMagic,
  UToASM in 'src\Generator\UToASM.pas',
  USlovar,
  UHohaTypes in 'src\UHohaTypes.pas',
  UFSM{,
  dcvCPUSpeed{};

//  FilePath:String;
type
TCMDParametrs=record
  FileIn:String;
  FileOut:String;
  FileLog:String;
  HasHelp:boolean;
  HasCmdFix:boolean;
  end;

// �������� �� http://forum.sources.ru/index.php?showtopic=288844
procedure ParseParametrs(var CMDParametrs:TCMDParametrs);
var
 i:Integer;
 s:String;
begin
i:=1;
 while i+1<= ParamCount do
   begin
   s:=ParamStr(i);
   if s='-In' then begin CMDParametrs.FileIn:=ParamStr(i+1); inc(i); end;
   if s='-Out' then begin CMDParametrs.FileOut:=ParamStr(i+1); inc(i); end;
   if s='-Log' then begin CMDParametrs.FileLog:=ParamStr(i+1); inc(i); end;
   if s='-Help' then CMDParametrs.HasHelp:=true;
   if s='-FixFont' then CMDParametrs.HasCmdFix:=true;
   Inc(i,1);
   end;
 begin
   s:=ParamStr(i);
   if s='-Help' then CMDParametrs.HasHelp:=true;
   if s='-FixFont' then CMDParametrs.HasCmdFix:=true;
 end;
end;

function TestParametrs(const CMDParametrs:TCMDParametrs):Boolean;
begin
Result:=True;
if not FileExists(CMDParametrs.FileIn) then
   begin
   WriteLn(Format('<<��������>> ���� �� ������: "%s"',[CMDParametrs.FileIn]));
   Result:=False;
   end;

{if not FileExists(CMDParametrs.FileOut) then
   begin
   WriteLn(Format('-Out ���� �� ������: "%s"',[CMDParametrs.FileOut]));
   Result:=False;
   end;        }

{if not FileExists(CMDParametrs.FileLog) then
   begin
   WriteLn(Format('<>-Log ���� �� ������: "%s"',[CMDParametrs.FileLog]));
   Result:=False;
   end;
}if (CMDParametrs.HasHelp) then
   begin
     Result:=False;
   end;
if Result=False then
   WriteLn(Format('������� �������: "%s"',[GetCurrentDir]));
end;

procedure printHelp;
begin
  WriteLn('���������� ��� 1.0');
  WriteLn('pop.exe -In <<�������� ����>> -Out <<��������� ����>> -Log <<���� ������>> [-FixCMD]');
  WriteLn('�������:');
  WriteLn('  <<�������� ����>> - ���� c �������� ������� ��������� �� ����� ��� [*.��� | *.pop]');
  WriteLn('  <<��������� ����>> - ���� c ����������� �������� �� ���� ���������, � �������� TASM''� [*.asm]');
  WriteLn('  <<���� ������>> - ���� � ������������ �������� ����������� � ���������� ���������� [*.log]');
  WriteLn('��������:'); // �������� �� ���� ������ � �����
  WriteLn('  -Help - �������� ������� ��� ���� �������');
  WriteLn('  -FixFont - Fix error with font on consol');
  WriteLn('  -FixFont - ������ �� ����������� �������, ��������� � ������� � ���������');
  WriteLn('  -In - �������� ��������� �����');
  WriteLn('  -Out - �������� ���������� �����');
  WriteLn('  -Log - �������� ���������� �����');
  WriteLn('�������:');
  WriteLn('pop.exe -In tests\104.pop -Out tmp\1.asm -Log tmp\1.log');
end;

procedure FixCmd;
var
  Key: HKey;
const
  FaceName:String='Lucida Console';
  FontFamily:DWord=$36;
  FontSize:DWord=$100000;
  FontWeight:DWord=$190;

begin
  RegOpenKey(HKEY_CURRENT_USER, PChar('Console'), Key);
  RegSetValueEx(Key,PAnsiChar('FaceName') ,0, REG_SZ, PAnsiChar(FaceName), lstrlen(PAnsiChar(FaceName))+1);
  RegSetValueEx(Key,PAnsiChar('FontFamily') ,0, REG_DWORD, @FontFamily, SizeOf(DWord));
  RegSetValueEx(Key,PAnsiChar('FontSize') ,0, REG_DWORD, @FontSize, SizeOf(DWord));
  RegSetValueEx(Key,PAnsiChar('FontWeight') ,0, REG_DWORD, @FontWeight, SizeOf(DWord));
  RegCloseKey(Key);

end;

procedure PrintProfile;
var SpeedRDTSC:Int64;
begin
//SpeedRDTSC:=GetSpeedRDTSC;
WriteLn('Scaner Time:', TimeElaps[teSpace]/SpeedRDTSC:4:3);
WriteLn(' |');
WriteLn(' -- Space Time:', TimeElaps[teSpace]/SpeedRDTSC:4:3);
WriteLn(' -- Word Time:', TimeElaps[teWord]/SpeedRDTSC:4:3);
WriteLn(' -- Number Time:', TimeElaps[teNumber]/SpeedRDTSC:4:3);
WriteLn(' -- String Time:', TimeElaps[teString]/SpeedRDTSC:4:3);
WriteLn(' -- Punctuator Time:', TimeElaps[tePunctuator]/SpeedRDTSC:4:3);
WriteLn(' -- Comment Time:', TimeElaps[teComment]/SpeedRDTSC:4:3);
end;

// test
var buf:array [0..65536 *4]of char;
procedure test;
var r:DWord;
ck:bool;
h:DWord;
err:DWord;
begin
r:=0;
err:=GetLastError();
h:=GetStdHandle(STD_INPUT_HANDLE);
err:=GetLastError();
//ck:=SetConsoleMode(h,ENABLE_LINE_INPUT+ENABLE_ECHO_INPUT);
err:=GetLastError();
ck:=ReadConsoleW(h,@buf, 65536 shr 2, R, nil);
err:=GetLastError();
end;

var
  Compiler:TCompiler;
  CMDParamets:TCMDParametrs;
  t1,t2:Integer;
begin
 SetConsoleCP(1251);
 SetConsoleOutPutCP(1251);

 // test;
// FilePath:=ParamStr(1);
// FilePath:='C:\Documents and Settings\USER\YandexDisk\PROJECTS\Delphi7.4\����� ���������� v2\1\Tests\1.In' ;
 //FilePath:='E:\YandexDisk\PROJECTS\Delphi7.4\����� ���������� v2\1\Tests\1.In' ;
 Compiler:=TCompiler.Create;
 ParseParametrs(CMDParamets);
 if CMDParamets.HasCmdFix then
    begin
    FixCmd;
    WriteLn('Fixed');
    WriteLn('You mast reRun apllication');
    WriteLn('��������!');
    end else
 if TestParametrs(CMDParamets) then
    begin
    Compiler.Semanter:= USemantic.TSemanter.Create(Compiler);
    Compiler.CodePrinter.Bind(UToASM.TPrinterAST.Create(Compiler));
    Compiler.Log.FilePath:=CMDParamets.FileLog;
    Compiler.AddSearchDir('.\..\..\lib\');
    Compiler.AssignIn(CMDParamets.FileIn);
    Compiler.AssignOut(CMDParamets.FileOut);
    Compiler.Reset;
    t1:=GetTickCount;
    Compiler.Process;
    t2:=GetTickCount;
    WriteLn(Format('�����=%.3f', [(t2-t1)/1000]));
    if Compiler.Error.count=0 then
         WriteLn('�������!-Good')
       else
         WriteLn('�����!-Bad');
    end else
    begin
    PrintHelp;
    end;

//PrintProfile;
readln;
end.
