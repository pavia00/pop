{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UCompiler;

interface

uses LICompiler, USlovar, UAST, UASTFabric;

type
  TCompiler=class;


  ICodePrinter= interface(ICompilerWoker)
    procedure AssignFile(Path:String);
    procedure print;
//    function LastPosRow:Integer;
//    function LastPosCol:Integer;
  end;

  TCodePrinter=class(TCompilerWoker, ICodePrinter)
  protected
  public
    procedure AssignFile(Path:String); virtual;
    procedure print; virtual;
    procedure reset; virtual;
    procedure process; virtual;
    function LastPosRow:Integer; override;
    function LastPosCol:Integer; override;
  end;

  TLog  = class(TInterfacedObject, ILOg)
  private
  FFile:TextFile;
  FFilePath:String;
  function GetFilePath:String;
  procedure SetFilePath(const Value: String);
  protected
  public
  property FilePath:String Read GetFilePath Write SetFilePath;
  procedure WriteLn(const s:String);
  published
  end;

  TListSearchDir=array of String;

  TCompiler = class(TInterfacedObject, ICompiler)
  private
    FLexAnaliser:TObject;
    FGramAnaliser:TCompilerWoker;
    FSemanter:TCompilerWoker;
    FCodePrinter:TCodePrinter;
    ListSearchDir:TListSearchDir;
    { private declarations }
  protected
    { protected declarations }
  public
    FLog:ILog;
    FError:ICompilerError;
    ParserStack:TObject;
    Slovar:TSlovar;
    property LexAnaliser:TObject read FLexAnaliser;
    property GramAnaliser:TCompilerWoker read FGramAnaliser;
    property Semanter:TCompilerWoker  read FSemanter write FSemanter;
    property CodePrinter:TCodePrinter read FCodePrinter write FCodePrinter;
    constructor Create; Overload;
    destructor Destroy;
    procedure AssignIn(FilePath:String);
    procedure AssignOut(FilePath:String);
    procedure Reset;
    procedure Process;
    function GetLog:ILog;
    function GetError:ICompilerError;
    property Log:ILog read GetLog;
    property Error:ICompilerError read GetError;
    function getUnit(name:WideString):TUnitAST;
    procedure AddSearchDir(Dir:String);
    function nameToPath(name:String):String;
    { public declarations }
  published
    { published declarations }
  end;
  procedure FreeAndNil(Var Obj);

implementation

uses SysUtils, UCompilerError, UASTPrint, USimvler, UGramAnaliser, UParserUnit;

procedure FreeAndNil(Var Obj);
begin
SysUtils.FreeAndNil(Obj);
end;

{ TCompiler }

procedure TCompiler.AssignIn(FilePath: String);
var
 LexAnaliser:TFileSimvler;
begin
 AddSearchDir(ExtractFileDir(FilePath));
 LexAnaliser:=(FLexAnaliser as TFileSimvler);
 LexAnaliser.Assign(FilePath);
end;

procedure TCompiler.AssignOut(FilePath: String);
begin
 CodePrinter.AssignFile(FilePath);
end;

constructor TCompiler.Create;
var
 LexAnaliser:TSimvler;
 GramAnaliser:TGramAnaliser;
 Err:TCompilerError;
begin
  inherited Create;
    ParserStack:=TStackAST.Create;

    LexAnaliser:=TFileSimvler.Create;
  //  LexAnaliser.Compiler:=Self;
    FLexAnaliser:=LexAnaliser;

    GramAnaliser:=TGramAnaliser.Create(self);
    FGramAnaliser:=GramAnaliser;

    GramAnaliser.UnitParser:=TUnitParser.Create(Self);

    FCodePrinter:=TCodePrinter.Create;
    FCodePrinter.Bind(TPrinterAST.Create(Self));

 FCodePrinter._AddRef;

    Slovar:=TSlovar.Create;

    Err:=TCompilerError.Create(Self);
    FError:=Err;
    FLog:=TLog.Create;

end;

destructor TCompiler.Destroy;
begin
  FLexAnaliser.Destroy;
  FGramAnaliser.Destroy;
  Error.DoDestroy;
  inherited;
end;

function TCompiler.GetError: ICompilerError;
begin
Result:=FError;
end;

function TCompiler.GetLog: ILog;
begin
Result:=FLog;
end;


function TCompiler.getUnit(name: WideString): TUnitAST;
var Compiler:TCompiler;
Var Path:String;
 GramAnaliser:TGramAnaliser;
 LastNode:TNodeAST;
begin
  LastNode:=ASTFabric.ActivLeaf;

  Compiler:=TCompiler.Create;
  Compiler.FLog:=Self.FLog;
  Compiler.ListSearchDir:=Self.ListSearchDir;

  Path:=nameToPath(name);
  if (Path='') then
    begin
    self.Error.Add(001,name);
    Result:=TUnitAST.Create;
    end else
    begin
    Compiler.AssignIn(Path);
    Compiler.Reset;

    GramAnaliser:=(Compiler.FGramAnaliser as TGramAnaliser);
    GramAnaliser.Process;
    Result:=GramAnaliser.AST;
    end;
  Self.Error.Count:=Self.Error.Count+Compiler.Error.Count;
  Compiler.Free;
  ASTFabric.ActivLeaf:=LastNode;
end;

procedure TCompiler.AddSearchDir(Dir:String);
begin
  SetLength(ListSearchDir, Length(ListSearchDir)+1);
  ListSearchDir[Length(ListSearchDir)-1]:=Dir;
end;

function TCompiler.nameToPath(name: String): String;
var
  Path:String;
  SearchDir:String;
  i:Integer;
begin
  for i:=0 to Length(ListSearchDir)-1 do
    begin
    SearchDir:=ListSearchDir[i];
    result:='';
    Path:=SearchDir+name+'.pop';
    if FileExists(Path) then
        begin
          result:=Path;
          exit;
        end
       else
         begin
          Path:=SearchDir+name+'.���';
          if FileExists(Path) then
             begin
             result:=Path;
             exit;
             end;
          end;
    end;
end;

procedure TCompiler.Process;
begin
 Error.CompilerWoker:=GramAnaliser;
 GramAnaliser.Process;
 Error.CompilerWoker.Bind(Semanter);
 Semanter.process;
 Error.CompilerWoker.Bind(CodePrinter);
 CodePrinter.print;
end;

procedure TCompiler.Reset;
var
 LexAnaliser:TFileSimvler;
begin
 LexAnaliser:=(FLexAnaliser as TFileSimvler);
 LexAnaliser.Reset;
 GramAnaliser.Reset;
end;

{ TLog }

procedure TLog.WriteLn(const s: String);
begin
System.WriteLn(FFile,s);
end;

procedure TLog.SetFilePath(const Value: String);
begin
  FFilePath := Value;
  AssignFile(FFile, Value);
  Rewrite(FFile);
end;

function TLog.GetFilePath: String;
begin
 Result:=FFilePath;
end;

{ TCodePrinter }

procedure TCodePrinter.AssignFile(Path: String);
begin
  if (AObj<>Self) and (AObj<>nil) then
     (AObj as TCodePrinter).AssignFile(Path);

end;

function TCodePrinter.LastPosCol: Integer;
begin
  if (AObj<>Self) and (AObj<>nil) then
     result:=AObj.LastPosCol
     else
     Result:=FLastPosCol;
end;

function TCodePrinter.LastPosRow: Integer;
begin
  if (AObj<>Self) and (AObj<>nil) then
     result:=AObj.LastPosRow
     else
     Result:=FLastPosRow;
end;

procedure TCodePrinter.print;
begin
  if (AObj<>Self) and (AObj<>nil) then
     (AObj as TCodePrinter).print;
end;

procedure TCodePrinter.process;
begin
  if (AObj<>Self) and (AObj<>nil) then
     (AObj as TCodePrinter).process
     else
     print;
end;

procedure TCodePrinter.reset;
begin
  if (AObj<>Self) and (AObj<>nil) then
     (AObj as TCodePrinter).reset;
end;

end.
