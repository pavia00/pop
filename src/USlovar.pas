{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit USlovar;

interface

uses SysUtils;

type
  TESlovar= record
    Key:WideString;
    Trans:WideString;
    KeyObj:TObject;
    TransObj:TObject;
    end;
  TList_TESlovar=array of TESlovar;

  TSlovar = class
  private
  protected

  public
    List:TList_TESlovar;
    Count:Integer;
    procedure Add(Key, Trans:WideString); overload;
    procedure Add(Key:WideString; const KeyObj:TObject; Trans:WideString; const TransObj:TObject); overload;
    function IsExist(Word:WideString):Boolean;
    function IsKeyExist(Key: WideString): Boolean;
    function GenUniWord(Word:WideString):WideString;
    function GetUniWord(Word:WideString):WideString;
    function FindObj(const obj:TObject):Integer;
    constructor Create;
    destructor Destroy;
  published

  end;

implementation

{ TSlovar }

procedure TSlovar.Add(Key:WideString; const KeyObj:TObject; Trans:WideString; const TransObj:TObject);
begin
 SetLength(List,Length(List)+1);
 List[Length(List)-1].Key:=Key;
 List[Length(List)-1].Trans:=Trans;
 List[Length(List)-1].KeyObj:=KeyObj;
 List[Length(List)-1].TransObj:=TransObj;
 Inc(Count);
end;

procedure TSlovar.Add(Key, Trans: WideString);
begin
  Add(Key,nil, Trans, Nil);
end;

constructor TSlovar.Create;
begin
  count:=0;
end;

destructor TSlovar.Destroy;
begin

end;

function TSlovar.GenUniWord(Word: WideString): WideString;
var i,di:Integer;
  WordMod:WideString;
begin
WordMod:=Word;
i:=0;
di:=1;
 while IsExist(WordMod) do
   begin
    WordMod:=Word+IntToStr(i);
    Inc(i, di);
//    if i=7 then di:=random(100)+1;
   end;
Result:=WordMod;
end;

function TSlovar.GetUniWord(Word: WideString): WideString;
begin
Result:=GenUniWord(Word);
Self.Add(Word, Result);
end;

function TSlovar.IsExist(Word: WideString): Boolean;
var
 i:Integer;
begin
Result:=False;
 for i:=0 to Count-1 do
     if (WideCompareText(List[i].Key,Word)=0) or
        (WideCompareText(List[i].Trans,Word)=0) then
         begin
           Result:=True;
           Break;
         end;
end;

function TSlovar.IsKeyExist(Key: WideString): Boolean;
var
 i:Integer;
begin
Result:=False;
 for i:=0 to Count-1 do
     if (List[i].Key=Key) then
         begin
           Result:=True;
           Break;
         end;
end;

function TSlovar.FindObj(const obj:TObject):Integer;
var
  i:Integer;
begin
  result:=-1;
  for i:=0 to Count-1 do
    if (List[i].KeyObj=Obj) or
       (List[i].TransObj=Obj)  then
       begin
       Result:=i;
       Break;
       end;
end;

end.
