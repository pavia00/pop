{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UScaner;

interface

uses SysUtils;

type
  TScanerByte=Class; //Forward;
  TScanerChar=Class; //Forward;
  TGetByte= function ():Byte of object;
  TGetChar= function ():Char of object;
  TGetWideChar= function ():WideChar of object;

  IScanerByte=interface
    procedure Assign(Scaner:TScanerByte);
    procedure AssignTo(Scaner:TScanerByte);
    procedure Reset;
    function  FReadByte:Byte;
    end;

  IScanerChar=interface
    procedure Assign(Scaner:TScanerChar);
    procedure AssignTo(Scaner:TScanerChar);
    procedure Reset;
    function  FReadChar:Char;
    end;

  TScanerByte = class(TInterfacedObject, IScanerByte)
  private
    FCurrentByte:Byte;
    FNextByte:Byte;
    function FReadByte:Byte;
  protected
  public
    perent:TScanerByte;
    ReadByte:TGetByte;
    property CurrentByte:Byte read FCurrentByte;
    property NextByte:Byte read FNextByte;
    procedure Assign(Scaner:TScanerByte);
    procedure AssignTo(Scaner:TScanerByte);
    procedure Reset;
    constructor Create; //override;
    destructor Destroy; //override;
  published

  end;

  TScanerFileByte = class(TScanerByte)
  private
    FFile:File of Byte;
    function FReadByte:Byte;
  protected
  public
    procedure AssignTo(Scaner:TScanerByte);
    procedure AssignFile(Path:String);
    procedure Reset; //override;
    constructor Create; //override;
    destructor Destroy; //override;
  published
  end;

  TScanerChar = class(TInterfacedObject, IScanerChar)
  private
    FCurrentChar:Char;
    FNextChar:Char;
    function  FReadChar:Char;
  protected
  public
    perent:TScanerChar;
    ReadChar:TGetChar;
    procedure Assign(Scaner:TScanerChar);
    procedure AssignTo(Scaner:TScanerChar);
    property  CurrentChar:Char read FCurrentChar;
    property  NextChar:Char read FNextChar;
    procedure Reset;
    constructor Create; //override;
    destructor Destroy; //override;
  published
  end;

  TScanerSimpleChar = class(TScanerChar)
  private
    function FReadChar:Char;
  protected
  public
    perent:TScanerByte;
    ReadChar:TGetChar;
    constructor Create; //override;
    destructor Destroy; //override;
  published
  end;

  TScanerWideChar = class(TScanerChar)
  private
    FCurrentWChar:WideChar;
    FNextWChar:WideChar;
    function FReadWChar:WideChar;
  protected
  public
    ReadWChar: TGetWideChar;
    perent:TScanerChar;
    property CurrentWChar:WideChar read FCurrentWChar;
    property NextWChar:WideChar read FNextWChar;
    constructor Create; //override;
    destructor Destroy; //override;
  published
  end;

  TScanerWideCharFromANSI = class(TScanerWideChar)
  private
    function FReadWChar:WideChar;
  protected
  public
    constructor Create;// override;
    destructor Destroy; //override;
  published
  end;

implementation

{ TScaner }

procedure TScanerByte.Assign(Scaner: TScanerByte);
begin
Scaner.AssignTo(Self);
end;

procedure TScanerByte.AssignTo(Scaner: TScanerByte);
begin
raise Exception.Create('Base class can''t Assign');
end;

constructor TScanerByte.Create;
begin
  ReadByte:=Self.FReadByte;
end;

destructor TScanerByte.Destroy;
begin

end;

function TScanerByte.FReadByte: Byte;
begin
Result:=FCurrentByte;
FCurrentByte:=FNextByte;
FNextByte:=0;
end;

procedure TScanerByte.Reset;
begin
If Perent<>Nil then Perent.Reset;
end;

{ TScanerFile }

procedure TScanerFileByte.AssignFile(Path: String);
begin
  System.AssignFile(FFile, Path);
end;

procedure TScanerFileByte.AssignTo(Scaner: TScanerByte);
begin
  Scaner.ReadByte:=FReadByte;
  Scaner.perent:=Self;
end;

constructor TScanerFileByte.Create;
begin
  inherited;
  ReadByte:=FReadByte;
end;

destructor TScanerFileByte.Destroy;
begin

end;

function TScanerFileByte.FReadByte: Byte;
begin
  Result:=(Self as TScanerByte).FReadByte;
  if not EOF(FFIle) then
     Read(FFile,FNextByte)
     else FNextByte:=0;
end;

procedure TScanerFileByte.Reset;
var
  Len:Integer;
begin
  System.Reset(FFile);
  FCurrentByte:=0;
  FNextByte:=0;
  Len:=FileSize(FFile);
  if Len>1 then
     Read(FFile,FCurrentByte);
  if Len>2 then
     Read(FFile,FNextByte);
end;

{ TScanerSimpleChar }

constructor TScanerSimpleChar.Create;
begin
  inherited;

end;

destructor TScanerSimpleChar.Destroy;
begin

  inherited;
end;


function TScanerSimpleChar.FReadChar: Char;
begin
  Result:=FCurrentChar;
  FCurrentChar:=FNextChar;
  FNextChar:=Char(Perent.FReadByte);
end;

{ TScanerWideCharFromANSI }

constructor TScanerWideCharFromANSI.Create;
begin
  inherited;
  Perent:=TScanerSimpleChar.Create;
end;

destructor TScanerWideCharFromANSI.Destroy;
begin
  inherited;
  perent.Destroy;
end;

function TScanerWideCharFromANSI.FReadWChar: WideChar;
begin
  Result:=FCurrentWChar;
  FCurrentWChar:=FNextWChar;
  FNextWChar:=WideChar(Perent.ReadChar);
end;

{ TScanerWideChar }

constructor TScanerWideChar.Create;
begin
  ReadChar:=Self.FReadChar;
end;

destructor TScanerWideChar.Destroy;
begin

end;

function TScanerWideChar.FReadWChar: WideChar;
begin
  Result:=FCurrentWChar;
  FCurrentWChar:=FNextWChar;
  FNextWChar:=#0;
end;

{ TScanerChar }

procedure TScanerChar.Assign(Scaner: TScanerChar);
begin
Scaner.AssignTo(Self);
end;

procedure TScanerChar.AssignTo(Scaner: TScanerChar);
begin
raise Exception.Create('Can''t assign self TScanerChar');
end;

constructor TScanerChar.Create;
begin

end;

destructor TScanerChar.Destroy;
begin

end;

function TScanerChar.FReadChar: Char;
begin
Result:=FCurrentChar;
FCurrentChar:=FNextChar;
FNextChar:=#0;
end;

procedure TScanerChar.Reset;
begin
if Perent<>nil then Perent.Reset;
end;

end.
