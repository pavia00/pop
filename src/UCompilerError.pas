{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UCompilerError;

interface

uses SysUtils, LICompiler, UCompiler, UGramAnaliser;

type

  TCompilerError = class(TInterfacedObject, ICompilerError)
  private
    FParserErrorCount: Integer;
    FSemanterErrorCount: Integer;
    FCount:Integer;
    FCompilerWoker:TCompilerWoker;
    procedure SetParserErrorCount(const Value: Integer);
    procedure SetSemanterErrorCount(const Value: Integer);
    { private declarations }
  protected
    { protected declarations }
  public
    Compiler:TCompiler;
    property SemanterErrorCount:Integer read FSemanterErrorCount write SetSemanterErrorCount;
    property ParserErrorCount:Integer read FParserErrorCount write SetParserErrorCount;
    constructor Create(TheCompiler:TCompiler); Overload;
    procedure Add(Code:Integer; SubText:String);
    procedure WriteError(Code:Integer; Text:String);
    procedure WriteOtherError(Code:Integer; Text:String);
    procedure WriteGramerError(Code:Integer; Text:String);
    procedure WriteSemanterError(Code: Integer; Text:String);
    procedure WriteCoderError(Code:Integer; Text:String);
    procedure DoDestroy;
    function GetCount:Integer;
    procedure SetCount(const Value: Integer);
    property Count:Integer Read GetCount Write SetCount;
    function GetCompilerWoker:TCompilerWoker;
    procedure SetCompilerWoker(const Value: TCompilerWoker);
    property CompilerWoker:TCompilerWoker Read GetCompilerWoker Write SetCompilerWoker;
    { public declarations }

  published
    { published declarations }
  end;

implementation

{ TCompilerError }

procedure TCompilerError.Add(Code: Integer; SubText:String);
begin
if (Code>=100) and (Code<200) then
   ParserErrorCount:=ParserErrorCount+1;
if (Code>=300) and (Code<400) then
   SemanterErrorCount:=SemanterErrorCount+1;
   WriteError(code, SubText);
end;

constructor TCompilerError.Create(TheCompiler: TCompiler);
begin
 Compiler:=TheCompiler;
end;

procedure TCompilerError.DoDestroy;
begin
 Destroy;
end;

function TCompilerError.GetCount: Integer;
begin
Result:=FCount;
end;

procedure TCompilerError.SetCount(const Value: Integer);
begin
FCount:=Value;
end;

procedure TCompilerError.WriteError(Code: Integer; Text:String);
begin
if (code<100) then WriteOtherError(Code, Text);
if (100<=code) and (code<190) then WriteGramerError(Code, Text);
if (190<=code) and (code<200) then WriteCoderError(Code, Text);
if (300<=code) and (code<400) then WriteSemanterError(Code, Text);
if (10100<=code) and (code<10190) then WriteCoderError(Code-10000, Text);  // ��������� ��������
end;

procedure TCompilerError.WriteOtherError(Code: Integer; Text:String);
begin
case code of
000: WriteLn('���� �� ������:"'+Text+'"');
001: WriteLn('���� ������ �� ������:"'+Text+'"');
end;
end;

procedure TCompilerError.WriteGramerError(Code: Integer; Text:String);
var Gramer:TGramAnaliser;
begin
Gramer:=Compiler.GramAnaliser as TGramAnaliser;
Write(Format('�����=%d; �������=%d; ������ �%d; ',[Gramer.LastPosRow, Gramer.LastPosCol, Code]));
with Compiler.Log do
case code of
100: WriteLn('��������� �������:"'+Text+'"');
101: WriteLn('����� ��������������� ���');
102: WriteLn('����� ����� � ������ �������� ����(Integer, Byte, Real � ��)');
103: WriteLn('����� ����� � ������ �����');
104: WriteLn('����� ����� "To" ��� "DownTo"');
105: WriteLn('������� �����:"'+Text+'"- ��������� ���� ��: "Label","Var",'+
             '"Type", "Const", "Function", "Procedure","Implementetion" ��� "End"');
106: WriteLn('������ '+Text+' - ��������� ����������, ������� ��� ���������');
107: WriteLn('������� �� ���������� �����: "'+Text+'"');
108: WriteLn('��������� ����������');
109: WriteLn('��������� ����������, � �������� ��������� �����:"'+Text+'"');
110: WriteLn('������c� ��������, � �������� ������:"'+Text+'"');
111: WriteLn('����� ����� � ������ ����');
112: WriteLn('����� ������������');
113: WriteLn('����� ����� ����� � ��������� �� 0 �� 2147483647');
114: WriteLn('������� �����:"'+Text+'"- ��������� ���� ��: "Function",'+
                                      ' "Procedure","begin" ��� "End"');
115: WriteLn('��� ������ ���� ��������, � ������� �����: "'+Text+'"');
116: WriteLn('��� ������ ���� ������� �� ���������� ����� asm � end.');
117: WriteLn('��� ������ ���� ���������� ��� ����������.');


140: WriteLn('��������� �����!');
141: WriteLn('�������� ������������ ������� ��� ����!');
142: WriteLn('���������� ����������!');
143: WriteLn('��������� ����� �������, �� ��������� � ���������� ������');
144: WriteLn('������������ ������ ����������� ���������!');
end;
end;

procedure TCompilerError.WriteCoderError(Code: Integer; Text:String);
//var Coder:ICodePrinter;
begin
//Coder:=Compiler.CodePrinter;
Write(Format('�����=%d; �������=%d; ������ �%d; ',[CompilerWoker.LastPosRow, CompilerWoker.LastPosCol, Code]));
with Compiler.Log do
case code of
108: WriteLn('��������� ����������');
117: WriteLn('��� ������ ���� ���������� ��� ����������.');

140: WriteLn('��������� �����!');
141: WriteLn('�������� ������������ ������� ��� ����!');
142: WriteLn('���������� ����������!');
143: WriteLn('��������� ����� �������, �� ��������� � ���������� ������');
144: WriteLn('������������ ������ ����������� ���������!');

193: WriteLn('��� ��� ������� "'+Text+'"�� ��������!');
194: WriteLn('����������� ���');
195: WriteLn('�������� "^" �������� ������ � ����������');
196: WriteLn('���������� ������: "'+Text+'"');
//197: WriteLn('��������� ���������� �������');
198: WriteLn('����������� ��������� ����');
199: WriteLn('������ ����������');
end;
end;

procedure TCompilerError.WriteSemanterError(Code: Integer; Text:String);
begin
Write(Format('�����=%d; �������=%d; ������ �%d; ',[CompilerWoker.LastPosRow, CompilerWoker.LastPosCol, Code]));
with Compiler.Log do
case code of
300: WriteLn('��������� ���������� ������� ���: "'+Text+'"');
301: WriteLn('��������� ���������� ���������� ������� ���: "'+Text+'"');
302: WriteLn('������������ ��������. ������ ��������� ����� ������ ����� ������');
303: WriteLn('�������� �� ��������� � ������ �����.');
304: WriteLn('�������� ^ �������� ������ � ����������.');
305: WriteLn('�������� [] �������� ������ � ��������.');
end;
end;

procedure TCompilerError.SetParserErrorCount(const Value: Integer);
begin
  Count:=Count+(Value-FParserErrorCount);
  FParserErrorCount := Value;
end;

procedure TCompilerError.SetSemanterErrorCount(const Value: Integer);
begin
  Count:=Count+(Value-FSemanterErrorCount);
  FSemanterErrorCount := Value;
end;

function TCompilerError.GetCompilerWoker: TCompilerWoker;
begin
  Result:=FCompilerWoker;
end;

procedure TCompilerError.SetCompilerWoker(const Value: TCompilerWoker);
begin
  FCompilerWoker:=Value;
end;

end.
