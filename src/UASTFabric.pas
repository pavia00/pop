{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UASTFabric;

interface

uses USystemMagic, UAST, LICompiler, USimvler;
type
  TASTFabric=class
    private
    FActivLeaf:TNodeAST;
    procedure SetActivLeaf(const Value: TNodeAST);
    public
    Compiler:ICompiler;
    IndentKind:TIndentKind;
    constructor Create;
    function NodeCreate(const NodeType:TASTNodeType):TNodeAST;

    function IndentCreate(Simvol:TSimvol):TIndentAST;
    function IntegerCreate(Simvol:TSimvol):TIntegerAST;
    function RealCreate(Simvol:TSimvol):TRealAST;
    function UnitCreate:TUnitAST;
    function DeclarationCreate:TDeclarationAST;
    function ImplimentaionCreate:TImplimentaionAST;
    function FuncDeclCreate:TFuncDeclAST;
    function FuncImpCreate:TFuncImpAST;
    function ForeignAsmCreate:TForeignAsmAST;
    function TypeCreate:TTypeAST;

    procedure StackCreate(var Goal:TStackAST);
    procedure StackDestroy(var Goal:TStackAST);
    property ActivLeaf:TNodeAST read FActivLeaf write SetActivLeaf;
    function GetIndentKind(const Indent:TIndentAST):TIndentKind;
    function SearchDeclParent(Node:TNodeAST):TNodeAST;
    function GetIndentRef(const Indent:TIndentAST):TNodeAST; overload;
    function GetIndentRef(const Signature:TSignatureAST; const Indent:TIndentAST):TNodeAST; overload;
    function GetDeclRef(const Node:TNodeAST):TDeclarationAST; overload;
    function GetHostStatementRef(const Node:TNodeAST):THostStatementAST; overload;
    function GetStatementRef(const Node:TNodeAST):TStatementAST; overload;
    function GetLoopRef(const Node:TNodeAST):TLoopStmAST;
    function GetFuncRef(const Node:TNodeAST):TFuncImpAST;

    procedure LevalUp;
    end;

var
   ASTFabric:TASTFabric;

implementation

{ TASTFabric }

function TASTFabric.NodeCreate(const NodeType:TASTNodeType):TNodeAST;
begin
case NodeType of
atTUnitAST:Result:=TUnitAST.Create;
atTForeignAsmAST:Result:=TForeignAsmAST.Create;
atTImplimentaionAST:Result:=TImplimentaionAST.Create;
atTDeclarationAST:Result:=TDeclarationAST.Create;
atTUsesDeclAST:Result:=TUsesDeclAST.Create;
atTTypeDeclAST:Result:=TTypeDeclAST.Create;
atTConstDeclAST:Result:=TConstDeclAST.Create;
atTVarDeclAST:Result:=TVarDeclAST.Create;
atTLabelDeclAST:Result:=TLabelDeclAST.Create;
atTTypeAST:Result:=TTypeAST.Create;
atTConstAST:Result:=TConstAST.Create;
atTIntegerAST:Result:=TIntegerAST.Create;
atTRealAST:Result:=TRealAST.Create;
atTIndentAST:Result:=TIndentAST.Create;
atTFuncDeclAST:Result:=TFuncDeclAST.Create;
atTFuncImpAST:Result:=TFuncImpAST.Create;
atTTypeArrayAST:Result:=TTypeArrayAST.Create;
atTTypeRecordAST:Result:=TTypeRecordAST.Create;
atTFieldDeclAST:Result:=TFieldDeclAST.Create;
atTFormalParametrsAST:Result:=TFormalParametrsAST.Create;

atTStatementAST:Result:=TStatementAST.Create;
atTGotoStmAST:Result:=TGotoStmAST.Create;
atTForStmAST:Result:=TForStmAST.Create;
atTWhileStmAST:Result:=TWhileStmAST.Create;
atTRepeatStmAST:Result:=TRepeatStmAST.Create;
atTIfStmAST:Result:=TIfStmAST.Create;
atTCaseStmAST:Result:=TCaseStmAST.Create;
atTCaseEdgeAST:Result:=TCaseEdgeAST.Create;
atTLabelingStatementAST:Result:=TLabelingStatementAST.Create;
atTCompaundStatementAST:Result:=TCompaundStatementAST.Create;
atTSimpleStatementAST:Result:=TSimpleStatementAST.Create;
atTExpressionAST:Result:=TExpressionAST.Create;
atTFuncCallAST:Result:=TFuncCallAST.Create;
atTTypeCastAST:Result:=TTypeCastAST.Create;
atTVarAssignAST:Result:=TVarAssignAST.Create;
atTFactorAST:Result:=TFactorAST.Create;
atTImmConstAST:result:=TImmConstAST.Create;
//else
// MessageBox('������ ��� ������ ������ � AST');
end; // case
Result.Parent:=ActivLeaf;
end;



procedure TASTFabric.SetActivLeaf(const Value: TNodeAST);
var
  tmp:TNodeAST;
begin
  Self.FActivLeaf := Value;
end;

procedure TASTFabric.StackCreate(var Goal: TStackAST);
begin
Goal:=TStackAST.Create;
end;

procedure TASTFabric.StackDestroy(var Goal: TStackAST);
begin
Goal.Destroy;

end;

function TASTFabric.IndentCreate(Simvol:TSimvol):TIndentAST;
begin
 Result:=ASTFabric.NodeCreate(atTIndentAST) as TIndentAST;
 Result.Simvol:=Simvol;
end;

function TASTFabric.IntegerCreate(Simvol:TSimvol):TIntegerAST;
begin
 Result:=ASTFabric.NodeCreate(atTIntegerAST) as TIntegerAST;
 Result.Simvol:=Simvol;
end;

function TASTFabric.RealCreate(Simvol:TSimvol):TRealAST;
begin
 Result:=ASTFabric.NodeCreate(atTRealAST) as TRealAST;
 Result.Simvol:=Simvol;
end;

function TASTFabric.UnitCreate:TUnitAST;
begin
 Result:=ASTFabric.NodeCreate(atTUnitAST) as TUnitAST;;
end;

function TASTFabric.DeclarationCreate:TDeclarationAST;
begin
 Result:=ASTFabric.NodeCreate(atTDeclarationAST) as TDeclarationAST;;
end;

function TASTFabric.ImplimentaionCreate:TImplimentaionAST;
begin
 Result:=ASTFabric.NodeCreate(atTImplimentaionAST) as TImplimentaionAST;
end;

function TASTFabric.FuncDeclCreate:TFuncDeclAST;
begin
  Result:=ASTFabric.NodeCreate(atTFuncDeclAST) as TFuncDeclAST;
end;

function TASTFabric.FuncImpCreate:TFuncImpAST;
begin
  Result:=ASTFabric.NodeCreate(atTFuncImpAST) as TFuncImpAST;
end;

function TASTFabric.ForeignAsmCreate: TForeignAsmAST;
begin
  Result:=ASTFabric.NodeCreate(atTForeignAsmAST) as TForeignAsmAST;
end;

function TASTFabric.GetIndentKind(const Indent:TIndentAST):TIndentKind;
var
  Goal:TNodeAST;
begin
  Result:=ikUndefine;
  Goal:=GetIndentRef(Indent);
  if Goal=Nil then
     begin
//     Result:=SystemMagic.GetIndentKind(Indent);
        Result:=ikUndefine
     end else
     begin
     Result:=(Goal as TIndentAST).Kind;
     end;
end;

procedure TASTFabric.LevalUp;
begin
if ActivLeaf<>Nil then
  ActivLeaf:=ActivLeaf.Parent;
end;

function TASTFabric.SearchDeclParent(Node:TNodeAST):TNodeAST;
var
 Flag:Boolean;
begin
if Node=nil then
   begin
   Result:=Nil;
   exit;
   end;
Flag:=False;
repeat
if Node is TFuncImpAST then Flag:=True;
if Node is TImplimentaionAST then Flag:=True;
if Node is TDeclarationAST then Flag:=True;
if Node is TUnitAST then Flag:=True;
if (Flag=False) then Node:=Node.Parent;
until (Node=nil) or (Flag=True);
Result:=Node;
end;

function TASTFabric.GetIndentRef(const Indent:TIndentAST):TNodeAST;

  function SearchInImplimentaion(const Implimentaion:TImplimentaionAST; var Goal:TNodeAST):Boolean;
  var
    Indents:TList_TIndentAST;
    procedure IndentsAdd(const Indent:TIndentAST);
    begin
        SetLength(Indents,Length(Indents)+1);
        Indents[Length(Indents)-1]:=Indent;
    end;

    function GetLast:TNodeAST;
    var
     MaxPos:Integer;
     Id:Integer;
     i:Integer;
    begin
    Id:=0;
    MaxPos:=Indents[ID].Pos;
    for i:=0 to Length(Indents)-1 do
      begin
        if Indents[I].Pos> MaxPos then
           begin
           MaxPos:=Indents[I].Pos;
           ID:=I;
           end;
      end;
    Result:=Indents[ID];
    end;

    procedure SearchInListFunc(const ListFunc:TList_TFuncImpAST);
    var i:Integer;
    begin
     for i:=0 to Length(ListFunc)-1 do
       if SimvolsEq(ListFunc[i].FuncDecl.Indent.Simvol, Indent.Simvol) then
          IndentsAdd(ListFunc[i].FuncDecl.Indent);
     end;
  begin
  Result:=False;
  SearchInListFunc(Implimentaion.ListFunc);
  if Length(Indents)<>0 then
     begin
     Result:=True;
     Goal:=GetLast;
     end;
  end;

  function SearchInDeclaration(const Declaration:TDeclarationAST;var Goal:TNodeAST):Boolean;
  var
    Indents:TList_TIndentAST;
    procedure IndentsAdd(const Indent:TIndentAST);
    begin
        SetLength(Indents,Length(Indents)+1);
        Indents[Length(Indents)-1]:=Indent;
    end;

    function GetLast:TNodeAST;
    var
     MaxPos:Integer;
     Id:Integer;
     i:Integer;
    begin
    Id:=0;
    MaxPos:=Indents[ID].Pos;
    for i:=0 to Length(Indents)-1 do
      begin
        if Indents[I].Pos> MaxPos then
           begin
           MaxPos:=Indents[I].Pos;
           ID:=I;
           end;
      end;
    Result:=Indents[ID];
    end;

    procedure SearchInListType(const ListType:TList_TTypeDeclAST);
    var i:Integer;
    begin
     for i:=0 to Length(ListType)-1 do
       if SimvolsEq(ListType[i].Indent.Simvol, Indent.Simvol) then
          IndentsAdd(ListType[i].Indent);
     end;
    procedure SearchInListConst(const ListConst:TList_TConstDeclAST);
    var i:Integer;
    begin
     for i:=0 to Length(ListConst)-1 do
       if SimvolsEq(ListConst[i].Indent.Simvol, Indent.Simvol) then
          IndentsAdd(ListConst[i].Indent);
     end;
    procedure SearchInListVar(const ListVar:TList_TVarDeclAST);
    var i:Integer;
    begin
     for i:=0 to Length(ListVar)-1 do
       if SimvolsEq(ListVar[i].Indent.Simvol, Indent.Simvol) then
          IndentsAdd(ListVar[i].Indent);
     end;
    procedure SearchInListLabel(const ListLabel:TList_TLabelDeclAST);
    var i:Integer;
    begin
     for i:=0 to Length(ListLabel)-1 do
       if SimvolsEq(ListLabel[i].Indent.Simvol, Indent.Simvol) then
          IndentsAdd(ListLabel[i].Indent);
     end;
    procedure SearchInListFunc(const ListFunc:TList_TFuncDeclAST);
    var i:Integer;
    begin
     for i:=0 to Length(ListFunc)-1 do
       if SimvolsEq(ListFunc[i].Indent.Simvol, Indent.Simvol) then
          IndentsAdd(ListFunc[i].Indent);
     end;
  begin
  result:=False;
  if (Indent=Nil) or (Declaration=Nil) then exit;
  SearchInListType(Declaration.ListType);
  SearchInListConst(Declaration.ListConst);
  SearchInListVar(Declaration.ListVar);
  SearchInListLabel(Declaration.ListLabel);
  SearchInListFunc(Declaration.ListFunc);
  If Length(Indents)<>0 then
     begin
       result:=True;
       Goal:=GetLast;
     end else
       result:=False;

  end;

  function SearchInFuncImp(const FuncImp:TFuncImpAST;var Goal:TNodeAST):Boolean;
    function SearchInName(const FuncDecl:TFuncDeclAST; var Goal:TNodeAST):Boolean;
    begin
    Result:=False;
    if SimvolsEq(FuncDecl.Indent.Simvol, Indent.Simvol) then
       begin
         Goal:=FuncDecl.Indent;
         Result:=True;
       end;
    end;
    function SearchInFormalParametrs(const FormalParametrs:TFormalParametrsAST; var Goal:TNodeAST):Boolean;
    var i:Integer;
    begin
    Result:=False;
     for i:=0 to FormalParametrs.Count-1 do
       if SimvolsEq(FormalParametrs.List[i].VarDecl.Indent.Simvol, Indent.Simvol) then
          begin
          Goal:=FormalParametrs.List[i].VarDecl.Indent;
          Result:=True;
          end;
     end;
  begin
  Result:=False;
  Result:= SearchInDeclaration(FuncImp.LocalDec, Goal);
  if Result=False then
     if FuncImp.FuncDecl<>nil then
        begin
        result:=SearchInFormalParametrs(FuncImp.FuncDecl.FormalParametrs, Goal);
        if Result=False then
           result:=SearchInName(FuncImp.FuncDecl, Goal);
        end;
  end;

var
  Node:TNodeAST;
  Flag:Boolean;
  Goal:TNodeAST;
begin
  Node:=FActivLeaf;
  Flag:=False;
  Goal:=Nil;

  Node:=SearchDeclParent(Node);
  if Node<>Nil then
    repeat
      if Node is TFuncImpAST then Flag:=SearchInFuncImp(Node as TFuncImpAST, Goal);
      if Node is TImplimentaionAST then  Flag:=SearchInImplimentaion(Node as TImplimentaionAST, Goal);
      if Node is TDeclarationAST then Flag:=SearchInDeclaration(Node as TDeclarationAST, Goal);
      if Node is TUnitAST then  Flag:=SearchInDeclaration((Node as TUnitAST).Dec, Goal);
      if (Flag=False) and (node<>nil) then Node:=Node.Parent;
    Node:=SearchDeclParent(Node);
    until (Node=nil) or (Flag=True);
  Result:=Goal;
  If Goal=Nil then
     Result:=SystemMagic.GetIndentRef(Indent);
end;

function TASTFabric.GetIndentRef(const Signature:TSignatureAST; const Indent:TIndentAST):TNodeAST;
var
  VarDeclAST:TVarDeclAST;
  FieldDeclAST:TFieldDeclAST;
  _Type:TTypeAST;
  ListField:TList_TFieldDeclAST;
  i:Integer;
begin
Result:=Nil;
if (Signature.IndentDeclRef is TVarDeclAST) or
   (Signature.IndentDeclRef is TFieldDeclAST) then
  begin
  if (Signature.IndentDeclRef is TVarDeclAST)  then
      begin
      VarDeclAST:=(Signature.IndentDeclRef as TVarDeclAST);
     _type:=VarDeclAST._Type;
     end;
  if (Signature.IndentDeclRef is TFieldDeclAST)  then
      begin
      FieldDeclAST:=(Signature.IndentDeclRef as TFieldDeclAST);
     _type:=FieldDeclAST._Type;
     end;
  while _Type.Kind=tkAlias do
    _Type:=_Type.AliasType._Type;
  if _Type.Kind=tkPointer then
     _Type:=_Type._Pointer;
  while _Type.Kind=tkAlias do
    _Type:=_Type.AliasType._Type;

  if _Type.Kind=tkRecord then
     begin
     ListField:=_Type._Record.Fields;
     for i:=0 to Length(ListField)-1 do
       begin
         if SimvolsEq(ListField[i].Indent.Simvol, Indent.Simvol) then
            begin
            Result:=ListField[i].Indent;
            exit;
            end;
       end;
     end;
  end;
end;

function TASTFabric.GetDeclRef(const Node:TNodeAST):TDeclarationAST;
var DeclParent:TNodeAST;
begin
 Result:=Nil;
 DeclParent:=SearchDeclParent(Node);
 if DeclParent is TFuncImpAST then result:=(DeclParent as TFuncImpAST).LocalDec;
 if DeclParent is TImplimentaionAST then  result:=((DeclParent as TImplimentaionAST).Parent as TUnitAST).Dec;
 if DeclParent is TDeclarationAST then result:=(DeclParent as TDeclarationAST);
 if DeclParent is TUnitAST then  result:=(DeclParent as TUnitAST).Dec;
end;

function TASTFabric.GetHostStatementRef(const Node:TNodeAST):THostStatementAST;
var Goal:TNodeAST;
begin
Goal:=Node;
if (Goal=Nil) then Goal:=Goal.Parent;
while not(Goal=Nil) and not (Goal is THostStatementAST) do
  begin
  Goal:=Goal.Parent;
  end;
Result:=Goal as THostStatementAST;
end;

function TASTFabric.GetStatementRef(const Node:TNodeAST):TStatementAST;
var Goal:TNodeAST;
begin
Goal:=Node;
while not(Goal=Nil) and not (Goal is TStatementAST) do
  begin
  Goal:=Goal.Parent;
  end;
Result:=Goal as TStatementAST;
end;

function TASTFabric.GetLoopRef(const Node:TNodeAST):TLoopStmAST;
var Goal:TNodeAST;
begin
Goal:=Node;
while not(Goal is TLoopStmAST) and
      not (Goal  is TFuncImpAST) and
      not (Goal  is TImplimentaionAST) do
      Goal:=Goal.Parent;
if (Goal is TLoopStmAST) then Result:=Goal as TLoopStmAST
  else Result:=Nil;
end;

function TASTFabric.GetFuncRef(const Node:TNodeAST):TFuncImpAST;
var Goal:TNodeAST;
begin
Goal:=Node;
while not (Goal  is TFuncImpAST) and
      not (Goal  is TImplimentaionAST) do
      Goal:=Goal.Parent;
if (Goal is TFuncImpAST) then Result:=Goal as TFuncImpAST
  else Result:=Nil;
end;

constructor TASTFabric.Create;
begin

end;

function TASTFabric.TypeCreate: TTypeAST;
begin
   Result:=ASTFabric.NodeCreate(atTTypeAST) as TTypeAST;
end;

begin
ASTFabric:=TASTFabric.Create;
end.
