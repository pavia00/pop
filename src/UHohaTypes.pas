{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UHohaTypes;

interface

uses SysUtils, USimvler;

function IsIndentSimplyType(Value:TSimvlerString):Boolean;
function GetSimpleTypeSize(const TypeName: WideString): Integer;

type
  TTypeDesc= record
      Name:WideString;
      Size:Integer;
      end;
const TypesTable:array [0..09] of TTypeDesc=
    (
    (Name:'UInt8';  Size:1),
    (Name:'UInt16'; Size:2),
    (Name:'UInt32'; Size:4),
    (Name:'UInt64'; Size:8),
    (Name:'Integer';Size:4),
    (Name:'Real';   Size:8),
    (Name:'Boolean';Size:4),
    (Name:'Pointer';Size:4),
//    (Name:'PByte';  Size:4),
//    (Name:'PInteger';Size:4),
//    (Name:'PReal';  Size:4),
    (Name:'String'; Size:4),
    (Name:'Char';   Size:2){,
    (Name:'PChar';  Size:4),
    (Name:'DWORD';  Size:4)  }
    );

implementation

function IsIndentSimplyType(Value:TSimvlerString):Boolean;
var
  i:Integer;
  Text:WideString;
begin
Result:=False;

Text:=WideString(Value);
for i :=Low(TypesTable)  to High(TypesTable)  do
  begin
  // Compares value without case sensitivity.
  if WideCompareText(TypesTable[i].Name, Text)=0 then
    begin
      Result:=True;
      Break;
    end;
  end;
end;

function GetSimpleTypeSize(const TypeName: WideString): Integer;
var
  i:Integer;
begin
Result:=-1;
for i :=Low(TypesTable)  to High(TypesTable)  do
  begin
  // Compares value without case sensitivity.
  if CompareText(TypesTable[i].Name,TypeName)=0 then
    begin
      Result:=TypesTable[i].Size;
      Break;
    end;
  end;
end;

end.
