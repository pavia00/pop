{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit USemanticCheking;

interface

uses SysUtils, UAST, UCompiler, USimvler;

type
  TSemanticCheking=class
  private
  protected
  public
     Compiler:TCompiler;
     constructor Create(TheCompiler:TCompiler);
     function unAlias(_Type:TTypeAST):TTypeAST;
     function getSimple(_Type:TTypeAST):TTypeSimpleAST;
     function isType(_Type:TTypeAST; Simvol:TSimvol):boolean;
     function isTypeFast(_Type:TTypeAST; Simvol:TSimvol):boolean;
     function TypeFamily(_Type:TTypeAST):TTypeFamily;
     function TypeFamilyFromSimplePointerDesignator(_Type:TTypeAST):TTypeFamily;
     function TypeFromSimplePointerDesignator(_Type:TTypeAST):TTypeAST;
     function DesignatorTypeFamily(_Type:TTypeAST; Designator: TDesignatorAST):TTypeFamily;
     function SignatureTypeFamily(Signature: TSignatureAST):TTypeFamily;
     function ImmConstTypeFamily(ImmConst: TImmConstAST): TTypeFamily;
     function FactorTypeFamily(Factor:TFactorAST):TTypeFamily;

     function DesignatorType(_Type:TTypeAST; Designator: TDesignatorAST):TTypeAST;
     function SignatureType(Signature: TSignatureAST):TTypeAST;
     function ImmConstType(ImmConst: TImmConstAST): TTypeAST;
     function FactorType(Factor:TFactorAST):TTypeAST;
     function getVarDeclKind(var VarDeclAST:TVarDeclAST):TVarDeclKind;

  end;

implementation


{ TSemanticCheking }

constructor TSemanticCheking.Create(TheCompiler: TCompiler);
begin
  Compiler:=TheCompiler;
end;

function TSemanticCheking.TypeFamilyFromSimplePointerDesignator(_Type: TTypeAST): TTypeFamily;
begin
if (SimvolsEQ(_Type.Simple.Simvol, TS('Pointer'))) then
             result:=tfVoid
else if (SimvolsEQ(_Type.Simple.Simvol, TS('PByte'))) then
             Result:=tfOrdinal
else if (SimvolsEQ(_Type.Simple.Simvol, TS('PInteger'))) then
             Result:=tfOrdinal
else if (SimvolsEQ(_Type.Simple.Simvol, TS('PReal'))) then
             Result:=tfReal
else if (SimvolsEQ(_Type.Simple.Simvol, TS('PChar'))) then
             Result:=tfString
else
   begin
         Compiler.Error.Add(195,'����� ���������');
         Result:=tfUndefine;
   end;
end;

function TSemanticCheking.TypeFromSimplePointerDesignator(_Type: TTypeAST): TTypeAST;
begin
if (SimvolsEQ(_Type.Simple.Simvol, TS('Pointer'))) then
    begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    Result.Simple:=TTypeSimpleAST.Create(TS('Void'));
    end
else if (SimvolsEQ(_Type.Simple.Simvol, TS('PByte'))) then
    begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    Result.Simple:=TTypeSimpleAST.Create(TS('Byte'));
    end
else if (SimvolsEQ(_Type.Simple.Simvol, TS('PInteger'))) then
    begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    Result.Simple:=TTypeSimpleAST.Create(TS('Integer'));
    end
else if (SimvolsEQ(_Type.Simple.Simvol, TS('PReal'))) then
    begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    Result.Simple:=TTypeSimpleAST.Create(TS('Real'));
    end
else if (SimvolsEQ(_Type.Simple.Simvol, TS('PChar'))) then
    begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    Result.Simple:=TTypeSimpleAST.Create(TS('Char'));
    end
else
   begin
      Compiler.Error.Add(195,'����� ���������');
      Result:=TTypeAST.Create;
      Result.Kind:=tkUndefine;
   end;
end;

function TSemanticCheking.DesignatorTypeFamily(_Type: TTypeAST;
  Designator: TDesignatorAST): TTypeFamily;
begin
  if Designator.Kind=dkDesignature then
     begin
     case (_Type.Kind) of
     tkPointer:  Result:=TypeFamily(_Type._Pointer);
     tkSimple:  Result:=TypeFamilyFromSimplePointerDesignator(_Type);
     tkAlias:   Result:= DesignatorTypeFamily(unAlias(_Type), Designator);
     else
       begin
         Compiler.Error.Add(195,'����� ���������'+_Type.Simple.Simvol.Value);
         Result:=tfUndefine;
       end;
     end;
     end
  else if Designator.Kind=dkDesignatureDot then
     begin
     Result:=SignatureTypeFamily(Designator.Signature)
     end
  else if Designator.Kind=dkSignature then
    begin
      Result:=TypeFamily(_Type);
    end;
end;

function TSemanticCheking.ImmConstTypeFamily(
  ImmConst: TImmConstAST): TTypeFamily;
begin
case ImmConst.Kind of
immUndefine: Result:=tfUndefine;                         // ��������� ������
immChar: Result:=tfString;
immString: Result:=tfString;
immInteger: Result:=tfOrdinal;
immReal: Result:=tfReal;
immNil: Result:=tfPointer;
immAddr: Result:=tfPointer;
end; // case

end;

function TSemanticCheking.FactorTypeFamily(
  Factor: TFactorAST): TTypeFamily;
begin
case Factor.Kind of
fkImmConst: Result:=ImmConstTypeFamily(Factor.ImmConst);
fkSignature: Result:=SignatureTypeFamily(Factor.Signature);
end; // case

end;

function TSemanticCheking.SignatureTypeFamily(
  Signature: TSignatureAST): TTypeFamily;
var
  VarDeclAST:TVarDeclAST;
  FieldDeclAST:TFieldDeclAST;
  _Type:TTYpeAST;
  SubDesignator:TDesignatorAST;
begin
   if (Signature.IndentDeclRef is TVarDeclAST) then
     begin
     while (Signature.Designator.Kind=dkDesignatureDot) do
        Signature:=Signature.Designator.Signature;
     if Signature.IndentDeclRef is TVarDeclAST then
        begin
          VarDeclAST:=Signature.IndentDeclRef as TVarDeclAST;
          _Type:=VarDeclAST._Type;
        end;
     if Signature.IndentDeclRef is TFieldDeclAST then
        begin
          FieldDeclAST:=Signature.IndentDeclRef as TFieldDeclAST;
          _Type:=FieldDeclAST._Type;
        end;

     if Signature.Designator.Kind=dkDesignature then
        begin
        Result:=TypeFamily(DesignatorType(_Type, Signature.Designator))
        end else
        begin
        Result:=TypeFamily(_Type);
        end;
     end else Result:=tfUndefine;

end;

function TSemanticCheking.TypeFamily(_Type: TTypeAST): TTypeFamily;
const
 KeyWordsList:array [1..15] of
    record
    Simvol:string;
    Family:TTypeFamily;
    end
    =
  (
  (Simvol:'Byte'; Family:tfOrdinal),
  (Simvol:'Integer'; Family:tfOrdinal),
  (Simvol:'UInt8'; Family:tfOrdinal),
  (Simvol:'UInt16'; Family:tfOrdinal),
  (Simvol:'UInt32'; Family:tfOrdinal),
  (Simvol:'Real'; Family:tfReal),
  (Simvol:'Boolean'; Family:tfBoolean),
  (Simvol:'Pointer'; Family:tfPointer),
  (Simvol:'PByte'; Family:tfPointer),
  (Simvol:'PInteger'; Family:tfPointer),
  (Simvol:'PReal'; Family:tfPointer),
  (Simvol:'PBoolean'; Family:tfPointer),
  (Simvol:'string'; Family:tfString),
  (Simvol:'Char'; Family:tfString),
  (Simvol:'PChar'; Family:tfPointer));
var
  I:Integer;
begin
 Result:=tfUndefine;
 case _Type.Kind of
 tkUndefine: Result:=tfUndefine;
 tkImmConst:
    begin
    Result:=tfUndefine;
    for i :=Low(KeyWordsList)  to High(KeyWordsList)  do
      if CompareText(KeyWordsList[i].Simvol, _Type.ImmConst.Simvol.Value)=0 then
         Result:=KeyWordsList[i].Family;
    end;
 tkSimple:
    begin
    Result:=tfUndefine;
    for i :=Low(KeyWordsList)  to High(KeyWordsList)  do
      if CompareText(KeyWordsList[i].Simvol, _Type.Simple.Simvol.Value)=0 then
         Result:=KeyWordsList[i].Family;
    end;
 tkAlias:   Result:=TypeFamily(unAlias(_Type));
 tkRecord:  Result:=tfStructure;
 tkArray:   Result:=tfStructure;
 tkPointer: Result:=tfPointer;
 end;
end;

function TSemanticCheking.DesignatorType(_Type: TTypeAST;
  Designator: TDesignatorAST): TTypeAST;
var AType: TTypeAST;
begin
  if Designator.Kind=dkDesignature then
     begin
       case _Type.Kind of
       tkPointer: Result:=_Type._Pointer;
       tkSimple: Result:=TypeFromSimplePointerDesignator(_Type);
       tkAlias: Result:= DesignatorType(unAlias(_Type), Designator);
       else
         begin
         Compiler.Error.Add(304,'�������� ^ �������� ������ � ����������.');
         Result:=TTYpeAST.Create;
         Result.Kind:=tkUndefine;
         end;
       end;
     end
  else if Designator.Kind=dkDesignatureDot then
     begin
     Result:=SignatureType(Designator.Signature)
     end
  else if Designator.Kind=dkDeSignatureArray then
     begin
     AType:=unAlias(_Type);
     if  (AType._Array<> nil) then
           result:=AType._Array._Type
         else
         begin
         Compiler.Error.Add(305,'�������� [] �������� ������ � ��������');
         Result:=TTYpeAST.Create;
         Result.Kind:=tkUndefine;
         end;
     end
  else if Designator.Kind=dkSignature then
    begin
      Result:=_Type;
    end;
  if (Designator.Kind<>dkSignature) and (_Type.Kind<>tkAlias)  then
    begin
    Result:=DesignatorType(Result, Designator.SubDesignator);
    end;
end;

function TSemanticCheking.SignatureType(
  Signature: TSignatureAST): TTypeAST;
var
  VarDeclAST:TVarDeclAST;
  FieldDeclAST:TFieldDeclAST;
  _Type:TTYpeAST;
begin
     if Signature.IndentDeclRef is TFieldDeclAST then
        begin
          FieldDeclAST:=Signature.IndentDeclRef as TFieldDeclAST;
          Result:=FieldDeclAST._Type;
        end else
   if (Signature.IndentDeclRef is TVarDeclAST) then
     begin
     while (Signature.Designator.Kind=dkDesignatureDot) do
        Signature:=Signature.Designator.Signature;
     if Signature.IndentDeclRef is TVarDeclAST then
        begin
          VarDeclAST:=Signature.IndentDeclRef as TVarDeclAST;
          _Type:=VarDeclAST._Type;
        end;
     if Signature.IndentDeclRef is TFieldDeclAST then
        begin
          FieldDeclAST:=Signature.IndentDeclRef as TFieldDeclAST;
          _Type:=FieldDeclAST._Type;
        end;

     Result:=DesignatorType(_Type, Signature.Designator)
{     if (Signature.Designator.Kind=dkDesignature) then
        begin
        end else
        if  (Signature.Designator.Kind= dkDeSignatureArray) then
          begin
          Result:=DesignatorType(_Type, Signature.Designator)
          end else
          begin
          Result:=_Type;
          end;
}     end else
     begin
       Result:=TTypeAST.Create;
       Result.Kind:=tkUndefine;
     end;
end;

function TSemanticCheking.ImmConstType(ImmConst: TImmConstAST): TTypeAST;
var
 Indent:TIndentAST;
 ConstDeclAST:TConstDeclAST;
 VarDeclAST:TVarDeclAST;
begin
case ImmConst.Kind of
immUndefine:
  begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkUndefine;
  end;
immChar:
  begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    Result.Simple:=TTypeSimpleAST.Create(TS('Char'));
  end;
immString:
  begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    if Length(ImmConst._String.Simvol.Value)=1 then
         Result.Simple:=TTypeSimpleAST.Create(TS('Char'))
       else
         Result.Simple:=TTypeSimpleAST.Create(TS('String'));
  end;
immInteger:
  begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    Result.Simple:=TTypeSimpleAST.Create(TS('Integer'))
  end;
immReal:
  begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    Result.Simple:=TTypeSimpleAST.Create(TS('Real'));
  end;
immNil:
  begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkSimple;
    Result.Simple:=TTypeSimpleAST.Create(TS('Pointer'));
  end;
immAddr:
  begin
    Result:=TTypeAST.Create;
    Result.Kind:=tkPointer;
    Result._Pointer:=SignatureType(ImmConst.Addr.Signature);

  end;
end; //case
end;

function TSemanticCheking.FactorType(Factor: TFactorAST): TTypeAST;
begin
case Factor.Kind of
fkSignature: Result:=SignatureType(Factor.Signature);
fkImmConst: Result:=ImmConstType(Factor.ImmConst);
end; //case
end;

function TSemanticCheking.unAlias(_Type: TTypeAST): TTypeAST;
begin
Result:=_Type;
while (Result.Kind=tkAlias) do
  begin
    Result:=Result.AliasType._Type;
  end;
end;

function TSemanticCheking.getSimple(_Type: TTypeAST): TTypeSimpleAST;
var tmp:TTypeAST;
begin
if _Type=nil then
   begin
   result:=UndefineTypeSimple;
   end else
   begin
   tmp:=unAlias(_Type);
   if (tmp.Kind=tkSimple) then
      begin
        result:=tmp.Simple;
      end else
      begin
        result:=UndefineTypeSimple;
      end;
   end;   
end;

function TSemanticCheking.isType(_Type: TTypeAST;
  Simvol: TSimvol): boolean;
begin
  result:=SimvolsEQ(getSimple(_Type).Simvol, Simvol);
end;

function TSemanticCheking.isTypeFast(_Type: TTypeAST;
  Simvol: TSimvol): boolean;
begin
  result:=SimvolsEQ(_Type.Simple.FSimvol, Simvol);
end;

function TSemanticCheking.getVarDeclKind(var VarDeclAST:TVarDeclAST):TVarDeclKind;
begin
result:=vdkUndefine;
if (VarDeclAST.Parent is TParametrAST) then
   begin
   result:=vdkParam;
   end else
   if VarDeclAST.Parent is TDeclarationAST then
   begin
   if VarDeclAST.Parent.parent is TFuncImpAST then
      result:=vdkLocal
      else
      result:=vdkGlobal;
   end;
end;

end.
