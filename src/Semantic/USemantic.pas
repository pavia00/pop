{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit USemantic;

interface

uses SysUtils, UAST, UASTFabric, LICompiler, UCompiler, UGramAnaliser, USimvler,
 USystemMagic, USemanticCheking;

type
  TSemanter = class(TCompilerWoker)
  private
    FSemanticCheking:TSemanticCheking;
  protected
    function SearchDecl(Node:TNodeAST):TDeclarationAST;
  public
    Compiler:TCompiler;
    procedure reset; override;
    procedure process; override;
    constructor Create(TheCompiler:TCompiler);
    procedure PUnitAST(const UnitAST:TUnitAST);
    procedure PImplimentaion(const ImplimentaionAST:TImplimentaionAST);
    procedure PDeclaration(const DeclarationAST:TDeclarationAST);
    procedure PTypeDecl(const TypeDeclAST:TTypeDeclAST);
    procedure PVarDecl(const VarDeclAST:TVarDeclAST);
    procedure PConstDecl(const ConstDeclAST:TConstDeclAST);
    procedure PLabelDecl(const LabelDeclAST:TLabelDeclAST);
    procedure PFuncDecl(const FuncDeclAST:TFuncDeclAST);
    procedure PIndent(const IndentAST:TIndentAST);
    procedure PType(const TypeAST:TTypeAST);
    procedure PTypeSimple(const TypeSimpleAST:TTypeSimpleAST);
    procedure PTypeRecord(const TypeRecordAST:TTypeRecordAST);
    procedure PFieldDecl(const FieldDeclAST:TFieldDeclAST);
    procedure PConst(const ConstAST:TConstAST);
    procedure PFormalParametrs(const FormalParametrsAST:TFormalParametrsAST);
    procedure PParametr(const ParametrAST:TParametrAST);
    procedure PFuncImp(const FuncImpAST:TFuncImpAST);
    procedure PCompaundStatement(const CompaundStatementAST:TCompaundStatementAST);
    procedure PMainBlock(const CompaundStatementAST:TCompaundStatementAST);
    procedure PStatement(const StatementAST:TStatementAST);
    procedure PGoto(const GotoStmAST:TGotoStmAST);
    procedure PFor(const ForStmAST:TForStmAST);
    procedure PWhile(const WhileStmAST:TWhileStmAST);
    procedure PRepeat(const RepeatStmAST:TRepeatStmAST);
    procedure PIf(const IfStmAST:TIfStmAST);
    procedure PCase(const CaseStmAST:TCaseStmAST);
    procedure PCaseEdge(const CaseEdgeAST:TCaseEdgeAST);
    procedure PLabelingStatement(const LabelingStatementAST:TLabelingStatementAST);
    procedure PSimpleStatement(const SimpleStatementAST:TSimpleStatementAST);
    procedure PFuncCall(const FuncCallAST:TFuncCallAST);
    procedure PCallParametr(const CallParametrAST:TCallParametrAST);
    procedure PCallParametrs(const CallParametrsAST:TCallParametrsAST);
    procedure PVarAssignVar_String(const VarAssignAST: TVarAssignAST);
    procedure PVarAssignTypeCast(const VarAssignAST:TVarAssignAST);
    procedure PVarAssignFunc(const VarAssignAST:TVarAssignAST);
    procedure PVarAssignVar(const VarAssignAST:TVarAssignAST);
    procedure PVarAssignConst(const VarAssignAST:TVarAssignAST);
    procedure PVarAssignNumber(const VarAssignAST: TVarAssignAST);
    procedure PVarAssignAddr(const VarAssignAST:TVarAssignAST);

    procedure PVarAssignSimpleSignature(const VarAssignAST: TVarAssignAST);
    procedure PVarAssignSimpleImmConst(const VarAssignAST:TVarAssignAST);
    procedure PVarAssignSimple(const VarAssignAST:TVarAssignAST);
    procedure PVarAssignUnari(const VarAssignAST: TVarAssignAST);
    procedure PVarAssignBinary(const VarAssignAST: TVarAssignAST);
    procedure PVarAssign(const VarAssignAST:TVarAssignAST);
    procedure PSignature(const SignatureAST:TSignatureAST);
    procedure PDesignator(const DesignatorAST:TDesignatorAST);
    procedure PExpression(const ExpressionAST:TExpressionAST);
    procedure PExpressionList(const ExpressionListAST:TExpressionListAST);
    procedure POperator(const OperatorAST:TOperatorAST);
    procedure PImmConst(const ImmConstAST: TImmConstAST);
    procedure PFactor(const FactorAST:TFactorAST);
    procedure PForeignAsm( ForeignAsm:TForeignAsmAST);
    function  IsMagicFunc(const FuncCallAST: TFuncCallAST):Boolean;
    procedure PMagicFunc(const FuncCallAST: TFuncCallAST);
    function  GenNewLabel():TLabelDeclAST;
    function  GenCompaundStatement(StatementAST:TStatementAST):TCompaundStatementAST;
    function  GenLabelStatement(LabelDeclAST:TLabelDeclAST):TStatementAST;
    function  GenGotoStatement(LabelDeclAST: TLabelDeclAST): TStatementAST;
    function  CreateNewCall(const StatementNode: TNodeAST; Simvol:TSimvol):TFuncCallAST;
    procedure DestroyFuncCall(var FuncCallAST: TFuncCallAST);
    procedure DestroyVarAssign(var VarAssign: TVarAssignAST);
    function  PathFuncCall(const Simvol:TSimvol; OldFuncCallAST:TFuncCallAST):boolean;
    function  PathAssignToFuncCall(const Simvol:TSimvol; OldVarAssignAST: TVarAssignAST):boolean;
    function  getTypeFamily(_Type:TTypeAST): TTypeFamily;
    function  TypeFamily(_Type:TTypeAST):TTypeFamily;
    function  DesignatorTypeFamily(_Type:TTypeAST; Designator: TDesignatorAST):TTypeFamily;
    function  SignatureTypeFamily(Signature: TSignatureAST):TTypeFamily;
    function  FactorTypeFamily(Factor:TFactorAST):TTypeFamily;
    function  SignatureType(Signature: TSignatureAST): TTypeAST;
    function  FactorType(Factor:TFactorAST):TTypeAST;
    function  DesignatorType(_Type:TTypeAST; Designator: TDesignatorAST):TTypeAST;
    function  unAlias(const TypeAST: TTypeAST):TTypeAST;
    function  isType(_Type:TTypeAST; Simvol:TSimvol):boolean;
    function  isTypeFast(_Type:TTypeAST; Simvol:TSimvol):boolean;

  published

  end;

implementation

{ TSemanter }

constructor TSemanter.Create(TheCompiler: TCompiler);
begin
  Compiler:=TheCompiler;
  FSemanticCheking:=TSemanticCheking.Create(Compiler);
end;

procedure TSemanter.reset;
begin

end;

procedure TSemanter.Process;
var
  GA:TGramAnaliser;
begin
  GA:=Compiler.GramAnaliser as TGramAnaliser;
  if Compiler.Error.Count=0 then
     begin
     PUnitAST(GA.AST);
     end;
end;

function TSemanter.SearchDecl(Node:TNodeAST):TDeclarationAST;
var
 Flag:Boolean;
begin
  if Node=nil then
     begin
     Result:=Nil;
     exit;
     end;
  Flag:=False;
  repeat
  if Node is TFuncImpAST then Flag:=True;
  if Node is TImplimentaionAST then Flag:=True;
  if Node is TDeclarationAST then Flag:=True;
  if Node is TUnitAST then Flag:=True;
  if (Flag=False) then Node:=Node.Parent;
  until (Node=nil) or (Flag=True);

  if Node is TFuncImpAST then Result:= (Node as TFuncImpAST).LocalDec;
  if Node is TImplimentaionAST then Result:= (Node.parent as TUnitAST).Dec;
  if Node is TDeclarationAST then Result:=Node as TDeclarationAST;
  if Node is TUnitAST then  Result:= (Node as TUnitAST).Dec;
//  Result:=Node;
end;

procedure TSemanter.PCallParametr(const CallParametrAST:TCallParametrAST);
begin
case CallParametrAST.kind of
cpVarDecl: PIndent(CallParametrAST.VarDecl.Indent);
cpConstDecl: PIndent(CallParametrAST.ConstDecl.Indent);
end;
end;

procedure TSemanter.PCallParametrs(
  const CallParametrsAST: TCallParametrsAST);
var i:Integer;
begin
 With CallParametrsAST do
   if Length(Items)<>0 then
      begin
      for i:=0 to Length(Items)-2 do
        begin
        PCallParametr(Items[i]);
        end;
      i:=Length(Items)-1;
      PCallParametr(Items[i]);
      end;

end;

procedure TSemanter.PCase(const CaseStmAST: TCaseStmAST);
var
  I:Integer;
begin
  PIndent(CaseStmAST.RefVar.Indent);
  for i:=0 to Length(CaseStmAST.Edges)-1 do
    PCaseEdge(CaseStmAST.Edges[i]);
  if CaseStmAST.Defult<>nil then
     begin
       PStatement(CaseStmAST.Defult);
     end;
end;

procedure TSemanter.PCaseEdge(const CaseEdgeAST: TCaseEdgeAST);
begin
  Case CaseEdgeAST.Kind of
  kceUndefine:;
  kceRef: PIndent(CaseEdgeAST.ConstRef.Indent);
  kceImmConst:PImmConst(CaseEdgeAST.ImmConst);
  end;
  PStatement(CaseEdgeAST.Statement);
end;

procedure TSemanter.PCompaundStatement(
  const CompaundStatementAST: TCompaundStatementAST);
var
  i:Integer;
  ListStatemt:TList_TStatementAST;
begin
ListStatemt:=Copy(CompaundStatementAST.ListStatemt);
for i:=0 to Length(ListStatemt)-1 do
  begin
  PStatement(ListStatemt[i]);
  end;
end;

procedure TSemanter.PConst(const ConstAST: TConstAST);
begin
if ConstAST=Nil then
   else
    begin
    end;
end;

procedure TSemanter.PConstDecl(const ConstDeclAST: TConstDeclAST);
begin
PIndent(ConstDeclAST.Indent);
PType(ConstDeclAST._Type);
PConst(ConstDeclAST._const);
end;

procedure TSemanter.PDeclaration(
  const DeclarationAST: TDeclarationAST);
var i:Integer;
begin
for i:=0 to Length(DeclarationAST.ListType)-1 do
  begin
  PTypeDecl(DeclarationAST.ListType[i]);
  end;

for i:=0 to Length(DeclarationAST.ListVar)-1 do
  begin
  PVarDecl(DeclarationAST.ListVar[i]);
  end;

for i:=0 to Length(DeclarationAST.ListConst)-1 do
  begin
  PConstDecl(DeclarationAST.ListConst[i]);
  end;

for i:=0 to Length(DeclarationAST.ListLabel)-1 do
  begin
  PLabelDecl(DeclarationAST.ListLabel[i]);
  end;

for i:=0 to Length(DeclarationAST.ListFunc)-1 do
  begin
  PFuncDecl(DeclarationAST.ListFunc[i]);
  end;
end;

procedure TSemanter.PDesignator(const DesignatorAST: TDesignatorAST);
begin
  case DesignatorAST.Kind of
  dkSignature:;{������ �������}
  dkDesignature:
     begin
     end;
  dkDesignatureDot:
     begin
       PSignature(DesignatorAST.Signature);
     end;
  dkDeSignatureArray:
     begin
       PExpression(DesignatorAST.Expression);
     end;
  dkDeSignatureFunc:
     begin
       PExpressionList(DesignatorAST.ExpressionList);
     end;
  end; // case
if DesignatorAST.SubDesignator<>nil then
   PDesignator(DesignatorAST.SubDesignator);
end;

procedure TSemanter.PExpression(const ExpressionAST: TExpressionAST);
begin
  case ExpressionAST.kind of
  ekSimple:
    begin
      PFactor(ExpressionAST.Factor1);
    end;
  ekUnari:
    begin
      POperator(ExpressionAST.Op);
      PFactor(ExpressionAST.Factor1);
    end;
  ekBinary:
    begin
      PFactor(ExpressionAST.Factor1);
      POperator(ExpressionAST.Op);
      PFactor(ExpressionAST.Factor2);
    end;
  end;
end;

procedure TSemanter.PExpressionList(
  const ExpressionListAST: TExpressionListAST);
var i:Integer;
begin
  for i:=0 to Length(ExpressionListAST.Items)-1 do
    begin
    PExpression(ExpressionListAST.Items[i]);
    end;
end;



procedure TSemanter.PImmConst(const ImmConstAST: TImmConstAST);
begin
if ImmConstAST<>nil then
   begin
   case ImmConstAST.Kind of
   immUndefine: ;
   immChar: PConst(ImmConstAST._Char);   
   immString: PConst(ImmConstAST._String);
   immInteger: PConst(ImmConstAST._Integer);
   immReal: PConst(ImmConstAST._Real);
   immNil: ;
   immAddr:
     begin
     PSignature(ImmConstAST.Addr.Signature);
     end;
   end; // case
   end;
end;

procedure TSemanter.PFactor(const FactorAST: TFactorAST);
begin
 if FactorAST<> nil then
  case FactorAST.Kind of
  fkSignature: PSignature(FactorAST.Signature);
  fkImmConst: PImmConst(FactorAST.ImmConst);
  end; // case
end;

procedure TSemanter.PFieldDecl(const FieldDeclAST: TFieldDeclAST);
begin
  PIndent(FieldDeclAST.Indent);
  PType(FieldDeclAST._Type);
end;

procedure TSemanter.PFor(const ForStmAST: TForStmAST);
var  LoacalDecl:TDeclarationAST;
  StatementAST:TStatementAST;
  HostStatementAST:THostStatementAST;
  CompaundStatementAST:TCompaundStatementAST;
  LabelStatemnt:TStatementAST;
begin
 LoacalDecl:=ASTFabric.GetDeclRef(ForStmAST);
 ForStmAST.StartLabel := GenNewLabel();
 ForStmAST.EndLabel := GenNewLabel();


 HostStatementAST:=ASTFabric.GetHostStatementRef(ForStmAST.parent);
 StatementAST:=ASTFabric.GetStatementRef(ForStmAST);
 if StatementAST<>nil then
   begin
   if HostStatementAST is  TCompaundStatementAST then
        CompaundStatementAST:=HostStatementAST as TCompaundStatementAST
        else
        begin
        CompaundStatementAST:=GenCompaundStatement(StatementAST);
        HostStatementAST.Replace(StatementAST, CompaundStatementAST.ListStatemt[0]);
        CompaundStatementAST.Parent:=HostStatementAST;
        end;

//   CompaundStatementAST:= StatementAST.Parent as TCompaundStatementAST;
   LabelStatemnt:=GenLabelStatement(ForStmAST.StartLabel);
   LabelStatemnt.Parent:=CompaundStatementAST;
   CompaundStatementAST.AddUpper(StatementAST, LabelStatemnt);
   LabelStatemnt:=GenLabelStatement(ForStmAST.EndLabel);
   LabelStatemnt.Parent:=CompaundStatementAST;
   CompaundStatementAST.AddLower(StatementAST, LabelStatemnt);
   end;
 {Process tree}
 PIndent(ForStmAST.RefVar.Indent);
 PExpression(ForStmAST.Expr1);

 PExpression(ForStmAST.Expr2);
 PStatement(ForStmAST.Statement);

end;

procedure TSemanter.PFormalParametrs(
  const FormalParametrsAST: TFormalParametrsAST);
var
  i:Integer;
begin
 for i:=0 to FormalParametrsAST.Count-1 do
   begin
     PParametr(FormalParametrsAST.List[i]);
   end;
end;

procedure TSemanter.PFuncCall(const FuncCallAST: TFuncCallAST);
begin
  if IsMagicFunc(FuncCallAST) then
     PMagicFunc(FuncCallAST)
   else
   begin
   PIndent(FuncCallAST.RefFunc.Indent);
   PCallParametrs(FuncCallAST.Parametrs);
   end;
end;

procedure TSemanter.PFuncDecl(const FuncDeclAST: TFuncDeclAST);
begin
if FuncDeclAST.kind=kfFunction then
   begin
   PIndent(FuncDeclAST.Indent);
   PFormalParametrs(FuncDeclAST.FormalParametrs);
   PType(FuncDeclAST.result);

   end;
if FuncDeclAST.kind=kfProcedure then
   begin
   PIndent(FuncDeclAST.Indent);
   PFormalParametrs(FuncDeclAST.FormalParametrs);
   end;

end;

procedure TSemanter.PFuncImp(const FuncImpAST: TFuncImpAST);
var  LoacalDecl:TDeclarationAST;
   CompaundStatementAST: TCompaundStatementAST;
   StatementAST:TStatementAST;
   LabelStatemnt:TStatementAST;
begin
 LoacalDecl:=ASTFabric.GetDeclRef(FuncImpAST);
 FuncImpAST.EndLabel := GenNewLabel();


 if FuncImpAST.CompaundStatement<>nil then
   begin
   CompaundStatementAST:= FuncImpAST.CompaundStatement;
   LabelStatemnt:=GenLabelStatement(FuncImpAST.EndLabel);
   LabelStatemnt.Parent:=CompaundStatementAST;

   //[!] �������� ������� ������� � begin
   if CompaundStatementAST.Count>=1 then
      begin
      StatementAST:=CompaundStatementAST.ListStatemt[CompaundStatementAST.Count-1];
      LabelStatemnt.Line:=StatementAST.Line;
      end;
   CompaundStatementAST.AddLower(StatementAST, LabelStatemnt);
   end;

PFuncDecl(FuncImpAST.FuncDecl);
PDeclaration(FuncImpAST.LocalDec);
if FuncImpAST.CompaundStatement<> nil then
   PCompaundStatement(FuncImpAST.CompaundStatement);
if FuncImpAST.AsmBlock<> nil then
   PForeignAsm(FuncImpAST.AsmBlock);
end;

procedure TSemanter.PGoto(const GotoStmAST: TGotoStmAST);
begin
 PIndent(GotoStmAST.RefLabel.Indent);
end;

procedure TSemanter.PIf(const IfStmAST: TIfStmAST);
begin
 PIndent(IfStmAST.BoolExpr.Indent);
 if IfStmAST.TrueEdge<> nil then
    PStatement(IfStmAST.TrueEdge);
 if IfStmAST.FalseEdge<> nil then
    begin
    PStatement(IfStmAST.FalseEdge);
    end;
end;

procedure TSemanter.PImplimentaion(
  const ImplimentaionAST: TImplimentaionAST);
var
  i:Integer;
begin
for i:=0 to Length(ImplimentaionAST.ListFunc)-1 do
  begin
  PFuncImp(ImplimentaionAST.ListFunc[i]);
  end;
PMainBlock(ImplimentaionAST.MainBlock);
end;

procedure TSemanter.PIndent(const IndentAST: TIndentAST);
begin
end;

procedure TSemanter.PLabelDecl(const LabelDeclAST: TLabelDeclAST);
begin
PIndent(LabelDeclAST.Indent);
end;

procedure TSemanter.PLabelingStatement(
  const LabelingStatementAST: TLabelingStatementAST);
begin
With LabelingStatementAST do
 case Kind of
 ksLabeling:
   begin
   if RefLabel= nil then
      else
      begin
        RefLabel.LabelingStatement:=LabelingStatementAST;
        PIndent(RefLabel.Indent);
      end;
   PLabelingStatement(LabelingStatementAST.LabelingStatement);
   end;
 ksSimple:
   begin
   if SimpleStatement<>nil then
      PSimpleStatement(SimpleStatement);
   end;
 end;
end;

procedure TSemanter.PMainBlock(
  const CompaundStatementAST: TCompaundStatementAST);
begin
if CompaundStatementAST=Nil then
   else  PCompaundStatement(CompaundStatementAST);
end;

procedure TSemanter.POperator(const OperatorAST: TOperatorAST);
begin

end;

procedure TSemanter.PParametr(const ParametrAST: TParametrAST);
begin
  PIndent(ParametrAST.VarDecl.Indent);
  PType(ParametrAST.VarDecl._Type);
end;

procedure TSemanter.PRepeat(const RepeatStmAST: TRepeatStmAST);
var  LoacalDecl:TDeclarationAST;
  StatementAST:TStatementAST;
  HostStatementAST:THostStatementAST;
  CompaundStatementAST:TCompaundStatementAST;
  LabelStatemnt:TStatementAST;
begin
 LoacalDecl:=ASTFabric.GetDeclRef(RepeatStmAST);
 RepeatStmAST.StartLabel := GenNewLabel();
 RepeatStmAST.EndLabel := GenNewLabel();

 HostStatementAST:=ASTFabric.GetHostStatementRef(RepeatStmAST.parent);
 StatementAST:=ASTFabric.GetStatementRef(RepeatStmAST);
 if StatementAST<>nil then
   begin
   if HostStatementAST is  TCompaundStatementAST then
        CompaundStatementAST:=HostStatementAST as TCompaundStatementAST
        else
        begin
        CompaundStatementAST:=GenCompaundStatement(StatementAST);
        HostStatementAST.Replace(StatementAST, CompaundStatementAST.ListStatemt[0]);
        CompaundStatementAST.Parent:=HostStatementAST;
        end;
   LabelStatemnt:=GenLabelStatement(RepeatStmAST.StartLabel);
   LabelStatemnt.Parent:=CompaundStatementAST;
   CompaundStatementAST.AddUpper(StatementAST, LabelStatemnt);
   LabelStatemnt:=GenLabelStatement(RepeatStmAST.EndLabel);
   LabelStatemnt.Parent:=CompaundStatementAST;
   CompaundStatementAST.AddLower(StatementAST, LabelStatemnt);
   end;
 {process}
  PCompaundStatement(RepeatStmAST.CompaundStatement);
  PIndent(RepeatStmAST.BoolExpr.Indent);
end;

procedure TSemanter.PSignature(const SignatureAST: TSignatureAST);
begin
  PIndent(SignatureAST.IndentDeclRef.Indent);
  PDesignator(SignatureAST.Designator);
end;

procedure TSemanter.PSimpleStatement(
  const SimpleStatementAST: TSimpleStatementAST);
begin
  case SimpleStatementAST.kind of
  ssUndefine: ;
  ssFuncCall: PFuncCall(SimpleStatementAST.FuncCall);
  ssTypeCast: ;
  ssVarAssign: PVarAssign(SimpleStatementAST.VarAssign);
  end;

end;

procedure TSemanter.PStatement(const StatementAST: TStatementAST);
begin
FLastPosRow:=StatementAST.Line;
FLastPosCol:=0;

With StatementAST do
  case Kind of
  ksGoto: PGoto(_Goto);
  ksFor: PFor(_For);
  ksWhile: PWhile(_While);
  ksRepeat:PRepeat(_Repeat);
  ksIf: PIf(_IF);
  ksCase: PCase(_Case);
  ksLabelingStatement: PLabelingStatement(LabelingStatement);
  ksCompaundStatement: PCompaundStatement(CompaundStatement);
  end;
end;

procedure TSemanter.PType(const TypeAST: TTypeAST);
begin
if TypeAST=nil then
  else
  begin
   case TypeAST.Kind of
   tkUndefine: ;
   tkAlias:  PIndent(TypeAST.AliasType.Indent);
   tkSimple: PTypeSimple(TypeAST.Simple);
   tkRecord: PTypeRecord(TypeAST._Record);
   tkArray: ;
   end;
  end;

end;

procedure TSemanter.PTypeDecl(const TypeDeclAST: TTypeDeclAST);
begin
  PIndent(TypeDeclAST.Indent);
  PType(TypeDeclAST._Type);
end;

procedure TSemanter.PTypeRecord(const TypeRecordAST: TTypeRecordAST);
var
 I:Integer;
begin
  for i:=0 to Length(TypeRecordAST.Fields)-1 do
    PFieldDecl(TypeRecordAST.Fields[i]);
end;

procedure TSemanter.PTypeSimple(const TypeSimpleAST: TTypeSimpleAST);
begin

end;

procedure TSemanter.PUnitAST(const UnitAST: TUnitAST);
begin
  PDeclaration(UnitAst.Dec);
  if UnitAst.Imp= Nil then
    else PImplimentaion(UnitAst.Imp);
end;

procedure TSemanter.PVarAssignAddr(const VarAssignAST: TVarAssignAST);
begin
  PSignature(VarAssignAST.Signature);
  PExpression(VarAssignAST.Expression);
end;

procedure TSemanter.PVarAssignNumber(const VarAssignAST: TVarAssignAST);
begin
  PSignature(VarAssignAST.Signature);
  PExpression(VarAssignAST.Expression);
end;

procedure TSemanter.PVarAssignVar_String(
  const VarAssignAST: TVarAssignAST);
var LeftType, RightType:TTypeAST;
  Flag:Boolean;
begin
  Assert(VarAssignAST<> nil, '96');
  LeftType:=SignatureType(VarAssignAST.Signature);
  RightType:=FactorType(VarAssignAST.Expression.Factor1);
  if  (TypeFamily(RightType)=tfString) then
    begin
      if isType(LeftType, TS('Char')) and  isType(RightType, TS('Char'))  then
         begin
         //���������� �� ������������ ������ � ����������
         {NOP}
         end;
      if isType(LeftType, TS('Char')) and  isType(RightType, TS('String'))  then
         begin
         Compiler.Error.Add(302,'������������ ��������. ������ ��������� ����� ������ ����� ������');
         end;
      if isType(LeftType, TS('String')) and  isType(RightType, TS('Char'))  then
         begin
         Flag:=PathAssignToFuncCall(TS('AssignStringChar'),VarAssignAST);
         if Flag=false then Compiler.Error.Add(300 {'��������� ���������� �������'}, 'AssignStringChar');
         end;
      if isType(LeftType, TS('String')) and  isType(RightType, TS('String'))  then
         begin
         Flag:=PathAssignToFuncCall(TS('AssignStringString'),VarAssignAST);
         if Flag=false then Compiler.Error.Add(300 {'��������� ���������� �������'}, 'AssignStringString');
         end;
    end;
end;

procedure TSemanter.PVarAssignSimpleImmConst(
  const VarAssignAST: TVarAssignAST);
var ImmConst:TImmConstAST;
  LeftType, RightType:TTypeAST;
  Flag:Boolean;
begin
  ImmConst:=VarAssignAST.Expression.Factor1.ImmConst;
  case ImmConst.Kind of
  immChar,immString:
     begin
      LeftType:=SignatureType(VarAssignAST.Signature);
      RightType:=FactorType(VarAssignAST.Expression.Factor1);
      if isType(LeftType, TS('String')) and  isType(RightType, TS('Char'))  then
         begin
         Flag:=PathAssignToFuncCall(TS('AssignStringChar'),VarAssignAST);
         if Flag=false then Compiler.Error.Add(300 {'��������� ���������� �������'}, 'AssignStringChar');
         end;
      if isType(LeftType, TS('String')) and  isType(RightType, TS('String'))  then
         begin
         Flag:=PathAssignToFuncCall(TS('AssignStringString'),VarAssignAST);
         if Flag=false then Compiler.Error.Add(300 {'��������� ���������� �������'}, 'AssignStringString');
         end;
     end;
  else
     {nop}
  end; // case
end;


procedure TSemanter.PVarAssignConst(const VarAssignAST: TVarAssignAST);
begin
  PSignature(VarAssignAST.Signature);
  PExpression(VarAssignAST.Expression);
end;

procedure TSemanter.PVarAssignFunc(const VarAssignAST: TVarAssignAST);
begin
  PSignature(VarAssignAST.Signature);
  PExpression(VarAssignAST.Expression);
end;

procedure TSemanter.PVarAssignTypeCast(const VarAssignAST: TVarAssignAST);
begin
  PSignature(VarAssignAST.Signature);
  PExpression(VarAssignAST.Expression);
end;

procedure TSemanter.PVarAssignVar(const VarAssignAST: TVarAssignAST);
var
  TypeFamily:TTypeFamily;
begin
  Assert(VarAssignAST<> nil, '98');
  TypeFamily:=SignatureTypeFamily(VarAssignAST.Signature);
  case TypeFamily of
    tfString:    PVarAssignVar_String(VarAssignAST);
  else
    {nop}
  end;
end;

procedure TSemanter.PVarAssignSimpleSignature(
  const VarAssignAST: TVarAssignAST);
var Signature:TSignatureAST;
begin
  Assert(VarAssignAST<> nil, '113');
  Signature:=VarAssignAST.Expression.Factor1.Signature;
  if (Signature.IndentDeclRef is TTypeDeclAST) then
      begin
      PVarAssignTypeCast(VarAssignAST);
      end;
  if (Signature.IndentDeclRef is TFuncDeclAST) then
      begin
      PVarAssignFunc(VarAssignAST);
      end;
  if (Signature.IndentDeclRef is TVarDeclAST) then
      begin
      PVarAssignVar(VarAssignAST);
      end else
  if (Signature.IndentDeclRef is TConstDeclAST) then
      begin
      PVarAssignConst(VarAssignAST);
      end;
end;

procedure TSemanter.PVarAssignSimple(const VarAssignAST: TVarAssignAST);
var
  Factor:TFactorAST;
begin
  Assert(VarAssignAST<> nil, '115');
  Factor:=VarAssignAST.Expression.Factor1;
  case Factor.Kind of
  fkSignature:  PVarAssignSimpleSignature(VarAssignAST);
  fkImmConst:   PVarAssignSimpleImmConst(VarAssignAST);
  end;
end;

procedure TSemanter.PVarAssignBinary(const VarAssignAST: TVarAssignAST);
var LeftType, CentreType, RightType:TTypeAST;
  Flag:Boolean;
  CompatableTypes:Boolean;
begin
  Assert(VarAssignAST<> nil, '96');
  LeftType:=SignatureType(VarAssignAST.Signature);
  if isType(LeftType, TS('String')) then
    begin
      CentreType:=FactorType(VarAssignAST.Expression.Factor1);
      RightType:=FactorType(VarAssignAST.Expression.Factor2);
      CompatableTypes:=False;
      if isType(CentreType, TS('String')) and  SimvolsEQ(VarAssignAST.Expression.Op.Simvol,TS('+')) and  isType(RightType, TS('Char'))  then
        begin
        Flag:=PathAssignToFuncCall(TS('ConcatinatedStringChar'),VarAssignAST);
        if Flag=false then Compiler.Error.Add(300 {'��������� ���������� �������'}, 'ConcatinatedStringChar');
        CompatableTypes:=True;
        end;
      if isType(CentreType, TS('Char')) and  SimvolsEQ(VarAssignAST.Expression.Op.Simvol,TS('+')) and  isType(RightType, TS('String'))  then
        begin
        Flag:=PathAssignToFuncCall(TS('ConcatinatedCharString'),VarAssignAST);
        if Flag=false then Compiler.Error.Add(300 {'��������� ���������� �������'}, 'ConcatinatedCharString');
        CompatableTypes:=True;
        end;
      if isType(CentreType, TS('String')) and  SimvolsEQ(VarAssignAST.Expression.Op.Simvol,TS('+')) and  isType(RightType, TS('String'))  then
        begin
        Flag:=PathAssignToFuncCall(TS('ConcatinatedStringString'),VarAssignAST);
        if Flag=false then Compiler.Error.Add(300 {'��������� ���������� �������'}, 'ConcatinatedCharString');
        CompatableTypes:=True;
        end;
      if not CompatableTypes then Compiler.Error.Add(303 {'�������� �� ��������� � ������ �����'}, '');
    end;

  //=======
  PSignature(VarAssignAST.Signature);
  PExpression(VarAssignAST.Expression);
end;

procedure TSemanter.PVarAssignUnari(const VarAssignAST: TVarAssignAST);
begin
  PSignature(VarAssignAST.Signature);
  PExpression(VarAssignAST.Expression);
end;

procedure TSemanter.PVarAssign(const VarAssignAST: TVarAssignAST);
begin
  case VarAssignAST.Expression.kind of
  ekSimple: PVarAssignSimple(VarAssignAST);
  ekUnari:  PVarAssignUnari(VarAssignAST);
  ekBinary: PVarAssignBinary(VarAssignAST);
  end;
end;

procedure TSemanter.PVarDecl(const VarDeclAST: TVarDeclAST);
begin
  PIndent(VarDeclAST.Indent);
  PType(VarDeclAST._Type);
  PConst(VarDeclAST._const);
end;

procedure TSemanter.PWhile(const WhileStmAST: TWhileStmAST);
var  LoacalDecl:TDeclarationAST;
  StatementAST:TStatementAST;
  HostStatementAST:THostStatementAST;
  CompaundStatementAST:TCompaundStatementAST;
  LabelStatemnt:TStatementAST;
begin
 LoacalDecl:=ASTFabric.GetDeclRef(WhileStmAST);
 WhileStmAST.StartLabel := GenNewLabel();
 WhileStmAST.EndLabel := GenNewLabel();

 HostStatementAST:=ASTFabric.GetHostStatementRef(WhileStmAST.Parent);
 StatementAST:=ASTFabric.GetStatementRef(WhileStmAST);
 if StatementAST<>nil then
   begin
   if HostStatementAST is  TCompaundStatementAST then
        CompaundStatementAST:=HostStatementAST as TCompaundStatementAST
        else
        begin
        CompaundStatementAST:=GenCompaundStatement(StatementAST);
        HostStatementAST.Replace(StatementAST, CompaundStatementAST.ListStatemt[0]);
        CompaundStatementAST.Parent:=HostStatementAST;
        end;
   LabelStatemnt:=GenLabelStatement(WhileStmAST.StartLabel);
   LabelStatemnt.Parent:=CompaundStatementAST;
   CompaundStatementAST.AddUpper(StatementAST, LabelStatemnt);
   LabelStatemnt:=GenLabelStatement(WhileStmAST.EndLabel);
   LabelStatemnt.Parent:=CompaundStatementAST;
   CompaundStatementAST.AddLower(StatementAST, LabelStatemnt);
   end;
 {process}
 PIndent(WhileStmAST.BoolExpr.Indent);
 PStatement(WhileStmAST.Statement);
end;

procedure TSemanter.PForeignAsm(ForeignAsm: TForeignAsmAST);
begin

end;

function TSemanter.IsMagicFunc(const FuncCallAST: TFuncCallAST):Boolean;
begin
  result:=SystemMagic.GetIndentRef(FuncCallAST.RefFunc.Indent)<>nil;
end;

procedure TSemanter.PMagicFunc(const FuncCallAST: TFuncCallAST);
var
  Flag:Boolean;
  sParametr:TSimvol;
  _Type:TTYpeAST;
  TypeFamily:TTypeFamily;
  // ��� Break;
  StatementAST:TStatementAST;
  CompaundStatementAST:TCompaundStatementAST;
  HostStatementAST:THostStatementAST;
  LoopStmAST:TLoopStmAST;
  GotoStatemnt:TStatementAST;
  // exit
  FuncImpAST:TFuncImpAST;
begin
Flag:=False;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('exit')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=0 then
       begin
        StatementAST:=ASTFabric.GetStatementRef(FuncCallAST);
        if StatementAST<>nil then
           begin
           HostStatementAST:= StatementAST.Parent as THostStatementAST;
           FuncImpAST:=ASTFabric.GetFuncRef(FuncCallAST);
           if FuncImpAST<>nil then
              begin
              GotoStatemnt:=GenGotoStatement(FuncImpAST.EndLabel);
              HostStatementAST.Replace(StatementAST, GotoStatemnt);
              GotoStatemnt.Free;
              end;
           HostStatementAST:=Nil;
           Flag:=True;
           end;
       end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('Break')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=0 then
       begin

        HostStatementAST:=ASTFabric.GetHostStatementRef(FuncCallAST);
        StatementAST:=ASTFabric.GetStatementRef(FuncCallAST);

        if StatementAST<>nil then
           begin
           LoopStmAST:=ASTFabric.GetLoopRef(FuncCallAST);
           if LoopStmAST<>nil then
              begin
              GotoStatemnt:=GenGotoStatement(LoopStmAST.EndLabel);
              HostStatementAST.Replace(StatementAST, GotoStatemnt);
              GotoStatemnt.Free;
              end;
           Flag:=True;
           end;
       end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('Inc')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=2 then
     begin
     TypeFamily:=getTypeFamily(FuncCallAST.Parametrs.Items[0]._Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfPointer: Flag:=PathFuncCall(TS('IncPtr'), FuncCallAST);
         tfOrdinal: Flag:=PathFuncCall(TS('IncInt'), FuncCallAST);
         end;
       end;
     end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('Dec')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=2 then
     begin
     TypeFamily:=getTypeFamily(FuncCallAST.Parametrs.Items[0]._Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfPointer: Flag:=PathFuncCall(TS('DecPtr'), FuncCallAST);
         tfOrdinal: Flag:=PathFuncCall(TS('DecInt'), FuncCallAST);
         end;
       end;
     end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('Write')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=1 then
     begin
     _Type:=FuncCallAST.Parametrs.Items[0]._Type;
     TypeFamily:=getTypeFamily(_Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfString: begin
                   if IsType(_Type,TS('Char')) then
                       Flag:=PathFuncCall(TS('WriteChar'), FuncCallAST)
                     else Flag:=PathFuncCall(TS('WriteStr'), FuncCallAST);
                   end;
         tfOrdinal: Flag:=PathFuncCall(TS('WriteInt'), FuncCallAST);
         tfBoolean: Flag:=PathFuncCall(TS('WriteBoolean'), FuncCallAST);
         end;
       end;
     end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('WriteLn')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=1 then
     begin
     _Type:=FuncCallAST.Parametrs.Items[0]._Type;
     TypeFamily:=getTypeFamily(_Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfString: begin
                   if IsType(_Type,TS('Char')) then
                       Flag:=PathFuncCall(TS('WriteLnChar'), FuncCallAST)
                     else Flag:=PathFuncCall(TS('WriteLnStr'), FuncCallAST);
                   end;
         tfOrdinal: Flag:=PathFuncCall(TS('WriteLnInt'), FuncCallAST);
         tfBoolean: Flag:=PathFuncCall(TS('WriteLnBoolean'), FuncCallAST);
         end;
       end;
     end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('Read')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=1 then
     begin
     _Type:=FuncCallAST.Parametrs.Items[0]._Type;
     TypeFamily:=getTypeFamily(_Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfString: Flag:=PathFuncCall(TS('ReadStr'), FuncCallAST);
         tfOrdinal: Flag:=PathFuncCall(TS('ReadInt'), FuncCallAST);
         end;
       end;
     end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('ReadLn')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=1 then
     begin
     _Type:=FuncCallAST.Parametrs.Items[0]._Type;
     TypeFamily:=getTypeFamily(_Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfString: Flag:=PathFuncCall(TS('ReadLnStr'), FuncCallAST);
         tfOrdinal: Flag:=PathFuncCall(TS('ReadLnInt'), FuncCallAST);
         end;
       end;
     end;
    end;
if Flag=false then Compiler.Error.Add(300 {'��������� ���������� �������'}, FuncCallAST.RefFunc.Indent.Simvol.Value);
end;

function TSemanter.GenNewLabel():TLabelDeclAST;
var sKey, sIndent:WideString;
begin
  sKey:='Label';
  sIndent:=Compiler.Slovar.GenUniWord(sKey);

  result:=TLabelDeclAST.Create;
  result.Indent:=TIndentAST.Create(TS(sIndent));
end;

function TSemanter.GenLabelStatement(
  LabelDeclAST: TLabelDeclAST): TStatementAST;
begin
 result:=TStatementAST.Create;
 Result.kind:=ksLabelingStatement;
 Result.LabelingStatement:=TLabelingStatementAST.Create;
 Result.LabelingStatement.Parent:=result;
 Result.LabelingStatement.kind:=ksLabeling;
 Result.LabelingStatement.RefLabel:=LabelDeclAST;
 Result.LabelingStatement.LabelingStatement:=TLabelingStatementAST.Create;
 Result.LabelingStatement.LabelingStatement.Parent:= Result.LabelingStatement;
 Result.LabelingStatement.LabelingStatement.kind:=ksSimple;
 Result.LabelingStatement.LabelingStatement.SimpleStatement:=nil;
end;

function TSemanter.GenGotoStatement(LabelDeclAST: TLabelDeclAST): TStatementAST;
begin
 result:=TStatementAST.Create;
 Result.kind:=ksGoto;
 Result._Goto:=TGotoStmAST.Create;
 Result._Goto.Parent:=result;
 Result._Goto.RefLabel:=LabelDeclAST;
end;

function TSemanter.GenCompaundStatement(
  StatementAST: TStatementAST): TCompaundStatementAST;
var
  AStatemetAST:TStatementAST;
begin
 Result:=TCompaundStatementAST.Create;
 AStatemetAST:=TStatementAST.Create;
 AStatemetAST.Parent:=Result;
 AStatemetAST.Add(Result);
 AStatemetAST.Line:=StatementAST.Line;
 Result.Add(StatementAST);
end;


function TSemanter.CreateNewCall(const StatementNode: TNodeAST; Simvol:TSimvol):TFuncCallAST;
var
  indent:TIndentAST;
  IndentRef:TNodeAST;
begin
  result:= TFuncCallAST.Create;
  Indent:=TIndentAST.create(Simvol);
  ASTFabric.ActivLeaf:=StatementNode;
  IndentRef:=ASTFabric.GetIndentRef(Indent);
  if IndentRef<>nil then  result.RefFunc:= IndentRef.parent as TFuncDeclAST
    else Compiler.Error.Add(301,simvol.Value);
end;

procedure TSemanter.DestroyFuncCall(var FuncCallAST: TFuncCallAST);
begin
//  FuncCallAST.Free;   //[!]  ������� �������� ����� ����� ���������.
end;

procedure TSemanter.DestroyVarAssign(var VarAssign: TVarAssignAST);
begin
//  VarAssign.Free;   //[!]  ������� �������� ����� ����� ���������.
end;

function TSemanter.PathFuncCall(const Simvol:TSimvol; OldFuncCallAST:TFuncCallAST):boolean;
var
  NewFuncCallAST:TFuncCallAST;
  SimpleStatementAST:TSimpleStatementAST;
begin
  NewFuncCallAST:=CreateNewCall(OldFuncCallAST, Simvol);
  NewFuncCallAST.RefResult:=OldFuncCallAST.RefResult;
  NewFuncCallAST.Parametrs:=OldFuncCallAST.Parametrs;
  SimpleStatementAST:=OldFuncCallAST.Parent as TSimpleStatementAST;
  NewFuncCallAST.parent:=SimpleStatementAST;
  SimpleStatementAST.FuncCall:=NewFuncCallAST;
  DestroyFuncCall(OldFuncCallAST);
  Result:=true;
end;

function TSemanter.PathAssignToFuncCall(const Simvol:TSimvol; OldVarAssignAST: TVarAssignAST):boolean;
var
  NewFuncCallAST:TFuncCallAST;
  SimpleStatementAST:TSimpleStatementAST;
begin
  NewFuncCallAST:=CreateNewCall(OldVarAssignAST, Simvol);
  NewFuncCallAST.Parametrs:=TCallParametrsAST.Create;
  NewFuncCallAST.Parametrs.Parent:=NewFuncCallAST;
  NewFuncCallAST.Parametrs.Add(OldVarAssignAST.Signature.IndentDeclRef.Indent.parent);

  if OldVarAssignAST.Expression.Factor1.Kind=fkSignature then
     NewFuncCallAST.Parametrs.Add(OldVarAssignAST.Expression.Factor1.Signature.IndentDeclRef.Indent.parent)
     else
     NewFuncCallAST.Parametrs.Add(OldVarAssignAST.Expression.Factor1.ImmConst);

  if OldVarAssignAST.Expression.Factor2<>nil then
   if OldVarAssignAST.Expression.Factor2.Kind=fkSignature then
     NewFuncCallAST.Parametrs.Add(OldVarAssignAST.Expression.Factor2.Signature.IndentDeclRef.Indent.parent)
     else
     NewFuncCallAST.Parametrs.Add(OldVarAssignAST.Expression.Factor2.ImmConst);

  SimpleStatementAST:=OldVarAssignAST.Parent as TSimpleStatementAST;
  NewFuncCallAST.parent:=SimpleStatementAST;
  SimpleStatementAST.kind:=ssFuncCall;
  SimpleStatementAST.FuncCall:=NewFuncCallAST;
  SimpleStatementAST.VarAssign:=nil;
  DestroyVarAssign(OldVarAssignAST);
  Result:=true;
end;

function TSemanter.getTypeFamily(_Type:TTypeAST): TTypeFamily;
begin
  result:=FSemanticCheking.TypeFamily(_Type);
end;

function TSemanter.TypeFamily(_Type:TTypeAST): TTypeFamily;
begin
  result:=FSemanticCheking.TypeFamily(_Type);
end;

function TSemanter.DesignatorTypeFamily(_Type:TTypeAST; Designator: TDesignatorAST): TTypeFamily;
begin
  result:=FSemanticCheking.DesignatorTypeFamily(_Type, Designator);
end;

function TSemanter.SignatureTypeFamily(Signature: TSignatureAST): TTypeFamily;
begin
  result:=FSemanticCheking.SignatureTypeFamily(Signature);
end;

function TSemanter.FactorTypeFamily(Factor: TFactorAST): TTypeFamily;
begin
  result:=FSemanticCheking.FactorTypeFamily(Factor);
end;

function TSemanter.SignatureType(Signature: TSignatureAST): TTypeAST;
begin
  result:=FSemanticCheking.SignatureType(Signature);
end;

function TSemanter.FactorType(Factor: TFactorAST): TTypeAST;
begin
  result:=FSemanticCheking.FactorType(Factor);
end;

function TSemanter.DesignatorType(_Type:TTypeAST; Designator: TDesignatorAST):TTypeAST;
begin
  result:=FSemanticCheking.DesignatorType(_Type, Designator);
end;

function TSemanter.unAlias(const TypeAST: TTypeAST):TTypeAST;
begin
  result:=FSemanticCheking.unAlias(TypeAST);
end;

function TSemanter.isType(_Type:TTypeAST; Simvol:TSimvol):boolean;
begin
  result:=FSemanticCheking.isType(_Type, Simvol);
end;

function TSemanter.isTypeFast(_Type:TTypeAST; Simvol:TSimvol):boolean;
begin
  result:=FSemanticCheking.isTypeFast(_Type, Simvol);
end;

end.
