{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UToASM;

interface

uses SysUtils, UAST, UASTFabric, UCompiler, UGramAnaliser, USemanticCheking,
  USimvler, USystemMagic, UHohaTypes;

type
  TCodeTable= (ctUSE16,ctUSE32);
  TDirectForCmd = (ToIndent, ToReg);

  TNewOperatorAST= class(TOperatorAST)
    NewOperator:TSimvol;
  end;

  TPrinterAST = class(TCodePrinter)
  private
    PointerSize:Cardinal;
    CharSize:Cardinal;
    FPath:String;
    FFile:TextFile;
    StartLabel:String;
    FSemanticCheking:TSemanticCheking;
    SubStack:Integer;
  protected

  public
    CodeTable:TCodeTable;
    WithDebugInfo:Boolean;
    Compiler:TCompiler;
    procedure AssignFile(Path:String); override;
    procedure print; override;
    procedure reset; override;
    procedure process; override;
    function LastPosRow:Integer; override;
    function LastPosCol:Integer; override;
    constructor Create(TheCompiler:TCompiler);
    procedure PrintUnitAST(const UnitAST:TUnitAST);
    procedure PrintImplimentaion(const ImplimentaionAST:TImplimentaionAST);
    procedure DeclarationPrepare(const DeclarationAST:TDeclarationAST);
    procedure ImplimentaionPrepare(const ImplimentaionAST: TImplimentaionAST);
    procedure PrintDeclaration(const DeclarationAST:TDeclarationAST);
    procedure PrintDebugDeclaration(const DeclarationAST:TDeclarationAST);
    procedure PrintAlias(const Indent:TIndentDeclAST);
    procedure PrintTypeDecl(const TypeDeclAST:TTypeDeclAST);
    procedure PrintVarDecl(const VarDeclAST:TVarDeclAST);
    procedure PrintConstDecl(const ConstDeclAST:TConstDeclAST);
    procedure PrintLocalVarDecl(const VarDeclAST:TVarDeclAST; Var Ofs:Integer);
    procedure PrintLocalConstDecl(const ConstDeclAST:TConstDeclAST; Var Ofs:Integer);
    procedure PrintLabelDecl(const LabelDeclAST:TLabelDeclAST);
    procedure ResultToParam(const FuncImpAST:TFuncImpAST);
    procedure ResultToParamPre(const FuncDeclAST: TFuncDeclAST);
    procedure DeclarationToImplementetion(const FuncDeclAST: TFuncDeclAST);
    procedure PrintIndent(const IndentAST:TIndentAST);
    procedure PrintFuncName(const IndentAST:TIndentAST);
    function  unAlias(const TypeAST: TTypeAST):TTypeAST;
    function  isType(_Type:TTypeAST; Simvol:TSimvol):boolean;
    function  isTypeFast(_Type:TTypeAST; Simvol:TSimvol):boolean;
    function  GetArrayTypeSize(const TypeArrayAST:TTypeArrayAST):Integer;
    function  GetRecordTypeSize(const TypeRecordAST:TTypeRecordAST):Integer;
    function  GetSimpleTypeSize(const TypeSimpleAST: TTypeSimpleAST):Integer;
    function  GetTypeSize(const TypeAST: TTypeAST):Integer;
    function  GetFormalParametrsSize(const FormalParametrsAST:TFormalParametrsAST):Integer;
    function  GetUniIndent(Indent:WideString):WideString;
    function  GetRegName(const Reg:Integer; const Size:Integer):String;
    function  GetRegNameFpu(const Reg:Integer):String;
    function  TypeFamily(_Type:TTypeAST):TTypeFamily;
    function  DesignatorTypeFamily(_Type:TTypeAST; Designator: TDesignatorAST):TTypeFamily;
    function  SignatureTypeFamily(Signature: TSignatureAST):TTypeFamily;
    function  FactorTypeFamily(Factor:TFactorAST):TTypeFamily;
    function  SignatureType(Signature: TSignatureAST): TTypeAST;
    function  FactorType(Factor:TFactorAST):TTypeAST;
    function  DesignatorType(_Type:TTypeAST; Designator: TDesignatorAST):TTypeAST;
    function  FindIndentResult(const FuncImpAST:TFuncImpAST):TIndentAST;
    function  IsMagicFunc(const FuncCallAST: TFuncCallAST):Boolean;
    function  IsParamertrRef(const ParametrAST:TParametrAST):Boolean;
    function  getVarDeclKind(var VarDeclAST:TVarDeclAST):TVarDeclKind;
    function  IsIndentDeclString(IndentDecl:TIndentDeclAST):boolean;
    function  GetFildType(Signature:TSignatureAST):TTypeAST;
    function  GetFildOfs(Signature:TSignatureAST):Integer;
    procedure DivPrintChangeReg( Arg1, Arg2:String);
    function  IsOpReletiv(Op:TSimvol):Boolean;
    procedure PrintTypeSize(const TypeAST:TTypeAST);
    procedure PrintDataType(const TypeAST:TTypeAST);
    procedure PrintDataTypeSimple(const TypeSimpleAST:TTypeSimpleAST);
    procedure PrintDataTypeRecord(const TypeRecordAST:TTypeRecordAST);
    procedure PrintDataTypeArray(const TypeArrayAST: TTypeArrayAST);
    procedure PrintCodeType(const TypeAST:TTypeAST);
    procedure PrintCodeTypeSimple(const TypeSimpleAST:TTypeSimpleAST);
    procedure PrintCodeTypeRecord(const TypeRecordAST:TTypeRecordAST);
    procedure PrintCodeTypePointer;
    procedure PrintFieldDecl(const FieldDeclAST:TFieldDeclAST);
    procedure PrintConst(const ConstAST: TConstAST; _Type:TTypeAST);
    procedure PrintFormalParametrs(const FormalParametrsAST:TFormalParametrsAST);
    procedure PrintParametr(const ParametrAST:TParametrAST);
    procedure PrintDebugLocalDeclaration(const DeclarationAST:TDeclarationAST; Var Ofs:Integer);
    procedure PrintLocalDeclaration(const DeclarationAST:TDeclarationAST; Var Ofs:Integer);
    procedure PrintFuncImp(const FuncImpAST:TFuncImpAST);
    procedure PrintCompaundStatement(const CompaundStatementAST:TCompaundStatementAST);
    procedure PrintMainBlock(const CompaundStatementAST:TCompaundStatementAST);
    procedure PrintDebugStatement(const StatementAST:TStatementAST);
    procedure PrintStatement(const StatementAST:TStatementAST);
    procedure PrintGoto(const GotoStmAST:TGotoStmAST);
    procedure PrintFor(const ForStmAST:TForStmAST);
    procedure PrintWhile(const WhileStmAST:TWhileStmAST);
    procedure PrintRepeat(const RepeatStmAST:TRepeatStmAST);
    procedure PrintIf(const IfStmAST:TIfStmAST);
    procedure PrintCase(const CaseStmAST:TCaseStmAST);
    procedure PrintCaseEdge(const IndentRef:TVarDeclAST; const CaseEdgeAST: TCaseEdgeAST;
                            const lCaseEnd:WideString);
    procedure PrintLabelingStatement(const LabelingStatementAST:TLabelingStatementAST);
    procedure PrintSimpleStatement(const SimpleStatementAST:TSimpleStatementAST);
    procedure PrintMagicFunc(const FuncCallAST:TFuncCallAST);
    procedure PrintFuncCallAbstract(const FuncCallAST: TFuncCallAST);
    procedure PrintFuncDecl(const FuncDeclAST: TFuncDeclAST);
    procedure PrintFuncCall(const FuncCallAST:TFuncCallAST);
    procedure PrintCallParametr(const FormalParametrAST:TParametrAST;
                                const CallParametrAST:TCallParametrAST);
    procedure PrintCallParametrs(const FormalParametrsAST:TFormalParametrsAST;
                                 const CallParametrsAST:TCallParametrsAST);
    procedure ErrorTypeCast;
    procedure PrintVarAssignCast_Ordinal(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssignCast_Real(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssignCast_Boolean(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssignCast_String(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssignCast_Pointer(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssignCast_Structure(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssign_RealOrdinal(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssign_OrdinalOrdinal(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssign_OrdinalPointer(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssign_OrdinalString(const Left, Righte: TVarDeclAST);
    procedure PrintVarAssign_ByteCopy(const Left, Righte: TVarDeclAST);

    procedure PrintVarAssignTypeCast(const VarAssignAST:TVarAssignAST);
    procedure PrintRegAssignTypeCast(const Reg:Integer; const FactorAST:TFactorAST);
    procedure PrintVarAssignSizeOf_Ordinal(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignFunc_Ordinal(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignFunc_Real(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignFunc_String(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignFunc_Structure(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignFunc(const VarAssignAST:TVarAssignAST);
    procedure PrintRegAssignFunc(const Reg:Integer; const FactorAST:TFactorAST);
    procedure PrintRegAssignFuncFPU(const Reg:Integer; const FactorAST:TFactorAST);
    procedure PrintVarAssignVar(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignVar_Real(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignVar_Ordinal(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignVar_String(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignVar_Structure(const VarAssignAST:TVarAssignAST);
    procedure PrintRegAssignRefToVar(const Reg:Integer; const FactorAST:TFactorAST);
    procedure PrintRegAssignVar(const Reg:Integer; const FactorAST:TFactorAST);
    procedure PrintRegAssignVarFPU(const FactorAST:TFactorAST);
    procedure PrintVarAssignString(const VarAssignAST:TVarAssignAST);
    procedure PrintRegAssignString(leval:Integer; const Reg:Integer; const ImmConst: TImmConstAST);
    procedure PrintVarAssignChar(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignChar_String(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignConst(const VarAssignAST:TVarAssignAST);
    procedure PrintRegAssignConst(const Reg:Integer; const FactorAST:TFactorAST);
    procedure PrintRegAssignConstFPU(const FactorAST:TFactorAST);
    procedure PrintVarAssignNumber(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignNumber_Real(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignNumber_Ordinal(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignNumber_String(const VarAssignAST: TVarAssignAST);
    procedure PrintRegAssignNumber_Real(const ImmConst: TImmConstAST);
    procedure PrintRegAssignNumber_Ordinal(const Reg:Integer; const ImmConst: TImmConstAST);
    procedure PrintVarAssignAddr(const VarAssignAST:TVarAssignAST);
    procedure PrintRegAssignAddr(const Reg:Integer; const ImmConst:TImmConstAST);
    procedure PrintVarAssignSimpleSignature(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignSimpleImmConst(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignSimple(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignUnari_Ordinal(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignUnari_Real(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignUnari(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignBinary_Ordinal(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignBinary_Real(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignBinary_Boolean(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignBinary_String(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssignBinary(const VarAssignAST: TVarAssignAST);
    procedure PrintVarAssign(const VarAssignAST:TVarAssignAST);
    procedure PrintVarAssignReg(Const Signature:TSignatureAST; const Reg:Integer);
    procedure PrintVarAssignRegFPU(Const Signature:TSignatureAST);
    procedure PrintSignature(const Reg:Integer; const FactorAST: TFactorAST);
    procedure PrintSignatureFPU(const FactorAST: TFactorAST);
    procedure PrintDesignator(const DesignatorAST:TDesignatorAST);
    procedure PrintExpression(const Reg:Integer; const ExpressionAST:TExpressionAST);
    procedure PrintExpressionFPU(const Reg:Integer; const ExpressionAST:TExpressionAST);
    procedure PrintOperationUnari_Ordinal(const Reg1:Integer; const OperatorAST:TOperatorAST);
    procedure PrintOperationUnari_Boolean(const Reg1:Integer; const OperatorAST:TOperatorAST);
    procedure PrintOperationUnari_Real(const OperatorAST:TOperatorAST);
    procedure PrintOperationUnariFPU(const OperatorAST:TOperatorAST;
                                     const TypeFamily:TTypeFamily);
    procedure PrintOperationUnari(const Reg1:Integer; const OperatorAST:TOperatorAST;
                                  const TypeFamily:TTypeFamily);
    procedure PrintOperationBinary_Ordinal(const Reg1, Reg2:Integer; const OperatorAST:TOperatorAST);
    procedure PrintOperationBinary_Boolean(const Reg1, Reg2:Integer; const OperatorAST:TOperatorAST);
    procedure PrintOperationBinary_Pointer(const Reg1, Reg2:Integer; const OperatorAST:TOperatorAST);
    procedure PrintOperationBinary_String(const Reg1, Reg2:Integer; const OperatorAST:TOperatorAST);
    procedure PrintOperationBinary_Real(const Reg1, Reg2:Integer; const OperatorAST:TOperatorAST);
    procedure PrintOperationBinary(const Reg1, Reg2:Integer; const OperatorAST:TOperatorAST;
                                   const TypeFamily:TTypeFamily);
    procedure PrintImmConst(leval:integer; const Reg:Integer; const ImmConstAST: TImmConstAST);
    procedure PrintImmConstReal(const ImmConstAST: TImmConstAST);
    procedure PrintFactor(const Reg:Integer; const FactorAST:TFactorAST);
    procedure PrintFactorFPU(const FactorAST:TFactorAST);
    function  GentIndentForImmString(const _string:TStringAST):TConstDeclAST;
    function  GentIndentForImmInteger(const _Integer:TIntegerAST):TConstDeclAST;
    function  GentIndentForImmReal(const _Real:TRealAST):TConstDeclAST;
    procedure AddConstImm( AtNode:TNodeAST; ConstDeclAST:TConstDeclAST);
    procedure PrintCmdAssign2(const Leval:Integer; const Reg: String;
                             const IndentAST: TIndentAST; const Direct:TDirectForCmd);
    procedure PrintCmdAssign(const cmd:String; const Reg: String;
                             const IndentAST: TIndentAST; const Direct:TDirectForCmd);
    procedure PrintCmdAssignFpu(const cmd:String; const IndentAST: TIndentAST;
                                const Direct:TDirectForCmd);
    procedure PrintCmdRegAssignIndent2(leval:Integer; const Reg1: String;
                                       const IndentAST: TIndentAST);
    procedure PrintCmdRegAssignIndent(const cmd:String; const Reg1: String;
                                      const IndentAST: TIndentAST);
    procedure PrintCmdIndentAssignReg2(leval:Integer;
                            const IndentAST: TIndentAST; const Reg1: String);
    procedure PrintCmdIndentAssignReg(const cmd:String; const IndentAST: TIndentAST;
                                      const Reg1: String);
    procedure PrintForeignAsm(const ForeignAsm:TForeignAsmAST);
    procedure PrintPush(const reg:String);
    procedure PrintPop(const reg:String);
    procedure PrintCreateVar(LocalDec: TDeclarationAST);
    procedure PrintDestroyVar(LocalDec: TDeclarationAST);
  published

  end;

const
  rEAX=0;
  rEBX=1;
  rECX=2;
  rEDX=3;
  rESI=4;
  rEDI=5;
  rEBP=6;
  rESP=7;

implementation

{ TPrinterAST }

procedure TPrinterAST.AssignFile(Path: String);
begin
  System.AssignFile(FFile,Path);
  FPath:=Path;
end;

constructor TPrinterAST.Create(TheCompiler: TCompiler);
const
 // ��������
 Table:array [1..50] of string= ('AX', 'BX', 'CX', 'DX',
                                'SI', 'DI', 'SP', 'BP',
                                'IP', 'FLAGS',
                                'CS','DS','ES','SS',
                                'AH','AL','BH','BL',
                                'CH','CL','DH','DL',
                                'EAX','EBX','ECX','EDX',
                                'ESI','EDI','ESP','EBP',
                                'EIP','EFLAGS',
                                'FS','GS',
                                'CR0', 'CR1', 'CR2', 'CR4',
                                'CR5', 'CR6', 'CR7', 'CR8',
                                'ST(0)','ST(1)','ST(2)','ST(3)',
                                'ST(4)','ST(5)','ST(6)','ST(7)');
 // �������
 Table2:array [1..100] of string=('MOV', 'PUSH', 'PUSHA', 'POP', 'POPA',    //5
                                 'XCHG',                                   //6
                                 'OUT', 'IN',                              //8
                                 'XLAT',                                   //9
                                 'LEA', 'LDS', 'LES',                      //12
                                 'LAHF', 'SAHF', 'PUSHF', 'POPF',          //16
                                 'ADD', 'ADC', 'INC',                      //19
                                 'SUB', 'SBB', 'DEC',                      //22
                                 'CMP', 'NEG',                             //24
                                 'AAA', 'DAA', 'AAS', 'DAS',               //28
                                 'MUL', 'IMUL', 'DIV', 'IDIV',             //32
                                 'AAM', 'AAD',                             //34
                                 'CSW', 'CWD',                             //36
                                 'ROL', 'ROR', 'RCL', 'RCR',               //40
                                 'SHL', 'SAL', 'SHR', 'SAR',               //44
                                 'AND', 'TEST', 'OR', 'XOR', 'NOT',        //49
                                 'MOVS', 'CMPS', 'SCAS', 'LODS',           //53
                                 'STOS', 'INS', 'OUTS', 'REP',             //57
                                 'CALL', 'JMP', 'RET',                     //60
                                 'JE', 'JZ', 'JL', 'JNGE', 'JLE', 'JNG',   //66
                                 'JB', 'JNAE', 'JBE', 'JNA', 'JP', 'JPE',  //72
                                 'JO', 'JS', 'JNE', 'JNZ', 'JNL', 'JGE',   //78
                                 'JNLE', 'JG', 'JNB', 'JAE', 'JNBE', 'JA', //84
                                 'JNP', 'JPO', 'JNO', 'JNS',               //88
                                 'LOOP', 'LOOPZ', 'LOOPE', 'LOOPNE', 'LOOPPNE', //93
                                 'ENTER', 'LEAVE',                         //95
                                 'INT', 'INT3', 'INTO',                    //98
                                 'IRET', 'BOUND');                         //100
  // ��������� TASM'�
  Table3:array [1..28] of string=('PTR', 'EQU', 'ORG', 'ARG,', 'LOCAL',    //5
                                 'PROC', 'ENDP', 'SEGMENT', 'ENDS',        //9
                                 'PUBLICK', 'EXTERN', 'INCLUDELIB',        //12
                                 'INCLUDE', 'TITLE',                       //14
                                 'USE16', 'USE32',                         //16
                                 'Code', 'data', 'TEXT',                   //19
                                 'PARA',                                   //20
                                 'NL', '_Start', '_Halt',                  //23
                                 'Length', 'name', 'SIZE', 'STR',          //27
                                 'substr');                                //28
  // �������������� ��������� ����������
  Table4:array [1..1] of string=('Data0');                                 //1
var
 i:Integer;
begin

  Assert(TheCompiler <> nil, '1');
  Compiler:=TheCompiler;
  FSemanticCheking:=TSemanticCheking.Create(Compiler);

  Self.CodeTable:=ctUSE32;
  PointerSize:=4;
  CharSize:=2;
  WithDebugInfo:=False;
  for i:=1 to Length(Table) do
    begin
    Compiler.Slovar.Add('', Table[i]);
    end;
  for i:=1 to Length(Table2) do
    begin
    Compiler.Slovar.Add('', Table2[i]);
    end;
  for i:=1 to Length(Table3) do
    begin
    Compiler.Slovar.Add('', Table3[i]);
    end;
  for i:=1 to Length(Table4) do
    begin
    Compiler.Slovar.Add('', Table4[i]);
    end;
end;


procedure TPrinterAST.process;
begin
  Print;
end;

procedure TPrinterAST.reset;
begin

end;

function TPrinterAST.LastPosRow:Integer;
begin
  result:=FLastPosRow;
end;

function TPrinterAST.LastPosCol:Integer; 
begin
  result:=FLastPosCol;
end;


function TPrinterAST.unAlias(const TypeAST: TTypeAST):TTypeAST;
begin
  result:=FSemanticCheking.unAlias(TypeAST);
end;

function TPrinterAST.isType(_Type:TTypeAST; Simvol:TSimvol):boolean;
begin
  result:=FSemanticCheking.isType(_Type, Simvol);
end;

function TPrinterAST.isTypeFast(_Type:TTypeAST; Simvol:TSimvol):boolean;
begin
  result:=FSemanticCheking.isTypeFast(_Type, Simvol);
end;

function TPrinterAST.GetArrayTypeSize(
  const TypeArrayAST: TTypeArrayAST): Integer;
begin
  Assert(TypeArrayAST <> nil, '2');
  Result:=TypeArrayAST.Length*GetTypeSize(TypeArrayAST._Type);
end;

function TPrinterAST.GetRecordTypeSize(
  const TypeRecordAST: TTypeRecordAST): Integer;
var
  i:Integer;
begin
  Assert(TypeRecordAST <> nil, '3');
  Result:=0;
  for i:=0 to  Length(TypeRecordAST.Fields)-1 do
    Result:=Result+GetTypeSize(TypeRecordAST.Fields[i]._Type);
end;

function TPrinterAST.GetSimpleTypeSize(
  const TypeSimpleAST: TTypeSimpleAST): Integer;
begin
  Assert(TypeSimpleAST <> nil, '4');
  Result:=UHohaTypes.GetSimpleTypeSize(TypeSimpleAST.FSimvol.Value);
end;

function TPrinterAST.GetTypeSize(const TypeAST: TTypeAST):Integer;
begin
  Assert(TypeAST <> nil, '5');
  Result:=-1;
  case TypeAST.Kind of
   tkUndefine: Result:=0;
   tkSimple:   Result:=GetSimpleTypeSize(TypeAST.Simple);
   tkAlias:    Result:=GetTypeSize(unAlias(TypeAST));
   tkRecord:   Result:=GetRecordTypeSize(TypeAST._Record);
   tkArray:    Result:=GetArrayTypeSize(TypeAST._Array);
   tkPointer:  Result:= PointerSize;
  end;
end;

function  TPrinterAST.GetRegName(const Reg:Integer; const Size:Integer):String;
begin
Result:='';
case Reg of
0: case Size of
   1: Result:='AL';
   2: Result:='AX';
   4: Result:='EAX';
   end;
1: case Size of
   1: Result:='BL';
   2: Result:='BX';
   4: Result:='EBX';
   end;
2: case Size of
   1: Result:='CL';
   2: Result:='CX';
   4: Result:='ECX';
   end;
3: case Size of
   1: Result:='DL';
   2: Result:='DX';
   4: Result:='EDX';
   end;
4: case Size of
   2: Result:='SI';
   4: Result:='ESI';
   end;
5: case Size of
   2: Result:='DI';
   4: Result:='EDI';
   end;
6: case Size of
   2: Result:='BP';
   4: Result:='EBP';
   end;
7: case Size of
   2: Result:='SP';
   4: Result:='ESP';
   end;
end;
end;

function  TPrinterAST.GetRegNameFpu(const Reg:Integer):String;
begin
Result:='';
case Reg of
0: Result:='ST(0)';
1: Result:='ST(1)';
2: Result:='ST(2)';
3: Result:='ST(3)';
4: Result:='ST(4)';
5: Result:='ST(5)';
6: Result:='ST(6)';
7: Result:='ST(7)';
end;
end;

procedure TPrinterAST.print;
var
  GA:TGramAnaliser;
begin
  GA:=Compiler.GramAnaliser as TGramAnaliser;
  if Compiler.Error.Count=0 then
     begin
     rewrite(FFile);
     PrintUnitAST(GA.AST);
     CloseFile(FFile);
     end else
     begin
     DeleteFile(FPath);
     end;
end;

procedure TPrinterAST.PrintCallParametr(
  const FormalParametrAST:TParametrAST;
  const CallParametrAST: TCallParametrAST);
var
  Indent:TIndentAST;
  _Type:TTypeAST;
begin
  Assert(FormalParametrAST <> nil, '6');
  Assert(CallParametrAST <> nil, '7');
if CallParametrAST.kind=cpImmConst then
   begin
   _Type:=FSemanticCheking.ImmConstType(CallParametrAST.ImmConst);
   if FormalParametrAST.mode=pmVar then
        begin
          Compiler.Error.Add(10108,'');
        end;
   if FormalParametrAST.mode in [pmConst, pmParam] then
      begin
      PrintImmConst(-4, rEAX,  CallParametrAST.ImmConst);
      PrintPush('EAX');
      end;
   end else
   begin
   Indent:= CallParametrAST.GetIndent;
   _Type:= CallParametrAST.Get_Type;
   if FormalParametrAST.mode=pmVar then
     if CallParametrAST.Kind=cpConstDecl then
        begin
          Compiler.Error.Add(10108,'');
        end else
        begin
        if IsIndentDeclString(CallParametrAST.VarDecl) then
           begin
           case getVarDeclKind(CallParametrAST.VarDecl) of
           vdkUndefine,
           vdkGlobal:
             begin
             PrintCmdAssign2(-4,'EAX', Indent, ToReg);    // -2 - 2 ref ������
             end;
           vdkParam,
           vdkLocal:
             begin
             PrintCmdAssign2(-2,'EAX', Indent, ToReg);    // -2 - 2 ref ������
             end;
           end; // case
           end else
//           PrintCMDAssign('LEA','EAX', Indent, ToReg);
           PrintCmdAssign2(0,'EAX', Indent, ToReg);
         PrintPush('EAX');
        end;

   if FormalParametrAST.mode in [pmConst, pmParam] then
   case GetTypeSize(_Type) of
   1: begin
      PrintCmdAssign2(1,'AL', Indent, ToReg);
      //PrintCmdAssign('MOV', 'AL', Indent, ToReg);
      PrintPush('EAX');
      end;
   2: begin
      PrintCmdAssign2(1,'AX', Indent, ToReg);
//      PrintCmdAssign('MOV', 'AX', Indent, ToReg);
      PrintPush('EAX');
      end;
   4: begin
      if IsIndentDeclString(CallParametrAST.VarDecl) then
         begin
           case getVarDeclKind(CallParametrAST.VarDecl) of
           vdkUndefine,
           vdkGlobal: PrintCmdAssign2(-4,'EAX', Indent, ToReg);
           vdkParam:  PrintCmdAssign2(-2,'EAX', Indent, ToReg);
           vdkLocal:  PrintCmdAssign2(-2,'EAX', Indent, ToReg);
           end; // case

{           if CallParametrAST.kind in [cpConstDecl,cpImmConst] then
              PrintCmdAssign('MOV', 'EAX', Indent, ToReg)
              else
              PrintCmdAssign('MOV', 'EAX', Indent, ToReg);  }
         PrintPush('EAX');
         end else
         begin
           PrintCmdAssign2(1,'EAX', Indent, ToReg);
//         PrintCmdAssign('MOV', 'EAX', Indent, ToReg);
         PrintPush('EAX');
         end;
      end;
   8: begin
      PrintCmdAssign2(0,'EAX', Indent, ToReg);
//      PrintCmdAssign('LEA', 'EAX', Indent, ToReg);
      PrintPush('DWord PTR [EAX+4]');
      PrintPush('DWord PTR [EAX]');
      end;
   end; // case
   end;
end;

procedure TPrinterAST.PrintCallParametrs(
  const FormalParametrsAST:TFormalParametrsAST;
  const CallParametrsAST:TCallParametrsAST);
var i:Integer;
begin
  Assert(FormalParametrsAST <> nil, '8');
  Assert(CallParametrsAST <> nil, '9');
  With CallParametrsAST do
    if Length(Items)<>0 then
       begin
       for i:=Length(Items)-1 downto 0 do  //stdcall ������� � ����� �� ����
         begin
         if (FormalParametrsAST<>nil) and (i<Length(FormalParametrsAST.List)) then
            PrintCallParametr(FormalParametrsAST.List[i],Items[i])
            else
            PrintCallParametr(NilFormalParamet,Items[i]);
         end;
       end;
end;

procedure TPrinterAST.PrintCase(const CaseStmAST: TCaseStmAST);
var
  lCaseEnd:WideString;
  i:Integer;
begin
  Assert(CaseStmAST <> nil, '10');
  lCaseEnd:=Compiler.Slovar.GetUniWord('@@CaseEnd');
  CaseStmAST.SubStack:=SubStack;
  for i:=0 to Length(CaseStmAST.Edges)-1 do
    begin
    PrintCaseEdge(CaseStmAST.RefVar, CaseStmAST.Edges[i], lCaseEnd);
    end;
 if (CaseStmAST.Defult<>nil) then
    PrintStatement(CaseStmAST.Defult);
 WriteLn(FFile, lCaseEnd+':');
end;

procedure TPrinterAST.PrintCaseEdge(const IndentRef:TVarDeclAST; const CaseEdgeAST: TCaseEdgeAST;
                   const lCaseEnd:WideString);
var
  IfExpression:TExpressionAST;
var
  lIfFalse, lIfTrue:WideString;
begin
  Assert(IndentRef <> nil, '11');
  Assert(CaseEdgeAST <> nil, '12');
  IfExpression:=TExpressionAST.Create;
  IfExpression.kind:=ekBinary;
    IfExpression.Op:=TOperatorAST.Create(TS('='));
    IfExpression.Factor1:=TFactorAST.Create;
    IfExpression.Factor1.Kind:=fkSignature;
    IfExpression.Factor1.Signature:=TSignatureAST.Create;
      IfExpression.Factor1.Signature.IndentDeclRef:=IndentRef;
      IfExpression.Factor1.Signature.Designator:=TDesignatorAST.Create;
      IfExpression.Factor1.Signature.Designator.kind:=dkSignature;
    IfExpression.Factor2:=TFactorAST.Create;
    IfExpression.Factor2.Parent:=CaseEdgeAST;
  case CaseEdgeAST.Kind of
  kceUndefine:
    begin
      IfExpression.Factor2.Kind:=fkImmConst;
      IfExpression.Factor2.ImmConst:=TImmConstAST.Create;
      IfExpression.Factor2.ImmConst.Parent:=IfExpression.Factor2;
    end;
  kceRef:
    begin
      IfExpression.Factor2.Kind:=fkSignature;
      IfExpression.Factor2.Signature:=TSignatureAST.Create;
      IfExpression.Factor2.Signature.Parent:=IfExpression.Factor2;
      IfExpression.Factor2.Signature.IndentDeclRef:=CaseEdgeAST.ConstRef;
    end;
  kceImmConst:
    begin
      IfExpression.Factor2.Kind:=fkImmConst;
      IfExpression.Factor2.ImmConst:=TImmConstAST.Create;
      IfExpression.Factor2.ImmConst.Parent:=IfExpression.Factor2;
      IfExpression.Factor2.ImmConst.Add(CaseEdgeAST.ImmConst);
    end;
  end;


  lIfTrue:=Compiler.Slovar.GetUniWord('@@CETrue');
  lIfFalse:=Compiler.Slovar.GetUniWord('@@CEFalse');
  CaseEdgeAST.SubStack:=SubStack;


  PrintExpression(rEAX, IfExpression);
  WriteLn(FFile,#9#9'CMP EAX, True');
  WriteLn(FFile,#9#9'JNE '+lIfFalse);
  WriteLn(FFile, lIfTrue+':');
  PrintStatement(CaseEdgeAST.Statement);
  WriteLn(FFile,#9#9'JMP '+lCaseEnd);
  WriteLn(FFile,lIfFalse+':');





  IfExpression.Factor2.Destroy;
  IfExpression.Factor1.Signature.Destroy;
  IfExpression.Factor1.Destroy;
  IfExpression.Op.Destroy;
  IfExpression.Destroy;
end;

procedure TPrinterAST.PrintCompaundStatement(
  const CompaundStatementAST: TCompaundStatementAST);
var
  i:Integer;
begin
  Assert(CompaundStatementAST <> nil, '13');
  PrintPush('ECX');
  CompaundStatementAST.SubStack:=SubStack;
  for i:=0 to Length(CompaundStatementAST.ListStatemt)-1 do
    begin
    PrintStatement(CompaundStatementAST.ListStatemt[i]);
    end;
  PrintPop('ECX');
end;

procedure TPrinterAST.PrintConst(const ConstAST: TConstAST; _Type:TTypeAST);
// ������������ ������� ��������� Unicod
// ������� �� �������.
  function GetStrDirect(s:WideString):String;
  var i:Integer;
  begin
  SetLength(Result,Length(s));
  for i:=1 to Length(s) do
    Result[i]:=Char(s[i]);
  end;

  function GetStr(s:WideString):String;
  var i:Integer;
  begin
  Result:='';
  for i:=1 to Length(s)-1 do
    if ('a'<=s[i]) and (s[i]<='z') or
       ('A'<=s[i]) and (s[i]<='Z') or
       ('0'<=s[i]) and (s[i]<='9') then
      result:=result+''''+s[i]+''', '
      else
      result:=result+format('%d, ',[Integer(s[i])]);
  i:=Length(s);
    if ('a'<=s[i]) and (s[i]<='z') or
       ('A'<=s[i]) and (s[i]<='Z') or
       ('0'<=s[i]) and (s[i]<='9') then
      result:=result+''''+s[i]+''''
      else
      result:=result+format('%d',[Integer(s[i])]);
  end;
  function GetStr2(s:WideString):String;
  var i:Integer;
  begin
  Result:='';
  for i:=1 to Length(s) do
    if not((#0<=s[i]) and (s[i]<=#31)or (s[i]='''')) then
      result:=result+s[i]
      else
      result:=result+format('''#%d''',[Integer(s[i])]);
  While pos('''''', result)>1 do
    delete(result,pos('''''', result),2);
  end;

var s:String;
begin
  Assert(ConstAST <> nil, '14');
if ConstAST=Nil then
  begin
  Write(FFile, '[ConstAST=Nil]');
  end else
    begin
      if  isType(_Type, TS('String')) or isType(_Type, TS('Char')) then
          S:=GetStr(ConstAST.FSimvol.Value)
        else
        if  isType(_Type, TS('Real')) then
          begin
          if Pos('.', ConstAST.FSimvol.Value)<=0 then
             ConstAST.FSimvol.Value:=ConstAST.FSimvol.Value+'.0';
          S:=GetStrDirect(ConstAST.FSimvol.Value);
          end else
          if (_Type=ImmTypeChar) then
          begin
            s:='0'+Format('0%xh',[PInteger(@ConstAST.FSimvol.Value[1])^])+#9';'''+GetStr2(ConstAST.FSimvol.Value)+'''';
          end else
           S:=GetStrDirect(ConstAST.FSimvol.Value);
      Write(FFile,s);

      if  isType(_Type, TS('String')) or isType(_Type, TS('Char')) then
      Write(FFile,#9';''',GetStr2(ConstAST.FSimvol.Value),'''');

    end;
end;

procedure TPrinterAST.PrintConstDecl(const ConstDeclAST: TConstDeclAST);
var
  Indent:WideString;
begin
  Assert(ConstDeclAST <> nil, '15');
  PrintIndent(ConstDeclAST.Indent);
  Write(FFile, #9#9);
  PrintDataType(ConstDeclAST._Type);
  Write(FFile, ' ');
  if (unAlias(ConstDeclAST._Type).Kind=tkSimple) then
     if IsType(ConstDeclAST._Type, TS('String')) then
        begin
          Indent:=GetUniIndent('TMP');
          WriteLn(FFile, Indent+' ');
//          Write(FFile, Indent+''#9'DD ');
//          Indent:=GetUniIndent('TMP');
//          WriteLn(FFile, Indent+' ');
          WriteLn(FFile, #9#9'DD '+IntToStr(PointerSize+CharSize*2+CharSize*Length(ConstDeclAST._const.Simvol.Value)));
          WriteLn(FFile, #9#9'DD '+IntToStr(Length(ConstDeclAST._const.Simvol.Value)));
          Write(FFile, Indent+''#9'DW 0, ');
          PrintConst(ConstDeclAST._const, constDeclAST._Type);
          WriteLn(FFile, '');
          Write(FFile, #9#9'DW 0');
        end else
        begin
          PrintConst(ConstDeclAST._const, constDeclAST._Type);
        end;
  WriteLn(FFile, '        '#9'; [Const]');

end;

procedure TPrinterAST.ResultToParam(const FuncImpAST:TFuncImpAST);
var
  ResultVar:TVarDeclAST;
  FormalParametrs:TFormalParametrsAST;
  FuncDeclAST:TFuncDeclAST;
begin
    FuncDeclAST:=FuncImpAST.FuncDecl;
    if (FuncDeclAST.kind=kfFunction) and (GetTypeSize(FuncDeclAST.result)>PointerSize)
        and (not isType(FuncDeclAST.result, TS('Real'))) then
        begin
        ResultVar:=FuncImpAST.LocalDec.ListVar[0];
        ResultVar.Parent:=FuncDeclAST.FormalParametrs;
        FormalParametrs:=FuncDeclAST.FormalParametrs;
        if (Length(FormalParametrs.List)>0) and
           (FormalParametrs.List[Length(FormalParametrs.List)-1].VarDecl=nil) then
           begin
             FormalParametrs.List[Length(FormalParametrs.List)-1].VarDecl:=ResultVar;
           end else
            FormalParametrs.Add(pmVar, ResultVar);
        Move(FuncImpAST.LocalDec.ListVar[1], FuncImpAST.LocalDec.ListVar[0], (Length(FuncImpAST.LocalDec.ListVar)-1)* SizeOf(TVarDeclAST));
        SetLength(FuncImpAST.LocalDec.ListVar, Length(FuncImpAST.LocalDec.ListVar)-1);
        end;
end;

procedure TPrinterAST.ResultToParamPre(const FuncDeclAST: TFuncDeclAST);
const
  ResultVar:TVarDeclAST=nil;
begin
    if (FuncDeclAST.kind=kfFunction) and (GetTypeSize(FuncDeclAST.result)>PointerSize)
      and  (not isType(FuncDeclAST.result, TS('Real'))) then
        begin
           FuncDeclAST.FormalParametrs.Add(pmVar, ResultVar);
        end;
end;

procedure TPrinterAST.DeclarationToImplementetion(const FuncDeclAST: TFuncDeclAST);
var UnitAST:TUnitAST;
 i:Integer;
 tmp:TIndentAST;
 flag:Boolean;
begin
flag:=False;
UnitAST:=FuncDeclAST.Parent.Parent as TUnitAST;
for i:=0 to Length(UnitAST.Imp.ListFunc)-1 do
  begin
  tmp:=UnitAST.Imp.ListFunc[i].FuncDecl.Indent;
  if SimvolsEQ(tmp.Simvol, FuncDeclAST.Indent.Simvol) then
     begin
       UnitAST.Imp.ListFunc[i].FuncDecl.Indent:=FuncDeclAST.Indent;
       tmp.Destroy;
//       UnitAST.Imp.ListFunc[i].FuncDecl.Destroy;
//       UnitAST.Imp.ListFunc[i].FuncDecl:=FuncDeclAST;
       Flag:=True;
       Break;
     end;
  end;
if (Flag=False) then Compiler.Error.Add(193, FuncDeclAST.Indent.FSimvol.Value);
end;



procedure TPrinterAST.PrintDebugDeclaration(
  const DeclarationAST: TDeclarationAST);
var i:Integer;
begin
  Assert(DeclarationAST <> nil, '16');
  if WithDebugInfo then
    begin
      for i:=0 to Length(DeclarationAST.ListType)-1 do
        PrintAlias(DeclarationAST.ListType[i]);

      for i:=0 to Length(DeclarationAST.ListVar)-1 do
        PrintAlias(DeclarationAST.ListVar[i]);

      for i:=0 to Length(DeclarationAST.ListConst)-1 do
       PrintAlias(DeclarationAST.ListConst[i]);
    end;
end;

procedure TPrinterAST.DeclarationPrepare(
  const DeclarationAST: TDeclarationAST);
var i:Integer;
begin
for i:=0 to Length(DeclarationAST.ListFunc)-1 do
    begin
    ResultToParamPre(DeclarationAST.ListFunc[i]);
    DeclarationToImplementetion(DeclarationAST.ListFunc[i]);
    end;
end;

procedure TPrinterAST.ImplimentaionPrepare(
  const ImplimentaionAST: TImplimentaionAST);
var i:Integer;
begin
for i:=0 to Length(ImplimentaionAST.ListFunc)-1 do
    begin
    ResultToParam(ImplimentaionAST.ListFunc[i]);
    end;
end;

procedure TPrinterAST.PrintDeclaration(
  const DeclarationAST: TDeclarationAST);
var i:Integer;
begin
  Assert(DeclarationAST <> nil, '17');
  WriteLn(FFile, 'Data	SEGMENT	DWORD PUBLIC "data" ');
  PrintForeignAsm(DeclarationAST.ForeignAsm);

//  for i:=0 to Length(DeclarationAST.ListType)-1 do
 //   PrintTypeDecl(DeclarationAST.ListType[i]);

  for i:=0 to Length(DeclarationAST.ListVar)-1 do
    PrintVarDecl(DeclarationAST.ListVar[i]);

  for i:=0 to Length(DeclarationAST.ListConst)-1 do
    PrintConstDecl(DeclarationAST.ListConst[i]);


  // Debug
  PrintDebugDeclaration(DeclarationAST);

  WriteLn(FFile, 'Data	ENDS');

end;

procedure TPrinterAST.PrintDesignator(const DesignatorAST: TDesignatorAST);
begin
  Assert(DesignatorAST <> nil, '18');
end;

procedure TPrinterAST.PrintExpression(const Reg:Integer; const ExpressionAST: TExpressionAST);
var Type1,Type2:TTypeAST;
var RegName:String;
begin
  Assert(ExpressionAST <> nil, '19');
  case ExpressionAST.kind of
  ekSimple:
     PrintFactor(Reg, ExpressionAST.Factor1);
  ekUnari:
    begin
      PrintFactor(Reg, ExpressionAST.Factor1);
      PrintOperationUnari(Reg, ExpressionAST.Op, FactorTypeFamily(ExpressionAST.Factor1));
    end;
  ekBinary:
     begin
       PrintFactor(Reg, ExpressionAST.Factor1);
       PrintFactor(rEBX, ExpressionAST.Factor2);

       Type1:=FactorType(ExpressionAST.Factor1);
       Type2:=FactorType(ExpressionAST.Factor2);
       if IsType(Type1, TS('String')) and IsType(Type2, TS('Pointer')) then
          begin
          RegName:=GetRegName(Reg, 4);
          WriteLn(FFile, Format(#9#9'MOV %s, [%s]',[RegName, RegName]));
//          WriteLn(FFile, Format(#9#9'MOV %s, [%s]',[RegName, RegName]));
          end;
       if IsType(Type1, TS('Pointer')) and IsType(Type2, TS('String')) then
          begin
          RegName:=GetRegName(rEBX, 4);
          WriteLn(FFile, Format(#9#9'MOV %s, [%s]',[RegName, RegName]));
//          WriteLn(FFile, Format(#9#9'MOV %s, [%s]',[RegName, RegName]));
          end;
       if IsType(Type1, TS('Char')) and IsType(Type2, TS('Char')) then
          PrintOperationBinary(Reg,rEBX, ExpressionAST.Op, tfOrdinal)
          else
          PrintOperationBinary(Reg,rEBX, ExpressionAST.Op, FactorTypeFamily(ExpressionAST.Factor2));
     end;
  end;

end;

procedure TPrinterAST.PrintExpressionFPU(const Reg:Integer; const ExpressionAST: TExpressionAST);
begin
  Assert(ExpressionAST <> nil, '20');
  case ExpressionAST.kind of
  ekSimple:
     PrintFactorFPU(ExpressionAST.Factor1);
  ekUnari:
    begin
      PrintFactorFPU(ExpressionAST.Factor1);
      PrintOperationUnariFPU(ExpressionAST.Op, FactorTypeFamily(ExpressionAST.Factor1));
    end;
  ekBinary:
     begin
       PrintFactorFPU(ExpressionAST.Factor1);
       PrintFactorFPU(ExpressionAST.Factor2);
       PrintOperationBinary_Real(rEAX,rEBX, ExpressionAST.Op);
     end;
  end;
end;

function TPrinterAST.GetFildType(Signature:TSignatureAST):TTypeAST;
var
  FieldDeclAST:TFieldDeclAST;
  _Record:TTypeRecordAST;
  i:Integer;
begin
  Assert(Signature <> nil, '21');
  FieldDeclAST:=Signature.IndentDeclRef as TFieldDeclAST;
  _Record:= FieldDeclAST.Parent as TTypeRecordAST;
  for i:=0 to Length(_Record.Fields)-1 do
    if _Record.Fields[i]=FieldDeclAST then
       begin
         Result:=_Record.Fields[i]._Type;
         Break;
       end;
end;

function TPrinterAST.GetFildOfs(Signature:TSignatureAST):Integer;
var
  FieldDeclAST:TFieldDeclAST;
  _Record:TTypeRecordAST;
  i:Integer;
begin
  Assert(Signature <> nil, '22');
  Result:=0;
  FieldDeclAST:=Signature.IndentDeclRef as TFieldDeclAST;
  _Record:= FieldDeclAST.Parent as TTypeRecordAST;
  for i:=0 to Length(_Record.Fields)-1 do
    begin
    if _Record.Fields[i]=FieldDeclAST then break;
    Result:=Result+GetTypeSize(_Record.Fields[i]._Type);
    end;
end;

procedure TPrinterAST.PrintSignature(const Reg:Integer; const FactorAST: TFactorAST);
var
  Signature, Signature2: TSignatureAST;
  Ofs:Integer;
  RegName:String;
begin
  Assert(FactorAST <> nil, '23');
  Signature:=FactorAST.Signature;
  if Signature.IndentDeclRef=nil then WriteLn(FFile, '[nil]')
    else
    begin
    if (Signature.IndentDeclRef is TTypeDeclAST) then
       begin
       PrintRegAssignTypeCast(Reg, FactorAST);
       end;
    if (Signature.IndentDeclRef is TFuncDeclAST) then
       begin
       PrintRegAssignFunc(Reg, FactorAST);
       end;
    if (Signature.IndentDeclRef is TVarDeclAST) then
       begin
       case Signature.Designator.Kind of
       dkSignature: PrintRegAssignVar(Reg, FactorAST);
       dkDesignature:
         begin
           PrintRegAssignVar(Reg, FactorAST);
         end;
       dkDesignatureDot:
          begin
          PrintCmdAssign2(0, 'ESI', Signature.IndentDeclRef.Indent, ToReg);
//          PrintCmdAssign('LEA', 'ESI', Signature.IndentDeclRef.Indent, ToReg);
          Ofs:=0;
          Signature2:=Signature;
          While  Signature2.Designator.Kind= dkDesignatureDot do
             begin
             Signature2:=Signature2.Designator.Signature;
             Ofs:=Ofs+GetFildOfs(Signature2);
             end;
          WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
          RegName:=GetRegName(reg, 4);
          WriteLn(FFile, #9#9'MOV '+RegName+', [ESI+EDI]');
          end;
       end; // case
       end;
    end;
end;

procedure TPrinterAST.PrintSignatureFPU(const FactorAST: TFactorAST);
var
  Signature, Signature2: TSignatureAST;
  Ofs:Integer;
  _Type:TTypeAST;
begin
  Assert(FactorAST <> nil, '24');
  Signature:=FactorAST.Signature;
  if Signature.IndentDeclRef=nil then WriteLn(FFile, '[nil]')
    else
    begin
    if (Signature.IndentDeclRef is TTypeDeclAST) then
       begin
        // Internal error
       end;
    if (Signature.IndentDeclRef is TFuncDeclAST) then
       begin
       PrintRegAssignFuncFPU(rEAX, FactorAST);
       end;
    if (Signature.IndentDeclRef is TVarDeclAST) then
       begin
       case Signature.Designator.Kind of
       dkSignature: PrintRegAssignVarFPU(FactorAST);
       dkDesignature:
          begin
            PrintRegAssignVarFPU(FactorAST);
          end;
       dkDesignatureDot:
          begin
          PrintCmdAssign2(0, 'ESI', Signature.IndentDeclRef.Indent, ToReg);
//          PrintCmdAssign('LEA', 'ESI', Signature.IndentDeclRef.Indent, ToReg);
          Ofs:=0;
          Signature2:=Signature;
          While  Signature2.Designator.Kind= dkDesignatureDot do
             begin
             Signature2:=Signature2.Designator.Signature;
             Ofs:=Ofs+GetFildOfs(Signature2);
             end;
          WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
          _Type:=GetFildType(Signature2);
          if isTypeFast(_Type, TS('Integer')) then
               WriteLn(FFile, #9#9'FILD DWord PTR [ESI+EDI]')
             else
               WriteLn(FFile, #9#9'FLD QWord PTR [ESI+EDI]');
          end;
       end; // case
       end;
    end;
end;

procedure TPrinterAST.PrintImmConst(leval:Integer; const Reg:Integer; const ImmConstAST: TImmConstAST);
begin
  Assert(ImmConstAST <> nil, '25');
  case ImmConstAST.Kind of
  immUndefine:
     begin
     Write(FFile, '[undefine Factor]');
     end;
  immChar:
     begin
     PrintRegAssignNumber_Ordinal(Reg, ImmConstAST);
     end;
  immString:
     begin
     PrintRegAssignString(leval, Reg, ImmConstAST);
     end;
  immNil, immInteger:
     begin
     PrintRegAssignNumber_Ordinal(Reg, ImmConstAST);
     end;
  immReal:
     begin
     PrintRegAssignNumber_Real(ImmConstAST);
     end;
  immAddr:
     begin
     PrintRegAssignAddr(Reg, ImmConstAST);
     end;
  end;  // case
end;

procedure TPrinterAST.PrintImmConstReal(const ImmConstAST: TImmConstAST);
begin
  Assert(ImmConstAST <> nil, '26');
  case ImmConstAST.Kind of
  immInteger, immReal:
     begin
     PrintRegAssignNumber_Real(ImmConstAST);
     end;
  else
     Write(FFile, '[undefine Factor]');
  end; // case
end;

procedure TPrinterAST.PrintFactor(const Reg:Integer; const FactorAST: TFactorAST);
begin
  Assert(FactorAST<> nil, '27');
  case FactorAST.Kind of
  fkImmConst:   PrintImmConst(-2, Reg, FactorAST.ImmConst);
  fkSignature:  PrintSignature(Reg, FactorAST);
  end;
end;

procedure TPrinterAST.PrintFactorFPU(const FactorAST: TFactorAST);
begin
  Assert(FactorAST<> nil, '28');
  case FactorAST.Kind of
  fkImmConst:  PrintImmConstReal(FactorAST.ImmConst);
  fkSignature: PrintSignatureFPU(FactorAST);
  end;
end;

procedure TPrinterAST.PrintFieldDecl(const FieldDeclAST: TFieldDeclAST);
begin
  Assert(FieldDeclAST<> nil, '29');
end;

procedure TPrinterAST.PrintFor(const ForStmAST: TForStmAST);
var
  VarAssign:TVarAssignAST;
  VarDeclAST:TVarDeclAST;
  lLoop,lLoopEnd:WideString;
begin
  Assert(ForStmAST<> nil, '30');
  VarAssign:=TVarAssignAST.Create;
  VarAssign.Signature:=TSignatureAST.Create;
  VarAssign.Signature.IndentDeclRef:=ForStmAST.RefVar;
    VarAssign.Signature.Designator:= TDesignatorAST.Create;
    VarAssign.Signature.Designator.Kind:=dkSignature;
    VarAssign.Expression:=ForStmAST.Expr1;
    VarAssign.Parent:=ForStmAST;

    lLoop:=Compiler.Slovar.GetUniWord('@@For');
    lLoopEnd:=Compiler.Slovar.GetUniWord('@@LoopEnd');

    printPush('ECX');
    ForStmAST.SubStack:=SubStack;

    PrintVarAssign(VarAssign);
    VarDeclAST:=VarAssign.Signature.IndentDeclRef as TVarDeclAST;
    PrintCmdAssign2(1,'ECX',VarDeclAST.Indent, ToReg);
//    PrintCmdAssign('MOV','ECX',VarDeclAST.Indent, ToReg);
    WriteLn(FFile,lLoop+':');

    PrintFactor(rEAX, ForStmAST.Expr2.Factor1);
    WriteLn(FFile,#9#9'CMP ECX, EAX');
    WriteLn(FFile,#9#9'JG '+lLoopEnd);

    PrintStatement(ForStmAST.Statement);
    Write(FFile,#9#9'INC ');
    PrintIndent(VarDeclAST.Indent);
    WriteLn(FFile,'');

    WriteLn(FFile,#9#9'INC ECX');
    WriteLn(FFile,#9#9'JMP '+lLoop);
    WriteLn(FFile,lLoopEnd+':');
    printPop('ECX');


  VarAssign.Signature.Free;
  VarAssign.Free;
end;

procedure TPrinterAST.PrintRepeat(const RepeatStmAST: TRepeatStmAST);
var
  lLoop, lLoopEnd:WideString;
begin
  Assert(RepeatStmAST<> nil, '31');
  lLoop:=Compiler.Slovar.GetUniWord('@@Repeat');
  lLoopEnd:=Compiler.Slovar.GetUniWord('@@LoopEnd');
//  ForStmAST.LabelStart:=lLoop;
//  ForStmAST.LabelEnd:= lLoopEnd;

  RepeatStmAST.SubStack:=SubStack;
  WriteLn(FFile,lLoop+':');
  PrintCompaundStatement(RepeatStmAST.CompaundStatement);
  WriteLn(FFile,#9#9'XOR EAX, EAX');
  PrintCmdAssign2(1, 'EAX', RepeatStmAST.BoolExpr.Indent, ToReg);
//  PrintCmdAssign('MOV', 'EAX', RepeatStmAST.BoolExpr.Indent, ToReg);
  WriteLn(FFile,#9#9'CMP EAX, True');
  WriteLn(FFile,#9#9'JNE '+lLoop);
  WriteLn(FFile,lLoopEnd+':');

end;

procedure TPrinterAST.PrintWhile(const WhileStmAST: TWhileStmAST);
var
  lLoopEnd, lLoop:WideString;
begin
  Assert(WhileStmAST<> nil, '32');
  lLoop:=Compiler.Slovar.GetUniWord('@@While');
  lLoopEnd:=Compiler.Slovar.GetUniWord('@@LoopEnd');

  WhileStmAST.SubStack:=SubStack;
  WriteLn(FFile,lLoop+':');
  WriteLn(FFile,#9#9'XOR EAX, EAX');
  PrintCmdAssign2(1, 'EAX', WhileStmAST.BoolExpr.Indent, ToReg);
//  PrintCmdAssign('MOV', 'EAX', WhileStmAST.BoolExpr.Indent, ToReg);
  WriteLn(FFile,#9#9'CMP EAX, True');
  WriteLn(FFile,#9#9'JNE '+lLoopEnd);

  PrintStatement(WhileStmAST.Statement);
  WriteLn(FFile,#9#9'JMP '+lLoop);
  WriteLn(FFile,lLoopEnd+':');
end;

procedure TPrinterAST.PrintFormalParametrs(
  const FormalParametrsAST: TFormalParametrsAST);
var
  i:Integer;
begin
  Assert(FormalParametrsAST<> nil, '33');
  if FormalParametrsAST.Count<>0 then
    begin
    Write(FFile,'         ');
    Write(FFile,'ARG ');
     for i:=0 to FormalParametrsAST.Count-2 do
      begin
        PrintParametr(FormalParametrsAST.List[i]);
        Write(FFile,', ');
      end;
    if FormalParametrsAST.Count<>0 then
      begin
        i:=FormalParametrsAST.Count-1;
        PrintParametr(FormalParametrsAST.List[i]);
      end;
    WriteLn(FFile,'');
  end;
end;

function TPrinterAST.IsMagicFunc(const FuncCallAST: TFuncCallAST):Boolean;
begin
  Assert(FuncCallAST<> nil, '34');
  result:=SystemMagic.GetIndentRef(FuncCallAST.RefFunc.Indent)<>nil;
end;

procedure TPrinterAST.PrintMagicFunc(const FuncCallAST: TFuncCallAST);
var
  Flag:Boolean;
  sParametr:TSimvol;
  TypeFamily:TTypeFamily;
  function CreateNewCall(const FuncCallAST: TFuncCallAST; Simvol:TSimvol):TFuncCallAST;
  var
    indent:TIndentAST;
  begin
  result:= TFuncCallAST.Create;
  Indent:=TIndentAST.create(Simvol);
  result.RefFunc:= ASTFabric.GetIndentRef(Indent).parent as TFuncDeclAST;
{  result.RefFunc:=TFuncDeclAST.Create;
     result.RefFunc.kind:=FuncCallAST.RefFunc.kind;
     Indent:=TIndentAST.create(Simvol);
     result.RefFunc.Indent:= ASTFabric.GetIndentRef(Indent) as TIndentAST;
     Indent.Destroy;}
  result.RefResult:=FuncCallAST.RefResult;
  result.Parametrs:=FuncCallAST.Parametrs;
{  result.RefFunc.FormalParametrs:=TFormalParametrsAST.Create;
  result.RefFunc.FormalParametrs.Add(pmParam,TVarDeclAST.Create()); }
  end;
  procedure DestroyNewCall(var FuncCallAST: TFuncCallAST);
  begin
{  FuncCallAST.RefFunc.FormalParametrs.List[0].VarDecl.Free;
  FuncCallAST.RefFunc.FormalParametrs.List[0].Free;
  FuncCallAST.RefFunc.FormalParametrs.Free;
  FuncCallAST.RefFunc.Free;
}  FuncCallAST.Free;
  end;
  function MakeNewCall(const Simvol:TSimvol):boolean;
  var NewFuncCallAST:TFuncCallAST;
  begin
    NewFuncCallAST:=CreateNewCall(FuncCallAST, Simvol);
    PrintFuncCall(NewFuncCallAST);
    DestroyNewCall(NewFuncCallAST);
    Result:=true;
  end;

begin
  Assert(FuncCallAST<> nil, '35');
Flag:=False;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('Write')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=1 then
     begin
     TypeFamily:=self.FSemanticCheking.TypeFamily(FuncCallAST.Parametrs.Items[0]._Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfString:
           begin
             if IsType(FuncCallAST.Parametrs.Items[0]._Type,TS('Char')) then
                  Flag:=MakeNewCall(TS('WriteChar'))
                else Flag:=MakeNewCall(TS('WriteStr'));
           end;
         tfOrdinal: Flag:=MakeNewCall(TS('WriteInt'));
         end;
       end;
     end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('WriteLn')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=1 then
     begin
     TypeFamily:=self.FSemanticCheking.TypeFamily(FuncCallAST.Parametrs.Items[0]._Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfString:
           begin
             if IsType(FuncCallAST.Parametrs.Items[0]._Type,TS('Char')) then
                  Flag:=MakeNewCall(TS('WriteLnChar'))
                else Flag:=MakeNewCall(TS('WriteLnStr'));
           end;
         tfOrdinal: Flag:=MakeNewCall(TS('WriteLnInt'));
         end;
       end;
     end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('Read')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=1 then
     begin
     TypeFamily:=self.FSemanticCheking.TypeFamily(FuncCallAST.Parametrs.Items[0]._Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfString:
           begin
             if IsType(FuncCallAST.Parametrs.Items[0]._Type,TS('Char')) then
                  Flag:=MakeNewCall(TS('ReadChar'))
                else Flag:=MakeNewCall(TS('ReadStr'));
           end;
         tfOrdinal: Flag:=MakeNewCall(TS('ReadInt'));
         end;
       end;
     end;
    end;
  if SimvolsEQ(FuncCallAST.RefFunc.Indent.Simvol,TS('ReadLn')) then
    begin
    if Length(FuncCallAST.Parametrs.Items)=1 then
     begin
     TypeFamily:=self.FSemanticCheking.TypeFamily(FuncCallAST.Parametrs.Items[0]._Type);
     if not (TypeFamily in [tfUndefine, tfStructure]) then
       begin
         case TypeFamily of
         tfString:
           begin
             if IsType(FuncCallAST.Parametrs.Items[0]._Type,TS('Char')) then
                  Flag:=MakeNewCall(TS('ReadLnChar'))
                else Flag:=MakeNewCall(TS('ReadLnStr'));
           end;
         tfOrdinal: Flag:=MakeNewCall(TS('ReadLnInt'));
         end;
       end;
     end;
    end;
if Flag=False then Compiler.Error.Add(197,'��������� ���������� �������');
end;

procedure TPrinterAST.PrintFuncCall(const FuncCallAST: TFuncCallAST);
begin
  Assert(FuncCallAST<> nil, '36');
  PrintCallParametrs(FuncCallAST.RefFunc.FormalParametrs,FuncCallAST.Parametrs);
  Write(FFile, #9#9'CALL ');
  PrintIndent(FuncCallAST.RefFunc.Indent);
  WriteLn(FFile, '');
  Dec(SubStack, Length(FuncCallAST.RefFunc.FormalParametrs.List)*PointerSize);
end;

procedure TPrinterAST.PrintFuncCallAbstract(const FuncCallAST: TFuncCallAST);
begin
  Assert(FuncCallAST<> nil, '37');
  if IsMagicFunc(FuncCallAST) then
     PrintMagicFunc(FuncCallAST)
   else
     PrintFuncCall(FuncCallAST);
end;

procedure TPrinterAST.PrintFuncDecl(const FuncDeclAST: TFuncDeclAST);
begin
  Assert(FuncDeclAST<> nil, '38');
  PrintFuncName(FuncDeclAST.Indent);
  WriteLn(FFile,'PROC');
  PrintFormalParametrs(FuncDeclAST.FormalParametrs);
end;

function TPrinterAST.FindIndentResult(const FuncImpAST:TFuncImpAST):TIndentAST;
var
  TmpIndent:TIndentAST;
begin
  Assert(FuncImpAST<> nil, '39');
  TmpIndent:=TIndentAST.Create(ts('Result'));
  ASTFabric.ActivLeaf:=FuncImpAST;
  Result:=ASTFabric.GetIndentRef(TmpIndent as TNodeAST) as TIndentAST;
  TmpIndent.Destroy;
end;

procedure TPrinterAST.PrintFuncImp(const FuncImpAST: TFuncImpAST);
Var Ofs:Integer;
  procedure PrintFuncPrefix;
  begin
    WriteLn(FFile,#9#9'PUSH EBP');
    WriteLn(FFile,#9#9'MOV EBP, ESP');
    if ofs<>0 then
       WriteLn(FFile, format(#9#9'SUB ESP, %d',[ofs]));
  end;

  procedure PrintResultAsOrdinal(IndentResult:TIndentAST);
  var s:String;
  begin
    Write(FFile,#9#9'MOV ');
    case GetTypeSize(FuncImpAST.FuncDecl.result) of
    1:s:='AL';
    2:s:='AX';
    4:s:='EAX';
    end;
    Write(FFile,S+', ');
    PrintIndent(IndentResult);
    WriteLn(FFile,'');
  end;

  procedure PrintResultAsReal(IndentResult:TIndentAST);
  var s:String;
  begin
    Write(FFile,#9#9'FSTP ');
    PrintIndent(IndentResult);
    WriteLn(FFile,'');
  end;
  procedure PrintResultAsString(IndentResult:TIndentAST);
  var s:String;
  begin
    Write(FFile,#9#9'MOV EAX');
    Write(FFile,S+', ');
    PrintIndent(IndentResult);
    WriteLn(FFile,'');
  end;
  procedure PrintResultAsStructure(IndentResult:TIndentAST);
  var s:String;
  begin
    Write(FFile,#9#9'LEA EAX');
    Write(FFile,S+', ');
    PrintIndent(IndentResult);
    WriteLn(FFile,'');
  end;

  procedure PrintFuncSufix;
  var ArgSize:Integer;
  IndentResult:TIndentAST;
  begin
    if FuncImpAST.FuncDecl.kind=kfFunction then
       begin
         IndentResult:=FindIndentResult(FuncImpAST);
         case TypeFamily(FuncImpAST.FuncDecl.result) of
         tfUndefine: ;
         tfOrdinal: PrintResultAsOrdinal(IndentResult);
         tfReal:    PrintResultAsReal(IndentResult);
         tfBoolean: PrintResultAsOrdinal(IndentResult);
         tfPointer: PrintResultAsOrdinal(IndentResult);
         tfString:  if (IsType(FuncImpAST.FuncDecl.result, TS('Char'))) then
                       PrintResultAsOrdinal(IndentResult)
                       else PrintResultAsString(IndentResult);
         tfStructure: PrintResultAsStructure(IndentResult);
         end;
       end;
    ArgSize:=GetFormalParametrsSize(FuncImpAST.FuncDecl.FormalParametrs);
    WriteLn(FFile,#9#9'MOV ESP, EBP');
    WriteLn(FFile,#9#9'POP EBP');
    WriteLn(FFile,Format(#9#9'RET 0%xh',[ArgSize]));
  end;
var
  ResultVar:TVarDeclAST;
  FormalParametrs:TFormalParametrsAST;
begin
  Assert(FuncImpAST<> nil, '40');
  SubStack:=0;
{    if (FuncImpAST.FuncDecl.kind=kfFunction) and (GetTypeSize(FuncImpAST.FuncDecl.result)>PointerSize) then
        begin
        ResultVar:=FuncImpAST.LocalDec.ListVar[0];
        ResultVar.Parent:=FuncImpAST.FuncDecl.FormalParametrs;
        FormalParametrs:=FuncImpAST.FuncDecl.FormalParametrs;
        if (Length(FormalParametrs.List)>0) and
           (FormalParametrs.List[Length(FormalParametrs.List)-1].VarDecl=nil) then
           begin
             FormalParametrs.List[Length(FormalParametrs.List)-1].VarDecl:=ResultVar;
           end else
            FormalParametrs.Add(pmVar, ResultVar);
        Move(FuncImpAST.LocalDec.ListVar[1], FuncImpAST.LocalDec.ListVar[0], (Length(FuncImpAST.LocalDec.ListVar)-1)* SizeOf(TVarDeclAST));
        SetLength(FuncImpAST.LocalDec.ListVar, Length(FuncImpAST.LocalDec.ListVar)-1);
        end;
}

    PrintFuncDecl(FuncImpAST.FuncDecl);
    Ofs:=0;
    PrintLocalDeclaration(FuncImpAST.LocalDec, ofs);
    if ofs mod 4 <>0 then ofs:=((ofs+4) div 4) *4; // ������������� �����
    if FuncImpAST.AsmBlock<>nil then
       PrintForeignAsm(FuncImpAST.AsmBlock)
       else
       begin
       PrintFuncPrefix;
       PrintCreateVar(FuncImpAST.LocalDec);
       PrintCompaundStatement(FuncImpAST.CompaundStatement);
       PrintDestroyVar(FuncImpAST.LocalDec);
       PrintFuncSufix;
       end;
    PrintFuncName(FuncImpAST.FuncDecl.Indent);
    WriteLn(FFile,'ENDP');
end;

procedure TPrinterAST.PrintGoto(const GotoStmAST: TGotoStmAST);
var StatementAST:TStatementAST;
  HostStatement:THostStatementAST;
begin
  Assert(GotoStmAST<> nil, '41');
  StatementAST:=ASTFabric.GetStatementRef(GotoStmAST.RefLabel.LabelingStatement);
  if StatementAST<>nil then
     begin
     HostStatement:= StatementAST.Parent as THostStatementAST;
     if SubStack-HostStatement.SubStack>0 then
        WriteLn(FFile,#9#9'ADD ESP, '+IntToStr(SubStack-HostStatement.SubStack));
     end;
  Write(FFile,#9#9'JMP @@');
  PrintIndent(GotoStmAST.RefLabel.Indent);
  WriteLn(FFile,'');
end;

procedure TPrinterAST.PrintIf(const IfStmAST: TIfStmAST);
var
  lIfFalse, lIfTrue, lIfEnd:WideString;
begin
  Assert(IfStmAST<> nil, '42');
  lIfTrue:=Compiler.Slovar.GetUniWord('@@IfTrue');
  lIfFalse:=Compiler.Slovar.GetUniWord('@@IfFalse');
  lIfEnd:=Compiler.Slovar.GetUniWord('@@IfEnd');

  IfStmAST.SubStack:=SubStack;
  WriteLn(FFile,#9#9'XOR EAX, EAX');
  PrintCmdAssign2(1, 'EAX', IfStmAST.BoolExpr.Indent, ToReg);
//  PrintCmdAssign('MOV', 'EAX', IfStmAST.BoolExpr.Indent, ToReg);
  WriteLn(FFile,#9#9'CMP EAX, True');
  WriteLn(FFile,#9#9'JNE '+lIfFalse);
  WriteLn(FFile, lIfTrue+':');
  if IfStmAST.TrueEdge<>nil then
     PrintStatement(IfStmAST.TrueEdge);
  WriteLn(FFile,#9#9'JMP '+lIfEnd);
  WriteLn(FFile,lIfFalse+':');
  if IfStmAST.FalseEdge<>nil then
     PrintStatement(IfStmAST.FalseEdge);
  WriteLn(FFile, lIfEnd+':');

end;

procedure TPrinterAST.PrintImplimentaion(
  const ImplimentaionAST: TImplimentaionAST);
var
  i:Integer;
begin
  Assert(ImplimentaionAST<> nil, '43');
  Write(FFile, 'Code	SEGMENT	PARA PUBLIC "code" ');
  if CodeTable=ctUSE16 then WriteLn(FFile, 'USE16');
  if CodeTable=ctUSE32 then WriteLn(FFile, 'USE32');
  WriteLn(FFile, 'Assume ds:Data');

  for i:=0 to Length(ImplimentaionAST.ListFunc)-1 do
    begin
    PrintFuncImp(ImplimentaionAST.ListFunc[i]);
    end;
  PrintMainBlock(ImplimentaionAST.MainBlock);
  PrintForeignAsm(ImplimentaionAST.ForeignAsm);
  WriteLn(FFile, 'Code	ENDS');
end;

procedure TPrinterAST.PrintIndent(const IndentAST: TIndentAST);
var
  sKey,sIndent:WideString;
  Index:Integer;
begin
  Assert(IndentAST<> nil, '44');
  if IndentAST=Nil then  Write(FFile, '[Nil]')
   else
     begin                              
     sKey:=IndentAST.FSimvol.Value;
     if Compiler.Slovar.IsExist(sKey) then
        begin
        Index:=Compiler.Slovar.FindObj(IndentAST);
        if (Index<0) then
           begin
           sIndent:=Compiler.Slovar.GenUniWord(sKey);
           Compiler.Slovar.Add(IndentAST.Simvol.Value,IndentAST, sIndent, nil);
           end else
           begin
           sIndent:=Compiler.Slovar.List[index].Trans;
           end;
        end else
        begin
        sIndent:=Compiler.Slovar.GenUniWord(sKey);
        Compiler.Slovar.Add(IndentAST.Simvol.Value,IndentAST, sIndent, nil);
        end;
     Write(FFile, sIndent);
     end;
end;

procedure TPrinterAST.PrintLabelDecl(const LabelDeclAST: TLabelDeclAST);
var
  sKey,sIndent:WideString;
  Index:Integer;
begin
  Assert(LabelDeclAST<> nil, '45');
with LabelDeclAST do
   if Indent=Nil then  Write(FFile, '[Nil]')
   else
     begin
     sKey:=Indent.FSimvol.Value;
     if Compiler.Slovar.IsExist(sKey) then
        begin
        Index:=Compiler.Slovar.FindObj(Indent);
        if (Index<0) then
           begin
           sIndent:=Compiler.Slovar.GenUniWord(sKey);
           Compiler.Slovar.Add(Indent.Simvol.Value,Indent, sIndent, nil);
           end else
           begin
           sIndent:=Compiler.Slovar.List[index].Trans;
           end;
        end else
        begin
        sIndent:=Compiler.Slovar.GenUniWord(sKey);
        Compiler.Slovar.Add(Indent.Simvol.Value,Indent, sIndent, nil);
        end;
     end;
end;

procedure TPrinterAST.PrintLabelingStatement(
  const LabelingStatementAST: TLabelingStatementAST);
begin
  Assert(LabelingStatementAST<> nil, '46');
With LabelingStatementAST do
 case Kind of
 ksLabeling:
   begin
   if RefLabel= nil then
        WriteLn(FFile, '[DecLabel=Nil]:')
      else
        begin
        Write(FFile, '@@');
        PrintIndent(RefLabel.Indent);
        end;
   WriteLn(FFile, ':');
   PrintLabelingStatement(LabelingStatementAST.LabelingStatement);
   end;
 ksSimple:
   begin
   if SimpleStatement<>nil then
      PrintSimpleStatement(SimpleStatement);
   end;
 end;
end;

procedure TPrinterAST.PrintFuncName(const IndentAST: TIndentAST);
var
  i:Integer;
begin
  Assert(IndentAST<> nil, '47');
   if IndentAST=Nil then  Write(FFile, '[Nil]')
   else
    begin
      PrintIndent(IndentAST);
      Write(FFile, ' ');
      for i:=0 to 10-1-Length(IndentAST.FSimvol.Value) do
         Write(FFile, ' ');

    end;
end;

procedure TPrinterAST.PrintMainBlock(
  const CompaundStatementAST: TCompaundStatementAST);
begin
  Assert(CompaundStatementAST<> nil, '48');
  StartLabel:=GetUniIndent('Main');
  WriteLn(FFile, StartLabel+':');
  WriteLn(FFile, #9#9'CALL _start');
  SubStack:=0;
  if CompaundStatementAST<>Nil then
     PrintCompaundStatement(CompaundStatementAST);
  WriteLn(FFile, #9#9'CALL _halt');
end;

procedure TPrinterAST.PrintOperationUnari_Ordinal(const Reg1:Integer;
          const OperatorAST: TOperatorAST);
var
  Arg1:String;
  Op:TSimvol;
begin
  Assert(OperatorAST<> nil, '49');
  Arg1:=GetRegName(Reg1, 4);
  Op:=OperatorAST.Simvol;

  if SimvolsEQ(Op,TS('-')) then
     WriteLn(FFile, #9#9'NEG '+Arg1)
  else
  if SimvolsEQ(Op,TS('+')) then
     WriteLn(FFile, #9#9'MOV '+Arg1+' , '+Arg1)
  else
    if SimvolsEQ(Op,TS('NOT')) then
       WriteLn(FFile, #9#9'NOT '+Arg1)
       else
       begin
         Write(FFile, '[undefine] ');
         Compiler.Error.Add(199,'; ��������� ������');
       end;
end;

procedure TPrinterAST.PrintOperationUnari_Boolean(const Reg1:Integer;
          const OperatorAST: TOperatorAST);
var
  Arg1:String;
  Op:TSimvol;
begin
  Assert(OperatorAST<> nil, '49');
  Arg1:=GetRegName(Reg1, 4);
  Op:=OperatorAST.Simvol;

  if SimvolsEQ(Op,TS('NOT')) then
     begin
       WriteLn(FFile, #9#9'NOT '+Arg1);
       WriteLn(FFile, #9#9'AND '+Arg1+', 1');
     end else
     begin
       Write(FFile, '[undefine] ');
       Compiler.Error.Add(199,'; ��������� ������');
     end;
end;
procedure TPrinterAST.PrintOperationUnari_Real(
          const OperatorAST: TOperatorAST);
var
  Op:TSimvol;
begin
  Assert(OperatorAST<> nil, '50');
  Op:=OperatorAST.Simvol;

  if SimvolsEQ(Op,TS('-')) then
     WriteLn(FFile, #9#9'FCHS')
     else
       begin
         Write(FFile, '[undefine] ');
         Compiler.Error.Add(199,'; ��������� ������');
       end;
end;

procedure TPrinterAST.PrintOperationUnari(const Reg1:Integer;
          const OperatorAST: TOperatorAST; const TypeFamily:TTypeFamily);
begin
  Assert(OperatorAST<> nil, '50');
  if  TypeFamily=tfOrdinal then
      PrintOperationUnari_Ordinal(Reg1, OperatorAST);
  if  TypeFamily=tfBoolean then
      PrintOperationUnari_Boolean(Reg1, OperatorAST);
end;

procedure TPrinterAST.PrintOperationUnariFPU(
          const OperatorAST: TOperatorAST; const TypeFamily:TTypeFamily);
begin
  Assert(OperatorAST<> nil, '51');
  if  (TypeFamily=tfOrdinal) or
      (TypeFamily=tfReal) then
     PrintOperationUnari_Real(OperatorAST);
end;

procedure TPrinterAST.PrintOperationBinary_Boolean(const Reg1,
  Reg2: Integer; const OperatorAST: TOperatorAST);
var
 Arg1, Arg2:String;
 Op:TSimvol;
  lRelTrue, lRelFalse, lRelEnd:WideString;
begin
  Assert(OperatorAST<> nil, '52');
  Op:=OperatorAST.Simvol;
  Arg1:=GetRegName(Reg1, 4);
  Arg2:=GetRegName(Reg2, 4);
  if IsOpReletiv(Op) then
     begin
     lRelTrue:=Compiler.Slovar.GetUniWord('@@RelTrue');
     lRelFalse:=Compiler.Slovar.GetUniWord('@@RelFalse');
     lRelEnd:=Compiler.Slovar.GetUniWord('@@RelEnd');
     end;

  if SimvolsEQ(Op,TS('-')) or
     SimvolsEQ(Op,TS('+')) or
     SimvolsEQ(Op,TS('*')) or
     SimvolsEQ(Op,TS('div')) or
     SimvolsEQ(Op,TS('mod')) then
     begin
      {�� �������������� ��������}
     end;
  if SimvolsEQ(Op,TS('AND')) then
     begin
     Write(FFile, #9#9'AND ');
     WriteLn(FFile, Arg1+', '+Arg2);
     end;
  if SimvolsEQ(Op,TS('OR')) then
     begin
     Write(FFile, #9#9'OR ');
     WriteLn(FFile, Arg1+', '+Arg2);
     end;
  if SimvolsEQ(Op,TS('XOR')) then
     begin
     Write(FFile, #9#9'XOR ');
     WriteLn(FFile, Arg1+', '+Arg2);
     end;

  if SimvolsEQ(Op,TS('<')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JL '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JG '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('=')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<>')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JNE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<=')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JLE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>=')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JGE '+lRelTrue);
     end;

  if IsOpReletiv(Op) then
     begin
     WriteLn(FFile, lRelFalse+':');
     WriteLn(FFile, #9#9'MOV EAX, False');
     WriteLn(FFile, #9#9'JMP '+lRelEnd);
     WriteLn(FFile, lRelTrue+':');
     WriteLn(FFile, #9#9'MOV EAX, True');
     WriteLn(FFile, lRelEnd+':');
     end;

end;

procedure TPrinterAST.PrintOperationBinary_Ordinal(const Reg1,
  Reg2: Integer; const OperatorAST: TOperatorAST);
var
 Arg1, Arg2:String;
 Op:TSimvol;
  lRelTrue, lRelFalse, lRelEnd:WideString;
begin
  Assert(OperatorAST<> nil, '53');
  Op:=OperatorAST.Simvol;
  Arg1:=GetRegName(Reg1, 4);
  Arg2:=GetRegName(Reg2, 4);
  if IsOpReletiv(Op) then
     begin
     lRelTrue:=Compiler.Slovar.GetUniWord('@@RelTrue');
     lRelFalse:=Compiler.Slovar.GetUniWord('@@RelFalse');
     lRelEnd:=Compiler.Slovar.GetUniWord('@@RelEnd');
     end;

  if SimvolsEQ(Op,TS('shl')) then
     begin
     PrintPush('ECX');
     WriteLn(FFile, #9#9'MOV ECX, '+Arg2);
     Write(FFile, #9#9'SHL ');
     WriteLn(FFile, Arg1+', CL');
     PrintPop('ECX');
     end;
  if SimvolsEQ(Op,TS('shr')) then
     begin
     PrintPush('ECX');
     WriteLn(FFile, #9#9'MOV ECX, '+Arg2);
     Write(FFile, #9#9'SHR ');
     WriteLn(FFile, Arg1+', CL');
     PrintPop('ECX');
     end;
  if SimvolsEQ(Op,TS('and')) then
     begin
     Write(FFile, #9#9'AND ');
     WriteLn(FFile, Arg1+', '+Arg2);
     end;
  if SimvolsEQ(Op,TS('OR')) then
     begin
     Write(FFile, #9#9'OR ');
     WriteLn(FFile, Arg1+', '+Arg2);
     end;
  if SimvolsEQ(Op,TS('XOR')) then
     begin
     Write(FFile, #9#9'XOR ');
     WriteLn(FFile, Arg1+', '+Arg2);
     end;
  if SimvolsEQ(Op,TS('-')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     end;
  if SimvolsEQ(Op,TS('+')) then
     begin
     Write(FFile, #9#9'ADD ');
     WriteLn(FFile, Arg1+', '+Arg2);
     end;
  if SimvolsEQ(Op,TS('*')) then
     begin
     Write(FFile, #9#9'IMUL ');
     WriteLn(FFile, Arg1+', '+Arg2);
     end;
  if SimvolsEQ(Op,TS('div')) then
     begin
     DivPrintChangeReg(Arg1, Arg2);
     WriteLn(FFile, #9#9'XOR EDX, EDX');
     WriteLn(FFile, #9#9'IDIV EBX');
     Write(FFile, #9#9'MOV ');
     WriteLn(FFile, Arg1+', EAX');
     end;
  if SimvolsEQ(Op,TS('mod')) then
     begin
     DivPrintChangeReg(Arg1, Arg2);
     WriteLn(FFile, #9#9'XOR EDX, EDX');
     WriteLn(FFile, #9#9'IDIV EBX');
     Write(FFile, #9#9'MOV ');
     WriteLn(FFile, Arg1+', EDX');
     end;

  if SimvolsEQ(Op,TS('<')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JL '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JG '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('=')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<>')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JNE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<=')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JLE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>=')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JGE '+lRelTrue);
     end;

  if IsOpReletiv(Op) then
     begin
     WriteLn(FFile, lRelFalse+':');
     WriteLn(FFile, #9#9'MOV EAX, False');
     WriteLn(FFile, #9#9'JMP '+lRelEnd);
     WriteLn(FFile, lRelTrue+':');
     WriteLn(FFile, #9#9'MOV EAX, True');
     WriteLn(FFile, lRelEnd+':');
     end;
end;

procedure TPrinterAST.PrintOperationBinary_Pointer(const Reg1,
  Reg2: Integer; const OperatorAST: TOperatorAST);
var
 Arg1, Arg2:String;
 Op:TSimvol;
  lRelTrue, lRelFalse, lRelEnd:WideString;
begin
  Assert(OperatorAST<> nil, '54');
  Op:=OperatorAST.Simvol;
  Arg1:=GetRegName(Reg1, 4);
  Arg2:=GetRegName(Reg2, 4);
  if IsOpReletiv(Op) then
     begin
     lRelTrue:=Compiler.Slovar.GetUniWord('@@RelTrue');
     lRelFalse:=Compiler.Slovar.GetUniWord('@@RelFalse');
     lRelEnd:=Compiler.Slovar.GetUniWord('@@RelEnd');
     end;

  if SimvolsEQ(Op,TS('-')) or
     SimvolsEQ(Op,TS('+')) or
     SimvolsEQ(Op,TS('*')) or
     SimvolsEQ(Op,TS('div')) or
     SimvolsEQ(Op,TS('mod')) or
     SimvolsEQ(Op,TS('AND')) or
     SimvolsEQ(Op,TS('OR')) or
     SimvolsEQ(Op,TS('XOR')) then
     begin
      {�� �������������� ��������}
     end;

  if SimvolsEQ(Op,TS('<')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JL '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JG '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('=')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<>')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JNE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<=')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JLE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>=')) then
     begin
     Write(FFile, #9#9'SUB ');
     WriteLn(FFile, Arg1+', '+Arg2);
     WriteLn(FFile, #9#9'JGE '+lRelTrue);
     end;

  if IsOpReletiv(Op) then
     begin
     WriteLn(FFile, lRelFalse+':');
     WriteLn(FFile, #9#9'MOV EAX, False');
     WriteLn(FFile, #9#9'JMP '+lRelEnd);
     WriteLn(FFile, lRelTrue+':');
     WriteLn(FFile, #9#9'MOV EAX, True');
     WriteLn(FFile, lRelEnd+':');
     end;

end;

procedure TPrinterAST.PrintOperationBinary_Real(const Reg1, Reg2: Integer;
  const OperatorAST: TOperatorAST);
var
 Arg1, Arg2:String;
 Op:TSimvol;
  lRelTrue, lRelFalse, lRelEnd:WideString;
begin
  Assert(OperatorAST<> nil, '55');
  Op:=OperatorAST.Simvol;
  Arg1:=GetRegName(Reg1, 4);
  Arg2:=GetRegName(Reg2, 4);
  if IsOpReletiv(Op) then
     begin
     lRelTrue:=Compiler.Slovar.GetUniWord('@@RelTrue');
     lRelFalse:=Compiler.Slovar.GetUniWord('@@RelFalse');
     lRelEnd:=Compiler.Slovar.GetUniWord('@@RelEnd');
     end;

  if SimvolsEQ(Op,TS('AND')) or
     SimvolsEQ(Op,TS('OR')) or
     SimvolsEQ(Op,TS('XOR')) or
     SimvolsEQ(Op,TS('div')) or
     SimvolsEQ(Op,TS('mod')) then
     begin
      {�� �������������� ��������}
     end;

  if SimvolsEQ(Op,TS('+')) then
     begin
     WriteLn(FFile, #9#9'FADDP');
     end;
  if SimvolsEQ(Op,TS('-')) then
     begin
     WriteLn(FFile, #9#9'FSUBP ');
     end;
  if SimvolsEQ(Op,TS('*')) then
     begin
     WriteLn(FFile, #9#9'FMULP ');
     end;

  if SimvolsEQ(Op,TS('/')) then
     begin
     WriteLn(FFile, #9#9'FDIVP ');
     end;

  if SimvolsEQ(Op,TS('<')) then
     begin
     WriteLn(FFile, #9#9'FCOMPP');
     WriteLn(FFile, #9#9'FSTSW AX');
     WriteLn(FFile, #9#9'SAHF');
     WriteLn(FFile, #9#9'JL '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>')) then
     begin
     WriteLn(FFile, #9#9'FCOMPP ');
     WriteLn(FFile, #9#9'FSTSW AX');
     WriteLn(FFile, #9#9'SAHF');
     WriteLn(FFile, #9#9'JG '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('=')) then
     begin
     WriteLn(FFile, #9#9'FCOMPP ');
     WriteLn(FFile, #9#9'FSTSW AX');
     WriteLn(FFile, #9#9'SAHF');
     WriteLn(FFile, #9#9'JE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<>')) then
     begin
     WriteLn(FFile, #9#9'FCOMPP ');
     WriteLn(FFile, #9#9'FSTSW AX');
     WriteLn(FFile, #9#9'SAHF');
     WriteLn(FFile, #9#9'JNE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<=')) then
     begin
     WriteLn(FFile, #9#9'FCOMPP ');
     WriteLn(FFile, #9#9'FSTSW AX');
     WriteLn(FFile, #9#9'SAHF');
     WriteLn(FFile, #9#9'JLE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>=')) then
     begin
     WriteLn(FFile, #9#9'FCOMPP ');
     WriteLn(FFile, #9#9'FSTSW AX');
     WriteLn(FFile, #9#9'SAHF');
     WriteLn(FFile, #9#9'JGE '+lRelTrue);
     end;

  if IsOpReletiv(Op) then
     begin
     WriteLn(FFile, lRelFalse+':');
     WriteLn(FFile, #9#9'MOV EAX, False');
     WriteLn(FFile, #9#9'JMP '+lRelEnd);
     WriteLn(FFile, lRelTrue+':');
     WriteLn(FFile, #9#9'MOV EAX, True');
     WriteLn(FFile, lRelEnd+':');
     end;


end;

procedure TPrinterAST.PrintOperationBinary_String(const Reg1,
  Reg2: Integer; const OperatorAST: TOperatorAST);
var
 Arg1, Arg2:String;
 Op:TSimvol;
  lRelTrue, lRelFalse, lRelEnd:WideString;
 NewOperator:TNewOperatorAST;
begin
  Assert(OperatorAST<> nil, '56');
  Op:=OperatorAST.Simvol;
  Arg1:=GetRegName(Reg1, 4);
  Arg2:=GetRegName(Reg2, 4);
  if IsOpReletiv(Op) then
     begin
     lRelTrue:=Compiler.Slovar.GetUniWord('@@RelTrue');
     lRelFalse:=Compiler.Slovar.GetUniWord('@@RelFalse');
     lRelEnd:=Compiler.Slovar.GetUniWord('@@RelEnd');
     end;

  if SimvolsEQ(Op,TS('+')) then
     begin
     if OperatorAST is TNewOperatorAST then
        begin
        PrintPush(Arg2);
        PrintPush(Arg1);
        WriteLn(FFile, #9#9'CALL ConcatinatedStringChar');
        Dec(SubStack, PointerSize+CharSize);
        end else
        begin
        PrintPush(Arg2);
        PrintPush(Arg1);
        WriteLn(FFile, #9#9'CALL ConcatinatedStringString');
        Dec(SubStack, 2*PointerSize);
        end;
     end;

  if SimvolsEQ(Op,TS('-')) or
     SimvolsEQ(Op,TS('*')) or
     SimvolsEQ(Op,TS('div')) or
     SimvolsEQ(Op,TS('mod')) or
     SimvolsEQ(Op,TS('AND')) or
     SimvolsEQ(Op,TS('OR')) or
     SimvolsEQ(Op,TS('XOR')) then
     begin
      {�� �������������� ��������}
     end;

  if SimvolsEQ(Op,TS('<')) then
     begin
     PrintPush(Arg2);
     PrintPush(Arg1);
     WriteLn(FFile, #9#9'CALL CompareStr');
     WriteLn(FFile, #9#9'CMP EAX,0');
     Dec(SubStack, 2*PointerSize);

     WriteLn(FFile, #9#9'JL '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>')) then
     begin
     PrintPush(Arg2);
     PrintPush(Arg1);
     WriteLn(FFile, #9#9'CALL CompareStr');
     WriteLn(FFile, #9#9'CMP EAX,0');
     Dec(SubStack, 2*PointerSize);

     WriteLn(FFile, #9#9'JG '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('=')) then
     begin
     PrintPush(Arg2);
     PrintPush(Arg1);
     WriteLn(FFile, #9#9'CALL CompareStr');
     WriteLn(FFile, #9#9'CMP EAX,0');
     Dec(SubStack, 2*PointerSize);

     WriteLn(FFile, #9#9'JE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<>')) then
     begin
     PrintPush(Arg2);
     PrintPush(Arg1);
     WriteLn(FFile, #9#9'CALL CompareStr');
     WriteLn(FFile, #9#9'CMP EAX,0');
     Dec(SubStack, 2*PointerSize);

     WriteLn(FFile, #9#9'JNE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('<=')) then
     begin
     PrintPush(Arg2);
     PrintPush(Arg1);
     WriteLn(FFile, #9#9'CALL CompareStr');
     WriteLn(FFile, #9#9'CMP EAX,0');
     Dec(SubStack, 2*PointerSize);
     WriteLn(FFile, #9#9'JLE '+lRelTrue);
     end;
  if SimvolsEQ(Op,TS('>=')) then
     begin
     PrintPush(Arg2);
     PrintPush(Arg1);
     WriteLn(FFile, #9#9'CALL CompareStr');
     WriteLn(FFile, #9#9'CMP EAX,0');
     Dec(SubStack, 2*PointerSize);
     WriteLn(FFile, #9#9'JGE '+lRelTrue);
     end;

  if IsOpReletiv(Op) then
     begin
     WriteLn(FFile, lRelFalse+':');
     WriteLn(FFile, #9#9'MOV EAX, False');
     WriteLn(FFile, #9#9'JMP '+lRelEnd);
     WriteLn(FFile, lRelTrue+':');
     WriteLn(FFile, #9#9'MOV EAX, True');
     WriteLn(FFile, lRelEnd+':');
     end;

end;

procedure TPrinterAST.PrintOperationBinary(const Reg1, Reg2:Integer; const OperatorAST: TOperatorAST;
    const TypeFamily:TTypeFamily);
begin
  Assert(OperatorAST<> nil, '57');
  case TypeFamily of
  tfUndefine:  Compiler.Error.Add(194, '');  // ����������� ���
  tfOrdinal:   PrintOperationBinary_Ordinal(Reg1, Reg2, OperatorAST);
  tfReal:      Compiler.Error.Add(194, '');  // ����������� ���,  ��������� ������
  tfBoolean:   PrintOperationBinary_Boolean(Reg1, Reg2, OperatorAST);
  tfPointer:   PrintOperationBinary_Pointer(Reg1, Reg2, OperatorAST);
  tfString:    PrintOperationBinary_String(Reg1, Reg2, OperatorAST);
  tfStructure: Compiler.Error.Add(194, '');  // ����������� ���
  end;

end;

procedure TPrinterAST.PrintParametr(const ParametrAST: TParametrAST);
begin
  Assert(ParametrAST<> nil, '58');
    //Write(FFile, '@@');
  PrintIndent(ParametrAST.VarDecl.Indent);
  Write(FFile, ':');
  PrintCodeType(ParametrAST.VarDecl._Type);
end;

procedure TPrinterAST.PrintSimpleStatement(
  const SimpleStatementAST: TSimpleStatementAST);
begin
  Assert(SimpleStatementAST<> nil, '59');
  case SimpleStatementAST.kind of
  ssUndefine: Write(FFile, '[Undefine]');
  ssFuncCall: PrintFuncCallAbstract(SimpleStatementAST.FuncCall);
  ssTypeCast: Write(FFile, '[Undefine]');
  ssVarAssign: PrintVarAssign(SimpleStatementAST.VarAssign);
  end;

end;

procedure TPrinterAST.PrintDebugStatement(const StatementAST: TStatementAST);
begin
  Assert(StatementAST<> nil, '60');
  if WithDebugInfo then WriteLn(FFile, format(#9#9#9#9#9#9#9#9#9#9'?debug	Line %d',[StatementAST.Line]));
end;

procedure TPrinterAST.PrintStatement(const StatementAST: TStatementAST);
begin
  Assert(StatementAST<> nil, '61');
  FLastPosRow:=StatementAST.Line;
  FLastPosCol:=0;
//  WriteLn(#$1b'[1000D Process Line=', FLastPosRow);
  PrintDebugStatement(StatementAST);
  With StatementAST do
    case Kind of
    ksGoto: PrintGoto(_Goto);
    ksFor: PrintFor(_For);
    ksWhile: PrintWhile(_While);
    ksRepeat:PrintRepeat(_Repeat);
    ksIf: PrintIf(_IF);
    ksCase: PrintCase(_Case);
    ksLabelingStatement: PrintLabelingStatement(LabelingStatement);
    ksCompaundStatement: PrintCompaundStatement(CompaundStatement);
    end; // case
  Flush(FFile);
end;

procedure TPrinterAST.PrintDataType(const TypeAST: TTypeAST);
begin
  Assert(TypeAST<> nil, '62');
if TypeAST=nil then Write(FFile,'[nil]')
  else
  begin
   case TypeAST.Kind of
   tkUndefine: Write(FFile, '[undefine]');
   tkAlias:  PrintDataType(unAlias(TypeAST));
   tkSimple: PrintDataTypeSimple(TypeAST.Simple);
   tkRecord: PrintDataTypeRecord(TypeAST._Record);
   tkArray: PrintDataTypeArray(TypeAST._Array);
   tkPointer: Write(FFile, 'DD');
   end;
  end;

end;

procedure TPrinterAST.PrintCodeType(const TypeAST: TTypeAST);
begin
  Assert(TypeAST<> nil, '63');
if TypeAST=nil then Write(FFile,'[nil]')
  else
  begin
   case TypeAST.Kind of
   tkUndefine: Write(FFile, '[undefine]');
   tkAlias:  PrintCodeType(unAlias(TypeAST));
   tkSimple: PrintCodeTypeSimple(TypeAST.Simple);
   tkRecord: PrintCodeTypeRecord(TypeAST._Record);
   tkArray:  Write(FFile, '[array]');
   tkPointer: PrintCodeTypePointer();

   end;
  end;
end;

procedure TPrinterAST.PrintTypeDecl(const TypeDeclAST: TTypeDeclAST);
begin
  Assert(TypeDeclAST<> nil, '64');
if (TypeDeclAST._Type.Kind=tkRecord) then
  begin
    PrintIndent(TypeDeclAST.Indent);
    PrintDataTypeRecord(TypeDeclAST._Type._Record);
  end;
end;

procedure TPrinterAST.PrintDataTypeRecord(const TypeRecordAST: TTypeRecordAST);
begin
  Assert(TypeRecordAST<> nil, '65');
  Write(FFile, 'DB ');
  Write(FFile, IntToStr(GetRecordTypeSize(TypeRecordAST)));
end;

procedure TPrinterAST.PrintDataTypeArray(const TypeArrayAST: TTypeArrayAST);
begin
  Assert(TypeArrayAST<> nil, '66');
  Write(FFile, 'DB ');
  Write(FFile, IntToStr(GetArrayTypeSize(TypeArrayAST)));
end;

procedure TPrinterAST.PrintDataTypeSimple(const TypeSimpleAST: TTypeSimpleAST);
var
  Table:array [0..12] of
          record
            ASTType:WideString;
            ASMType:WideString;
          end;

  procedure Init;
  begin
  Table[0].ASTType:='Integer';  Table[0].ASmType:='DD';
  Table[1].ASTType:='Real';     Table[1].ASmType:='DQ';
  Table[2].ASTType:='Boolean';  Table[2].ASmType:='DD';
  Table[3].ASTType:='Pointer';  Table[3].ASmType:='DD';
  Table[4].ASTType:='PByte';    Table[4].ASmType:='DD';
  Table[5].ASTType:='PInteger'; Table[5].ASmType:='DD';
  Table[6].ASTType:='PReal';    Table[6].ASmType:='DD';
  Table[7].ASTType:='PBoolean'; Table[7].ASmType:='DD';
  Table[8].ASTType:='String';   Table[8].ASmType:='DD';
  Table[9].ASTType:='Char';     Table[9].ASmType:='DW';
  Table[10].ASTType:='UInt8';   Table[10].ASmType:='DB';
  Table[11].ASTType:='UInt16';  Table[11].ASmType:='DW';
  Table[12].ASTType:='UInt32';  Table[12].ASmType:='DD';
  end;

var I:Integer;
Result:Boolean;
ASTType:WideString;
begin
  Assert(TypeSimpleAST<> nil, '67');
  Init;
  Result:=False;
  for i:=Low(Table) to High(Table) do
     begin
     ASTType:=Table[i].ASTType;
      if CompareText(ASTType, TypeSimpleAST.FSimvol.Value)=0 then
         begin
           Write(FFile, Table[i].ASMType);
           Result:=True;
           Break;
         end;
     end;
  if Result=False then
     begin
        Write(FFile, '[undefine='+TypeSimpleAST.FSimvol.Value+']');
     end;
end;

procedure TPrinterAST.PrintCodeTypeRecord(const TypeRecordAST: TTypeRecordAST);
var
  i, Count, Size:Integer;
  _Record:TTypeRecordAST;
begin
  Assert(TypeRecordAST<> nil, '68');
  Write(FFile, ' BYTE ');
  _Record:=TypeRecordAST;
  Write(FFile, ':');
  Write(FFile, 4);
  //  Write(FFile, GetRecordTypeSize(_Record));
{
  Count:=Length(_Record.Fields);
  for i:=0 to Count-2 do
    begin
      PrintIndent(_Record.Fields[i].Indent);
      Write(FFile, ':');
      PrintTypeSize(_Record.Fields[i]._Type);
      Write(FFile, ', ');
    end;
  if (Count>=1) then
     begin
      i:=Count-1;
      PrintIndent(_Record.Fields[i].Indent);
      Write(FFile, ':');
     end; }
end;

procedure TPrinterAST.PrintCodeTypeSimple(const TypeSimpleAST: TTypeSimpleAST);
var
  Table:array [0..12] of
          record
            ASTType:WideString;
            ASMType:WideString;
          end;

  procedure Init;
  begin
  Table[0].ASTType:='Integer';  Table[0].ASmType:='DWord';
  Table[1].ASTType:='Real';     Table[1].ASmType:='QWord';
  Table[2].ASTType:='Boolean';  Table[2].ASmType:='DWord';
  Table[3].ASTType:='Pointer';  Table[3].ASmType:='DWord';
  Table[4].ASTType:='PByte';    Table[4].ASmType:='DWord';
  Table[5].ASTType:='PInteger'; Table[5].ASmType:='DWord';
  Table[6].ASTType:='PReal';    Table[6].ASmType:='DWord';
  Table[7].ASTType:='PBoolean'; Table[7].ASmType:='DWord';
  Table[8].ASTType:='String';   Table[8].ASmType:='DWord';
  Table[9].ASTType:='Char';     Table[9].ASmType:='Word';
  Table[10].ASTType:='UInt8';   Table[10].ASmType:='Byte';
  Table[11].ASTType:='UInt16';  Table[11].ASmType:='Word';
  Table[12].ASTType:='UInt32';  Table[12].ASmType:='DWord';
  end;

var I:Integer;
Result:Boolean;
ASTType:WideString;
begin
  Assert(TypeSimpleAST<> nil, '69');
  Init;
  Result:=False;
  for i:=Low(Table) to High(Table) do
     begin
     ASTType:=Table[i].ASTType;
      if CompareText(ASTType, TypeSimpleAST.FSimvol.Value)=0 then
         begin
           Write(FFile, Table[i].ASMType);
           Result:=True;
           Break;
         end;
     end;
  if Result=False then
     begin
        Write(FFile, '[undefine='+TypeSimpleAST.FSimvol.Value+']');
     end;
end;

procedure TPrinterAST.PrintCodeTypePointer;
var PonterType:TTypeSimpleAST;
begin
PonterType:=TTypeSimpleAST.Create(TS('Pointer'));
PrintCodeTypeSimple(PonterType);
PonterType.Free;
end;

procedure TPrinterAST.PrintTypeSize(const TypeAST: TTypeAST);
begin
  Assert(TypeAST<> nil, '70');
  WriteLn(FFile, IntToStr(GetTypeSize(TypeAST)));
end;

procedure TPrinterAST.PrintUnitAST(const UnitAST: TUnitAST);
begin
  Assert(UnitAST<> nil, '71');
  WriteLn(FFile, 'TITLE	No_name');
  WriteLn(FFile, '.586P');
  WriteLn(FFile, '.MODEL          FLAT, NOLANGUAGE');
  WriteLn(FFile, 'LOCALS @@');
  WriteLn(FFile, 'include system.asm');
  WriteLn(FFile, '');

  DeclarationPrepare(UnitAst.Dec);
  ImplimentaionPrepare(UnitAst.Imp);
  if UnitAst.Imp= Nil then WriteLn(FFile, '')
    else PrintImplimentaion(UnitAst.Imp);

  PrintDeclaration(UnitAst.Dec);
  WriteLn(FFile, 'END '+StartLabel);
end;

procedure TPrinterAST.PrintRegAssignTypeCast(const Reg:integer;
  const FactorAST: TFactorAST);
begin
  Assert(FactorAST<> nil, '72');
end;

procedure TPrinterAST.PrintVarAssignTypeCast(
  const VarAssignAST: TVarAssignAST);
var
  SignatureAST:TSignatureAST;
  Left, Righte:TVarDeclAST;
begin
  Assert(VarAssignAST<> nil, '73');
  SignatureAST:= VarAssignAST.Expression.Factor1.Signature;
  Left:=VarAssignAST.Signature.IndentDeclRef as TVarDeclAST;
  SignatureAST:=SignatureAST.Designator.ExpressionList.Items[0].Factor1.Signature;
  Righte:=SignatureAST.IndentDeclRef as TVarDeclAST;
  case TypeFamily(Left._Type) of
  tfOrdinal: PrintVarAssignCast_Ordinal(Left, Righte);
  tfReal:    PrintVarAssignCast_Real(Left, Righte);
  tfBoolean: PrintVarAssignCast_Boolean(Left, Righte);
  tfPointer: PrintVarAssignCast_Pointer(Left, Righte);
  tfString:  PrintVarAssignCast_String(Left, Righte);
  tfStructure: PrintVarAssignCast_Structure(Left, Righte);
  end;
end;

procedure TPrinterAST.PrintVarAssignCast_Ordinal(const Left,
  Righte: TVarDeclAST);
begin
  Assert(Righte<> nil, '74');
  case TypeFamily(Righte._Type) of
  tfOrdinal: PrintVarAssign_OrdinalOrdinal(Left, Righte);
  tfReal:    ErrorTypeCast;
  tfBoolean: ErrorTypeCast;
  tfPointer: PrintVarAssign_OrdinalPointer(Left, Righte);
  tfString:  PrintVarAssign_OrdinalString(Left, Righte);
  tfStructure: ErrorTypeCast;
  end;
end;

procedure TPrinterAST.PrintVarAssign_OrdinalOrdinal(const Left,
  Righte: TVarDeclAST);
begin
  Assert(Left<> nil, '75');
  Assert(Righte<> nil, '76');
  PrintVarAssign_ByteCopy(Left, Righte)
end;

procedure TPrinterAST.PrintVarAssign_OrdinalPointer(const Left,
  Righte: TVarDeclAST);
begin
  Assert(Left<> nil, '77');
  Assert(Righte<> nil, '78');
  if GetTypeSize(Righte._Type)<>PointerSize then ErrorTypeCast;
  PrintVarAssign_ByteCopy(Left, Righte)
end;

procedure TPrinterAST.PrintVarAssign_OrdinalString(const Left,
  Righte: TVarDeclAST);
begin
  Assert(Left<> nil, '79');
  Assert(Righte<> nil, '80');
  if  istype(Righte._Type, TS('char')) then
        PrintVarAssign_ByteCopy(Left, Righte)
     else
       ErrorTypeCast;
end;

procedure TPrinterAST.PrintVarAssign_ByteCopy(const Left,
  Righte: TVarDeclAST);
var Arg1, Arg2, Arg3:String;
begin
  Assert(Left<> nil, '81');
  Assert(Righte<> nil, '82');
  Case GetTypeSize(Righte._Type) of
  1: Arg1:='AL';
  2: Arg1:='AX';
  4: Arg1:='EAX';
  end;

  Case GetTypeSize(Left._Type) of
  1: Arg2:='AL';
  2: Arg2:='AX';
  4: Arg2:='EAX';
  end;

  Case GetTypeSize(Left._Type) of
  1: Arg3:='Byte PTR';
  2: Arg3:='Word PTR';
  4: Arg3:='DWord PTR';
  end;

  if (ARG1<>ARG2) then   WriteLn(FFile, #9#9'XOR EAX, EAX');
  PrintCmdAssign2(1, ARG1, Righte.Indent, ToReg);
//  PrintCmdAssign('MOV', ARG1, Righte.Indent, ToReg);

  PrintCmdAssign2(1, ARG1, Left.Indent, ToIndent);
//  PrintCmdAssign('MOV', ARG2, Left.Indent,  ToIndent);

end;

procedure TPrinterAST.PrintRegAssignFunc(const Reg:Integer; const FactorAST:TFactorAST);
var
  FuncCallAST:TFuncCallAST;
  Designator:TDesignatorAST;
  Expression:TExpressionAST;
  IndentDeclRef:TNodeAST;
  NImmConst:TNodeAST;
  Arg1,Arg2:String;
  i:Integer;
begin
  Assert(FactorAST<> nil, '83');
  FuncCallAST:=TFuncCallAST.Create;
  FuncCallAST.RefFunc:=FactorAST.Signature.IndentDeclRef as TFuncDeclAST;
  FuncCallAST.Parametrs:=TCallParametrsAST.Create;

    Designator:=FactorAST.Signature.Designator;
  if Designator.Kind =dkDeSignatureFunc then
    begin
    if Length(Designator.ExpressionList.Items)<>Length(FuncCallAST.RefFunc.FormalParametrs.List) then
       Self.Compiler.Error.Add(10143,'��������� ����� �������, �� ��������� � ���������� ������');
    for i:=0 to Length(FuncCallAST.RefFunc.FormalParametrs.List)-1 do
      begin
        if i<Length(Designator.ExpressionList.Items) then
             Expression:=Designator.ExpressionList.Items[i]
           else
             Expression:=nil;
       if (Expression<>nil) and (Expression.kind=ekSimple) then
           begin
           if  (Expression.Factor1.Kind =fkSignature) then
               begin
               if (Expression.Factor1.Signature.Designator.Kind=dkSignature) then
                  begin
                  IndentDeclRef:=Expression.Factor1.Signature.IndentDeclRef;
                  FuncCallAST.Parametrs.Add(IndentDeclRef);
                  end else Self.Compiler.Error.Add(10117,'��������� ���������� ��� ����������');
               end else
               begin
               NImmConst:=Expression.Factor1.ImmConst;
               FuncCallAST.Parametrs.Add(NImmConst);
               end;
           end else
           begin
 //          FuncCall.Parametrs.Add(Nil);
           Self.Compiler.Error.Add(10117,'��������� ���������� ��� ����������');
           end;
      end;
    end;
  PrintFuncCall(FuncCallAST);
  if Reg<>0 then
    begin
    Arg1:=GetRegName(Reg, GetTypeSize(FuncCallAST.RefFunc.result));
    Arg2:=GetRegName(0, GetTypeSize(FuncCallAST.RefFunc.result));
    Write(FFile, #9#9'MOV '+Arg1);
    Write(FFile,', '+Arg2);
    WriteLn(FFile,'');
    end;

  FuncCallAST.Parametrs.Destroy;
  FuncCallAST.RefFunc:=nil;
  FuncCallAST.Destroy;
end;

procedure TPrinterAST.PrintRegAssignFuncFPU(const Reg:Integer; const FactorAST:TFactorAST);
var
  FuncDeclAST:TFuncDeclAST;
begin
  Assert(FactorAST<> nil, '84');
  PrintRegAssignFunc(rEAX, FactorAST);
  FuncDeclAST:=FactorAST.Signature.IndentDeclRef as TFuncDeclAST;
  if isType(FuncDeclAST.result, TS('Integer')) then
     begin
     WriteLn(FFile, #9#9'PUSH EAX');
     WriteLn(FFile, #9#9'FILD [ESP]');
     WriteLn(FFile, #9#9'POP  EAX');
     end else
     begin
     WriteLn(FFile, #9#9'FLD QWord PTR [EAX]');
     end;
end;

procedure TPrinterAST.PrintVarAssignSizeOf_Ordinal(
  const VarAssignAST: TVarAssignAST);
var
  NewVarAssignAST:TVarAssignAST;
  Size:Integer;
  Signature:TSignatureAST;
  TypeDeclAST:TTypeDeclAST;
  VarDeclAST:TVarDeclAST;
begin
  Assert(VarAssignAST<> nil, '85');
  Signature:=VarAssignAST.Expression.Factor1.Signature.Designator.ExpressionList.Items[0].Factor1.Signature;
  Size:=0;
  if (Signature.IndentDeclRef is TVarDeclAST)  then
      begin
      VarDeclAST:=Signature.IndentDeclRef as TVarDeclAST;
      Size:=GetTypeSize(VarDeclAST._Type);
      end;
  if (Signature.IndentDeclRef is TTypeDeclAST)  then
      begin
      TypeDeclAST:=Signature.IndentDeclRef as TTypeDeclAST;
      Size:=GetTypeSize(TypeDeclAST._Type);
      end;

  NewVarAssignAST:=TVarAssignAST.Create;
  NewVarAssignAST.Parent:=VarAssignAST.Parent;
  NewVarAssignAST.Signature:=VarAssignAST.Signature;
    NewVarAssignAST.Expression:= TExpressionAST.Create;
    NewVarAssignAST.Expression.Parent:=NewVarAssignAST;
    NewVarAssignAST.Expression.kind:=ekSimple;
      NewVarAssignAST.Expression.Factor1:=TFactorAST.Create;
      NewVarAssignAST.Expression.Factor1.Parent:=NewVarAssignAST.Expression;
      NewVarAssignAST.Expression.Factor1.Kind:=fkImmConst;
        NewVarAssignAST.Expression.Factor1.ImmConst:=TImmConstAST.Create;
        NewVarAssignAST.Expression.Factor1.ImmConst.Parent:=NewVarAssignAST.Expression.Factor1;
        NewVarAssignAST.Expression.Factor1.ImmConst.Kind:=immInteger;
          NewVarAssignAST.Expression.Factor1.ImmConst._Integer:=TIntegerAST.Create(TS(IntToStr(Size)));
          NewVarAssignAST.Expression.Factor1.ImmConst._Integer.Parent:=NewVarAssignAST.Expression.Factor1.ImmConst;

            PrintVarAssign(NewVarAssignAST);

          NewVarAssignAST.Expression.Factor1.ImmConst._Integer.Destroy;
        NewVarAssignAST.Expression.Factor1.ImmConst.Destroy;
      NewVarAssignAST.Expression.Factor1.Destroy;
    NewVarAssignAST.Expression.Destroy;
  NewVarAssignAST.Destroy;
end;

procedure TPrinterAST.PrintVarAssignFunc_Ordinal(
  const VarAssignAST: TVarAssignAST);
var
  FuncDeclAST:TFuncDeclAST;
  VarDeclAST:TVarDeclAST;

begin
  Assert(VarAssignAST<> nil, '86');
  FuncDeclAST:=VarAssignAST.Expression.Factor1.Signature.IndentDeclRef as TFuncDeclAST;
  if (SimvolsEQ(FuncDeclAST.Indent.Simvol, TS('SizeOf'))) then
     begin
       PrintVarAssignSizeOf_Ordinal(VarAssignAST);
     end else
     begin
     VarDeclAST:=VarAssignAST.Signature.IndentDeclRef as TVarDeclAST;
     if (GetTypeSize(FuncDeclAST.result)<GetTypeSize(VarDeclAST._Type)) then
        WriteLn(FFile, #9#9'XOR EAX, EAX');
     PrintRegAssignFunc(rEAX, VarAssignAST.Expression.Factor1);
     PrintVarAssignReg(VarAssignAST.Signature,rEAX);
     end;
end;


procedure TPrinterAST.PrintVarAssignFunc_Real(
  const VarAssignAST: TVarAssignAST);
var
  FuncDeclAST:TFuncDeclAST;
  VarDeclAST:TVarDeclAST;
begin
  Assert(VarAssignAST<> nil, '87');
  FuncDeclAST:=VarAssignAST.Expression.Factor1.Signature.IndentDeclRef as TFuncDeclAST;
  VarDeclAST:=VarAssignAST.Signature.IndentDeclRef as TVarDeclAST;
  PrintRegAssignFuncFPU(rEAX, VarAssignAST.Expression.Factor1);
  PrintVarAssignRegFPU(VarAssignAST.Signature);
end;

procedure TPrinterAST.PrintVarAssignFunc_String(
  const VarAssignAST: TVarAssignAST);
var
  FuncDeclAST:TFuncDeclAST;
  VarDeclAST:TVarDeclAST;

begin
  Assert(VarAssignAST<> nil, '88');
  FuncDeclAST:=VarAssignAST.Expression.Factor1.Signature.IndentDeclRef as TFuncDeclAST;
  VarDeclAST:=VarAssignAST.Signature.IndentDeclRef as TVarDeclAST;
  if (GetTypeSize(FuncDeclAST.result)<GetTypeSize(VarDeclAST._Type)) then
     WriteLn(FFile, #9#9'XOR EAX, EAX');
  PrintRegAssignFunc(rEAX, VarAssignAST.Expression.Factor1);
  PrintVarAssignReg(VarAssignAST.Signature,rEAX);
end;

procedure TPrinterAST.PrintVarAssignFunc_Structure(
  const VarAssignAST: TVarAssignAST);
var
  FactorAST:TFactorAST;
  FuncCallAST:TFuncCallAST;
  Designator:TDesignatorAST;
  ResultExpression, Expression:TExpressionAST;
  IndentDeclRef:TNodeAST;
  NImmConst:TNodeAST;
  Arg1,Arg2:String;                                  
  i:Integer;
begin
  Assert(VarAssignAST<> nil, '89');
  FactorAST:= VarAssignAST.Expression.Factor1;

  FuncCallAST:=TFuncCallAST.Create;
  FuncCallAST.RefFunc:=FactorAST.Signature.IndentDeclRef as TFuncDeclAST;
  FuncCallAST.Parametrs:=TCallParametrsAST.Create;

    Designator:=FactorAST.Signature.Designator;
    if Designator.Kind=dkSignature then
       begin
       Designator.Kind:= dkDeSignatureFunc;
       Designator.ExpressionList:=TExpressionListAST.Create;
       Designator.ExpressionList.Parent:=Designator;
       end;
    ResultExpression:=TExpressionAST.Create;
      ResultExpression.kind:=ekSimple;
      ResultExpression.Factor1:=TFactorAST.Create;
      ResultExpression.Factor1.Parent:=ResultExpression;
      ResultExpression.Factor1.Kind:=fkSignature;
      ResultExpression.Factor1.Signature:=VarAssignAST.Signature;
    ResultExpression.parent:=Designator.ExpressionList;
    Designator.ExpressionList.Add(ResultExpression);
    if Length(Designator.ExpressionList.Items)<>Length(FuncCallAST.RefFunc.FormalParametrs.List) then
       Self.Compiler.Error.Add(10143,'��������� ����� �������, �� ��������� � ���������� ������');
    for i:=0 to Length(FuncCallAST.RefFunc.FormalParametrs.List)-1 do
      begin
        if i<Length(Designator.ExpressionList.Items) then
             Expression:=Designator.ExpressionList.Items[i]
           else
             Expression:=nil;
       if (Expression<>nil) and (Expression.kind=ekSimple) then
           begin
           if  (Expression.Factor1.Kind =fkSignature) then
               begin
               if (Expression.Factor1.Signature.Designator.Kind=dkSignature) then
                  begin
                  IndentDeclRef:=Expression.Factor1.Signature.IndentDeclRef;
                  FuncCallAST.Parametrs.Add(IndentDeclRef);
                  end else Self.Compiler.Error.Add(10117,'��������� ���������� ��� ����������');
               end else
               begin
               NImmConst:=Expression.Factor1.ImmConst;
               FuncCallAST.Parametrs.Add(NImmConst);
               end;
           end else
           begin
 //          FuncCall.Parametrs.Add(Nil);
           Self.Compiler.Error.Add(10117,'��������� ���������� ��� ����������');
           end;
      end;

  PrintFuncCall(FuncCallAST);
  FuncCallAST.Parametrs.Destroy;
  FuncCallAST.RefFunc:=nil;
  FuncCallAST.Destroy;
end;


procedure TPrinterAST.PrintVarAssignFunc(
  const VarAssignAST: TVarAssignAST);
var
  TypeFamily:TTypeFamily;
begin
  Assert(VarAssignAST<> nil, '90');
  TypeFamily:=SignatureTypeFamily(VarAssignAST.Signature);
  case TypeFamily of
  tfUndefine:  Compiler.Error.Add(194, '');  // ����������� ���
  tfOrdinal:   PrintVarAssignFunc_Ordinal(VarAssignAST);
  tfReal:      PrintVarAssignFunc_Real(VarAssignAST);
  tfBoolean:   PrintVarAssignFunc_Ordinal(VarAssignAST);
  tfPointer:   PrintVarAssignFunc_Ordinal(VarAssignAST);
  tfString:    PrintVarAssignFunc_String(VarAssignAST);
  tfStructure: PrintVarAssignFunc_Structure(VarAssignAST);
  end;

end;

procedure TPrinterAST.PrintRegAssignRefToVar(const Reg:Integer; const FactorAST:TFactorAST);
var
  IndentDecl:TVarDeclAST;
  RegName:String;
  RegName2:String;
  Ofs:Integer;
  Signature2:TSignatureAST;
begin
  Assert(FactorAST<> nil, '91');
  IndentDecl:=FactorAST.Signature.IndentDeclRef as TVarDeclAST;
  case FactorAST.Signature.Designator.Kind of
  dkSignature:
    begin
      RegName:=GetRegName(Reg, PointerSize);
      PrintCmdAssign2(0, RegName, IndentDecl.Indent, ToReg);
//      PrintCmdAssign('LEA', RegName, IndentDecl.Indent, ToReg);
    end;
  dkDesignature:
    begin
      RegName:=GetRegName(Reg, GetTypeSize(IndentDecl._Type));
      RegName2:=GetRegName(rEAX, PointerSize);
      PrintCmdAssign2(1, RegName, FactorAST.Signature.IndentDeclRef.Indent, ToReg);
//      PrintCmdAssign('MOV', RegName, FactorAST.Signature.IndentDeclRef.Indent, ToReg);
    end;
  dkDesignatureDot:
    begin
      PrintCmdAssign2(0, 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);
//      PrintCmdAssign('LEA', 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);
      Ofs:=0;
      Signature2:=FactorAST.Signature;
      While  Signature2.Designator.Kind= dkDesignatureDot do
        begin
          Signature2:=Signature2.Designator.Signature;
          Ofs:=Ofs+GetFildOfs(Signature2);
        end;
      RegName:=GetRegName(Reg, PointerSize);
      WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
      WriteLn(FFile, #9#9'LEA '+RegName+', [ESI+EDI]');
    end;
  end; // case
end;

procedure TPrinterAST.PrintRegAssignVar(const Reg:Integer; const FactorAST:TFactorAST);
var
  IndentDecl:TVarDeclAST;
  RegName:String;
  RegName2:String;
  RegName3:String;
  Ofs:Integer;
  Signature2:TSignatureAST;
begin
  Assert(FactorAST<> nil, '92');
  IndentDecl:=FactorAST.Signature.IndentDeclRef as TVarDeclAST;
  case FactorAST.Signature.Designator.Kind of
  dkSignature:
    begin
      RegName:=GetRegName(Reg, GetTypeSize(IndentDecl._Type));
      if GetTypeSize(IndentDecl._Type)<>4 then
         begin
         RegName2:=GetRegName(Reg, Pointersize);
         WriteLn(FFile,#9#9'XOR '+RegName2+', '+RegName2);
         end;
      if IsType(IndentDecl._Type, ts('String')) then
         begin
         PrintCmdAssign2(-2, RegName, IndentDecl.Indent, ToReg);  //[!] ����
//         PrintCmdAssign('MOV', RegName, IndentDecl.Indent, ToReg);  //[!] ����
//         WriteLn(FFile,#9#9'MOV '+RegName+', DWord PTR ['+RegName+']');
         end  else
          PrintCmdAssign2(1, RegName, IndentDecl.Indent, ToReg);
          //PrintCmdAssign('MOV', RegName, IndentDecl.Indent, ToReg);
    end;
  dkDesignature:
    begin
      // [!] �������� ��������
      RegName:=GetRegName(Reg, GetTypeSize(UnAlias(IndentDecl._Type)._Pointer));
      RegName3:=GetRegName(Reg, PointerSize);
      RegName2:=GetRegName(rESI, PointerSize);
      PrintCmdAssign2(1, RegName2, FactorAST.Signature.IndentDeclRef.Indent, ToReg);
//      PrintCmdAssign('MOV', RegName2, FactorAST.Signature.IndentDeclRef.Indent, ToReg);
      if  FactorAST.Signature.Designator.SubDesignator.Kind=dkSignature then
         begin
         case GetTypeSize(UnAlias(IndentDecl._Type)._Pointer) of
         1: begin
            WriteLn(FFile,#9#9'MOV '+RegName+', Byte PTR ['+RegName2+']');
            WriteLn(FFile,#9#9'AND '+RegName3+', 0FFh');
            end;
         2: begin
            WriteLn(FFile,#9#9'MOV '+RegName+', Word PTR ['+RegName2+']');
            WriteLn(FFile,#9#9'AND '+RegName3+', 0FFFFh');
            end;
         4: WriteLn(FFile,#9#9'MOV '+RegName+', DWord PTR ['+RegName2+']');
         end; // case
         end else
         if  FactorAST.Signature.Designator.SubDesignator.Kind=dkDeSignatureArray then
            begin
            PrintFactor(rEDI,  FactorAST.Signature.Designator.SubDesignator.Expression.Factor1);
            RegName:=GetRegName(Reg,  GetTypeSize(UnAlias(UnAlias(IndentDecl._Type)._Pointer)._Array._Type));
            RegName2:=GetRegName(Reg,  PointerSize);
            case GetTypeSize(UnAlias(UnAlias(IndentDecl._Type)._Pointer)._Array._Type) of
            1: begin
               WriteLn(FFile,#9#9'MOV '+RegName+', Byte PTR [ESI+1*EDI]');
               WriteLn(FFile,#9#9'AND '+RegName2+', 0FFh');
               end;
            2: begin
               WriteLn(FFile,#9#9'MOV '+RegName+', Word PTR [ESI+2*EDI]');
               WriteLn(FFile,#9#9'AND '+RegName2+', 0FFFFh');
               end;
            4: WriteLn(FFile,#9#9'MOV '+RegName+', DWord PTR [ESI+4*EDI]');
            end; // case
            end else
            if  FactorAST.Signature.Designator.SubDesignator.Kind=dkDesignatureDot then
                begin
                Ofs:=0;
                Signature2:=FactorAST.Signature.Designator.SubDesignator.Expression.Factor1.Signature;
                While  Signature2.Designator.Kind= dkDesignatureDot do
                  begin
                  Signature2:=Signature2.Designator.Signature;
                  Ofs:=Ofs+GetFildOfs(Signature2);
                  end;
                RegName:=GetRegName(rEAX, GetTypeSize((Signature2.IndentDeclRef as TFieldDeclAST)._Type));
                WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
                WriteLn(FFile, #9#9'MOV '+RegName+', [ESI+EDI]');
                end;
    end;
  dkDesignatureDot:
    begin
      PrintCmdAssign2(0, 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);
//      PrintCmdAssign('LEA', 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);
      Ofs:=0;
      Signature2:=FactorAST.Signature;
      While  Signature2.Designator.Kind= dkDesignatureDot do
        begin
          Signature2:=Signature2.Designator.Signature;
          Ofs:=Ofs+GetFildOfs(Signature2);
        end;
      RegName:=GetRegName(rEAX, GetTypeSize((Signature2.IndentDeclRef as TFieldDeclAST)._Type));
      WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
      WriteLn(FFile, #9#9'MOV '+RegName+', [ESI+EDI]');
    end;
  dkDeSignatureArray:
   begin
      if IsType(IndentDecl._Type, ts('String')) then
        begin
        PrintCmdAssign2(0, 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);
//        PrintCmdAssign('LEA', 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);
//        WriteLn(FFile, #9#9'MOV ESI, [ESI]');

        end else
        PrintCmdAssign2(0, 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);
//        PrintCmdAssign('LEA', 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);

      Signature2:=FactorAST.Signature;
      PrintFactor(rEDI, Signature2.Designator.Expression.Factor1);
      RegName:=GetRegName(rEAX, 2);
      WriteLn(FFile, #9#9'MOV '+RegName+', [ESI+2*EDI]');
   end;
  end; // case
end;

procedure TPrinterAST.PrintRegAssignVarFPU(const FactorAST:TFactorAST);
var
  IndentDecl:TVarDeclAST;
  Signature2:TSignatureAST;
  Ofs:Integer;
  _Type:TTypeAST;
  CmdName:String;
  RegName:String;
  RegName2:String;
begin
  Assert(FactorAST<> nil, '93');
  IndentDecl:=FactorAST.Signature.IndentDeclRef as TVarDeclAST;
  case FactorAST.Signature.Designator.Kind of
  dkSignature:
    begin
      if isTypeFast(IndentDecl._Type, TS('Real')) then
           PrintCmdAssignFPU('FLD', IndentDecl.Indent, ToReg)
         else
           PrintCmdAssignFPU('FILD', IndentDecl.Indent, ToReg);
    end;
  dkDesignature:
    begin
      RegName:=GetRegNameFPU(GetTypeSize(IndentDecl._Type));
      RegName2:=GetRegName(rEAX, PointerSize);
      PrintCmdAssign2(1, RegName2, FactorAST.Signature.IndentDeclRef.Indent, ToReg);
//      PrintCmdAssign('MOV', RegName2, FactorAST.Signature.IndentDeclRef.Indent, ToReg);
      if isTypeFast(IndentDecl._Type, TS('Real')) then
           CmdName:=#9#9'FLD '
         else
           CmdName:=#9#9'FILD ';
      case GetTypeSize(IndentDecl._Type) of
      1: WriteLn(FFile,CmdName+RegName2+', Byte PTR ['+RegName2+']');
      2: WriteLn(FFile,CmdName+RegName2+', Word PTR ['+RegName2+']');
      4: WriteLn(FFile,CmdName+RegName2+', DWord PTR ['+RegName2+']');
      8: WriteLn(FFile,CmdName+RegName2+', DWord PTR ['+RegName2+']');
      end;
    end;
  dkDesignatureDot:
    begin
      PrintCmdAssign2(0, 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);
//      PrintCmdAssign('LEA', 'ESI', FactorAST.Signature.IndentDeclRef.Indent, ToReg);
      Ofs:=0;
      Signature2:=FactorAST.Signature;
      While  Signature2.Designator.Kind= dkDesignatureDot do
        begin
          Signature2:=Signature2.Designator.Signature;
          Ofs:=Ofs+GetFildOfs(Signature2);
        end;
      WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
      _Type:=GetFildType(Signature2);
      if isTypeFast(_Type, TS('Real')) then
           WriteLn(FFile, #9#9'FLD QWord PTR [ESI+EDI]')
         else
           WriteLn(FFile, #9#9'FILD DWord PTR [ESI+EDI]');
    end;
  end; // case
end;

procedure TPrinterAST.PrintVarAssignVar_Real(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '94');
  PrintRegAssignVarFPU(VarAssignAST.Expression.Factor1);
  PrintVarAssignRegFPU(VarAssignAST.Signature);
end;


procedure TPrinterAST.PrintVarAssignVar_Ordinal(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '95');
  PrintRegAssignVar(rEAX, VarAssignAST.Expression.Factor1);
  PrintVarAssignReg(VarAssignAST.Signature, rEAX);
end;

procedure TPrinterAST.PrintVarAssignVar_String(
  const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '96');
  if  (FactorTypeFamily(VarAssignAST.Expression.Factor1)=tfPointer) then
      PrintVarAssignVar_Ordinal(VarAssignAST);
  if  (FactorTypeFamily(VarAssignAST.Expression.Factor1)=tfString) then
      if isType(FactorType(VarAssignAST.Expression.Factor1), TS('Char')) then
         PrintVarAssignVar_Ordinal(VarAssignAST)
         else
         if isType(SignatureType(VarAssignAST.Signature), TS('Char')) then
            PrintVarAssignVar_Ordinal(VarAssignAST)
            else
            PrintVarAssignString(VarAssignAST);
end;

procedure TPrinterAST.PrintVarAssignVar_Structure(
  const VarAssignAST: TVarAssignAST);
var
  FactorLeft,FactorRighte: TFactorAST;
  TypeAST:TTypeAST;
  i, Size:Integer;
begin
  Assert(VarAssignAST<> nil, '97');

  FactorRighte:=VarAssignAST.Expression.Factor1;
  FactorLeft:=TFactorAST.Create;
  FactorLeft.Kind:=fkSignature;
  FactorLeft.Signature:=VarAssignAST.Signature;
  PrintRegAssignRefToVar(rEAX, FactorRighte);
  PrintRegAssignRefToVar(rEBX, FactorLeft);
  FactorLeft.Destroy;
  TypeAST:=FSemanticCheking.FactorType(FactorLeft);
  Size:= GetTypeSize(TypeAST);
  for i:=0 to Size div 4 -1 do
    begin
       WriteLn(FFile,Format(#9#9'MOV EDX, DWord PTR [EAX+%d]', [4*i]));
       WriteLn(FFile,Format(#9#9'MOV DWord PTR [EBX+%d], EDX', [4*i]));
    end;

  for i:=0 to Size mod 4 -1 do
    begin
       WriteLn(FFile,Format(#9#9'MOV DL, DWord PTR [EAX+%d]', [(Size div 4) *4+i]));
       WriteLn(FFile,Format(#9#9'MOV DWord PTR [EBX+%d], DL', [(Size div 4) *4+i]));
    end;
end;

procedure TPrinterAST.PrintVarAssignVar(const VarAssignAST: TVarAssignAST);
var
  TypeFamily:TTypeFamily;
begin
  Assert(VarAssignAST<> nil, '98');
  TypeFamily:=self.TypeFamily(FSemanticCheking.SignatureType(VarAssignAST.Signature));
  case TypeFamily of
  tfUndefine:  Compiler.Error.Add(194, '');  // ����������� ���
  tfOrdinal:   PrintVarAssignVar_Ordinal(VarAssignAST);
  tfReal:      PrintVarAssignVar_Real(VarAssignAST);
  tfBoolean:   PrintVarAssignVar_Ordinal(VarAssignAST);
  tfPointer:   PrintVarAssignVar_Ordinal(VarAssignAST);
  tfString:    PrintVarAssignVar_String(VarAssignAST);
  tfStructure: PrintVarAssignVar_Structure(VarAssignAST);
  end;
end;

procedure TPrinterAST.PrintRegAssignString(leval:Integer; const Reg:Integer; const ImmConst: TImmConstAST);
var
  RegName:String;
  IndentDecl:TConstDeclAST;
begin
  Assert(ImmConst<> nil, '99');
  IndentDecl:=GentIndentForImmString(ImmConst._String);
  AddConstImm(ImmConst, IndentDecl);

  RegName:=GetRegName(Reg, GetTypeSize(IndentDecl._Type));
  PrintCmdAssign2(leval, RegName, IndentDecl.Indent, ToReg);    // -2-2 ��� ������
//  PrintCmdAssign('MOV', RegName, IndentDecl.Indent, ToReg);
end;


procedure TPrinterAST.PrintVarAssignString(const VarAssignAST: TVarAssignAST);
var
  RegName:String;
  IndentDecl:TVarDeclAST;
  Ofs:Integer;
  Signature:TSignatureAST;
  Signature2:TSignatureAST;
  IndentDecl1:TVarDeclAST;
  IndentDecl2:TConstDeclAST;
begin
  Assert(VarAssignAST<> nil, '100');
  Signature:=VarAssignAST.Signature;
  IndentDecl1:=Signature.IndentDeclRef as TVarDeclAST;

       case Signature.Designator.Kind of
       dkSignature:
          begin
            if VarAssignAST.Expression.Factor1.Kind=fkImmConst then
               begin
               IndentDecl2:=GentIndentForImmString(VarAssignAST.Expression.Factor1.ImmConst._String);
               AddConstImm(VarAssignAST, IndentDecl2);
               PrintCmdAssign2(0, 'EAX', IndentDecl2.Indent, ToReg);
//               PrintCmdAssign('LEA', 'EAX', IndentDecl2.Indent, ToReg);
               PrintPush('EAX');
               end  else
               begin
               IndentDecl:= VarAssignAST.Expression.Factor1.Signature.IndentDeclRef as TVarDeclAST;

               PrintCmdAssign2(0, 'EAX', IndentDecl.Indent, ToReg);
//               PrintCmdAssign('LEA', 'EAX', IndentDecl.Indent, ToReg);
               PrintPush('EAX');
               end;

            PrintCmdAssign2(0, 'EAX', IndentDecl1.Indent, ToReg);
//            PrintCmdAssign('LEA', 'EAX', IndentDecl1.Indent, ToReg);
            PrintPush('EAX');

            WriteLn(FFile,#9#9'CALL AssignString');
            Dec(SubStack, 2*PointerSize);
          end;
       dkDesignature: ;
       dkDesignatureDot:
          begin
          PrintCmdAssign2(0, 'ESI', Signature.IndentDeclRef.Indent, ToReg);
//          PrintCmdAssign('LEA', 'ESI', Signature.IndentDeclRef.Indent, ToReg);
          Ofs:=0;
          Signature2:=Signature;
          While  Signature2.Designator.Kind= dkDesignatureDot do
             begin
             Signature2:=Signature2.Designator.Signature;
             Ofs:=Ofs+GetFildOfs(Signature2);
             end;

          IndentDecl2:=GentIndentForImmString(VarAssignAST.Expression.Factor1.ImmConst._String);
          AddConstImm(VarAssignAST, IndentDecl2);

          PrintCmdAssign2(1, 'EAX', IndentDecl2.Indent, ToReg);
//          PrintCmdAssign('MOV', 'EAX', IndentDecl2.Indent, ToReg);
          PrintPush('EAX');

          WriteLn(FFile,#9#9'LEA EAX, [ESI+EDI]');
          PrintPush('EAX');

          WriteLn(FFile,#9#9'CALL AssignString');
          Dec(SubStack, 2*PointerSize);
          end;
       end; // case

end;

procedure TPrinterAST.PrintRegAssignConst(const Reg:Integer; const FactorAST: TFactorAST);
var
  RegName:String;
  IndentDecl:TConstDeclAST;
begin
  Assert(FactorAST<> nil, '101');
  IndentDecl:=FactorAST.Signature.IndentDeclRef as TConstDeclAST;
  RegName:=GetRegName(Reg, GetTypeSize(IndentDecl._Type));
  PrintCmdAssign2(1, RegName, IndentDecl.Indent, ToReg);
//  PrintCmdAssign('MOV', RegName, IndentDecl.Indent, ToReg);
end;

procedure TPrinterAST.PrintRegAssignConstFPU(const FactorAST: TFactorAST);
var
  IndentDecl:TConstDeclAST;
begin
  Assert(FactorAST<> nil, '102');
  IndentDecl:=FactorAST.Signature.IndentDeclRef as TConstDeclAST;
  if isTypeFast(IndentDecl._Type, TS('Integer')) then
       PrintCmdAssignFPU('FILD',IndentDecl.Indent,toReg)
     else if isTypeFast(IndentDecl._Type, TS('Real')) then
       PrintCmdAssignFPU('FLD',IndentDecl.Indent,toReg);
end;

procedure TPrinterAST.PrintVarAssignConst(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '103');
  PrintRegAssignConst(rEAX, VarAssignAST.Expression.Factor1);
  PrintVarAssignReg(VarAssignAST.Signature, rEAX)
end;

procedure TPrinterAST.PrintRegAssignNumber_Real(const ImmConst: TImmConstAST);
var
  ConstDeclAST:TConstDeclAST;
begin
  Assert(ImmConst<> nil, '104');
  if (ImmConst.Kind=immInteger) then
       ConstDeclAST:=GentIndentForImmInteger(ImmConst._Integer)
     else
       ConstDeclAST:=GentIndentForImmReal(ImmConst._Real);
  AddConstImm(ImmConst, ConstDeclAST);

  if (ImmConst.Kind=immInteger) then
       PrintCmdAssignFPU('FILD',ConstDeclAST.Indent,toReg)
     else
       PrintCmdAssignFPU('FLD',ConstDeclAST.Indent,toReg)
end;

procedure TPrinterAST.PrintRegAssignNumber_Ordinal(const Reg:Integer; const ImmConst: TImmConstAST);
var
  Arg1:String;
begin
  Assert(ImmConst<> nil, '105');
  if (ImmConst.Kind=immChar) then
      Arg1:=GetRegName(Reg, CharSize)
      else
      Arg1:=GetRegName(Reg, PointerSize);
  Write(FFile, #9#9'MOV '+Arg1);
  Write(FFile,', ');
  if (ImmConst.Kind=immNil) or  (ImmConst.Kind=immChar) then
     PrintConst(ImmConst._Const, ImmConst._Type)
     else
     PrintConst(ImmConst._Integer, nil);
  WriteLn(FFile,'');
end;

procedure TPrinterAST.PrintVarAssignNumber(const VarAssignAST: TVarAssignAST);
var
  TypeFamily:TTypeFamily;
begin
  Assert(VarAssignAST<> nil, '106');
  TypeFamily:=SignatureTypeFamily(VarAssignAST.Signature);
  case TypeFamily of
  tfUndefine:  Compiler.Error.Add(194, '');  // ����������� ���
  tfOrdinal:   PrintVarAssignNumber_Ordinal(VarAssignAST);
  tfReal:      PrintVarAssignNumber_Real(VarAssignAST);
  tfBoolean:   Compiler.Error.Add(194, '');  // ����������� ���
  tfPointer:   Compiler.Error.Add(194, '');  // ����������� ���
  tfString:    PrintVarAssignNumber_String(VarAssignAST); // ����������� nil ��� �����
  tfStructure: Compiler.Error.Add(194, '');  // ����������� ���
  end;
end;

procedure TPrinterAST.PrintVarAssignNumber_Real(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '107');
  PrintRegAssignNumber_Real(VarAssignAST.Expression.Factor1.ImmConst);
  PrintVarAssignRegFPU(VarAssignAST.Signature)
end;

procedure TPrinterAST.PrintVarAssignNumber_Ordinal(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '108');
  PrintRegAssignNumber_Ordinal(rEAX, VarAssignAST.Expression.Factor1.ImmConst);
  PrintVarAssignReg(VarAssignAST.Signature, rEAX)
end;

procedure TPrinterAST.PrintVarAssignNumber_String(
  const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '109');
  if VarAssignAST.Expression.Factor1.ImmConst.Kind=immNil then
     PrintVarAssignNumber_Ordinal(VarAssignAST);
end;

function TPrinterAST.IsIndentDeclString(IndentDecl:TIndentDeclAST):boolean;
var
  VarDecl:TVarDeclAST;
begin
  Assert(IndentDecl<> nil, '110');
  result:=false;
  if (IndentDecl is TVarDeclAST) then
     begin
      VarDecl:=IndentDecl as TVarDeclAST;
      result:=isType(VarDecl._Type, TS('String'));
     end;
end;

procedure TPrinterAST.PrintRegAssignAddr(const Reg:Integer; const ImmConst: TImmConstAST);
var
  Signature, Signature2:TSignatureAST;
  RegName:String;
  IndentDecl:TIndentDeclAST;
  VarDeclAST:TVarDeclAST;
  Ofs:Integer;
begin
  Assert(ImmConst<> nil, '111');
       Signature:=ImmConst.Addr.Signature;
       case Signature.Designator.Kind of
       dkSignature,
       dkDesignature:
         begin
          IndentDecl:=Signature.IndentDeclRef;
          RegName:=GetRegName(Reg, PointerSize);
          if IsIndentDeclString(IndentDecl) then
            PrintCmdAssign2(-2, RegName, IndentDecl.Indent, ToReg)
            else
            PrintCmdAssign2(0, RegName, IndentDecl.Indent, ToReg);
//          PrintCmdAssign('LEA', RegName, IndentDecl.Indent, ToReg);
//             WriteLn(FFile,#9#9'MOV '+RegName+', DWord PTR ['+RegName+']');
         end;
       dkDesignatureDot:
          begin
          PrintCmdAssign2(0, 'ESI', Signature.IndentDeclRef.Indent, ToReg);
//          PrintCmdAssign('LEA', 'ESI', Signature.IndentDeclRef.Indent, ToReg);
          Ofs:=0;
          Signature2:=Signature;
          While  Signature2.Designator.Kind= dkDesignatureDot do
             begin
             Signature2:=Signature2.Designator.Signature;
             Ofs:=Ofs+GetFildOfs(Signature2);
             end;
          WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
          RegName:=GetRegName(reg, 4);
          WriteLn(FFile, #9#9'LEA '+RegName+', [ESI+EDI]');
          IndentDecl:=Signature2.IndentDeclRef;
          if IsIndentDeclString(IndentDecl) then
             WriteLn(FFile,#9#9'MOV '+RegName+', DWord PTR ['+RegName+']');
          end;
       dkDeSignatureArray:
          begin
          IndentDecl:=Signature.IndentDeclRef;
          if IsIndentDeclString(IndentDecl) then
            begin
            VarDeclAST:= IndentDecl as TVarDeclAST;
            case getVarDeclKind(VarDeclAST) of
            vdkUndefine,
            vdkGlobal: PrintCmdAssign2(-2, 'ESI', IndentDecl.Indent, ToReg);
            vdkParam:  PrintCmdAssign2(0, 'ESI', IndentDecl.Indent, ToReg);
            vdkLocal:  PrintCmdAssign2(-2, 'ESI', IndentDecl.Indent, ToReg);
            end; // case
            end else
            PrintCmdAssign2(0, 'ESI', IndentDecl.Indent, ToReg);
//          PrintCmdAssign('LEA', 'ESI', Signature.IndentDeclRef.Indent, ToReg);
          PrintExpression(rEDI, Signature.Designator.Expression);
          RegName:=GetRegName(reg, 4);
          WriteLn(FFile, #9#9'LEA '+RegName+', [ESI+2*EDI]');

          end;
       end; // case
end;

procedure TPrinterAST.PrintVarAssignChar(const VarAssignAST:TVarAssignAST);
var
  TypeFamily:TTypeFamily;
begin
  Assert(VarAssignAST<> nil, '112');
  TypeFamily:=SignatureTypeFamily(VarAssignAST.Signature);
  case TypeFamily of
  tfUndefine:  Compiler.Error.Add(194, '');  // ����������� ���
  tfOrdinal:   Compiler.Error.Add(194, '');  // ����������� ���
  tfReal:      Compiler.Error.Add(194, '');  // ����������� ���
  tfBoolean:   Compiler.Error.Add(194, '');  // ����������� ���
  tfPointer:   Compiler.Error.Add(194, '');  // ����������� ���
  tfString:    PrintVarAssignChar_String(VarAssignAST); // ����������� char � char'�  ��� ������
  tfStructure: PrintVarAssignChar_String(VarAssignAST); // ����������� char � char'�  � ��������� ��� �������
  end;

end;

procedure TPrinterAST.PrintVarAssignChar_String(const VarAssignAST:TVarAssignAST);
var
  RegName, RegName2:String;
  IndentDecl:TVarDeclAST;
  ofs:Integer;
  Signature2:TSignatureAST;
begin
  Assert(VarAssignAST<> nil, '113');
  case VarAssignAST.Signature.Designator.Kind of
  dkSignature, dkDesignatureDot:
    begin
    PrintRegAssignNumber_Ordinal(rEAX, VarAssignAST.Expression.Factor1.ImmConst);
    IndentDecl:=VarAssignAST.Signature.IndentDeclRef as TVarDeclAST;
    RegName:=GetRegName(rEAX, CharSize);
    PrintCmdAssign2(1, RegName, IndentDecl.Indent, ToIndent);
//    PrintCmdAssign('MOV', RegName, IndentDecl.Indent, ToIndent);
    if IsIndentDeclString(IndentDecl) then
       WriteLn(FFile,#9#9'MOV '+RegName+', DWord PTR ['+RegName+']');
    end;
  dkDesignature:
    begin
    IndentDecl:=VarAssignAST.Signature.IndentDeclRef as TVarDeclAST;
      // [!] �������� ��������
      PrintRegAssignNumber_Ordinal(rEAX, VarAssignAST.Expression.Factor1.ImmConst);
      RegName:=GetRegName(rEAX, GetTypeSize(UnAlias(IndentDecl._Type)._Pointer));
      RegName2:=GetRegName(rESI, PointerSize);
      PrintCmdAssign2(1, RegName2, VarAssignAST.Signature.IndentDeclRef.Indent, ToReg);
//      PrintCmdAssign('MOV', RegName2, FactorAST.Signature.IndentDeclRef.Indent, ToReg);
      if  VarAssignAST.Signature.Designator.SubDesignator.Kind=dkSignature then
         begin
            WriteLn(FFile,#9#9'MOV Word PTR ['+RegName2+'], '+RegName);
         end else
         if  VarAssignAST.Signature.Designator.SubDesignator.Kind=dkDeSignatureArray then
            begin
            PrintFactor(rEDI,  VarAssignAST.Signature.Designator.SubDesignator.Expression.Factor1);
            RegName:=GetRegName(rEAX,  GetTypeSize(UnAlias(UnAlias(IndentDecl._Type)._Pointer)._Array._Type));
            case GetTypeSize(UnAlias(UnAlias(IndentDecl._Type)._Pointer)._Array._Type) of
            1: begin
               WriteLn(FFile,#9#9'MOV  Byte PTR [ESI+1*EDI],'+RegName);
               end;
            2: begin
               WriteLn(FFile,#9#9'MOV  Word PTR [ESI+2*EDI], '+RegName);
               end;
            4: WriteLn(FFile,#9#9'MOV  DWord PTR [ESI+4*EDI], '+RegName);
            end; // case
            end else
            if  VarAssignAST.Signature.Designator.SubDesignator.Kind=dkDesignatureDot then
                begin
                Ofs:=0;
                Signature2:=VarAssignAST.Signature.Designator.SubDesignator.Expression.Factor1.Signature;
                While  Signature2.Designator.Kind= dkDesignatureDot do
                  begin
                  Signature2:=Signature2.Designator.Signature;
                  Ofs:=Ofs+GetFildOfs(Signature2);
                  end;
                RegName:=GetRegName(rEAX, GetTypeSize((Signature2.IndentDeclRef as TFieldDeclAST)._Type));
                WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
                WriteLn(FFile, #9#9'MOV [ESI+EDI], '+RegName);
                end;
    end;
  dkDeSignatureArray:;
  dkDeSignatureFunc:;
  end;
end;

procedure TPrinterAST.PrintVarAssignAddr(const VarAssignAST:TVarAssignAST);
var
  RegName:String;
  IndentDecl:TVarDeclAST;
begin
  Assert(VarAssignAST<> nil, '114');
  PrintRegAssignAddr(rEAX, VarAssignAST.Expression.Factor1.ImmConst);
  IndentDecl:=VarAssignAST.Signature.IndentDeclRef as TVarDeclAST;
  RegName:=GetRegName(rEAX, PointerSize);
  PrintCmdAssign2(1, RegName, IndentDecl.Indent, ToIndent);
//  PrintCmdAssign('MOV', RegName, IndentDecl.Indent, ToIndent);
 // if IsIndentDeclString(IndentDecl) then
 //    WriteLn(FFile,#9#9'MOV '+RegName+', DWord PTR ['+RegName+']');
end;

procedure TPrinterAST.PrintVarAssignSimpleSignature(const VarAssignAST: TVarAssignAST);
var Signature:TSignatureAST;
begin
  Assert(VarAssignAST<> nil, '115');
  Signature:=VarAssignAST.Expression.Factor1.Signature;
  if (Signature.IndentDeclRef is TTypeDeclAST) then
      begin
      PrintVarAssignTypeCast(VarAssignAST);
      end;
  if (Signature.IndentDeclRef is TFuncDeclAST) then
      begin
      PrintVarAssignFunc(VarAssignAST);
      end;
  if (Signature.IndentDeclRef is TVarDeclAST) then
      begin
      PrintVarAssignVar(VarAssignAST);
      end else
  if (Signature.IndentDeclRef is TConstDeclAST) then
      begin
      PrintVarAssignConst(VarAssignAST);
      end;
end;

procedure TPrinterAST.PrintVarAssignSimpleImmConst(const VarAssignAST: TVarAssignAST);
var ImmConst:TImmConstAST;
begin
  Assert(VarAssignAST<> nil, '116');
  ImmConst:=VarAssignAST.Expression.Factor1.ImmConst;
  case ImmConst.Kind of
  immUndefine:
     begin
     Write(FFile, '[undefine ImmConst]');
     end;
  immChar:
     begin
     PrintVarAssignChar(VarAssignAST);
     end;
  immString:
     begin
     PrintVarAssignString(VarAssignAST);
     end;
  immNil, immInteger:
     begin
     PrintVarAssignNumber(VarAssignAST);
     end;
  immReal:
     begin
     PrintVarAssignNumber(VarAssignAST);
     end;
  immAddr:
     begin
     PrintVarAssignAddr(VarAssignAST);
     end;
  end; // case
end;

procedure TPrinterAST.PrintVarAssignSimple(const VarAssignAST: TVarAssignAST);
var
  Factor:TFactorAST;
begin
  Assert(VarAssignAST<> nil, '117');
  Factor:=VarAssignAST.Expression.Factor1;
  case Factor.Kind of
  fkSignature:  PrintVarAssignSimpleSignature(VarAssignAST);
  fkImmConst:   PrintVarAssignSimpleImmConst(VarAssignAST);
  end;
end;

procedure TPrinterAST.PrintVarAssignUnari_Ordinal(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '118');
  PrintExpression(rEAX, VarAssignAST.Expression);
  PrintVarAssignReg(VarAssignAST.Signature, rEAX);
end;

procedure TPrinterAST.PrintVarAssignUnari_Real(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '119');
  PrintExpressionFPU(rEAX, VarAssignAST.Expression);
  PrintVarAssignRegFPU(VarAssignAST.Signature);
end;

procedure TPrinterAST.PrintVarAssignUnari(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '120');
  case SignatureTypeFamily(VarAssignAST.Signature) of
  tfOrdinal: PrintVarAssignUnari_Ordinal(VarAssignAST);
  tfReal:    PrintVarAssignUnari_Real(VarAssignAST);
  tfBoolean: PrintVarAssignUnari_Ordinal(VarAssignAST);
  tfPointer: ErrorTypeCast;
  tfString:  ErrorTypeCast;
  tfStructure: ErrorTypeCast;
  end;

end;


procedure TPrinterAST.PrintVarAssignReg(Const Signature:TSignatureAST; const Reg:Integer);
var
  RegName:String;
  RegName2:String;
  IndentDecl:TVarDeclAST;
  Ofs:Integer;
  Signature2:TSignatureAST;
  _Type:TTYpeAST;
  tmpFactor:TFactorAST;
begin
  Assert(Signature<> nil, '121');
  IndentDecl:=Signature.IndentDeclRef as TVarDeclAST;
       case Signature.Designator.Kind of
       dkSignature:
          begin
            RegName:=GetRegName(rEAX, GetTypeSize(IndentDecl._Type));
            if IsIndentDeclString(IndentDecl) then
               begin
               PrintCmdAssign2(0, RegName, IndentDecl.Indent, ToIndent);
               end else
               PrintCmdAssign2(1, RegName, IndentDecl.Indent, ToIndent);
//            PrintCmdAssign('MOV', RegName, IndentDecl.Indent, ToIndent);
          end;
       dkDesignature:
          begin
            RegName2:=GetRegName(rEDX, PointerSize);
            PrintCmdAssign2(1, RegName2, IndentDecl.Indent, ToReg);   //[!]2
//            PrintCmdAssign('MOV', RegName2, IndentDecl.Indent, ToReg);   //[!]2
            _Type:= DesignatorType(IndentDecl._Type, Signature.Designator);
            RegName:=GetRegName(rEAX, GetTypeSize(_Type));
            WriteLn(FFile, #9#9'MOV ['+RegName2+'], '+RegName);

          end;
       dkDesignatureDot:
          begin
          PrintCmdAssign2(0, 'ESI', Signature.IndentDeclRef.Indent, ToReg);
//          PrintCmdAssign('LEA', 'ESI', Signature.IndentDeclRef.Indent, ToReg);
          Ofs:=0;
          Signature2:=Signature;
          While  Signature2.Designator.Kind= dkDesignatureDot do
             begin
             Signature2:=Signature2.Designator.Signature;
             Ofs:=Ofs+GetFildOfs(Signature2);
             end;
          RegName:=GetRegName(rEAX, GetTypeSize((Signature2.IndentDeclRef as TFieldDeclAST)._Type));
          WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
          WriteLn(FFile, #9#9'MOV [ESI+EDI], '+RegName);
          end;
       dkDeSignatureArray:
          begin
          if IsType(IndentDecl._Type, ts('String')) then
             begin
             PrintCmdAssign2(0, 'ESI', Signature.IndentDeclRef.Indent, ToReg);
//             PrintCmdAssign('LEA', 'ESI', Signature.IndentDeclRef.Indent, ToReg);
//             WriteLn(FFile, #9#9'MOV ESI, [ESI]');
             end else
             PrintCmdAssign2(0, 'ESI', Signature.IndentDeclRef.Indent, ToReg);
//             PrintCmdAssign('LEA', 'ESI', Signature.IndentDeclRef.Indent, ToReg);
          Signature2:=Signature;
          tmpFactor:=Signature.Designator.Expression.Factor1;
          PrintFactor(rEDI, tmpFactor);
//          RegName:=GetRegName(rEAX, GetTypeSize(unAlias(FSemanticCheking.SignatureType(Signature2))._Array._Type));
          RegName:=GetRegName(rEAX, 2);
          WriteLn(FFile, #9#9'MOV [ESI+2*EDI], '+RegName);
          end;
       end; // case
end;

procedure TPrinterAST.PrintVarAssignRegFPU(Const Signature:TSignatureAST);
var
  IndentDecl:TVarDeclAST;
  Ofs:Integer;
  Signature2:TSignatureAST;
begin
  Assert(Signature<> nil, '122');
  IndentDecl:=Signature.IndentDeclRef as TVarDeclAST;
       case Signature.Designator.Kind of
       dkSignature:
          begin
            PrintCmdAssignFPU('FSTP',IndentDecl.Indent,toIndent)
          end;
       dkDesignature: ;
       dkDesignatureDot:
          begin
          PrintCmdAssign2(0, 'ESI', Signature.IndentDeclRef.Indent, ToReg);
//          PrintCmdAssign('LEA', 'ESI', Signature.IndentDeclRef.Indent, ToReg);
          Ofs:=0;
          Signature2:=Signature;
          While  Signature2.Designator.Kind= dkDesignatureDot do
             begin
             Signature2:=Signature2.Designator.Signature;
             Ofs:=Ofs+GetFildOfs(Signature2);
             end;
          WriteLn(FFile, #9#9'MOV EDI, ', IntToStr(Ofs));
          WriteLn(FFile, #9#9'FSTP QWord PTR [ESI+EDI]');
          end;
       end; // case
end;

procedure TPrinterAST.PrintVarAssignBinary_Ordinal(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '123');
  PrintExpression(rEAX, VarAssignAST.Expression);
  PrintVarAssignReg(VarAssignAST.Signature, rEAX);
end;

procedure TPrinterAST.PrintVarAssignBinary_Real(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '124');
  PrintExpressionFPU(rEAX, VarAssignAST.Expression);
  PrintVarAssignRegFPU(VarAssignAST.Signature);
end;

procedure TPrinterAST.PrintVarAssignBinary_Boolean(const VarAssignAST: TVarAssignAST);
var
  TypeFamily1, TypeFamily2:TTypeFamily;
  Type1, Type2:TTypeAST;
begin
  Assert(VarAssignAST<> nil, '125');
  TypeFamily1:=FactorTypeFamily(VarAssignAST.Expression.Factor1);
  TypeFamily2:=FactorTypeFamily(VarAssignAST.Expression.Factor2);
  if (TypeFamily1=tfOrdinal) and (TypeFamily2=tfOrdinal) then
      PrintExpression(rEAX, VarAssignAST.Expression);
  if (TypeFamily1=tfOrdinal) and (TypeFamily2=tfReal) then
      PrintExpressionFPU(rEAX, VarAssignAST.Expression);
  if (TypeFamily1=tfReal) and (TypeFamily2=tfOrdinal) then
      PrintExpressionFPU(rEAX, VarAssignAST.Expression);
  if (TypeFamily1=tfBoolean) and (TypeFamily2=tfBoolean) then
      PrintExpression(rEAX, VarAssignAST.Expression);   //[!] �������� �� PrintExpressionBoolean
  if (TypeFamily1=tfString) and (TypeFamily2=tfPointer) then
      PrintExpression(rEAX, VarAssignAST.Expression);
  if (TypeFamily1=tfPointer) and (TypeFamily2=tfString) then
      PrintExpression(rEAX, VarAssignAST.Expression);
  if (TypeFamily1=tfPointer) and (TypeFamily2=tfPointer) then
      PrintExpression(rEAX, VarAssignAST.Expression);
  if (TypeFamily1=tfString)  and (TypeFamily2=tfString) then
      begin
      Type1:=FactorType(VarAssignAST.Expression.Factor1);
      Type2:=FactorType(VarAssignAST.Expression.Factor2);
      if IsType(Type1, TS('Char')) and IsType(Type2, TS('Char')) then
         PrintExpression(rEAX, VarAssignAST.Expression);
      if IsType(Type1, TS('String')) and IsType(Type2, TS('String')) then
         PrintExpression(rEAX, VarAssignAST.Expression);
      end;
  if (TypeFamily1=tfStructure)  and (TypeFamily2=tfString) then
      begin
      Type1:=FactorType(VarAssignAST.Expression.Factor1);
      Type2:=FactorType(VarAssignAST.Expression.Factor2);
      if IsType(Type1, TS('Char')) and IsType(Type2, TS('Char')) then
         PrintExpression(rEAX, VarAssignAST.Expression);
      if IsType(Type1, TS('String')) and IsType(Type2, TS('String')) then
         PrintExpression(rEAX, VarAssignAST.Expression);
      end;
  if (TypeFamily1=tfString)  and (TypeFamily2=tfStructure) then
      begin
      Type1:=FactorType(VarAssignAST.Expression.Factor1);
      Type2:=FactorType(VarAssignAST.Expression.Factor2);
      if IsType(Type1, TS('Char')) and IsType(Type2, TS('Char')) then
         PrintExpression(rEAX, VarAssignAST.Expression);
      if IsType(Type1, TS('String')) and IsType(Type2, TS('String')) then
         PrintExpression(rEAX, VarAssignAST.Expression);
      end;
  if (TypeFamily1=tfStructure)  and (TypeFamily2=tfStructure) then
      begin
      Type1:=FactorType(VarAssignAST.Expression.Factor1);
      Type2:=FactorType(VarAssignAST.Expression.Factor2);
      if IsType(Type1, TS('Char')) and IsType(Type2, TS('Char')) then
         PrintExpression(rEAX, VarAssignAST.Expression);
      if IsType(Type1, TS('String')) and IsType(Type2, TS('String')) then
         PrintExpression(rEAX, VarAssignAST.Expression);
      end;
  PrintVarAssignReg(VarAssignAST.Signature, rEAX);
end;

procedure TPrinterAST.PrintVarAssignBinary_String(const VarAssignAST: TVarAssignAST);
var
  TypeFamily1, TypeFamily2:TTypeFamily;
  Type1, Type2:TTypeAST;
  NewOperatorAST:TNewOperatorAST;
  Factor:TFactorAST;
begin
  Assert(VarAssignAST<> nil, '126');
  TypeFamily1:=FactorTypeFamily(VarAssignAST.Expression.Factor1);
  TypeFamily2:=FactorTypeFamily(VarAssignAST.Expression.Factor2);
  if (TypeFamily1=tfString) and (TypeFamily2=tfString) then
      begin
      Type1:=FactorType(VarAssignAST.Expression.Factor1);
      Type2:=FactorType(VarAssignAST.Expression.Factor2);

      if IsType(Type1, TS('String')) and IsType(Type2, TS('String')) then
         PrintExpression(rEAX, VarAssignAST.Expression);
      if IsType(Type1, TS('String')) and IsType(Type2, TS('Char')) then
         begin
         NewOperatorAST:=TNewOperatorAST.Create(VarAssignAST.Expression.Op.Simvol);
         NewOperatorAST.NewOperator:=TS('Add');
         NewOperatorAST.Parent:=VarAssignAST.Expression.Op.Parent;
         VarAssignAST.Expression.Op.Free;
         VarAssignAST.Expression.Op:=NewOperatorAST;
         PrintExpression(rEAX, VarAssignAST.Expression);
         end;
      if IsType(Type1, TS('Char')) and IsType(Type2, TS('String')) then
         begin
         NewOperatorAST:=TNewOperatorAST.Create(VarAssignAST.Expression.Op.Simvol);
         NewOperatorAST.NewOperator:=TS('Add');
         NewOperatorAST.Parent:=VarAssignAST.Expression.Op.Parent;
         VarAssignAST.Expression.Op.Free;
         VarAssignAST.Expression.Op:=NewOperatorAST;
         Factor:=VarAssignAST.Expression.Factor1;
         VarAssignAST.Expression.Factor1:=VarAssignAST.Expression.Factor2;
         VarAssignAST.Expression.Factor2:=Factor;
         PrintExpression(rEAX, VarAssignAST.Expression);
         end;
      end;
 // [!]   �������
 // PrintVarAssignReg(VarAssignAST.Signature, rEAX);
end;

procedure TPrinterAST.PrintVarAssignBinary(const VarAssignAST: TVarAssignAST);
var
  TypeFamily:TTypeFamily;
begin
  Assert(VarAssignAST<> nil, '127');
  TypeFamily:=SignatureTypeFamily(VarAssignAST.Signature);
  case TypeFamily of
  tfUndefine:  Compiler.Error.Add(194, '');  // ����������� ���
  tfOrdinal:   PrintVarAssignBinary_Ordinal(VarAssignAST);
  tfReal:      PrintVarAssignBinary_Real(VarAssignAST);
  tfBoolean:   PrintVarAssignBinary_Boolean(VarAssignAST);
  tfPointer:   Compiler.Error.Add(194, '');  // ����������� ���
  tfString:    PrintVarAssignBinary_String(VarAssignAST);
  tfStructure: Compiler.Error.Add(194, '');  // ����������� ���
  end;
end;

procedure TPrinterAST.PrintVarAssign(const VarAssignAST: TVarAssignAST);
begin
  Assert(VarAssignAST<> nil, '128');
  case VarAssignAST.Expression.kind of
  ekSimple: PrintVarAssignSimple(VarAssignAST);
  ekUnari:  PrintVarAssignUnari(VarAssignAST);
  ekBinary: PrintVarAssignBinary(VarAssignAST);
  end;
end;

procedure TPrinterAST.PrintVarDecl(const VarDeclAST: TVarDeclAST);
var Indent:String;
begin
  Assert(VarDeclAST<> nil, '129');
  case TypeFamily(VarDeclAST._Type) of
  tfStructure:
    begin
    PrintIndent(VarDeclAST.Indent);
    Write(FFile, ' '#9#9);
    PrintDataType(VarDeclAST._Type);
    WriteLn(FFile, ' dup(0)')
    end;
  tfString:
    begin
    if IsType(VarDeclAST._Type, TS('Char')) then
       begin
       PrintIndent(VarDeclAST.Indent);
       Write(FFile, ' '#9#9);
       PrintDataType(VarDeclAST._Type);
       WriteLn(FFile, ' 0');
       end else
       begin
       PrintIndent(VarDeclAST.Indent);
       Write(FFile, ' '#9#9'DD ');
//       Indent:=GetUniIndent('TMP');
//       WriteLn(FFile, Indent+' ');
//       Write(FFile, Indent);
//       Write(FFile, ' '#9#9);
//       PrintDataType(VarDeclAST._Type);
       WriteLn(FFile, ' 0');
       end;
    end;
  else
    PrintIndent(VarDeclAST.Indent);
    Write(FFile, ' '#9#9);
    PrintDataType(VarDeclAST._Type);
    WriteLn(FFile, ' 0');
  end;
end;

function TPrinterAST.GetUniIndent(Indent: WideString): WideString;
begin
  Result:=Compiler.Slovar.GenUniWord(Indent);
  Compiler.Slovar.Add(Indent, Result);
end;

procedure TPrinterAST.PrintDebugLocalDeclaration(
  const DeclarationAST: TDeclarationAST; Var Ofs:Integer);
var i:Integer;
begin
  Assert(DeclarationAST<> nil, '130');
  if WithDebugInfo then
    begin
    for i:=0 to Length(DeclarationAST.ListVar)-1 do
      PrintAlias(DeclarationAST.ListVar[i]);

    for i:=0 to Length(DeclarationAST.ListConst)-1 do
      PrintAlias(DeclarationAST.ListConst[i]);
    end;
end;

procedure TPrinterAST.PrintLocalDeclaration(
  const DeclarationAST: TDeclarationAST; Var Ofs:Integer);
var
 i:Integer;
 tmp:Integer;
begin
  Assert(DeclarationAST<> nil, '131');
  tmp:=ofs;
  for i:=0 to Length(DeclarationAST.ListVar)-1 do
    PrintLocalVarDecl(DeclarationAST.ListVar[i], ofs);

  for i:=0 to Length(DeclarationAST.ListConst)-1 do
    PrintLocalConstDecl(DeclarationAST.ListConst[i], ofs);
  // debug
  PrintDebugLocalDeclaration(DeclarationAST,tmp);
end;

procedure TPrinterAST.PrintLocalConstDecl(
  const ConstDeclAST: TConstDeclAST; Var Ofs:Integer);
var sKey, sIndent:WideString;
begin
  Assert(ConstDeclAST<> nil, '132');
  sKey:='c'+ConstDeclAST.Indent.FSimvol.Value;
  sIndent:=Compiler.Slovar.GenUniWord(sKey);
  ConstDeclAST.Indent.FSimvol.Value:=sIndent;
  AddConstImm(ConstDeclAST, ConstDeclAST);

end;

procedure TPrinterAST.PrintLocalVarDecl(const VarDeclAST: TVarDeclAST; Var Ofs:Integer);
  function GetTypeASM(const _Type:TTypeAST):String;
  begin
  case GetTypeSize(_Type) of
  1: Result:='Byte';
  2: Result:='Word';
  4: Result:='DWord';
  8: Result:='QWord';
  else
    Result:='Byte';
  end;
  end;
begin
  Assert(VarDeclAST<> nil, '133');
  PrintIndent(VarDeclAST.Indent);
  Write(FFile,#9#9'EQU ');
  Ofs:=Ofs+GetTypeSize(VarDeclAST._Type);
  WriteLn(FFile, Format('(%s PTR [EBP - %d])', [GetTypeASM(VarDeclAST._Type), ofs]));
end;

function TPrinterAST.GetFormalParametrsSize(
  const FormalParametrsAST: TFormalParametrsAST): Integer;
var i:Integer;
  function Allign4(d:Cardinal):Cardinal;
  begin
   result:=((d+3) div 4)*4;
  end;
begin
  Assert(FormalParametrsAST<> nil, '134');
  Result:=0;
  for i:=0 to FormalParametrsAST.Count-1 do
     begin
     case TypeFamily(FormalParametrsAST.List[i].VarDecl._Type) of
     tfUndefine:Inc(Result, 4);
     tfOrdinal,
     tfReal,
     tfBoolean,
     tfPointer,
     tfVoid:Inc(Result, Allign4(GetTypeSize(FormalParametrsAST.List[i].VarDecl._Type)));
     tfString,
     tfStructure:Inc(Result, Allign4(PointerSize));
     end;

     end;
end;

procedure TPrinterAST.ErrorTypeCast;
begin
  Compiler.Error.Add(198,'; ��������� ������');
end;

procedure TPrinterAST.PrintVarAssignCast_Boolean(const Left,
  Righte: TVarDeclAST);
begin
  Assert(Left<> nil, '135');
  Assert(Righte<> nil, '136');
  ErrorTypeCast;
end;

procedure TPrinterAST.PrintVarAssignCast_Pointer(const Left,
  Righte: TVarDeclAST);
begin
  Assert(Left<> nil, '137');
  Assert(Righte<> nil, '138');
  PrintVarAssign_OrdinalOrdinal(Left, Righte);
end;

procedure TPrinterAST.PrintVarAssign_RealOrdinal(const Left, Righte: TVarDeclAST);
begin
  Assert(Left<> nil, '139');
  Assert(Righte<> nil, '140');
  if isTypeFast(Righte._Type, TS('Integer')) then
    begin
     PrintCmdAssignFPU('FILD', Righte.Indent, ToReg);
     PrintCmdAssignFPU('FSTP', Left.Indent, ToIndent);
    end else
    begin
      ErrorTypeCast;
    end;
end;

procedure TPrinterAST.PrintVarAssignCast_Real(const Left, Righte: TVarDeclAST);
begin
  Assert(Left<> nil, '141');
  Assert(Righte<> nil, '142');
  case TypeFamily(Righte._Type) of
  tfOrdinal: PrintVarAssign_RealOrdinal(Left, Righte);
  tfReal:    ErrorTypeCast;
  tfBoolean: ErrorTypeCast;
  tfPointer: ErrorTypeCast;
  tfString:  ErrorTypeCast;
  tfStructure: ErrorTypeCast;
  end;
end;

procedure TPrinterAST.PrintVarAssignCast_String(const Left,
  Righte: TVarDeclAST);
begin
  Assert(Left<> nil, '143');
  Assert(Righte<> nil, '144');

  if isType(Left._Type, TS('Char')) then
     begin
       case TypeFamily(Righte._Type) of
       tfOrdinal: PrintVarAssign_OrdinalOrdinal(Left, Righte);
       tfReal:    ErrorTypeCast;
       tfBoolean: ErrorTypeCast;
       tfPointer: ErrorTypeCast;
       tfString:  ErrorTypeCast;
       tfStructure: ErrorTypeCast;
       end;
     end else ErrorTypeCast;

end;

procedure TPrinterAST.PrintVarAssignCast_Structure(const Left,
  Righte: TVarDeclAST);
begin
  Assert(Left<> nil, '145');
  Assert(Righte<> nil, '146');
  ErrorTypeCast;
end;

function TPrinterAST.GentIndentForImmString(const _string: TStringAST):TConstDeclAST;
var sKey, sIndent:WideString;
begin
  Assert(_string<> nil, '147');
  sKey:='t';
  sIndent:=Compiler.Slovar.GenUniWord(sKey);

  result:=TConstDeclAST.Create;
  result._Type:=TTypeAST.Create;
  result._Type.Kind:=tkSimple;
  if Length(_string.Simvol.Value)=1 then
      result._Type.Simple:=TTypeSimpleAST.Create(ts('Char'))
    else
      result._Type.Simple:=TTypeSimpleAST.Create(ts('string'));
  result._const:=_string;
  result.Indent:=TIndentAST.Create(TS(sIndent));
  result.Indent.Parent:=Result;

  Compiler.Slovar.Add(_string.Simvol.Value,_string, sIndent, result.Indent);

end;

function TPrinterAST.GentIndentForImmInteger(const _Integer: TIntegerAST):TConstDeclAST;
var sKey, sIndent:WideString;
begin
  Assert(_Integer<> nil, '148');
  sKey:='t';
  sIndent:=Compiler.Slovar.GenUniWord(sKey);

  result:=TConstDeclAST.Create;
  result._Type:=TTypeAST.Create;
  result._Type.Kind:=tkSimple;

  result._Type.Simple:=TTypeSimpleAST.Create(ts('Integer'));
  result._const:=_Integer;
  result.Indent:=TIndentAST.Create(TS(sIndent));
  result.Indent.Parent:=Result;

Compiler.Slovar.Add(_Integer.Simvol.Value,_Integer, sIndent, result.Indent);

end;

function TPrinterAST.GentIndentForImmReal(const _Real: TRealAST):TConstDeclAST;
var sKey, sIndent:WideString;
begin
  Assert(_Real<> nil, '149');
  sKey:='t';
  sIndent:=Compiler.Slovar.GenUniWord(sKey);

  result:=TConstDeclAST.Create;
  result._Type:=TTypeAST.Create;
  result._Type.Kind:=tkSimple;

  result._Type.Simple:=TTypeSimpleAST.Create(ts('Real'));
  result._const:=_Real;
  result.Indent:=TIndentAST.Create(TS(sIndent));
  result.Indent.Parent:=Result;

Compiler.Slovar.Add(_Real.Simvol.Value, _Real, sIndent, result.Indent);

end;

procedure TPrinterAST.AddConstImm( AtNode:TNodeAST; ConstDeclAST:TConstDeclAST);
var
  DeclarationAST:TDeclarationAST;
begin
  Assert(AtNode<> nil, '150');
  Assert(ConstDeclAST<> nil, '151');

  while (AtNode<>nil) and Not (AtNode is TUnitAST) do
    AtNode:=AtNode.Parent;
  if (AtNode<>nil) then
     begin
     DeclarationAST:=(AtNode as TUnitAST).Dec;
     ConstDeclAST.Parent:=DeclarationAST;
     DeclarationAST.Add(ConstDeclAST)
     end;
end;

procedure TPrinterAST.PrintAlias(const Indent: TIndentDeclAST);
var
 Index:Integer;
 sTrans:WideString;
 sKey:WideString;
begin
  Assert(Indent<> nil, '152');
  Index:=Compiler.Slovar.FindObj(Indent.Indent);
  if (Index>=0) then
      begin
      sKey:=Compiler.Slovar.List[index].Key;
      sTrans:=Compiler.Slovar.List[index].Trans;
      end;
 Compiler.Slovar.FindObj(Indent.Indent);
 WriteLn(FFile, #9#9#9#9#9#9#9#9#9#9'?debug alias '+sTrans+'='+sKey);
end;

procedure TPrinterAST.PrintCmdAssign2(const leval:Integer; const Reg: String;
  const IndentAST: TIndentAST; const Direct:TDirectForCmd);
begin
  Assert(IndentAST<> nil, '153');
  case Direct of
  ToIndent: PrintCmdIndentAssignReg2(leval, IndentAST, Reg);
  ToReg: PrintCmdRegAssignIndent2(leval, Reg, IndentAST);
  end;
end;

procedure TPrinterAST.PrintCmdAssign(const cmd:String; const Reg: String;
  const IndentAST: TIndentAST; const Direct:TDirectForCmd);
begin
  Assert(IndentAST<> nil, '153');
  case Direct of
  ToIndent: PrintCmdIndentAssignReg(cmd, IndentAST, Reg);
  ToReg: PrintCmdRegAssignIndent(cmd, Reg, IndentAST);
  end;
end;

procedure TPrinterAST.PrintCmdAssignFPU(const cmd:String;
  const IndentAST: TIndentAST; const Direct:TDirectForCmd);
var
  ParametrAST:TParametrAST;
begin
  Assert(IndentAST<> nil, '154');
  if (IndentAST.Parent is TVarDeclAST) then
  if (IndentAST.Parent.Parent is TParametrAST) then
     begin
     ParametrAST:=IndentAST.Parent.Parent as TParametrAST;
     if IsParamertrRef(ParametrAST) then
        begin
        Write(FFile, Format(#9#9'%s %s,',['MOV', 'EAX']));
        PrintIndent(IndentAST);
        WriteLn(FFile, '');
        WriteLn(FFile, Format(#9#9'%s %s, [%s]',[CMD, 'EAX', 'EAX']));
        end else
        begin
        Write(FFile, Format(#9#9'%s ',[CMD]));
        PrintIndent(IndentAST);
        WriteLn(FFile, '');
        end;
     end else
     begin
     Write(FFile, Format(#9#9'%s ',[CMD]));
     PrintIndent(IndentAST);
     WriteLn(FFile, '');
//     WriteLn(FFile, '+'+IntToStr(SubStack));
     end else
  if (IndentAST.Parent is TConstDeclAST) then
    begin
    Write(FFile, Format(#9#9'%s ',[CMD]));
    PrintIndent(IndentAST);
    WriteLn(FFile, '');
    end;
end;

function TPrinterAST.IsParamertrRef(const ParametrAST:TParametrAST):Boolean;
begin
  Assert(ParametrAST<> nil, '155');
  if isType(ParametrAST.VarDecl._Type, ts('String'))then
    begin
    result:=True;
    end else
    begin
    result:=False;
    case ParametrAST.mode of
    pmVar: result:=true;
    pmParam: if (getTypeSize(ParametrAST.VarDecl._Type)>PointerSize) then result:=True;
    pmConst: if (GetTypeSize(ParametrAST.VarDecl._Type)>PointerSize) then result:=True;
    end;
    end;
end;

function TPrinterAST.getVarDeclKind(var VarDeclAST:TVarDeclAST):TVarDeclKind;
begin
  result:=self.FSemanticCheking.getVarDeclKind(VarDeclAST);
end;

procedure TPrinterAST.PrintCmdRegAssignIndent2(leval:Integer; const Reg1: String;
  const IndentAST: TIndentAST);
var
  ParametrAST:TParametrAST;
  VarDeclAST:TVarDeclAST;
  i:Integer;
begin
  Assert(IndentAST<> nil, '157');
  if (IndentAST.Parent is TVarDeclAST) then
   begin
   VarDeclAST:=IndentAST.Parent as TVarDeclAST;
   case getVarDeclKind(VarDeclAST) of
   vdkUndefine,
   vdkGlobal:
     begin
     inc(Leval,0);
     // [!] �������� ������
     if IsIndentDeclString(IndentAST.Parent as TIndentDeclAST) then
      begin
        inc(Leval,2);
      end;
     end;
   vdkParam:
     begin
     ParametrAST:=VarDeclAST.Parent as TParametrAST;
     if IsParamertrRef(ParametrAST) then
        begin
        inc(Leval,2);
        end;
     end;
   vdkLocal: ;
   end; // case
   end;
   if IsIndentDeclString(IndentAST.Parent as TIndentDeclAST) then
      begin
        inc(Leval,2);
      end;

   if Leval=0 then
      begin
      Write(FFile, Format(#9#9'%s %s, ',['LEA', Reg1]));
      PrintIndent(IndentAST);
      WriteLn(FFile, '');
      end;
   if Leval=1 then
      begin
      Write(FFile, Format(#9#9'%s %s, ',['MOV', Reg1]));
      PrintIndent(IndentAST);
      WriteLn(FFile, '');
      end;
   if Leval>1 then
      begin
      Write(FFile, Format(#9#9'%s %s, ',['LEA', 'EDX']));
      PrintIndent(IndentAST);
      WriteLn(FFile, '');
      for i:=1 to (Leval-1) div 2 do
         WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV','EDX', 'EDX']));
      WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV',Reg1, 'EDX']));
      end;

end;

procedure TPrinterAST.PrintCmdRegAssignIndent(const cmd:String; const Reg1: String;
  const IndentAST: TIndentAST);
var
  ParametrAST:TParametrAST;
  VarDeclAST:TVarDeclAST;
begin
  Assert(IndentAST<> nil, '156');
  if (IndentAST.Parent is TVarDeclAST) then
  begin
  VarDeclAST:=IndentAST.Parent as TVarDeclAST;
  if (IndentAST.Parent.Parent is TParametrAST) then
     begin
     ParametrAST:=IndentAST.Parent.Parent as TParametrAST;
     if IsParamertrRef(ParametrAST) then
        begin
        if not IsIndentDeclString(IndentAST.Parent as TIndentDeclAST) then
          begin
          if  (unAlias(ParametrAST.VarDecl._Type).Kind=tkSimple) then
            begin
              Write(FFile, Format(#9#9'%s %s, ',['MOV', Reg1]));
              PrintIndent(IndentAST);
              WriteLn(FFile, '');
         //     if (CMD<>'LEA') then
                 WriteLn(FFile, Format(#9#9'%s %s, [%s]',[CMD, Reg1, Reg1]));
            end else
            begin
              if (CMD='MOV') then
                 begin
                 Write(FFile, Format(#9#9'%s %s, ',['MOV', Reg1]));
                 PrintIndent(IndentAST);
                 WriteLn(FFile, '');
                 WriteLn(FFile, Format(#9#9'%s %s, [%s]',[CMD, Reg1, Reg1]));
                 end else
                 begin
                 Write(FFile, Format(#9#9'%s %s, ',['LEA', Reg1]));
                 PrintIndent(IndentAST);
                 WriteLn(FFile, '');
                 WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', Reg1, Reg1]));
                 end;
            end;
          end else
          begin
          case getVarDeclKind(VarDeclAST) of
          vdkUndefine,
          vdkGlobal:
            begin
            Write(FFile, Format(#9#9'%s %s, ',['LEA', Reg1]));
            PrintIndent(IndentAST);
            WriteLn(FFile, '');
            if (ParametrAST.mode=pmVar) then
               WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', Reg1, Reg1]));

          //  if (CMD<>'LEA') then
          //     begin
          //     WriteLn(FFile, Format(#9#9'%s %s, [%s]',[CMD, Reg1, Reg1]));
         //      end;
            end;
          vdkParam:
            begin
            Write(FFile, Format(#9#9'%s %s, ',['LEA', Reg1]));
            PrintIndent(IndentAST);
            WriteLn(FFile, '');
            //if (ParametrAST.mode in [pmVar, pmConst, pmParam]) then
               WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', Reg1, Reg1]));
            if (CMD<>'LEA') then
               begin
               WriteLn(FFile, Format(#9#9'%s %s, [%s]',[CMD, Reg1, Reg1]));
               end;

            end;
          vdkLocal:
            begin
            Write(FFile, Format(#9#9'%s %s, ',[CMD, Reg1]));
            PrintIndent(IndentAST);
            WriteLn(FFile, '');
            end;
          end; // case
          end;
        end else
        begin
        Write(FFile, Format(#9#9'%s %s, ',[CMD, Reg1]));
        PrintIndent(IndentAST);
        WriteLn(FFile, '');
        end;
     end else
     begin
     if IsIndentDeclString(IndentAST.Parent as TIndentDeclAST) then
        begin
          case getVarDeclKind(VarDeclAST) of
          vdkUndefine,
          vdkGlobal:
            begin
            Write(FFile, Format(#9#9'%s %s, ',['LEA', Reg1]));  //[!]3
            PrintIndent(IndentAST);
            WriteLn(FFile, '');
            WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', Reg1, Reg1]));
     //       if (CMD<>'LEA') then
    //           WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', Reg1, Reg1]));
            end;
          vdkParam:
            begin
            Write(FFile, Format(#9#9'%s %s, ',['MOV', Reg1]));  //[!]3
            PrintIndent(IndentAST);
            WriteLn(FFile, '');
            WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', Reg1, Reg1]));
     //       if (CMD<>'LEA') then
    //           WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', Reg1, Reg1]));
            end;
          vdkLocal:
            begin
            Write(FFile, Format(#9#9'%s %s, ',['LEA', Reg1]));
            PrintIndent(IndentAST);
            WriteLn(FFile, '');
            if (CMD<>'LEA') then
               WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', Reg1, Reg1]));
            end;
          end;
        end else
        begin
          Write(FFile, Format(#9#9'%s %s, ',[CMD, Reg1]));
          PrintIndent(IndentAST);
          WriteLn(FFile, '');
    //      WriteLn(FFile, '+'+IntToStr(SubStack));
        end;
     end;
  end;
  {if (IndentAST.Parent is TConstDeclAST) then
    begin
    Write(FFile, Format(#9#9'%s %s, ',[CMD, Reg1]));
    PrintIndent(IndentAST);
    WriteLn(FFile, '');
    end;
}end;

procedure TPrinterAST.PrintCmdIndentAssignReg2(leval:Integer;
    const IndentAST: TIndentAST; const Reg1: String);
var
  ParametrAST:TParametrAST;
  VarDeclAST:TVarDeclAST;
  i:Integer;
  RegSize:Integer;
begin
  Assert(IndentAST<> nil, '157');
  if (IndentAST.Parent is TVarDeclAST) then
   begin
   VarDeclAST:=IndentAST.Parent as TVarDeclAST;
   case getVarDeclKind(VarDeclAST) of
   vdkUndefine,
   vdkGlobal: inc(Leval,0);
   vdkParam:
     begin
     ParametrAST:=VarDeclAST.Parent as TParametrAST;
     if IsParamertrRef(ParametrAST) then
        begin
        inc(Leval,2);
        end;
     end;
   vdkLocal:
   end; // case
   end;
   if IsIndentDeclString(IndentAST.Parent as TIndentDeclAST) then
      begin
        inc(Leval,2);
      end;

   if Leval>=0 then
      begin
      Write(FFile, Format(#9#9'%s %s, ',['LEA', 'EBX']));
      PrintIndent(IndentAST);
      WriteLn(FFile, '');
      end;
   for i:=1 to (Leval-1) div 2 do
       WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', 'EBX', 'EBX']));
   if Leval>=1 then
      begin
      RegSize:=2;
      if Reg1[1]='E' then RegSize:=4;
      if Reg1[2]='H' then RegSize:=1;
      if Reg1[2]='L' then RegSize:=1;
      case RegSize of
      4:  WriteLn(FFile, Format(#9#9'%s DWord PTR [%s], %s',['MOV', 'EBX', Reg1]));
      2:  WriteLn(FFile, Format(#9#9'%s Word PTR [%s], %s',['MOV', 'EBX', Reg1]));
      1:  WriteLn(FFile, Format(#9#9'%s Byte PTR [%s], %s',['MOV', 'EBX', Reg1]));
      end;
      end;


end;

procedure TPrinterAST.PrintCmdIndentAssignReg(const cmd:String;
    const IndentAST: TIndentAST; const Reg1: String);
var
  ParametrAST:TParametrAST;
  VarDeclAST:TVarDeclAST;
begin
  Assert(IndentAST<> nil, '157');
  if (IndentAST.Parent is TVarDeclAST) then
   begin
   VarDeclAST:=IndentAST.Parent as TVarDeclAST;
   if (IndentAST.Parent.Parent is TParametrAST) then
     begin
     ParametrAST:=IndentAST.Parent.Parent as TParametrAST;
     if IsParamertrRef(ParametrAST) then
        begin
        if not IsIndentDeclString(IndentAST.Parent as TIndentDeclAST) then
           begin
           Write(FFile, Format(#9#9'%s %s, ',['MOV', 'EBX']));
           PrintIndent(IndentAST);
           WriteLn(FFile, '');
           WriteLn(FFile, Format(#9#9'%s [%s], %s',[CMD,'EBX', Reg1]));
           end else
           begin
           Write(FFile, Format(#9#9'%s %s, ',['LEA', 'EBX']));
           PrintIndent(IndentAST);
           WriteLn(FFile, '');
           if  (ParametrAST.mode=pmVar) then
                WriteLn(FFile, Format(#9#9'%s %s, [%s]',['MOV', 'EBX', 'EBX']));
           WriteLn(FFile, Format(#9#9'%s [%s], %s',[CMD, 'EBX', Reg1]));
           end;
        end else
        begin
        Write(FFile, Format(#9#9'%s ',[CMD]));
        PrintIndent(IndentAST);
        WriteLn(FFile, Format(', %s',[Reg1]));
        end;
     end else
     begin
      if IsIndentDeclString(IndentAST.Parent as TIndentDeclAST) then
        begin
        Write(FFile, Format(#9#9'%s ',['LEA']));
        Write(FFile, Format('%s, ',['EBX']));
        PrintIndent(IndentAST);
        WriteLn(FFile, '');
        case getVarDeclKind(VarDeclAST) of
        vdkUndefine,
        vdkGlobal,
        vdkParam:
           WriteLn(FFile, Format(#9#9'%s %s, [%s]',[CMD,'EBX', 'EBX']));
        vdkLocal:
        end;
        WriteLn(FFile, Format(#9#9'%s [%s], %s',[CMD,'EBX', Reg1]));
        end else
        begin
        Write(FFile, Format(#9#9'%s ',[CMD]));
        PrintIndent(IndentAST);
   //     Write(FFile, '+'+IntToStr(SubStack));
        WriteLn(FFile, Format(', %s',[Reg1]));
        end;
     end;
   end;
  if (IndentAST.Parent is TConstDeclAST) then
    begin
      Compiler.Error.Add(196,'���������� ��������� ������, �� �����������');
    end;
end;

function TPrinterAST.TypeFamily(_Type:TTypeAST): TTypeFamily;
begin
  Assert(_Type<> nil, '158');
  result:=FSemanticCheking.TypeFamily(_Type);
end;

function TPrinterAST.DesignatorTypeFamily(_Type:TTypeAST; Designator: TDesignatorAST): TTypeFamily;
begin
  Assert(_Type<> nil, '159');
  Assert(Designator<> nil, '160');
  result:=FSemanticCheking.DesignatorTypeFamily(_Type, Designator);
end;

function TPrinterAST.SignatureTypeFamily(Signature: TSignatureAST): TTypeFamily;
begin
  Assert(Signature<> nil, '161');
  result:=FSemanticCheking.SignatureTypeFamily(Signature);
end;

function TPrinterAST.FactorTypeFamily(Factor: TFactorAST): TTypeFamily;
begin
  Assert(Factor<> nil, '162');
  result:=FSemanticCheking.FactorTypeFamily(Factor);
end;

function TPrinterAST.SignatureType(Signature: TSignatureAST): TTypeAST;
begin
  Assert(Signature<> nil, '163');
  result:=FSemanticCheking.SignatureType(Signature);
end;

function TPrinterAST.FactorType(Factor: TFactorAST): TTypeAST;
begin
  Assert(Factor<> nil, '164');
  result:=FSemanticCheking.FactorType(Factor);
end;

function TPrinterAST.DesignatorType(_Type:TTypeAST; Designator: TDesignatorAST):TTypeAST;
begin
  Assert(_Type<> nil, '165');
  Assert(Designator<> nil, '166');
  result:=FSemanticCheking.DesignatorType(_Type, Designator);
end;

function TPrinterAST.IsOpReletiv(Op:TSimvol):Boolean;
const
  Table:array [0..5] of String=('=','<>', '<', '>','>=', '<=');
var
  i:Integer;
begin
result:=False;
  for i:=0 to Length(Table)-1 do
    if SimvolsEQ(Op,Ts(Table[i])) then
       begin
       Result:=True;
       Break;
       end;
end;

procedure TPrinterAST.DivPrintChangeReg( Arg1, Arg2:String);
begin
 if (arg1<>'EAX') and  (arg2='EBX') then
    begin
     WriteLn(FFile, 'MOV EAX, '+Arg1);
    end;
 if (arg1='EAX') and  (arg2<>'EBX') then
    begin
     WriteLn(FFile, 'MOV EBX, '+Arg2);
    end;
 if (arg1<>'EAX') and  (arg2<>'EBX') then
    begin
     PrintPush(Arg1);
     PrintPush(Arg2);
     PrintPop('EBX');
     PrintPop('EAX');
    end;
end;

procedure TPrinterAST.PrintPop(const reg: String);
begin
  Dec(SubStack, PointerSize);
  WriteLn(FFile, #9#9'POP  '+reg);
end;

procedure TPrinterAST.PrintPush(const reg: String);
begin
  Inc(SubStack, PointerSize);
  WriteLn(FFile, #9#9'PUSH '+reg);
end;

procedure TPrinterAST.PrintForeignAsm(const ForeignAsm: TForeignAsmAST);
begin
  if ForeignAsm<>nil then
     WriteLn(FFile, String(ForeignAsm.Text));
end;

procedure TheAssertErrorProc(const Message, Filename: string;
    LineNumber: Integer; ErrorAddr: Pointer);
begin
WriteLn(Message+':'+Filename+':'+IntToStr(LineNumber));
end;

procedure TPrinterAST.PrintCreateVar(LocalDec: TDeclarationAST);
var
  i:Integer;
  VarDeclAST:TVarDeclAST;
begin
  WriteLn(FFile, #9#9'XOR EAX, EAX');
  for i:=0 to Length(LocalDec.ListVar)-1 do
    begin
    VarDeclAST:=LocalDec.ListVar[i];
    if  isType(VarDeclAST._Type, TS('String')) then
      begin
        Write(FFile, #9#9'MOV ');
        PrintIndent(VarDeclAST.Indent);
        WriteLn(FFile, ', EAX');
      end;
    end;
end;

procedure TPrinterAST.PrintDestroyVar(LocalDec: TDeclarationAST);
var
  i:Integer;
  VarDeclAST:TVarDeclAST;
begin
  for i:=0 to Length(LocalDec.ListVar)-1 do
    begin
    VarDeclAST:=LocalDec.ListVar[i];
    if  isType(VarDeclAST._Type, TS('String')) and (not (VarDeclAST.IsResult)) then
      begin
        PrintCMDAssign('LEA','EBX',VarDeclAST.Indent, ToReg);
        PrintPush('EBX');
        WriteLn(FFile, #9#9'CALL DestroyStr');
      end;
    end;
end;

begin
AssertErrorProc:=@TheAssertErrorProc;
end.
