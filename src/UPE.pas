unit UPE;

interface

uses SysUtils, UPE_Records;

type
  TDByte=array of Byte;

  TImageFileHeader=class
  end;

  TImageOptionalHeader32=class
  end;

  TImageNTHeaders32 = class
  private
    FImageNTHeaders32:RImageNTHeaders32;
    FFileHeader: TImageFileHeader;
    FOptionalHeader: TImageOptionalHeader32;
    procedure SetFileHeader(const Value: TImageFileHeader);
    procedure SetOptionalHeader(const Value: TImageOptionalHeader32);
  public
    function GetRecord:RImageNTHeaders32;
    property FileHeader:TImageFileHeader read FFileHeader write SetFileHeader;
    property OptionalHeader:TImageOptionalHeader32 read FOptionalHeader write SetOptionalHeader;
  end;

  TEXE_PE = class
  private
    FDos_Section: TDByte;
    FImageNTHeaders: TImageNTHeaders32;
    procedure SetDos_Section(const Value: TDByte);
    procedure SetImageNTHeaders(const Value: TImageNTHeaders32);

  protected

  public
    constructor Create;
    destructor Destroy;
    property Dos_Section:TDByte read FDos_Section write SetDos_Section;
    property ImageNTHeaders:TImageNTHeaders32 read FImageNTHeaders write SetImageNTHeaders;
  published

  end;

implementation

function IsEXE_MZ(Value:TDByte):Boolean;
var
 Head:PDOS_HEADER_2;
begin
  Result:=False;
  if Length(Value)<SizeOf(RDOS_HEADER_2) then exit;
  Head:=@(Value[0]);
  if (Head.Magic=MZ_SIGNATURE) then result:=True;
end;

{ TEXE_PE }

constructor TEXE_PE.Create;
begin
  inherited;

end;

destructor TEXE_PE.Destroy;
begin

  inherited;
end;

procedure TEXE_PE.SetDos_Section(const Value: TDByte);
begin
  if not IsEXE_MZ(Value) then
     begin
       raise Exception.Create('File is not EXE-MZ!');
     end
     else
       FDos_Section := Value;
end;

procedure TEXE_PE.SetImageNTHeaders(const Value: TImageNTHeaders32);
begin
  FImageNTHeaders := Value;
end;

{ TImageNTHeaders32 }

function TImageNTHeaders32.GetRecord: RImageNTHeaders32;
begin
Result:=FImageNTHeaders32;
end;

procedure TImageNTHeaders32.SetFileHeader(const Value: TImageFileHeader);
begin
  FFileHeader := Value;
end;

procedure TImageNTHeaders32.SetOptionalHeader(
  const Value: TImageOptionalHeader32);
begin
  FOptionalHeader := Value;
end;

end.
