{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UASTPrint;

interface

uses SysUtils, UAST, LICompiler, UCompiler, UGramAnaliser;

type
  TPrinterAST = class(TCodePrinter)
  private
    FFile:TextFile;
  protected

  public
    Compiler:TCompiler;
    procedure AssignFile(Path:String);  override;
    procedure print; override;
    procedure reset; override;
    procedure process;  override;
    constructor Create(TheCompiler:TCompiler);
    procedure PrintUnitAST(const UnitAST:TUnitAST);
    procedure PrintImplimentaion(const ImplimentaionAST:TImplimentaionAST);
    procedure PrintDeclaration(const DeclarationAST:TDeclarationAST);
    procedure PrintTypeDecl(const TypeDeclAST:TTypeDeclAST);
    procedure PrintVarDecl(const VarDeclAST:TVarDeclAST);
    procedure PrintConstDecl(const ConstDeclAST:TConstDeclAST);
    procedure PrintLabelDecl(const LabelDeclAST:TLabelDeclAST);
    procedure PrintFuncDecl(const FuncDeclAST:TFuncDeclAST);
    procedure PrintIndent(const IndentAST:TIndentAST);
    procedure PrintType(const TypeAST:TTypeAST);
    procedure PrintTypeSimple(const TypeSimpleAST:TTypeSimpleAST);
    procedure PrintTypeRecord(const TypeRecordAST:TTypeRecordAST);
    procedure PrintFieldDecl(const FieldDeclAST:TFieldDeclAST);
    procedure PrintConst(const ConstAST:TConstAST);
    procedure PrintFormalParametrs(const FormalParametrsAST:TFormalParametrsAST);
    procedure PrintParametr(const ParametrAST:TParametrAST);
    procedure PrintFuncImp(const FuncImpAST:TFuncImpAST);
    procedure PrintCompaundStatement(const CompaundStatementAST:TCompaundStatementAST);
    procedure PrintMainBlock(const CompaundStatementAST:TCompaundStatementAST);
    procedure PrintStatement(const StatementAST:TStatementAST);
    procedure PrintGoto(const GotoStmAST:TGotoStmAST);
    procedure PrintFor(const ForStmAST:TForStmAST);
    procedure PrintWhile(const WhileStmAST:TWhileStmAST);
    procedure PrintRepeat(const RepeatStmAST:TRepeatStmAST);
    procedure PrintIf(const IfStmAST:TIfStmAST);
    procedure PrintCase(const CaseStmAST:TCaseStmAST);
    procedure PrintCaseEdge(const CaseEdgeAST:TCaseEdgeAST);
    procedure PrintLabelingStatement(const LabelingStatementAST:TLabelingStatementAST);
    procedure PrintSimpleStatement(const SimpleStatementAST:TSimpleStatementAST);
    procedure PrintFuncCall(const FuncCallAST:TFuncCallAST);
    procedure PrintCallParametr(const CallParametrAST:TCallParametrAST);
    procedure PrintCallParametrs(const CallParametrsAST:TCallParametrsAST);
    procedure PrintVarAssign(const VarAssignAST:TVarAssignAST);
    procedure PrintSignature(const SignatureAST:TSignatureAST);
    procedure PrintDesignator(const DesignatorAST:TDesignatorAST);
    procedure PrintExpression(const ExpressionAST:TExpressionAST);
    procedure PrintExpressionList(const ExpressionListAST: TExpressionListAST);
    procedure PrintOperator(const OperatorAST:TOperatorAST);
    procedure PrintImmConst(const ImmConstAST: TImmConstAST);
    procedure PrintFactor(const FactorAST:TFactorAST);
    procedure PrintForeignAsm(ForeignAsm: TForeignAsmAST);

  published

  end;

implementation

{ TPrinterAST }

procedure TPrinterAST.AssignFile(Path: String);
begin
  if  (AObj<>nil) or  (AObj<>self)  then
        (AObj as TCodePrinter).AssignFile(Path)
      else
  System.AssignFile(FFile,Path);
end;

constructor TPrinterAST.Create(TheCompiler: TCompiler);
begin
  Compiler:=TheCompiler;
end;

procedure TPrinterAST.print;
var
  GA:TGramAnaliser;
begin
  if  (AObj<>nil) or  (AObj<>self)  then
        (AObj as TCodePrinter).print
      else
  begin
  GA:=Compiler.GramAnaliser as TGramAnaliser;
  rewrite(FFile);
  PrintUnitAST(GA.AST);
  CloseFile(FFile);
  end;
end;

procedure TPrinterAST.process;
begin
  if  (AObj<>nil) or  (AObj<>self)  then
        (AObj as TCodePrinter).print
      else
  Print;
end;

procedure TPrinterAST.reset;
begin
  if  (AObj<>nil) or  (AObj<>self)  then
        (AObj as TCodePrinter).reset;

end;

procedure TPrinterAST.PrintCallParametr(const CallParametrAST:TCallParametrAST);
begin
case CallParametrAST.kind of
cpVarDecl: PrintIndent(CallParametrAST.VarDecl.Indent);
cpConstDecl: PrintIndent(CallParametrAST.ConstDecl.Indent);
end;
end;

procedure TPrinterAST.PrintCallParametrs(
  const CallParametrsAST: TCallParametrsAST);
var i:Integer;
begin
 With CallParametrsAST do
   if Length(Items)<>0 then
      begin
      for i:=0 to Length(Items)-2 do
        begin
        PrintCallParametr(Items[i]);
        Write(FFile, ',');
        end;
      i:=Length(Items)-1;
      PrintCallParametr(Items[i]);
      end;

end;

procedure TPrinterAST.PrintCase(const CaseStmAST: TCaseStmAST);
var
  I:Integer;
begin
  Write(FFile, 'case ');
  Write(FFile, '(');
  PrintIndent(CaseStmAST.RefVar.Indent);
  Write(FFile, ')');
  WriteLn(FFile, ' of');
  for i:=0 to Length(CaseStmAST.Edges)-1 do
    PrintCaseEdge(CaseStmAST.Edges[i]);
  if CaseStmAST.Defult<>nil then
     begin
       WriteLn(FFile, 'else');
       PrintStatement(CaseStmAST.Defult);
     end;
  Write(FFile, 'end');
end;

procedure TPrinterAST.PrintCaseEdge(const CaseEdgeAST: TCaseEdgeAST);
begin
  Case CaseEdgeAST.Kind of
  kceUndefine:Write(FFile, '[Undefine]');
  kceRef: PrintIndent(CaseEdgeAST.ConstRef.Indent);
  kceImmConst:PrintImmConst(CaseEdgeAST.ImmConst);
  end;
  WriteLn(FFile, ':');
  PrintStatement(CaseEdgeAST.Statement);
  WriteLn(FFile, ';');
end;

procedure TPrinterAST.PrintCompaundStatement(
  const CompaundStatementAST: TCompaundStatementAST);
var
  i:Integer;
begin
WriteLn(FFile, 'begin');
for i:=0 to Length(CompaundStatementAST.ListStatemt)-1 do
  begin
  PrintStatement(CompaundStatementAST.ListStatemt[i]);
  WriteLn(FFile, ';');
  end;
Write(FFile, 'end');
end;

procedure TPrinterAST.PrintConst(const ConstAST: TConstAST);
// ������������ ������� ��������� Unicod
// ������� �� �������.
  function GetStr(s:WideString):String;
  var i:Integer;
  begin
  SetLength(Result,Length(s));
  for i:=1 to Length(s) do
    Result[i]:=Char(s[i]);
  end;
var s:String;
begin
if ConstAST=Nil then Write(FFile, '[ConstAST=Nil]')
   else
    begin
      S:=GetStr(ConstAST.FSimvol.Value);
      Write(FFile,s);
    end;
end;

procedure TPrinterAST.PrintConstDecl(const ConstDeclAST: TConstDeclAST);
begin
WriteLn(FFile, 'const');
PrintIndent(ConstDeclAST.Indent);
Write(FFile, ':');
PrintType(ConstDeclAST._Type);
Write(FFile, '=');
PrintConst(ConstDeclAST._const);
WriteLn(FFile, ';');

end;

procedure TPrinterAST.PrintDeclaration(
  const DeclarationAST: TDeclarationAST);
var i:Integer;
begin
for i:=0 to Length(DeclarationAST.ListType)-1 do
  begin
  PrintTypeDecl(DeclarationAST.ListType[i]);
  end;

for i:=0 to Length(DeclarationAST.ListVar)-1 do
  begin
  PrintVarDecl(DeclarationAST.ListVar[i]);
  end;

for i:=0 to Length(DeclarationAST.ListConst)-1 do
  begin
  PrintConstDecl(DeclarationAST.ListConst[i]);
  end;

for i:=0 to Length(DeclarationAST.ListLabel)-1 do
  begin
  PrintLabelDecl(DeclarationAST.ListLabel[i]);
  end;

for i:=0 to Length(DeclarationAST.ListFunc)-1 do
  begin
  PrintFuncDecl(DeclarationAST.ListFunc[i]);
  end;
end;

procedure TPrinterAST.PrintDesignator(const DesignatorAST: TDesignatorAST);
begin
  case DesignatorAST.Kind of
  dkSignature:Write(FFile, '');{������ �������}
  dkDesignature:
     begin
       Write(FFile, '^');
     end;
  dkDesignatureDot:
     begin
       Write(FFile, '.');
       PrintSignature(DesignatorAST.Signature);
     end;
  dkDeSignatureArray:
     begin
       Write(FFile, '[');
       PrintExpression(DesignatorAST.Expression);
       Write(FFile, ']');
     end;
  dkDeSignatureFunc:
     begin
       Write(FFile, '(');
       PrintExpressionList(DesignatorAST.ExpressionList);
       Write(FFile, ')');
     end;
  end; // case
if DesignatorAST.SubDesignator<>nil then
   PrintDesignator(DesignatorAST.SubDesignator);
end;

procedure TPrinterAST.PrintExpression(const ExpressionAST: TExpressionAST);
begin
  case ExpressionAST.kind of
  ekSimple:
    begin
      PrintFactor(ExpressionAST.Factor1);
    end;
  ekUnari:
    begin
      PrintOperator(ExpressionAST.Op);
      PrintFactor(ExpressionAST.Factor1);
    end;
  ekBinary:
    begin
      PrintFactor(ExpressionAST.Factor1);
      PrintOperator(ExpressionAST.Op);
      PrintFactor(ExpressionAST.Factor2);
    end;
  end;
end;

procedure TPrinterAST.PrintExpressionList(
  const ExpressionListAST: TExpressionListAST);
var i:Integer;
begin
  for i:=0 to Length(ExpressionListAST.Items)-1 do
    begin
    PrintExpression(ExpressionListAST.items[i]);
    if (i<>Length(ExpressionListAST.Items)-1) then Write(FFile, ',');
    end;
end;

procedure TPrinterAST.PrintImmConst(const ImmConstAST: TImmConstAST);
begin
if ImmConstAST<>nil then
   begin
   case ImmConstAST.Kind of
   immUndefine: Write(FFile, '[Undefine]');
   immString: PrintConst(ImmConstAST._String);
   immInteger: PrintConst(ImmConstAST._Integer);
   immReal: PrintConst(ImmConstAST._Real);
   immNil: Write(FFile, 'Nil');
   immAddr:
     begin
     Write(FFile, '@');
     PrintSignature(ImmConstAST.Addr.Signature);
     end;
   end; // case
   end;
end;

procedure TPrinterAST.PrintFactor(const FactorAST: TFactorAST);
begin
 if FactorAST<> nil then
  case FactorAST.Kind of
  fkSignature: PrintSignature(FactorAST.Signature);
  fkImmConst: PrintImmConst(FactorAST.ImmConst);
  end; // case
end;

procedure TPrinterAST.PrintFieldDecl(const FieldDeclAST: TFieldDeclAST);
begin
  PrintIndent(FieldDeclAST.Indent);
  Write(FFile, ':');
  PrintType(FieldDeclAST._Type);
  WriteLn(FFile, ';');
end;

procedure TPrinterAST.PrintFor(const ForStmAST: TForStmAST);
begin
 Write(FFile, 'For ');
 PrintIndent(ForStmAST.RefVar.Indent);
 Write(FFile, ':= ');
 PrintExpression(ForStmAST.Expr1);

 case ForStmAST.Mode of
 fmTo: Write(FFile, ' To ');
 fmDownTo: Write(FFile, ' DownTo ');
 fmError: Write(FFile, ' [Error]');
 end;
 PrintExpression(ForStmAST.Expr2);
 Write(FFile, ' Do ');
 PrintStatement(ForStmAST.Statement);
end;

procedure TPrinterAST.PrintFormalParametrs(
  const FormalParametrsAST: TFormalParametrsAST);
var
  i:Integer;
begin
 for i:=0 to FormalParametrsAST.Count-1 do
   begin
     PrintParametr(FormalParametrsAST.List[i]);
     Write(FFile, ';');
   end;
end;

procedure TPrinterAST.PrintFuncCall(const FuncCallAST: TFuncCallAST);
begin
   PrintIndent(FuncCallAST.RefFunc.Indent);
   Write(FFile, '(');
   PrintCallParametrs(FuncCallAST.Parametrs);
   Write(FFile, ')');

end;

procedure TPrinterAST.PrintFuncDecl(const FuncDeclAST: TFuncDeclAST);
begin
if FuncDeclAST.kind=kfFunction then
   begin
   Write(FFile, 'function ');
   PrintIndent(FuncDeclAST.Indent);
   Write(FFile, '(');
   PrintFormalParametrs(FuncDeclAST.FormalParametrs);
   Write(FFile, '):');
   PrintType(FuncDeclAST.result);
   WriteLn(FFile, ';');

   end;
if FuncDeclAST.kind=kfProcedure then
   begin
   Write(FFile, 'procedure ');
   PrintIndent(FuncDeclAST.Indent);
   Write(FFile, '(');
   PrintFormalParametrs(FuncDeclAST.FormalParametrs);
   WriteLn(FFile, ');');
   end;

end;

procedure TPrinterAST.PrintFuncImp(const FuncImpAST: TFuncImpAST);
begin
PrintFuncDecl(FuncImpAST.FuncDecl);
PrintDeclaration(FuncImpAST.LocalDec);
if (FuncImpAST.CompaundStatement<>nil) then PrintCompaundStatement(FuncImpAST.CompaundStatement);
if (FuncImpAST.AsmBlock<>nil) then PrintForeignAsm(FuncImpAST.AsmBlock);

end;

procedure TPrinterAST.PrintGoto(const GotoStmAST: TGotoStmAST);
begin
 Write(FFile, 'GoTo ');
 PrintIndent(GotoStmAST.RefLabel.Indent);
end;

procedure TPrinterAST.PrintIf(const IfStmAST: TIfStmAST);
begin
 Write(FFile, 'if ');
 Write(FFile, '(');
 PrintIndent(IfStmAST.BoolExpr.Indent);
 Write(FFile, ')');
 Write(FFile, ' then ');
 if IfStmAST.TrueEdge<> nil then
    PrintStatement(IfStmAST.TrueEdge);
 if IfStmAST.FalseEdge<> nil then
    begin
    Write(FFile, ' else ');
    PrintStatement(IfStmAST.FalseEdge);
    end;
end;

procedure TPrinterAST.PrintImplimentaion(
  const ImplimentaionAST: TImplimentaionAST);
var
  i:Integer;
begin
for i:=0 to Length(ImplimentaionAST.ListFunc)-1 do
  begin
  PrintFuncImp(ImplimentaionAST.ListFunc[i]);
  WriteLn(FFile, ';');
  end;
PrintMainBlock(ImplimentaionAST.MainBlock);
WriteLn(FFile, '.');
end;

procedure TPrinterAST.PrintIndent(const IndentAST: TIndentAST);
begin
 if IndentAST=Nil then  Write(FFile, '[Nil]')
   else Write(FFile, IndentAST.FSimvol.Value);
end;

procedure TPrinterAST.PrintLabelDecl(const LabelDeclAST: TLabelDeclAST);
begin
Write(FFile, 'Label ');
PrintIndent(LabelDeclAST.Indent);
WriteLn(FFile, ';');
end;

procedure TPrinterAST.PrintLabelingStatement(
  const LabelingStatementAST: TLabelingStatementAST);
begin
With LabelingStatementAST do
 case Kind of
 ksLabeling:
   begin
   if RefLabel= nil then
        WriteLn(FFile, '[DecLabel=Nil]:')
      else
        PrintIndent(RefLabel.Indent);
   WriteLn(FFile, ':');
   PrintLabelingStatement(LabelingStatementAST.LabelingStatement);
   end;
 ksSimple:
   begin
   if SimpleStatement<>nil then
      PrintSimpleStatement(SimpleStatement);
   end;
 end;
end;

procedure TPrinterAST.PrintMainBlock(
  const CompaundStatementAST: TCompaundStatementAST);
begin
if CompaundStatementAST=Nil then Write(FFile, 'end')
   else  PrintCompaundStatement(CompaundStatementAST);
end;

procedure TPrinterAST.PrintOperator(const OperatorAST: TOperatorAST);
begin
  Write(FFile, ' ');
  Write(FFile, OperatorAST.FSimvol.Value);
  Write(FFile, ' ');
end;

procedure TPrinterAST.PrintParametr(const ParametrAST: TParametrAST);
begin
  case ParametrAST.mode of
    pmVar: Write(FFile, 'Var ');
    pmConst: Write(FFile, 'Const ');
    pmParam: Write(FFile, 'Param ');
  end;

  PrintIndent(ParametrAST.VarDecl.Indent);
  Write(FFile, ':');
  PrintType(ParametrAST.VarDecl._Type);
end;

procedure TPrinterAST.PrintRepeat(const RepeatStmAST: TRepeatStmAST);
begin
  WriteLn(FFile, 'repeat');
  PrintCompaundStatement(RepeatStmAST.CompaundStatement);
  WriteLn(FFile, ';');
  Write(FFile, 'Until ');
  Write(FFile, '(');
  PrintIndent(RepeatStmAST.BoolExpr.Indent);
  Write(FFile, ')');
end;

procedure TPrinterAST.PrintSignature(const SignatureAST: TSignatureAST);
begin
  PrintIndent(SignatureAST.IndentDeclRef.Indent);
  PrintDesignator(SignatureAST.Designator);
end;

procedure TPrinterAST.PrintSimpleStatement(
  const SimpleStatementAST: TSimpleStatementAST);
begin
  case SimpleStatementAST.kind of
  ssUndefine: Write(FFile, '[Undefine]');
  ssFuncCall: PrintFuncCall(SimpleStatementAST.FuncCall);
  ssTypeCast: Write(FFile, '[Undefine]');
  ssVarAssign: PrintVarAssign(SimpleStatementAST.VarAssign);
  end;

end;

procedure TPrinterAST.PrintStatement(const StatementAST: TStatementAST);
begin
FLastPosRow:=StatementAST.Line;
FLastPosCol:=0;
WriteLn(FFile, format(';[Line=%d]',[StatementAST.Line]));

With StatementAST do
  case Kind of
  ksGoto: PrintGoto(_Goto);
  ksFor: PrintFor(_For);
  ksWhile: PrintWhile(_While);
  ksRepeat:PrintRepeat(_Repeat);
  ksIf: PrintIf(_IF);
  ksCase: PrintCase(_Case);
  ksLabelingStatement: PrintLabelingStatement(LabelingStatement);
  ksCompaundStatement: PrintCompaundStatement(CompaundStatement);
  end;
end;

procedure TPrinterAST.PrintType(const TypeAST: TTypeAST);
begin
if TypeAST=nil then Write(FFile,'nil')
  else
  begin
   case TypeAST.Kind of
   tkUndefine: Write(FFile, '[undefine]');
   tkAlias:  PrintIndent(TypeAST.AliasType.Indent);
   tkSimple: PrintTypeSimple(TypeAST.Simple);
   tkRecord: PrintTypeRecord(TypeAST._Record);
   tkArray: Write(FFile, '[array]');
   end;
  end;

end;

procedure TPrinterAST.PrintTypeDecl(const TypeDeclAST: TTypeDeclAST);
begin
  WriteLn(FFile, 'Type');
  PrintIndent(TypeDeclAST.Indent);
  Write(FFile, '=');
  PrintType(TypeDeclAST._Type);
  WriteLn(FFile, ';');
end;

procedure TPrinterAST.PrintTypeRecord(const TypeRecordAST: TTypeRecordAST);
var
 I:Integer;
begin
  WriteLn(FFile, 'record');
  for i:=0 to Length(TypeRecordAST.Fields)-1 do
    PrintFieldDecl(TypeRecordAST.Fields[i]);
  Write(FFile, 'end');
end;

procedure TPrinterAST.PrintTypeSimple(const TypeSimpleAST: TTypeSimpleAST);
begin
  Write(FFile, TypeSimpleAST.FSimvol.Value);
end;

procedure TPrinterAST.PrintUnitAST(const UnitAST: TUnitAST);
begin
  WriteLn(FFile, 'Declaration');
  PrintDeclaration(UnitAst.Dec);
  WriteLn(FFile, 'Implementation');
  if UnitAst.Imp= Nil then WriteLn(FFile, '[Imp=Nil]')
    else PrintImplimentaion(UnitAst.Imp);
end;

procedure TPrinterAST.PrintVarAssign(const VarAssignAST: TVarAssignAST);
begin
PrintSignature(VarAssignAST.Signature);
Write(FFile, ':=');
PrintExpression(VarAssignAST.Expression);
end;

procedure TPrinterAST.PrintVarDecl(const VarDeclAST: TVarDeclAST);
begin
  WriteLn(FFile, 'Var');
  PrintIndent(VarDeclAST.Indent);
  Write(FFile, ':');
  PrintType(VarDeclAST._Type);
  Write(FFile, '=');
  PrintConst(VarDeclAST._const);
  WriteLn(FFile, ';');
end;

procedure TPrinterAST.PrintWhile(const WhileStmAST: TWhileStmAST);
begin
  Write(FFile, 'While ');
  Write(FFile, '(');
  PrintIndent(WhileStmAST.BoolExpr.Indent);
  Write(FFile, ')');
  WriteLn(FFile, ' do');
  PrintStatement(WhileStmAST.Statement);
end;

procedure TPrinterAST.PrintForeignAsm(ForeignAsm: TForeignAsmAST);
begin
  Write(FFile, ForeignAsm.Text);
end;

end.
