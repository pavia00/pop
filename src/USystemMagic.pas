{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit USystemMagic;

interface

uses SysUtils, UAST, USimvler;

type
  TSystemMagic = class
  protected
    IndentsList:array of TIndentAST;
    function CreateType(Simvol:String):TTypeAST;
  public
    constructor Create;
    destructor Destroy;
    procedure AddIndent(Simvol:String; Kind:TIndentKind; ACase:Integer=0);
    function GetIndentKind(const Indent:TIndentAST):TIndentKind;
    function GetIndentRef(const Indent:TIndentAST):TNodeAST;
  published

  end;

var
  SystemMagic:TSystemMagic;

implementation

{ TSystemMagic }

function TSystemMagic.CreateType(Simvol:String):TTypeAST;
begin
  Result:=TTypeAST.Create;
  Result.Kind:=tkSimple;
  Result.Simple:=TTypeSimpleAST.Create(TS(Simvol));
end;

procedure TSystemMagic.AddIndent(Simvol:String; Kind:TIndentKind; ACase:Integer=0);
var
  Indent:TIndentAST;
  FuncDeclAST:TFuncDeclAST;
  TypeDeclAST:TTypeDeclAST;
  VarDeclAST:TVarDeclAST;
  ConstDeclAST:TConstDeclAST;
begin
 Indent:=TIndentAST.Create;
 SetLength(IndentsList,Length(IndentsList)+1);
 IndentsList[Length(IndentsList)-1]:=Indent;
 IndentsList[Length(IndentsList)-1].Simvol:=TS(Simvol);
 IndentsList[Length(IndentsList)-1].Kind:=Kind;
 case Kind of
 ikType:
   begin
   TypeDeclAST:=TTypeDeclAST.Create;
   TypeDeclAST.Indent:=Indent;
   TypeDeclAST._Type:=TTypeAST.Create;
   TypeDeclAST._Type.Kind:=tkSimple;
   TypeDeclAST._Type.Simple:=TTypeSimpleAST.Create(TS(Simvol));
   if CompareText(Simvol, 'String')=0 then
      begin
      TypeDeclAST._Type._Array:=TTypeArrayAST.Create;
      TypeDeclAST._Type._Array.Length:=MaxInt;
      TypeDeclAST._Type._Array._Type:=CreateType('Char');
      end;
   IndentsList[Length(IndentsList)-1].Parent:=TypeDeclAST;
   end;
 ikVar:
   begin
   VarDeclAST:=TVarDeclAST.Create;
   VarDeclAST.Indent:=Indent;
   VarDeclAST._Type:=TTypeAST.Create;
   VarDeclAST._Type.Kind:=tkUndefine;

   IndentsList[Length(IndentsList)-1].Parent:=VarDeclAST;
   end;
 ikConst:
   begin
   if CompareText(Simvol, 'Nil')=0 then
      begin
      ConstDeclAST:=TConstDeclAST.Create;
      ConstDeclAST.Indent:=Indent;
      ConstDeclAST._Type:=CreateType('Pointer');
      IndentsList[Length(IndentsList)-1].Parent:=ConstDeclAST;
      end else
      begin
      ConstDeclAST:=TConstDeclAST.Create;
      ConstDeclAST.Indent:=Indent;
      ConstDeclAST._Type:=CreateType('Boolean');
      IndentsList[Length(IndentsList)-1].Parent:=ConstDeclAST;
      end;
   end;
 ikFunction:
    begin
    FuncDeclAST:=TFuncDeclAST.Create;
    FuncDeclAST.FormalParametrs:=TFormalParametrsAST.Create;
    if ACase=1 then
      begin
        VarDeclAST:=TVarDeclAST.Create;
        VarDeclAST.Indent:=TIndentAST.Create(TS('value'));
        FuncDeclAST.FormalParametrs.Add(pmVar, VarDeclAST);
       end;
    if ACase=2 then
      begin
        VarDeclAST:=TVarDeclAST.Create;
        VarDeclAST.Indent:=TIndentAST.Create(TS('value1'));
        FuncDeclAST.FormalParametrs.Add(pmVar, VarDeclAST);
        VarDeclAST:=TVarDeclAST.Create;
        VarDeclAST.Indent:=TIndentAST.Create(TS('value2'));
        FuncDeclAST.FormalParametrs.Add(pmParam, VarDeclAST);
       end;
    FuncDeclAST.Indent:=Indent;
    IndentsList[Length(IndentsList)-1].Parent:=FuncDeclAST;
    end;
 end;
end;

constructor TSystemMagic.Create;
begin
  AddIndent('True', ikConst);
  AddIndent('False', ikConst);
  AddIndent('Nil', ikConst);
  AddIndent('Result', ikVar);
  AddIndent('UInt8', ikType);
  AddIndent('UInt16', ikType);
  AddIndent('UInt32', ikType);
  AddIndent('UInt64', ikType);
  AddIndent('Boolean', ikType);
  AddIndent('Integer', ikType);
  AddIndent('Real', ikType);
  AddIndent('Pointer', ikType);
  AddIndent('Char', ikType);
  AddIndent('String', ikType);
//  AddIndent('PByte', ikType);
//  AddIndent('PInteger', ikType);
//  AddIndent('PReal', ikType);
//  AddIndent('PChar', ikType);
  AddIndent('SizeOf', ikFunction, 1);
  AddIndent('break', ikFunction, 0);
  AddIndent('exit', ikFunction, 0);
  AddIndent('INC', ikFunction, 2);
  AddIndent('DEC', ikFunction, 2);
  AddIndent('Write', ikFunction, 1);
  AddIndent('WriteLn', ikFunction, 1);
  AddIndent('Read', ikFunction, 1);
  AddIndent('ReadLn', ikFunction, 1);
{  AddIndent('WriteStr', ikFunction, 1);
  AddIndent('WriteLnStr', ikFunction, 1);
  AddIndent('ReadStr', ikFunction, 1);
  AddIndent('ReadLnStr', ikFunction, 1);
  AddIndent('WriteInt', ikFunction, 1);
  AddIndent('WriteLnInt', ikFunction, 1);
  AddIndent('ReadInt', ikFunction, 1);
  AddIndent('ReadLnInt', ikFunction, 1);  }

end;

destructor TSystemMagic.Destroy;
var
 i:Integer;
begin
 for i:=0 to Length(IndentsList)-1 do
   IndentsList[i].Destroy;
  SetLength(IndentsList,0);
end;

function TSystemMagic.GetIndentKind(const Indent: TIndentAST): TIndentKind;
var
  Goal:TNodeAST;
begin
  Result:=ikUndefine;
  Goal:=GetIndentRef(Indent);
  if Goal=Nil then
     begin
       Result:=ikUndefine;
     end else
     begin
     Result:=(Goal as TIndentAST).Kind;
     end;
end;

function TSystemMagic.GetIndentRef(const Indent: TIndentAST): TNodeAST;
var I:Integer;
begin
  Result:=Nil;
  for I:=0 to Length(IndentsList)-1 do
    begin
    if SimvolsEq(Indent.Simvol,IndentsList[i].Simvol) then
       begin
         Result:=IndentsList[i];
         Exit;
       end;
    end;
end;

begin
  SystemMagic:=TSystemMagic.Create;
end.
