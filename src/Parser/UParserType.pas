{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UParserType;

interface

uses SysUtils, UCompiler, UAST, UFSM, UASTFabric, USimvler, UGramAnaliser,
  UHohaTypes;

type
  TTypeParser = class(TParser)
  private
    { private declarations }
  protected
    { protected declarations }
  public
     constructor Create(TheCompiler:TCompiler); Overload;
     function Parse:TTypeAST;
     procedure ErrorWay(Anchor:TSimvol);
     procedure NOP(var Node:TNodeAST);
     function  IsNop(var Proc): Boolean;
     procedure ParseArray(var Node:TNodeAST);
     procedure ParseReccord(var Node:TNodeAST);
     procedure ParseSimpleType(var Node:TNodeAST);
     procedure ParsePointer(var Node:TNodeAST);
     procedure ParseFieldDecl(var Node:TNodeAST);
     procedure ParseAliasType(var Node:TNodeAST);
    { public declarations }
  published
    { published declarations }
  end;

implementation

{ TTypeParser }


constructor TTypeParser.Create(TheCompiler: TCompiler);
begin
inherited Create(TheCompiler);
end;

procedure TTypeParser.ErrorWay(Anchor: TSimvol);
begin
  while SimvolInArray(NextSimvol,[TS(';'), TS('.'), EOFSimvol]) do
     ReadSimvol;
end;

function TTypeParser.IsNop(var Proc): Boolean;
var Ptr:Pointer;
  Proc2:TParserProcedure;
begin
  Ptr:=Pointer(Proc);
  Proc2:=Nop;
  Result:= (Ptr=@Proc2);
end;

procedure TTypeParser.NOP(var Node: TNodeAST);
begin
Node:=Nil;
end;

function TTypeParser.Parse: TTypeAST;
// ������ ��������� ����� ���������
const
  CommandCount=3;
var
  Prog:array [0..CommandCount-1] of TCaseCommand;
  procedure Init;
  begin
  Prog[0].Value:='record';           Prog[0].Woker:=ParseReccord;
  Prog[1].Value:='array';            Prog[1].Woker:=ParseArray;
  Prog[2].Value:='^';                Prog[2].Woker:=ParsePointer;
  end;

var
  Woker:TParserProcedure;
  NodeAST:TNodeAST;
begin
 Init;
 Result:=ASTFabric.TypeCreate;
 Result.Kind:=tkUndefine;
 ASTFabric.ActivLeaf:=Result;
    NodeAST:=Nil;
    Woker:=ChooseOfWork(CommandCount,@Prog, CurrentSimvol());

    if @Woker=nil then
       begin
       if IsIndentSimplyType(CurrentSimvol.Value) and (not SimvolsEQ(CurrentSimvol, TS('String'))) then
          begin
          ParseSimpleType(NodeAST);
          Result.Add(NodeAST);
          end else
          begin
          ParseAliasType(NodeAST);
          Result.Add(NodeAST);
          end;
       end else
       if Not IsNop(Woker) then
          begin
          Woker(NodeAST);
          Result.Add(NodeAST);
          end;
  ASTFabric.LevalUp;
end;

procedure TTypeParser.ParsePointer(var Node: TNodeAST);
var
  NType:TNodeAST;
begin

  NeedSimvol(TS('^'));
  NType:=Self.Parse;

  Node:=NType;
end;

procedure TTypeParser.ParseSimpleType(var Node: TNodeAST);
var
  Indent:TSimvol;
  Result:Boolean;
begin
   Result:=NeedIndent(Indent);
   if Result then
      begin
      Result:=IsIndentSimplyType(Indent.Value);
      if Result=False then Compiler.Error.Add(102,'');
      end;
  Node:=TTypeSimpleAST.Create(Indent);
end;

procedure TTypeParser.ParseArray(var Node: TNodeAST);
var
  NLength:TNodeAST;
  Length:Integer;
  ArrayType:TTypeArrayAST;
  _Type:TTypeAST;
begin
  ArrayType:=ASTFabric.NodeCreate(atTTypeArrayAST) as TTypeArrayAST;
  ASTFabric.ActivLeaf:=ArrayType;

  NeedSimvol(TS('Array'));
  NeedSimvol(TS('['));
  NeedInteger(NLength);
  NeedSimvol(TS(']'));
  NeedSimvol(TS('of'));
  _Type:=Parse;

  if Not(TryStrToInt((NLength as TSimvolAST).Simvol.Value,Length)) then
     begin
     Length:=0;
     Compiler.Error.Add(113,'');
     end;
  if Length<0 then
     begin
     Length:=0;
     Compiler.Error.Add(113,'');
     end;

  ASTFabric.LevalUp;
  ArrayType.Length:=Length;
  ArrayType._Type:=_Type;
  Node:=ArrayType;
end;

procedure TTypeParser.ParseReccord(var Node: TNodeAST);
var
  NField:TNodeAST;
  RecordType:TTypeRecordAST;
begin
  RecordType:=ASTFabric.NodeCreate(atTTypeRecordAST) as TTypeRecordAST;
  ASTFabric.ActivLeaf:=RecordType;

  NeedSimvol(TS('Record'));
   while not CheckSimvol(TS('End')) do
     begin
       ParseFieldDecl(NField);
       RecordType.Add(NField);
     end;
  NeedSimvol(TS('end'));
  ASTFabric.LevalUp;
  Node:=RecordType;
end;

procedure TTypeParser.ParseFieldDecl(var Node: TNodeAST);
var
  Field:TFieldDeclAST;
  NIndent:TNodeAST;
  _Type:TTypeAST;
begin
///
  Field:=ASTFabric.NodeCreate(atTFieldDeclAST) as TFieldDeclAST;
  ASTFabric.ActivLeaf:=Field;

    NeedIndent(NIndent);
    NeedSimvol(TS(':'));
    _Type:=Parse;
    NeedSimvol(TS(';'));
  ASTFabric.LevalUp;

  Field.Indent:=NIndent as TIndentAST;
  Field._Type:=_Type;
  Node:=Field;
end;

procedure TTypeParser.ParseAliasType(var Node: TNodeAST);
var
//  RefType:TTypeDeclAST;
  NAliasType:TNodeAST;
  NIndent:TNodeAST;
begin
  Node:=Nil;
  NeedIndent(NIndent);
  NAliasType:=ASTFabric.GetIndentRef(NIndent as TIndentAST);
  if NAliasType<> nil then
     begin
     NAliasType:=NAliasType.Parent;
     if (NAliasType<>Nil) and (NAliasType is TTypeDeclAST) then
        Node:=NAliasType
     end;
  if (Node=Nil) then
      begin
       Compiler.Error.Add(111,{����� ����� � ������ ����}'');
       ErrorWay(TS(';'));
      end;
end;

end.
