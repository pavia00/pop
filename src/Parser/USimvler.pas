{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit USimvler;

interface

uses SysUtils, Classes, Math;

// ����������� ������

type
   TSimvlerString=WideString;
   TSimvlerChar=WideChar;
   TSimvolKind=(skOther,skEndOfFile,skSpace, skWord, skNumber, skString,
                skPunctuator, skComment, skForeign);
   TSimvol=record
     Value:TSimvlerString;
     Kind:TSimvolKind;
     end;

   TSimvolList=Class(TList)
     end;
  // ���������� �� ����� � ������� ���������
   ILexer=interface
     function GetOffset:Integer;
     function GetPosCol:Integer;
     function GetPosRow:Integer;

     function CurrentChar:TSimvlerChar;
     function NextChar:TSimvlerChar;
     function ReadChar:TSimvlerChar;
     end;

   ISimvler=interface
     function GetPos:Integer;
     function CurrentSimvol:TSimvol;
     function NextSimvol:TSimvol;
     function ReadSimvol:TSimvol;
     end;

   TGetSimvol= function ():TSimvol of object;

   TSimvler = class(TInterfacedObject, ISimvler, ILexer)
     private
       FPosRow:Integer;
       FPosCol:Integer;
       FCurrentSimvol:TSimvol;
       FNextSimvol:TSimvol;
       FPos:Integer;
       FCurrentChar:TSimvlerChar;
       FNextChar:TSimvlerChar;
       function GetPosRow: Integer;
       function GetPosCol: Integer;
       function ReadStartFromPunctuation:TSimvol;
       function ReadNextSimvol:TSimvol;
       function GetOffset: Integer; Virtual; abstract;
       function GetPos: Integer;
       function ReadNextChar:TSimvlerChar; virtual; abstract;
       function ReadByte: Byte; virtual; abstract;
     protected

     public
       constructor Create; Virtual;
       destructor Destroy; override;
       procedure Reset; Virtual;
       property PosCol:Integer read GetPosCol;
       property PosRow:Integer read GetPosRow;
       property Pos:Integer read GetPos;
       function CurrentSimvol:TSimvol;
       function NextSimvol:TSimvol;
       function ReadSimvol:TSimvol;
       property Offset:Integer read GetOffset;
       function CurrentChar:TSimvlerChar;
       function NextChar:TSimvlerChar;
       function ReadChar:TSimvlerChar;
       function ReadNumber:TSimvol;
       function ReadWord:TSimvol;
       function ReadString:TSimvol;
       function ReadSpace:TSimvol;
       function ReadOther:TSimvol;
       function ReadEof:TSimvol;
       function ReadCommentary:TSimvol;
       function ReadPunctuator:TSimvol;
       function EOF:Boolean; virtual; abstract;
     end;

   TFileSimvler = class(TSimvler)
     private
       FOffset:Integer;
       FPos:Integer;
       FFile:File Of Byte;
       FCashPos:Integer;
       FCash:Array of Byte;
       FCashSize:Integer;
       function GetOffset: Integer;
       function ReadNextChar:TSimvlerChar;  override;
       function ReadByte: Byte; override;
     protected

     public
       constructor Create;
       destructor Destroy;
       procedure Assign(FilePath:String);
       procedure Reset;
       function CurrentChar:TSimvlerChar;
       function NextChar:TSimvlerChar;
       function ReadChar:TSimvlerChar;
       function EOF:Boolean; override;
     end;

const
 NilSimvol:TSimvol=(value:''; kind:skOther);
 EOFSimvol:TSimvol=(value:''; kind:skEndOfFile);
var
 RealSeparator:TSimvlerChar;


function TS(Text:WideString):TSimvol;
function SimvolsEQ(a,b:TSimvol):Boolean;
function SimvolInArray(value:TSimvol;const masiv:array of TSimvol):Boolean;
function IsIntegerNumber(const Simvol:TSimvol):Boolean;

procedure AddChar(var s:TSimvlerString; ch:TSimvlerChar);
procedure AddString(var s:TSimvlerString; sub:TSimvlerString);

type
  TimeElapsOrd=(teSpace, teWord, teNumber, teString, tePunctuator, teComment, teForeign);
var TimeElaps:array [TimeElapsOrd] of Int64;


implementation

// ������� ���������� � �������� ����������.
function RDTSC: Int64; register;
assembler;
asm
RDTSC
end;

Type
  TCharSet=Set of Char;

procedure AddChar(var s:TSimvlerString; ch:TSimvlerChar);
begin
{if s=nil then
  begin
  Setlength(s,2);
  s[0]:=0;
  end else Setlength(s,Length(s)+1);
s[Length(s)-1]:=ch;
}
s:=s+ch;
end;

procedure AddString(var s:TSimvlerString; sub:TSimvlerString);
var LenS:Integer;
begin
{if s=nil then
  begin
  Setlength(s,1);
  s[0]:=0;
  end;
LenS:=Length(s);
Setlength(s,LenS+Length(sub)-1);
Move(sub[1], s[LenS], Length(sub)-1);
}
s:=s+sub;
end;

function CharInSet(const Ch:WideChar; const CharSet:TCharSet):Boolean;
begin
if Ord(Ch)<256 then
   Result:=Char(Ch) in CharSet
   else
     Result:=False;
end;

function IsAlfa(const ch:TSimvlerChar):Boolean;
begin
Result:=CharInSet(WideChar(Ch),['A'..'Z','a'..'z']);
end;

function IsWordAlfa(const ch:TSimvlerChar):Boolean;
begin
Result:=CharInSet(WideChar(Ch),['_','A'..'Z','a'..'z']);
end;

function IsNumber(const ch:TSimvlerChar):Boolean;
begin
Result:=CharInSet(WideChar(Ch), ['0'..'9']);
end;

function IsSpace(const ch:TSimvlerChar):Boolean;
begin
Result:=CharInSet(WideChar(Ch), [#9, #10, #11, #12, #13, #32]);
end;

function IsPunctuation(const ch:TSimvlerChar):Boolean;
begin
Result:=CharInSet(WideChar(Ch), ['!', '"', '#', '$', '%', '&', '''', '(', ')', '*',
                       '+', ',', '-', '.', '/', ':', ';', '<', '=', '>',
                       '?', '@', '[', '\', ']', '^', '`', '{', '|', '}',
                       '~']);

end;

function IsOther(ch:TSimvlerChar):Boolean;
begin
Result:=Not (IsNumber(ch) or IsWordAlfa(ch) or IsSpace(ch) or IsPunctuation(ch));
end;

function SimvolsEQ(a,b:TSimvol):Boolean;
begin
  Result:=(a.Kind=b.Kind) and
  (Length(a.value)=Length(a.value)) and
  (WideCompareText(a.value, b.value)=0);
end;

function SimvolInArray(value:TSimvol;const masiv:array of TSimvol):Boolean;
var i:Integer;
begin
result:=False;
for i:=0 to Length(Masiv)-1 do
  begin
   Result:=SimvolsEQ(value,Masiv[i]);
   if (Result) then Break;
  end;
end;

function IsNil(value:TSimvol):Boolean;
begin
  Result:=SimvolsEQ(value,NilSimvol);
end;

function IsIntegerNumber(const Simvol:TSimvol):Boolean;
var ch:WideChar;
i:integer;
begin
  Result:=true;
  if Length(Simvol.Value)>=1 then
     begin
     ch:=WideChar(Simvol.Value[1]);
     if (ch='+') or (ch='-') then
        begin
        result:=True;
        if Length(Simvol.Value)=1 then Result:=False; // ���� ��� �����!
        end else if not IsNumber(TSimvlerChar(ch)) then Result:=False;

     end;
  for i:=2 to Length(Simvol.Value) do
    begin
    ch:=WideChar(Simvol.Value[i]);
    if  not IsNumber(TSimvlerChar(ch)) then
        result:=False;
    end;
end;

function TS(Text:WideString):TSimvol;
var IText:TSimvlerString;
begin
 IText:=TSimvlerString(Text);
 Result.value:=IText;
 if text='' then Result.Kind:=skEndOfFile
   else if IsWordAlfa(IText[1]) then Result.Kind:=skWord
     else if IsSpace(IText[1]) then Result.Kind:=skSpace
        else if IsNumber(IText[1]) then Result.Kind:=skNumber
           else if IsPunctuation(IText[1]) then Result.Kind:=skPunctuator
                else Result.Kind :=skOther;

end;


{ TSimvler }

constructor TSimvler.Create;
begin
  inherited;
  Reset;
end;

destructor TSimvler.Destroy;
begin

  inherited;
end;

function TSimvler.GetPosRow: Integer;
begin
  Result:=FPosRow;
end;

function TSimvler.GetPosCol: Integer;
begin
  Result:=FPosCol;
end;

function TSimvler.GetPos: Integer;
begin
  Result:=FPos;
end;

function TSimvler.NextSimvol: TSimvol;
begin
  Result:=FNextSimvol;
end;


function TSimvler.ReadCommentary:TSimvol;
  function ReadKind1:TSimvol;
  var
    s:TSimvlerString;
  begin
   S:='';
   while WideChar(CurrentChar)<>'}' do
     begin
       AddChar(S, ReadChar);
     end;
   AddChar(S, ReadChar);
   Result.value:=s;
   Result.Kind:=skComment;
  end;
  function ReadKind2:TSimvol;
  var
    s:TSimvlerString;
  const
    LineEnd=#10;
  begin
   AddChar(S, CurrentChar);
   while WideChar(NextChar)<>LineEnd do
     begin
       AddChar(S, ReadChar);
     end;
   Result.value:=s;
   Result.Kind:=skComment;
  end;
var
  t2, t1:Int64;
begin
 t1:=RDTSC;
 if (WideChar(CurrentChar)='{') then Result:=ReadKind1;
 if (WideChar(CurrentChar)='/') and (WideChar(NextChar)='/') then Result:=ReadKind2;
 t2:=RDTSC;
Inc(TimeElaps[teComment], t2-t1);

end;

function TSimvler.ReadEof: TSimvol;
begin
  Result.value:='';
  Result.Kind:=skEndOfFile;
end;

function TSimvler.ReadWord:TSimvol;
var
  s:TSimvlerString;
var
  t2, t1:Int64;
begin
 t1:=RDTSC;
 S:='';
 if IsWordAlfa(CurrentChar) then
   while (IsWordAlfa(CurrentChar)) or (IsNumber(CurrentChar)) do
     begin
       AddChar(S, ReadChar);
     end;
 Result.value:=s;
 Result.Kind:=skWord;
t2:=RDTSC;
Inc(TimeElaps[teWord], t2-t1);
end;

function TSimvler.ReadStartFromPunctuation:TSimvol;
begin
  if (WideChar(CurrentChar)='''') then Result:=ReadString
    else if (WideChar(CurrentChar)='{') then Result:=ReadCommentary
      else if (WideChar(CurrentChar)='/')then
        begin
        if (WideChar(NextChar)='/') then
            Result:=ReadCommentary
            else Result:=ReadPunctuator;
        end else Result:=ReadPunctuator;
end;

function TSimvler.ReadNextSimvol:TSimvol;
begin
  if IsWordAlfa(CurrentChar) then Result:=ReadWord
     else if IsSpace(CurrentChar) then Result:=ReadSpace
        else if IsNumber(CurrentChar) then Result:=ReadNumber
           else if IsPunctuation(CurrentChar) then Result:=ReadStartFromPunctuation
                else Result:=ReadOther;
  if Eof then Result:=ReadEof;

end;

function TSimvler.ReadNumber: TSimvol;
var
  s:TSimvlerString;
var
  t2, t1:Int64;
begin
 t1:=RDTSC;
 S:='';
 while IsNumber(CurrentChar) or (CurrentChar=RealSeparator) or IsAlfa(CurrentChar) do // IsAlfa ��� ���������������� � ������ �����
   begin
     AddChar(S, ReadChar);
   end;
 Result.value:=s;
 Result.Kind:=skNumber;

 t2:=RDTSC;
Inc(TimeElaps[teNumber], t2-t1);
end;

function TSimvler.ReadOther: TSimvol;
var
  s:TSimvlerString;
begin
 S:='';
 while IsOther(CurrentChar) and (not EOF) do
   begin
     AddChar(S, ReadChar);
   end;
 Result.value:=s;
 Result.Kind:=skOther;
end;

function TSimvler.ReadPunctuator: TSimvol;
var
  s:TSimvlerString;
  ch1,ch2:TSimvlerChar;
var
  t2, t1:Int64;
begin
 t1:=RDTSC;
 S:='';
 if IsPunctuation(CurrentChar) then
     begin
     ch1:=ReadChar;
     AddChar(S, Ch1);
     Ch2:=CurrentChar;
     if IsPunctuation(Ch2) then
         begin

         if ((WideChar(ch1)='<') and (WideChar(ch2)='>')) or
            ((WideChar(ch1)='<') and (WideChar(ch2)='=')) or
            ((WideChar(ch1)='>') and (WideChar(ch2)='=')) or
            ((WideChar(ch1)=':') and (WideChar(ch2)='=')) then
             begin
             Ch2:=ReadChar;
             AddChar(S, Ch2);
             end;
         end;
     end;

 Result.value:=s;
 Result.Kind:=skPunctuator;
t2:=RDTSC;
Inc(TimeElaps[tePunctuator], t2-t1);
end;

function TSimvler.ReadSimvol: TSimvol;
begin
 Result:=FCurrentSimvol;
 FCurrentSimvol:=FNextSimvol;
 FNextSimvol:=ReadNextSimvol;
 Inc(FPos);
end;

function TSimvler.ReadSpace: TSimvol;
var
  s:TSimvlerString;
var
  t2, t1:Int64;
begin
 t1:=RDTSC;
 S:='';
 while IsSpace(CurrentChar) do
   begin
     AddChar(S, ReadChar);
   end;
 Result.value:=s;
 Result.Kind:=skSpace;
t2:=RDTSC;
Inc(TimeElaps[teSpace], t2-t1);
end;

function TSimvler.ReadString: TSimvol;
var
  s:TSimvlerString;
var
  t2, t1:Int64;
begin
 t1:=RDTSC;
 AddChar(S, ReadChar);
 repeat
   AddChar(S, ReadChar);
 until WideChar(CurrentChar)='''';
 AddChar(S, ReadChar);
 Result.value:=s;
 Result.Kind:=skString;
t2:=RDTSC;
Inc(TimeElaps[teString], t2-t1);
end;

procedure TSimvler.Reset;
begin
  FPosRow:=1;
  FPosCol:=1;
  FPos:=0;
  FCurrentSimvol:=NilSimvol;

TimeElaps[teSpace]:=0;
TimeElaps[teWord]:=0;
TimeElaps[teNumber]:=0;
TimeElaps[teString]:=0;
TimeElaps[tePunctuator]:=0;
TimeElaps[teComment]:=0;
TimeElaps[teForeign]:=0;
end;

function TSimvler.CurrentSimvol: TSimvol;
begin
Result:=FCurrentSimvol;
end;

function TSimvler.CurrentChar: TSimvlerChar;
begin
  Result:=FCurrentChar;
end;

function TSimvler.NextChar: TSimvlerChar;
begin
  Result:=FNextChar;
end;

function TSimvler.ReadChar: TSimvlerChar;
begin
  Result:=FCurrentChar;
  FCurrentChar:=FNextChar;
  FNextChar:=ReadNextChar;
  if WideChar(FCurrentChar)=#10 then
     begin
       Inc(FPosRow);
       FPosCol:=0;
     end;
end;

{function TSimvler.ReadNextChar: TSimvlerChar;
begin
  Result:=#0;
end;
}

{ TFileSimvler }

procedure TFileSimvler.Assign(FilePath:String);
begin
  System.AssignFile(FFile,FilePath);
end;

constructor TFileSimvler.Create;
begin
  FcashSize:=256*1024;
  SetLength(Fcash,FcashSize);
  FPos:=0;
  FPosRow:=1;
  FCashPos:=0;
  FOffset:=0;
  FCurrentChar:=TSimvlerChar(#0);
  FNextChar:=TSimvlerChar(#0);

end;

function TFileSimvler.CurrentChar: TSimvlerChar;
begin
  Result:=FCurrentChar;
end;

destructor TFileSimvler.Destroy;
begin

end;

function TFileSimvler.EOF: Boolean;
begin
Result:= FOffset>FileSize(FFile)
//Result:=System.EOF(FFile);
end;

function TFileSimvler.GetOffset: Integer;
begin
  Result:=FOffset;
end;

function TFileSimvler.NextChar: TSimvlerChar;
begin
  Result:=FNextChar;
end;

function TFileSimvler.ReadChar: TSimvlerChar;
begin
  Result:=FCurrentChar;
  FCurrentChar:=FNextChar;
  FNextChar:=ReadNextChar;
  if WideChar(FCurrentChar)=#10 then
     begin
       Inc(FPosRow);
       FPosCol:=0;
     end;
end;

function TFileSimvler.ReadNextChar: TSimvlerChar;
var B0, B1, B2:Byte;
begin

  repeat
  B0:=ReadByte;
  until (B0 and $C0<>$D0); // UTF-8 ���� �������������.

if (B0 and $80)=$00 then
   begin
   Result:=TSimvlerChar(B0);
   end else
   if (B0 and $E0)=$C0 then
   begin
   B1:=ReadByte;
   Result:=TSimvlerChar((B1 and $3F) + (B0 and $1F) shl 6);
   end else
   if (B0 and $F0)=$E0 then
   begin
   B1:=ReadByte;
   B2:=ReadByte;
   Result:=TSimvlerChar((B2 and $3F) + (B1 and $3F) shl 6 + (B0 and $1F) shl 12);
   end;
Inc(FPosCol);
end;

function TFileSimvler.ReadByte: Byte;
begin
Inc(FOffset);
Inc(FCashPos);
if FCashPos>=FCashSize then
   begin
     FCashPos:=0;
     BlockRead(FFile,FCash[0], Min(FileSize(FFile)-FOffset, FCashSize));
   end;
Result:=FCash[FCashPos];
end;

procedure TFileSimvler.Reset;
  function IsBOM:Boolean;
  begin
  Result:=False;
  // UTF-8
  If (FCash[0]=$EF) and
     (FCash[1]=$BB) and
     (FCash[2]=$BF) then
      Result:=True;
  end;
  procedure ReadBOM;
  begin
  ReadChar;
  ReadChar;
  ReadChar;
  end;
begin
TimeElaps[teSpace]:=0;
TimeElaps[teWord]:=0;
TimeElaps[teNumber]:=0;
TimeElaps[teString]:=0;
TimeElaps[tePunctuator]:=0;
TimeElaps[teComment]:=0;
TimeElaps[teForeign]:=0;

  System.Reset(FFile);
  FPos:=0;
  FOffset:=0;
  FillChar(FCash[0], FCashSize, 0);
  BlockRead(FFile,FCash[0], Min(FileSize(FFile), FCashSize));

  FCurrentChar:=TSimvlerChar(#0);
  FNextChar:=TSimvlerChar(#0);
  If FileSize(FFile)>1 then FCurrentChar:=TSimvlerChar(FCash[0]);
  If FileSize(FFile)>2 then FNextChar:=TSimvlerChar(FCash[1]);
  FCashPos:=1;

  If IsBOM then
     ReadBOM;

  FCurrentSimvol:= ReadNextSimvol;
  FNextSimvol:= ReadNextSimvol;

end;

begin
RealSeparator:='.';
end.

