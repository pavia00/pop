{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UFSM;

interface

uses UAST, USimvler;

type
  TParserProcedure= procedure (var Node:TNodeAST) of object;
  TCheckProcedure = function ():Boolean of object;
  TReadProcedure  = function ():TSimvol of object;

  PCaseCommand=^TCaseCommand;
  TCaseCommand=record
     Value:String;
     Woker:TParserProcedure;
     end;
  TLevalCommand=record
     Check:TCheckProcedure;
     Woker:TReadProcedure;
     end;

function ChooseOfWork(Count:Integer; Prog:PCaseCommand; Signal:TSimvol):TParserProcedure;

implementation

function ChooseOfWork(Count:Integer; Prog:PCaseCommand; Signal:TSimvol):TParserProcedure;
var
 I:Integer;
 ComanPointer:PCaseCommand;
begin
  Result:=nil;
  ComanPointer:=Prog;
  for I:=0 to Count-1 do
     begin
      if SimvolsEQ(Signal,TS(ComanPointer.Value)) then
        begin
        Result:=ComanPointer.Woker;
        Break;
        end;
      Inc(ComanPointer);
     end;
end;


end.
 