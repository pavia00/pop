unit UParserAsm;

interface

uses LIParser, UCompiler, UAST, USimvler, UGramAnaliser, UASTFabric;

 Type
   TForeignAsmParser = class(TParser)
   private
     { private declarations }
   protected
     { protected declarations }
   public
     Compiler:TCompiler;
     function Parse:TForeignAsmAST;
     constructor Create(TheCompiler:TCompiler); Overload;
     Destructor Destroy; Overload;
     { public declarations }
   published
     { published declarations }
   end;

implementation

{ TUnitParser }

constructor TForeignAsmParser.Create(TheCompiler: TCompiler);
var
  LexAnaliser:TSimvler;
begin
  inherited Create(TheCompiler);
  Compiler:=TheCompiler;
  LexAnaliser:=Compiler.LexAnaliser as TSimvler;
  // �����������������
  // �������� ���� � ����������� ���� ����� ���.
  {self.NextSimvol:=LexAnaliser.NextSimvol;
  self.ReadSimvol:=LexAnaliser.ReadSimvol;
  self.CurrentSimvol:=LexAnaliser.CurrentSimvol;}
end;

destructor TForeignAsmParser.Destroy;
begin
end;

function TForeignAsmParser.Parse:TForeignAsmAST;
var FSimvol:TSimvol;
begin
  Result:=ASTFabric.ForeignAsmCreate;
  ASTFabric.ActivLeaf:=Result;
  NeedSimvol(TS('Asm'));
  FSimvol:=CurrentSimvol;
  if FSimvol.kind=skForeign then
     begin
     FSimvol:=ReadSimvol;
     Result.Text:=FSimvol.Value;
     end else Compiler.Error.Add(116, '');  // ����� ����������� � ����������.
  NeedSimvol(TS('end'));
  ASTFabric.LevalUp;
end;

end.
