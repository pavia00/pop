{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UParserFactor;

interface

uses UCompiler, LIParser, UAST, USimvler, UGramAnaliser, UASTFabric;

type
  TFactorParser = class(TParser)
  private
    Stack:TStackAST;
    { private declarations }
  protected
    { protected declarations }
  public
     ParseExpression:TParseNode;
     ParseExpressionList:TParseNode;
     constructor Create(TheCompiler:TCompiler); Overload;
     function  Parse(Anchor:TSimvol):TFactorAST;
     procedure ErrorWay(Anchor:TSimvol);
     procedure NOP(var Node:TNodeAST);
     procedure ParseWord(var Node:TNodeAST);
     procedure ParseImmNumber(var Node:TNodeAST);
     procedure ParseImmString(var Node:TNodeAST);
     procedure ParseImmNil(var Node:TNodeAST);
     procedure ParseAddr(var Node:TNodeAST);
     procedure ParseSignature(var Node:TNodeAST);
     procedure ParseDesignator(var Node:TNodeAST);
    { public declarations }
  published
    { published declarations }
  end;

implementation

{ TFactorParser }

constructor TFactorParser.Create(TheCompiler: TCompiler);
begin
inherited Create(TheCompiler);
 Stack:=TheCompiler.ParserStack as TStackAST;
end;

procedure TFactorParser.ErrorWay(Anchor: TSimvol);
begin

end;

procedure TFactorParser.NOP(var Node: TNodeAST);
begin
// ������ ��������
end;

function TFactorParser.Parse(Anchor: TSimvol): TFactorAST;
var
  NodeAST:TNodeAST;
begin
  Result:=ASTFabric.NodeCreate(atTFactorAST) as TFactorAST;
  ASTFabric.ActivLeaf:=Result;
  Result.Kind:=fkImmConst;

  case CurrentSimvol.Kind of
  skWord:ParseWord(NodeAST);
  skNumber:ParseImmNumber(NodeAST);
  skString:ParseImmString(NodeAST);
  skPunctuator:ParseAddr(NodeAST);
  else
    ErrorWay(Anchor);
  end; // case
  Result.Add(NodeAST);
  ASTFabric.LevalUp;

end;

procedure TFactorParser.ParseAddr(var Node: TNodeAST);
var
   ImmConst:TImmConstAST;
   Addr:TAddrAST;
   NSignature:TNodeAST;
   NIndent:TNodeAST;
begin
 ImmConst:=ASTFabric.NodeCreate(atTImmConstAST) as TImmConstAST;
 ASTFabric.ActivLeaf:=ImmConst;
 if SimvolsEQ(CurrentSimvol,ts('@')) then
    begin
    Addr:=TAddrAST.Create;
      ReadSimvol;

    NeedIndent(NIndent);
    Stack.Push(NIndent);
    ParseSignature(NSignature);
    Addr.Signature:=NSignature as TSignatureAST;
    ImmConst.Add(Addr);
    end;
  ASTFabric.LevalUp;
  Node:=ImmConst;
end;

procedure TFactorParser.ParseImmNumber(var Node: TNodeAST);
var
   ImmConst:TImmConstAST;
   RealNumber:TRealAST;
   IntNumber:TIntegerAST;
   Simvol:TSimvol;
begin
  ImmConst:=ASTFabric.NodeCreate(atTImmConstAST) as TImmConstAST;
  ASTFabric.ActivLeaf:=ImmConst;
  Simvol:=ReadSimvol;
    if pos(RealSeparator, Simvol.Value)<>0 then
       begin
       RealNumber:=TRealAST.Create;
       RealNumber.Simvol:=Simvol;
       ImmConst.Add(RealNumber);
       end else
       begin
       IntNumber:=TIntegerAST.Create;
       IntNumber.Simvol:=Simvol;
       ImmConst.Add(IntNumber);
       end;
  ASTFabric.LevalUp;
  Node:=ImmConst;
end;

procedure TFactorParser.ParseImmString(var Node: TNodeAST);
var
   ImmConst:TImmConstAST;
   ImmString:TStringAST;
   ImmChar:TCharAST;
begin
  ImmConst:=ASTFabric.NodeCreate(atTImmConstAST) as TImmConstAST;
  ASTFabric.ActivLeaf:=ImmConst;
  if Length(CurrentSimvol.Value)=1 then
    begin
    ImmChar:=TCharAST.Create;
    ImmChar.Simvol:=ReadSimvol;
    ImmConst.Add(ImmChar);
    end else
    begin
    ImmString:=TStringAST.Create;
    ImmString.Simvol:=ReadSimvol;
    ImmConst.Add(ImmString);
    end;
  ASTFabric.LevalUp;
  Node:=ImmConst;
end;

procedure TFactorParser.ParseImmNil(var Node: TNodeAST);
var
   ImmConst:TImmConstAST;
   NilWord:TNilAST;
begin
  ImmConst:=ASTFabric.NodeCreate(atTImmConstAST) as TImmConstAST;
  ASTFabric.ActivLeaf:=ImmConst;
  NilWord:=TNilAST.Create;
    ReadSimvol;
  ImmConst.Add(NilWord);
  ASTFabric.LevalUp;
  Node:=ImmConst;
end;

procedure TFactorParser.ParseWord(var Node: TNodeAST);
var
   NSignature:TNodeAST;
   NIndent:TNodeAST;
begin
  Node:=Nil;
  if SimvolsEQ(CurrentSimvol,ts('Nil')) then
     begin
       ParseImmNil(Node);
     end else
     begin
       if IsKeyWord(CurrentSimvol.Value) then
          Compiler.Error.Add(109,CurrentSimvol.Value)// ����� ���������� // �������� �� ���������
          else
          begin
            NeedIndent(NIndent);
            Stack.Push(NIndent);
            ParseSignature(NSignature);
            Node:=NSignature;
          end;
    end;
end;

procedure TFactorParser.ParseDesignator(var Node: TNodeAST);
var
  Designator:TDesignatorAST;
  NSignature:TNodeAST;
  NExper:TNodeAST;
  NExperList:TNodeAST;
  NSubDesignator:TNodeAST;
  NIndent:TNodeAST;
begin
  Designator:=TDesignatorAST.Create;
  Designator.Kind:=dkSignature;
  if CheckSimvol(TS('.')) then
     begin
       NeedSimvol(TS('.'));
       NeedIndent(NIndent);
       Stack.Push(NIndent);
       ParseSignature(NSignature);
       Designator.Kind:=dkDeSignatureDot;
       Designator.Signature:=NSignature as TSignatureAST;
     end;
  if CheckSimvol(TS('^')) then
     begin
       {!��� ������ ���������� ������������ ���������� ��������}
       NeedSimvol(TS('^'));
       Designator.Kind:=dkDeSignature;
       Stack.Push(Designator);
        ParseDesignator(NSubDesignator);
       Stack.Pop;
       Designator.SubDesignator:=NSubDesignator as TDesignatorAST;;
     end;
  if CheckSimvol(TS('[')) then
     begin
       {!��� ������ ���������� ������������ ���������� ��������}
       NeedSimvol(TS('['));
       Designator.Kind:=dkDeSignatureArray;
       Stack.Push(Designator);
       ParseExpression(NExper);
       Stack.pop;
       NeedSimvol(TS(']'));
       ParseDesignator(NSubDesignator);
       Designator.Expression:=NExper as TExpressionAST;
       Designator.SubDesignator:=NSubDesignator as TDesignatorAST;
     end;
  if CheckSimvol(TS('(')) then
     begin
       {!��� ������ ���������� ������������ ���������� ��������}
       NeedSimvol(TS('('));
       Designator.Kind:=dkDeSignatureFunc;
       Stack.Push(Designator);
       ParseExpressionList(NExperList);
       Stack.pop;
       NeedSimvol(TS(')'));
       ParseDesignator(NSubDesignator);
       Designator.ExpressionList:=NExperList as TExpressionListAST;
       Designator.SubDesignator:=NSubDesignator as TDesignatorAST;
     end;
   Node:=Designator;
end;

procedure TFactorParser.ParseSignature(var Node: TNodeAST);
var
  Signature:TSignatureAST;
  Designator:TDesignatorAST;
  IndentRef:TNodeAST;
  NIndent:TNodeAST;
  NSignature:TNodeAST;
  NDesignator:TNodeAST;
  IndentDeclAST:TIndentDeclAST;
begin
  Signature:=TSignatureAST.Create;
    NIndent:=Stack.Pop;
    NSignature:=Stack.Pop;
    if (NSignature is TDesignatorAST) then
         begin
         Designator:=NSignature as TDesignatorAST;
         if (Designator.Kind=dkDesignature) then
             NSignature := Stack.Pop;
         end;
    if (NSignature is TSignatureAST) then
       begin
       IndentRef:=ASTFabric.GetIndentRef(NSignature as TSignatureAST, NIndent as TIndentAST);
       end else
         begin
         Stack.Push(NSignature);
         NSignature:=Nil;
         IndentRef:=ASTFabric.GetIndentRef(NIndent as TIndentAST);
         end;

    if  (IndentRef=nil) then
       begin
       //Create undefine DeclRef
       IndentDeclAST:=TIndentDeclAST.Create;
       IndentDeclAST.Indent:=TIndentAST.Create((NIndent as TSimvolAST).Simvol);
       IndentRef:=IndentDeclAST.Indent;
       IndentRef.Parent:=IndentDeclAST;
       Compiler.Error.Add(107, IndentDeclAST.Indent.Simvol.Value);
       //Do error cheking on Semantik step;
       end;
    Signature.IndentDeclRef:=IndentRef.Parent as TIndentDeclAST;

    Stack.Push(Signature);
    ParseDesignator(NDesignator);
    Stack.Pop;
    Signature.Designator:=NDesignator as TDesignatorAST;
  Node:=Signature;
end;

end.
