{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UParserImplementation;

interface

uses UCompiler, UAST, UFSM, UASTFabric, USimvler, UGramAnaliser,
  UParserDeclarations, UParserStatement, UParserAsm;

type
  TImplemtSectionParser = class(TParser)
  private
    { private declarations }
  protected
    { protected declarations }
  public
     DeclSectionParser:TDeclSectionParser;
     StatementParser:TStatementParser;
     ForeignAsmParser:TForeignAsmParser;
     constructor Create(TheCompiler:TCompiler); Overload;
     function Parse(Anchor:TSimvol):TImplimentaionAST;
     procedure ErrorWay(Anchor:TSimvol);
     procedure MainBlock(var Node:TNodeAST);
     procedure ImplFunction(var Node:TNodeAST);
     procedure ImplProcedure(var Node:TNodeAST);
     procedure AsmBlock(var Node: TNodeAST);
     procedure ParseAsm(var Node: TNodeAST);
     procedure NOP(var Node:TNodeAST);
     function  IsNop(var Proc): Boolean;

    { public declarations }
  published
    { published declarations }
  end;

implementation

{ TImplementationsParser }

constructor TImplemtSectionParser.Create(TheCompiler: TCompiler);
begin
inherited Create(TheCompiler);
 StatementParser:=TStatementParser.Create(TheCompiler);
 ForeignAsmParser:=TForeignAsmParser.Create(TheCompiler);
end;

procedure TImplemtSectionParser.ErrorWay(Anchor: TSimvol);
begin
  repeat
     ReadSimvol;
  until SimvolInArray(CurrentSimvol,[Anchor, TS(';'), TS('.'), EOFSimvol]);
  if CheckSimvol(TS(';')) then
     ReadSimvol;
end;

procedure TImplemtSectionParser.ImplFunction(var Node: TNodeAST);
var
  NHeading:TNodeAST;
  FuncImpAST:TFuncImpAST;
  LocalDecl:TDeclarationAST;
  CompaundStatement:TNodeAST;
  NAsmBlock:TNodeAST;  
begin
  FuncImpAST:=ASTFabric.FuncImpCreate;
    ASTFabric.ActivLeaf:=FuncImpAST;
      DeclSectionParser.FunctionHeading(NHeading);

      DeclSectionParser.ResultExist:=True;
      DeclSectionParser.ResultType:=(NHeading as TFuncDeclAST).result;
  FuncImpAST.FuncDecl:=NHeading as TFuncDeclAST;
      if  FuncImpAST.FuncDecl.Attr.Imp=faHoha then
          begin
          LocalDecl:=DeclSectionParser.Parse(TS('Begin'));
          FuncImpAST.LocalDec:=LocalDecl;
          StatementParser.ParseCompaundStatement(CompaundStatement);
          FuncImpAST.CompaundStatement:=CompaundStatement as TCompaundStatementAST;
          end;
      if  FuncImpAST.FuncDecl.Attr.Imp=faAssemble then
          begin
          LocalDecl:=DeclSectionParser.Parse(TS('asm'));
          FuncImpAST.LocalDec:=LocalDecl;
          ParseAsm(NAsmBlock);
          FuncImpAST.AsmBlock:=NAsmBlock as TForeignAsmAST;
          end;
      NeedSimvol(TS(';'));
    ASTFabric.LevalUp;
  Node:=FuncImpAST;

end;

procedure TImplemtSectionParser.ImplProcedure(var Node: TNodeAST);
var
  NHeading:TNodeAST;
  FuncImpAST:TFuncImpAST;
  LocalDecl:TDeclarationAST;
  CompaundStatement:TNodeAST;
  NAsmBlock:TNodeAST;
begin
  FuncImpAST:=ASTFabric.FuncImpCreate;
    ASTFabric.ActivLeaf:=FuncImpAST;
      DeclSectionParser.ProcedureHeading(NHeading);
      DeclSectionParser.ResultExist:=False;
      DeclSectionParser.ResultType:=Nil;
  FuncImpAST.FuncDecl:=NHeading as TFuncDeclAST;
      if  FuncImpAST.FuncDecl.Attr.Imp=faHoha then
          begin
          LocalDecl:=DeclSectionParser.Parse(TS('Begin'));
          FuncImpAST.LocalDec:=LocalDecl;
          StatementParser.ParseCompaundStatement(CompaundStatement);
          FuncImpAST.CompaundStatement:=CompaundStatement as TCompaundStatementAST;
          end;
      if  FuncImpAST.FuncDecl.Attr.Imp=faAssemble then
          begin
          LocalDecl:=DeclSectionParser.Parse(TS('asm'));
          FuncImpAST.LocalDec:=LocalDecl;
          ParseAsm(NAsmBlock);
          FuncImpAST.AsmBlock:=NAsmBlock as TForeignAsmAST;
          end;
      NeedSimvol(TS(';'));
    ASTFabric.LevalUp;
  Node:=FuncImpAST;
end;

function TImplemtSectionParser.IsNop(var Proc): Boolean;
var Ptr:Pointer;
  Proc2:TParserProcedure;
begin
  Ptr:=Pointer(Proc);
  Proc2:=Nop;
  Result:= (Ptr=@Proc2);
end;

procedure TImplemtSectionParser.MainBlock(var Node: TNodeAST);
begin
  StatementParser.ParseCompaundStatement(Node);
  //NeedSimvol(TS('.'));
end;
{var
  CompaundStatement:TNodeAST;
  MainBlockAST:TMainBlockAST;
begin
  MainBlockAST:=TMainBlockAST.Create;

    StatementParser.ParseCompaundStatement(CompaundStatement);
    NeedSimvol(TS('.'));

  MainBlockAST.CompaundStatement:=CompaundStatement;
  Node:=MainBlockAST;
end;  }

procedure TImplemtSectionParser.NOP(var Node: TNodeAST);
begin
// ������ �� ������
end;

function TImplemtSectionParser.Parse(Anchor: TSimvol): TImplimentaionAST;
// ������ ��������� ����� ���������
const
  CommandCount=5;
var
  Prog:array [0..CommandCount-1] of TCaseCommand;
  procedure Init;
  begin
    Prog[0].Value:='function'; Prog[0].Woker:=ImplFunction;
    Prog[1].Value:='procedure'; Prog[1].Woker:=ImplProcedure;
    Prog[2].Value:='Begin'; Prog[2].Woker:=MainBlock;
    Prog[3].Value:='End';   Prog[3].Woker:=Nop;
    Prog[4].Value:='asm';   Prog[4].Woker:=AsmBlock;
  end;
var
  Woker:TParserProcedure;
  NodeAST:TNodeAST;
begin
 Init;
 Self.Anchor:=Anchor;

 Result:=ASTFabric.ImplimentaionCreate;
 ASTFabric.ActivLeaf:=Result;
 NeedSimvol(TS('Implementation'));
  repeat
    NodeAST:=Nil;
    Woker:=ChooseOfWork(CommandCount,@Prog, CurrentSimvol());

    if @Woker=nil then
       begin
       Compiler.Error.Add(114, CurrentSimvol().Value);
       ErrorWay(Anchor);
       end else
       if Not IsNop(Woker) then
          begin
          Woker(NodeAST);
          Result.Add(NodeAST);
          end;

  until  (IsAnyAnchor) or (IsNop(Woker));
  if  Result.MainBlock=nil then
      NeedSimvol(TS('end'));
  ASTFabric.LevalUp;
  //  if (Result.ListFunc=Nil) and (Result.MainBlock=nil) then
//      FreeAndNil(Result);
end;


procedure TImplemtSectionParser.ParseAsm(var Node: TNodeAST);
begin
  Node:=ForeignAsmParser.Parse;
end;

procedure TImplemtSectionParser.AsmBlock(var Node: TNodeAST);
begin
  ParseAsm(Node);
  NeedSimvol(TS(';'));
end;


end.
