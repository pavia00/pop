{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UGramAnaliser;

interface

uses SysUtils, UAST, LICompiler, UCompiler, USimvler, UASTFabric, UFSM;


type
  TParser = class
  private
    { private declarations }
  protected
     Anchor:TSimvol;
     NextSimvol:TGetSimvol;
     ReadSimvol:TGetSimvol;
     CurrentSimvol:TGetSimvol;
     Compiler:TCompiler;
     function  NeedSimvol(Simvol:TSimvol):Boolean;
     function  CheckSimvol(Simvol:TSimvol):Boolean;
     function  NeedIndent(var Indent:TSimvol): Boolean; overload;
     procedure NeedIndent(var Node:TNodeAST); overload;
     function  NeedNumber(var Number:TSimvol): Boolean; overload;
     procedure NeedInteger(var Node:TNodeAST);
 //    procedure NeedNumber(var Node:TNodeAST); overload;
     function  IsAnyAnchor:Boolean;
    { protected declarations }
  public
     constructor Create(TheCompiler:TCompiler);  virtual;
     destructor Destroy; Overload;
    { public declarations }
  published
    { published declarations }
  end;

  TGramAnaliser = class(TCompilerWoker)
  private
    FPosRow:array [-1..1] of Integer;
    FPosCol:array [-1..1] of Integer;
    FUnitParser:TObject;
    FCurrentSimvol:TSimvol;
    FForeignAsm:Boolean;
    FNextSimvol:TSimvol;
    function ReadNextSimvol:TSimvol;
    function GetPosRow: Integer;
    function GetPosCol: Integer;
    function GetLastPosRow: Integer;
    function GetLastPosCol: Integer;

    { private declarations }
  protected
    { protected declarations }
  public
    { public declarations }
    Compiler:TCompiler;
    AST:TUnitAST;
    property UnitParser:TObject read FUnitParser write FUnitParser;
    property PosCol:Integer read GetPosCol;
    property PosRow:Integer read GetPosRow;
    property LastPosCol:Integer read GetLastPosCol;
    property LastPosRow:Integer read GetLastPosRow;
    constructor Create(TheCompiler:TCompiler); Overload;
    destructor Destroy; Overload;
    procedure Process; override;
    procedure Reset; override;
    function CurrentSimvol:TSimvol;
    function NextSimvol:TSimvol;
    function ReadSimvol:TSimvol;
    procedure FilterComment;
    function  ReadNumber:TSimvol;
    function  ReadString:TSimvol;
    function  ReadChar:TSimvol;
    function  ReadOther:TSimvol;
    function  ReadForeignAsm: TSimvol;
    function  HavForeignASM:Boolean;
    function  HavNumber:Boolean;
    function  HavString:Boolean;
    function  HavOther:Boolean;
  published
    { published declarations }
  end;

function IsKeyWord(Value:WideString):Boolean;
function IsExpressOperator(Value:WideString):Boolean;
////////
///
type
   TSubProgramm = class(TParser)
   private
     Name:WideString;
     { private declarations }
   protected
     { protected declarations }
   public
     { public declarations }
   published
     { published declarations }
   end;


implementation

uses UParserUnit;

function IsKeyWord(Value:WideString):Boolean;
const
 KeyWordsList:array [1..38] of WideString=
  (                                                            //0
   'Declaration','Implementation','Begin','End', 'uses',       //5
   'Const', 'Type','VAR', 'Label', 'function', 'procedure',    //11
   'array', 'record', 'asm', 'assemble', 'stdcall',            //16
   'if', 'then', 'else', 'for', 'to', 'downto', 'do',          //23
   'repeat', 'until', 'while', 'goto', 'case', 'of',           //29
   'shl', 'shr', 'mod', 'div', 'not', 'or', 'and', 'xor',      //37
   'nil'                                                       //38
  );                                                           //38
  //result �������� ������ �����������, ��� ����� ������������ � procedure � main
  // ��� ������� ����������. � ��������� �� �������� ������ ��� �������.
var
  i:Integer;
begin
Result:=False;

for i :=Low(KeyWordsList)  to High(KeyWordsList)  do
  begin
  // Compares value without case sensitivity.
  if WideCompareText(KeyWordsList[i],Value)=0 then
    begin
      Result:=True;
      Break;
    end;
  end;
end;

function IsExpressOperator(Value:WideString):Boolean;
const
 OperatorList:array [1..18] of WideString=
  (                                                            //0
   'not', 'or', 'xor', 'and', '+', '-', '*', 'div', 'mod',     //9
   '<', '>', '<=', '>=', '=', '<>', '/', 'shl', 'shr'          //18                                               //11
  );                                                           //18
var
  i:Integer;
begin
Result:=False;

for i :=Low(OperatorList)  to High(OperatorList)  do
  begin
  // Compares value without case sensitivity.
  if WideCompareText(OperatorList[i],Value)=0 then
    begin
      Result:=True;
      Break;
    end;
  end;
end;

{ TParser }


constructor TParser.Create(TheCompiler: TCompiler);
var
 GramAnaliser:TGramAnaliser;
begin
  Compiler:=TheCompiler;
  GramAnaliser:=Compiler.GramAnaliser as TGramAnaliser;
  self.NextSimvol:=GramAnaliser.NextSimvol;
  self.ReadSimvol:=GramAnaliser.ReadSimvol;
  self.CurrentSimvol:=GramAnaliser.CurrentSimvol;
end;

destructor TParser.Destroy;
begin

end;

function TParser.NeedIndent(var Indent:TSimvol): Boolean;
begin
Result:=False;
if CurrentSimvol.Kind=skWord then
   begin
     if not IsKeyWord(CurrentSimvol.value) then
        begin
        Indent:=ReadSimvol;
        Result:=True;
        end else
        Compiler.Error.Add(101,'');
   end else
    begin
      Compiler.Error.Add(101,'')
    end;
end;

procedure TParser.NeedIndent(var Node: TNodeAST);
var
  IndentSimvol:TSimvol;
  Indent:TIndentAST;
begin
NeedIndent(IndentSimvol);
Indent:=ASTFabric.IndentCreate(IndentSimvol);
Indent.Pos:=(Compiler.LexAnaliser as TFileSimvler).Pos;
Indent.Kind:=ASTFabric.IndentKind;
Node:=Indent;
end;

function TParser.NeedNumber(var Number:TSimvol): Boolean;
begin
Result:=False;
if CurrentSimvol.Kind=skNumber then
   begin
     Number:=ReadSimvol;
     Result:=True;
   end else
    begin
      Compiler.Error.Add(112,'')
    end;
end;

procedure TParser.NeedInteger(var Node: TNodeAST);
var
  NumberSimvol:TSimvol;
  Integer:TIntegerAST;
begin
NeedNumber(NumberSimvol);
if pos(RealSeparator,NumberSimvol.Value)=0 then
   begin
   Integer:=ASTFabric.IntegerCreate(NumberSimvol);
   Node:=Integer;
   end else
   begin
   Compiler.Error.Add(113,''); {����� ����� �����}
   NumberSimvol.Value:='0';
   Integer:=ASTFabric.IntegerCreate(NumberSimvol);
   Node:=Integer;
   end;
end;

{procedure TParser.NeedNumber(var Node: TNodeAST);
var
  NumberSimvol:TSimvol;
  Number:TNumberAST;
begin
NeedNumber(NumberSimvol);
Number:=ASTFabric.NumberCreate(NumberSimvol);
Node:=Number;
end;        }

function TParser.NeedSimvol(Simvol: TSimvol): Boolean;
begin
if SimvolsEQ(CurrentSimvol(),Simvol) then
   begin
   ReadSimvol;
   Result:=True;
   end else
   begin
     Compiler.Error.Add(100,Simvol.Value);
     ReadSimvol;
     Result:=False;
   end;
end;

function TParser.CheckSimvol(Simvol: TSimvol): Boolean;
begin
if SimvolsEQ(CurrentSimvol(),Simvol) then
   begin
   Result:=True;
   end else
   begin
     Result:=False;
   end;
end;


function TParser.IsAnyAnchor: Boolean;
begin
  result:=SimvolInArray(CurrentSimvol,[Anchor, NilSimvol, EOFSimvol]);
end;

{ TGramAnaliser }

constructor TGramAnaliser.Create(TheCompiler:TCompiler);
begin
  Compiler:=TheCompiler;
  FCurrentSimvol:=NilSimvol;
  FNextSimvol:=NilSimvol;
  FForeignAsm:=False;
end;

destructor TGramAnaliser.Destroy;
begin

  inherited;
end;

procedure TGramAnaliser.Process;
begin
   AST:=(FUnitParser as TUnitParser).Parse;
end;

function TGramAnaliser.NextSimvol: TSimvol;
begin
Result:=FNextSimvol;
end;

function TGramAnaliser.ReadSimvol: TSimvol;
begin
FPosRow[-1]:=FPosRow[0];
FPosCol[-1]:=FPosCol[0];
FPosRow[0]:=FPosRow[1];
FPosCol[0]:=FPosCol[1];

Result:=CurrentSimvol;
FCurrentSimvol:=FNextSimvol;
FNextSimvol:=ReadNextSimvol;

end;

function TGramAnaliser.CurrentSimvol: TSimvol;
begin
Result:=FCurrentSimvol;
end;

procedure TGramAnaliser.Reset;
begin
FPosRow[-1]:=1;
FPosCol[-1]:=1;
FCurrentSimvol:=ReadNextSimvol;
FPosRow[0]:=FPosRow[1];
FPosCol[0]:=FPosCol[1];
FNextSimvol:=ReadNextSimvol;
end;

function TGramAnaliser.ReadNextSimvol: TSimvol;
// ������ ��������� ����� ���������
const
  CommandCount=4;
var
 pp:TCaseCommand;
  Prog:array [0..CommandCount-1] of TLevalCommand;
  procedure Init;
  begin
  Prog[0].Check:=HavForeignASM;    Prog[0].Woker:=ReadForeignASM;
  Prog[1].Check:=HavNumber;        Prog[1].Woker:=ReadNumber;
  Prog[2].Check:=HavString;        Prog[2].Woker:=ReadString;
  Prog[3].Check:=HavOther;         Prog[3].Woker:=ReadOther;
  end;

var
 Flag:Boolean;
 i:Integer;
  Simvler:TSimvler;
begin
  Init;
  Simvler:=(Compiler.LexAnaliser as TSimvler);
  FilterComment;
  FPosRow[1]:=Simvler.PosRow;
  FPosCol[1]:=Simvler.PosCol;
  for i:=0 to CommandCount-1 do
    begin
    Flag:=Prog[i].Check();
    if Flag then
       begin
       Result:=Prog[i].Woker;
       Break;
       end;
    end;
end;

function TGramAnaliser.GetPosCol: Integer;
begin
  Result:=FPosCol[0];
end;

function TGramAnaliser.GetPosRow: Integer;
begin
 Result:=FPosRow[0];
end;

function TGramAnaliser.GetLastPosCol: Integer;
begin
  Result:=FPosCol[-1];
end;

function TGramAnaliser.GetLastPosRow: Integer;
begin
 Result:=FPosRow[-1];
end;

procedure TGramAnaliser.FilterComment;
var
  tmp:TSimvol;
  Simvler:TSimvler;
begin
  if FForeignAsm=False then
    begin
    Simvler:=(Compiler.LexAnaliser as TSimvler);

    tmp:=Simvler.CurrentSimvol;
    while (tmp.Kind in [skSpace,skComment]) do
      begin
      Simvler.ReadSimvol;
      tmp:=Simvler.CurrentSimvol;
      end;
    end;  
end;

function TGramAnaliser.ReadNumber:TSimvol;
var
  Next:TSimvol;
  Simvler:TSimvler;
begin
  Simvler:=(Compiler.LexAnaliser as TSimvler);
  Result:=Simvler.ReadSimvol; // ��������� ������� ������� �������
  if Result.Kind=skNumber then exit;
  if Result.Value<>'$' then Compiler.Error.Add(100,'$');
  Next:=Simvler.ReadSimvol;
  Result.Kind:=skNumber;
  Result.Value:=IntToStr(StrToInt('$'+Next.Value));
end;

function TGramAnaliser.ReadString:TSimvol;
var
  Next:TSimvol;
  Simvler:TSimvler;
begin
  Simvler:=(Compiler.LexAnaliser as TSimvler);
  Next:=Simvler.CurrentSimvol;
  Result:=Next;  // ��������� ������� ������� �������
  Result.Value:='';
  while (Next.Value='#') or (Next.Kind=skString) do
    begin
    if Next.Value='#' then Next:=ReadChar
       else if Next.Kind=skString then
            begin
            Next:=Simvler.ReadSimvol;
            Next.Value:=Copy(Next.Value,2,Length(Next.Value)-2);
            end;
    Result.Value:=Result.Value+Next.Value;
    Next:=Simvler.CurrentSimvol;
    end;
  Result.Kind:=skString;
end;

function TGramAnaliser.ReadChar: TSimvol;
var
  Next:TSimvol;
  Simvler:TSimvler;
begin
  Simvler:=(Compiler.LexAnaliser as TSimvler);
  Result:=Simvler.ReadSimvol; // ��������� ������� ������� �������
  if Result.Value<>'#' then Compiler.Error.Add(100,'#');
  Next:=ReadNumber;
  Result.Kind:=skString;
  Result.Value:=WideChar(StrToInt(Next.Value));
end;

function TGramAnaliser.ReadOther: TSimvol;
var
  Simvler:TSimvler;
begin
  Simvler:=(Compiler.LexAnaliser as TSimvler);
  Result:=Simvler.ReadSimvol;
end;


function TGramAnaliser.ReadForeignAsm: TSimvol;
var
  Current:TSimvol;
  Next:TSimvol;
  Simvler:TSimvler;
begin
  Simvler:=(Compiler.LexAnaliser as TSimvler);
  Next:=Simvler.CurrentSimvol;
  Result:=Next;  // ��������� ������� ������� �������
  Result.Value:='';
  while (Next.Value<>'end') and (Next.kind<>skEndOfFile) do
    begin
    Current:=Simvler.ReadSimvol;
    Result.Value:=Result.Value+Current.Value;
    Next:=Simvler.CurrentSimvol;
    end;
  Result.Kind:=skForeign;
end;

function TGramAnaliser.HavForeignASM: Boolean;
var
  Simvol:TSimvol;
  Simvler:TSimvler;
begin
  Result:=False;
  Simvler:=(Compiler.LexAnaliser as TSimvler);
  Simvol:=Simvler.CurrentSimvol;
  if FForeignAsm then
     begin
     Result:=True;
     FForeignAsm:=False
     end else
     if (Simvol.Kind = skWord) then
         if  SimvolsEQ(Simvol, TS('asm')) then
             FForeignAsm:=True;

end;

function TGramAnaliser.HavNumber: Boolean;
var
  Simvol:TSimvol;
  Simvler:TSimvler;
begin
  Result:=False;
  Simvler:=(Compiler.LexAnaliser as TSimvler);
  Simvol:=Simvler.CurrentSimvol;
  if (Simvol.Kind = skPunctuator) then
      if Simvol.Value='$' then Result:=True;
end;

function TGramAnaliser.HavOther: Boolean;
begin
  Result:=True;
end;

function TGramAnaliser.HavString: Boolean;
var
  Simvol:TSimvol;
  Simvler:TSimvler;
begin
  Result:=False;
  Simvler:=(Compiler.LexAnaliser as TSimvler);
  Simvol:=Simvler.CurrentSimvol;
  if (Simvol.Kind = skPunctuator) then
      if Simvol.Value='#' then Result:=True;
  if (Simvol.Kind = skString) then
      Result:=True;
end;

end.
