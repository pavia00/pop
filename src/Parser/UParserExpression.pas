{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UParserExpression;

interface

uses  UCompiler, LIParser, UAST, USimvler, UGramAnaliser, UASTFabric, UParserFactor;

type
  TExpressionParser = class(TParser)
  private
    { private declarations }
    Stack:TStackAST;
  protected
    { protected declarations }
  public
     FactorParser:TFactorParser;
     constructor Create(TheCompiler:TCompiler); Overload;
     function Parse(Anchor:TSimvol):TSimpleStatementAST;
     function  IsAnyAnchor: Boolean;
     procedure ErrorWay(Anchor:TSimvol);
     procedure NOP(var Node:TNodeAST);
     procedure ParseFactor(var Node:TNodeAST);
     procedure ParseFuncCallParametrs(var Node:TNodeAST);
     procedure ParseFuncCall(var Node:TNodeAST);
     procedure ParseVarAssign(var Node:TNodeAST);
     procedure ParseTypeCast(var Node:TNodeAST);
     procedure ParseExpressionList(var Node:TNodeAST);
     procedure ParseExpression(var Node:TNodeAST);
     procedure ParseDesignator(var Node:TNodeAST);
     procedure ParseSignature(var Node:TNodeAST);
     procedure NeedOperator(var Node:TNodeAST);
     procedure NeedIndentRef(var Node:TNodeAST);
     procedure ErrorType(var Node:TNodeAST);
     procedure ErrorLabel(var Node:TNodeAST);
     procedure ErrorConst(var Node:TNodeAST);
     procedure ErrorUndefine(var Node: TNodeAST; Indent:TNodeAST);
    { public declarations }
  published
    { published declarations }
  end;

implementation

{ TExpressionParser }

constructor TExpressionParser.Create(TheCompiler: TCompiler);
begin
inherited Create(TheCompiler);
 FactorParser:=TFactorParser.Create(TheCompiler);
 FactorParser.ParseExpression:=ParseExpression;
 FactorParser.ParseExpressionList:=ParseExpressionList;
 Stack:=TheCompiler.ParserStack as TStackAST;
end;

procedure TExpressionParser.ErrorWay(Anchor: TSimvol);
begin
  repeat
     ReadSimvol;
  until SimvolInArray(CurrentSimvol,[Self.Anchor, TS('End'), Anchor,  NilSimvol, EOFSimvol]);

end;

procedure TExpressionParser.NeedOperator(var Node: TNodeAST);
var
  Operator:TOperatorAST;
begin
  Operator:=TOperatorAST.Create;
    if IsExpressOperator(CurrentSimvol.Value) then
       Operator.Simvol:=ReadSimvol
       else
       begin
       Operator.Simvol:=TS('');
       Compiler.Error.Add(110, CurrentSimvol.Value);
       ErrorWay(TS(';'));
       end;
  Node:=Operator;
end;

procedure TExpressionParser.NOP(var Node: TNodeAST);
begin
// ������ ��������
end;

function TExpressionParser.Parse(Anchor: TSimvol): TSimpleStatementAST;
var
    Indent:TNodeAST;
    IndentKind:TIndentKind;
    Node:TNodeAST;
begin
    Self.Anchor:=Anchor;
    Result:=ASTFabric.NodeCreate(atTSimpleStatementAST) as TSimpleStatementAST;
    ASTFabric.ActivLeaf:=Result;
    Result.Kind:=ssUndefine;
    if Not IsAnyAnchor then
       begin
       NeedIndent(Indent);
       Stack.Push(Indent);
       IndentKind:=ASTFabric.GetIndentKind(Indent as TIndentAST);
       case IndentKind of
       ikFunction: ParseFuncCall(Node);
       ikVar:   ParseVarAssign(Node);
       ikType:  ErrorType(Node);// �� ������� ParseTypeCast(Node);
       ikLabel: ErrorLabel(Node);
       ikConst: ErrorConst(Node);
       ikUndefine: ErrorUndefine(Node, Indent);
       end; //case
       Result.Add(Node);
       end else
       begin

       Result.Kind:=ssNil;
       end;
    ASTFabric.LevalUp;
end;

procedure TExpressionParser.ParseSignature(var Node: TNodeAST);
begin
  FactorParser.ParseSignature(Node);
end;

procedure TExpressionParser.ParseDesignator(var Node: TNodeAST);
begin
  FactorParser.ParseDesignator(Node);
end;

procedure TExpressionParser.ParseExpression(var Node: TNodeAST);
var
  Expression:TExpressionAST;
  NOperator:TNodeAST;
  NFactor1,NFactor2:TNodeAST;
  Factor:TFactorAST;
begin
  Expression:=ASTFabric.NodeCreate(atTExpressionAST) as TExpressionAST;
  ASTFabric.ActivLeaf:=Expression;

  NFactor1:=Nil;
  NFactor2:=Nil;
  NOperator:=Nil;
  if IsExpressOperator(CurrentSimvol.Value) then
     begin
     Expression.kind:=ekUnari;
     NeedOperator(NOperator);
     ParseFactor(NFactor1);
     end else
     begin
     Expression.kind:=ekSimple;
     ParseFactor(NFactor1);
     if IsExpressOperator(CurrentSimvol.Value) then
        begin
        Expression.kind:=ekBinary;
        NeedOperator(NOperator);
        ParseFactor(NFactor2);
        Factor:=NFactor2 as TFactorAST;
        if (Factor.Kind=fkSignature) and (Factor.Signature.Designator.Kind = dkDeSignatureFunc) then Compiler.Error.Add(144,'');
        Factor:=NFactor1 as TFactorAST;
        if (Factor.Kind=fkSignature) and (Factor.Signature.Designator.Kind = dkDeSignatureFunc) then Compiler.Error.Add(144,'');
        end else
        if not (IsKeyWord(CurrentSimvol.Value) or
               SimvolInArray(CurrentSimvol,[TS(','), TS(';'), TS(')'), TS(']'),  NilSimvol, EOFSimvol])) then
           begin
           Compiler.Error.Add(115, CurrentSimvol.Value);
           ReadSimvol;
           end;
     end;

  ASTFabric.LevalUp;
  Expression.Factor1:=NFactor1 as TFactorAST;
  Expression.Op:=NOperator as TOperatorAST;
  Expression.Factor2:=NFactor2 as TFactorAST;
  Node:=Expression;
end;

procedure TExpressionParser.ParseExpressionList(var Node: TNodeAST);
var
  NExpression:TNodeAST;
  ExpressionList:TExpressionListAST;
  First:Boolean;
begin
  ExpressionList:=TExpressionListAST.Create;
  if Not IsAnyAnchor then
    begin
    NExpression:=Nil;
    First:=True;
    while not SimvolInArray(CurrentSimvol,[TS(';'), TS(')'), TS(']'),  NilSimvol, EOFSimvol]) do
      begin
      if CheckSimvol(TS(',')) then
         begin
         NeedSimvol(TS(','));
         if (First) then
            begin
            ExpressionList.Add(NExpression);
            First:=False;
            end;
         end;
      ParseExpression(NExpression);
      ExpressionList.Add(NExpression);
      NExpression:=Nil;
      First:=False;
      end;
    end;
  Node:=ExpressionList;
end;

procedure TExpressionParser.ParseFactor(var Node: TNodeAST);
begin
  Node:=FactorParser.Parse(TS(';'));
end;

procedure TExpressionParser.ParseFuncCallParametrs(var Node:TNodeAST);
var
  FuncCall:TFuncCallAST;
  IndentDeclRef:TNodeAST;
  NDesignator:TNodeAST;
  Designator:TDesignatorAST;
  Expression:TExpressionAST;
  NImmConst:TNodeAST;
  i:Integer;
begin
  FuncCall:= Node as TFuncCallAST;
  FactorParser.ParseDesignator(NDesignator);
  Designator:=NDesignator as TDesignatorAST;
  if Length(Designator.ExpressionList.Items)<>Length(FuncCall.RefFunc.FormalParametrs.List) then
     Self.Compiler.Error.Add(143,'��������� ����� �������, �� ��������� � ������ ��������');
  for i:=0 to Length(FuncCall.RefFunc.FormalParametrs.List)-1 do
      begin
        if i<Length(Designator.ExpressionList.Items) then
             Expression:=Designator.ExpressionList.Items[i]
           else
             Expression:=nil;
        if (Expression<>nil) and (Expression.kind=ekSimple) then
           begin
           if  (Expression.Factor1.Kind =fkSignature) then
               begin
               if (Expression.Factor1.Signature.Designator.Kind=dkSignature) then
                  begin
                  IndentDeclRef:=Expression.Factor1.Signature.IndentDeclRef;
                  FuncCall.Parametrs.Add(IndentDeclRef);
                  end else Self.Compiler.Error.Add(117,'��������� ���������� ��� ����������');
               end else
               begin
               NImmConst:=Expression.Factor1.ImmConst;
               FuncCall.Parametrs.Add(NImmConst);
               end;
           end else
           begin
 //          FuncCall.Parametrs.Add(Nil);
           Self.Compiler.Error.Add(117,'��������� ���������� ��� ����������');
           end;
      end;
end;

procedure TExpressionParser.ParseFuncCall(var Node: TNodeAST);
var
  FuncCall:TFuncCallAST;
  NFuncCall:TNodeAST;
  NIndent:TNodeAST;
  IndentDeclRef:TNodeAST;
begin
  FuncCall:=ASTFabric.NodeCreate(atTFuncCallAST) as TFuncCallAST;
  ASTFabric.ActivLeaf:=FuncCall;

  FuncCall.Parametrs:=TCallParametrsAST.Create;
  NIndent:=Stack.Pop;
  IndentDeclRef:=ASTFabric.GetIndentRef(NIndent as TIndentAST);
  IndentDeclRef:=IndentDeclRef.Parent;
  if IndentDeclRef is TFuncImpAST then
      FuncCall.RefFunc:=(IndentDeclRef as TFuncImpAST).FuncDecl
    else if IndentDeclRef is TFuncDeclAST then
        FuncCall.RefFunc:=IndentDeclRef as TFuncDeclAST
      else
        begin
        Compiler.Error.Add(141,'��� ������ ���� �������� ������� ��� ����!');
        FuncCall.RefFunc:=Nil;
        end;
  if CheckSimvol(TS('(')) then
    begin
      NFuncCall:= FuncCall as TNodeAST;
      ParseFuncCallParametrs(NFuncCall);
    end;
  ASTFabric.LevalUp;
  Node:=FuncCall;
end;

procedure TExpressionParser.ParseTypeCast(var Node: TNodeAST);
var
  TypeCast:TTypeCastAST;
  NFactor:TNodeAST;
  NIndent:TNodeAST;
  NType:TNodeAST;
begin
  TypeCast:=ASTFabric.NodeCreate(atTTypeCastAST) as TTypeCastAST;
  ASTFabric.ActivLeaf:=TypeCast;
    NIndent:=Stack.Pop;
    NType:=ASTFabric.GetIndentRef(NIndent as TIndentAST);
    NType:=NType.Parent;
    NeedSimvol(TS('('));
    ParseFactor(NFactor);
//    NeedExpression(NExpression);
    NeedSimvol(TS(')'));
  //  NeedSimvol(TS(';'));
  ASTFabric.LevalUp;
  TypeCast.RefNewType:=NType as TTypeDeclAST;
  TypeCast.Factor:=NFactor as TFactorAST;
  Node:=TypeCast;
end;

procedure TExpressionParser.ParseVarAssign(var Node: TNodeAST);
var
 VarAssign:TVarAssignAST;
 NSignature:TNodeAST;
 NExpression:TNodeAST;
begin
  VarAssign:=ASTFabric.NodeCreate(atTVarAssignAST) as TVarAssignAST;
  ASTFabric.ActivLeaf:=VarAssign;
    ParseSignature(NSignature);
     NeedSimvol(TS(':='));
     ParseExpression(NExpression);
  ASTFabric.LevalUp;
  VarAssign.Signature:=NSignature as TSignatureAST;
  VarAssign.Expression:=NExpression as TExpressionAST;
  Node:=VarAssign;
end;

procedure TExpressionParser.ErrorType(var Node: TNodeAST);
begin
  Compiler.Error.Add(106,'���');
  ErrorWay(TS(';'));
  Node:=Nil;  
end;

procedure TExpressionParser.ErrorLabel(var Node: TNodeAST);
begin
  Compiler.Error.Add(106,'�����');
  ErrorWay(TS(';'));
  Node:=Nil;
end;

procedure TExpressionParser.ErrorConst(var Node: TNodeAST);
begin
  Compiler.Error.Add(106,'����������');
  ErrorWay(TS(';'));
  Node:=Nil;
end;

procedure TExpressionParser.ErrorUndefine(var Node: TNodeAST; Indent:TNodeAST);
begin
  Compiler.Error.Add(107, (Indent as TIndentAST).Simvol.Value);
  ErrorWay(TS(';'));
  Node:=Nil;
end;

procedure TExpressionParser.NeedIndentRef(var Node: TNodeAST);
var
  NIndent:TNodeAST;
  IndentRef:TNodeAST;
begin
  Node:=Nil;
  NeedIndent(NIndent);
  IndentRef:=ASTFabric.GetIndentRef(NIndent as TIndentAST);
  if IndentRef<>nil then
       Node:=IndentRef
       else Node:=TVarDeclAST.Create;
end;

function TExpressionParser.IsAnyAnchor: Boolean;
begin
  result:=SimvolInArray(CurrentSimvol,[Anchor, TS('End'), NilSimvol, EOFSimvol]);
end;

end.
