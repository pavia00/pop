{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UParserUnit;

interface

uses LIParser, UCompiler, UAST, USimvler, UGramAnaliser, UParserDeclarations, UParserImplementation, UASTFabric;

 Type
   TUnitParser = class(TSubProgramm)
   private
     { private declarations }
   protected
     { protected declarations }
   public
     DeclSection:TDeclSectionParser;
     ImplemtSection:TImplemtSectionParser;
     Compiler:TCompiler;
     function Parse:TUnitAST;
     constructor Create(TheCompiler:TCompiler); Overload;
     Destructor Destroy; Overload;
     { public declarations }
   published
     { published declarations }
   end;

implementation

{ TUnitParser }

constructor TUnitParser.Create(TheCompiler: TCompiler);
begin
  inherited Create(TheCompiler);
  Compiler:=TheCompiler;
  DeclSection:=TDeclSectionParser.Create(TheCompiler);
  ImplemtSection:=TImplemtSectionParser.Create(TheCompiler);
  ImplemtSection.DeclSectionParser:=DeclSection;
end;

destructor TUnitParser.Destroy;
begin
DeclSection.Destroy;
ImplemtSection.Destroy;
end;

function TUnitParser.Parse:TUnitAST;
var i:Integer;
begin
  Result:=ASTFabric.UnitCreate;
  ASTFabric.ActivLeaf:=Result;


  Result.Imp:=ASTFabric.ImplimentaionCreate;
  Result.Dec:=ASTFabric.DeclarationCreate;
  NeedSimvol(TS('Declaration'));
  Result.Dec.add(DeclSection.Parse(TS('Implementation')));
  if SimvolsEQ(CurrentSimvol,TS('Implementation')) then
     begin
       Result.Imp.add(ImplemtSection.Parse(TS('.')));
       NeedSimvol(TS('.'));
     end else
     begin
       NeedSimvol(TS('End'));
       NeedSimvol(TS('.'));
     end;
end;

end.
