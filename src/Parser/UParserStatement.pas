{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UParserStatement;

interface

uses  UCompiler, LIParser, UAST, UFSM, USimvler, UGramAnaliser, UASTFabric, UParserExpression;

type
  TStatementParser = class(TParser)
  private
    { private declarations }
  protected
    { protected declarations }
  public
     ExpressionParser:TExpressionParser;
     constructor Create(TheCompiler:TCompiler); Overload;
     function Parse(Anchor:TSimvol):TStatementAST;
     procedure ErrorWay(Anchor:TSimvol);
     procedure NOP(var Node:TNodeAST);
     function  IsNop(var Proc): Boolean;
     procedure ParseStatement(var Node:TNodeAST; Anchor:TSimvol);
     procedure ParseFor(var Node:TNodeAST);
     procedure ParseWhile(var Node:TNodeAST);
     procedure ParseRepeat(var Node:TNodeAST);
     procedure ParseCase(var Node:TNodeAST);
     procedure ParseIf(var Node:TNodeAST);
     procedure ParseGoto(var Node:TNodeAST);
     procedure ParseLabelingStatement(var Node:TNodeAST);
     procedure ParseSimpleStatement(var Node:TNodeAST);
     procedure ParseCompaundStatement(var Node:TNodeAST);
     procedure ParseCompaundRepeatStatement(var Node:TNodeAST);
     procedure ParseCaseEdge(var Node:TNodeAST);
     procedure NeedLabelRef(var Node:TNodeAST);
     procedure NeedConstRef(var Node:TNodeAST);
     procedure NeedVarRef(var Node:TNodeAST);
     procedure NeedConst(var Node:TNodeAST);
    { public declarations }
  published
    { published declarations }
  end;

implementation

{ TStatementParser }

constructor TStatementParser.Create(TheCompiler: TCompiler);
begin
inherited Create(TheCompiler);
 ExpressionParser:=TExpressionParser.Create(TheCompiler);
end;

procedure TStatementParser.ErrorWay(Anchor: TSimvol);
begin

end;

function TStatementParser.IsNop(var Proc): Boolean;
var Ptr:Pointer;
  Proc2:TParserProcedure;
begin
  Ptr:=Pointer(Proc);
  Proc2:=Nop;
  Result:= (Ptr=@Proc2);
end;

procedure TStatementParser.NeedConst(var Node: TNodeAST);
begin
    case CurrentSimvol.Kind of
    skWord:NeedConstRef(Node);
    skNumber:ExpressionParser.FactorParser.ParseImmNumber(Node);
    skString:ExpressionParser.FactorParser.ParseImmString(Node);
    end;

end;

procedure TStatementParser.NeedConstRef(var Node: TNodeAST);
var
  Indent:TNodeAST;
  IndentKind:TIndentKind;
  RefConst:TNodeAST;
begin
  NeedIndent(Indent);
  IndentKind:=ASTFabric.GetIndentKind(Indent as TIndentAST);
  if (IndentKind=ikLabel) then
      begin
        Node:=ASTFabric.GetIndentRef(Indent as TIndentAST).Parent;
      end else
      begin
        Node:=TConstDeclAST.Create;
        Compiler.Error.Add(142,'��� ������ ���� ����������!');
      end;

end;

procedure TStatementParser.NeedVarRef(var Node: TNodeAST);
var
  Indent:TNodeAST;
  IndentKind:TIndentKind;
  RefConst:TNodeAST;
begin
  NeedIndent(Indent);
  IndentKind:=ASTFabric.GetIndentKind(Indent as TIndentAST);
  if (IndentKind=ikVar) then
      begin
        Node:=ASTFabric.GetIndentRef(Indent as TIndentAST).Parent;
      end else
      begin
        Node:=TVarDeclAST.Create;
        Compiler.Error.Add(108,'��� ������ ���� ����������!');
      end;

end;

procedure TStatementParser.NeedLabelRef(var Node: TNodeAST);
var
  Indent:TNodeAST;
  IndentKind:TIndentKind;
  RefLabel:TNodeAST;
begin
  NeedIndent(Indent);
  IndentKind:=ASTFabric.GetIndentKind(Indent as TIndentAST);
  if (IndentKind=ikLabel) then
      begin
        Node:=ASTFabric.GetIndentRef(Indent as TIndentAST).Parent;
      end else
      begin
        Node:=TLabelDeclAST.Create;
        Compiler.Error.Add(140,'��� ������ ���� �����!');
      end;
end;


procedure TStatementParser.NOP(var Node: TNodeAST);
begin
// ������ �� ������
end;

function TStatementParser.Parse(Anchor: TSimvol): TStatementAST;
// ������ ��������� ����� ���������
const
  CommandCount=10;
var
  Prog:array [0..CommandCount-1] of TCaseCommand;
  procedure Init;
  begin
  Prog[0].Value:='For';    Prog[0].Woker:=ParseFor;
  Prog[1].Value:='While';  Prog[1].Woker:=ParseWhile;
  Prog[2].Value:='Repeat'; Prog[2].Woker:=ParseRepeat;
  Prog[3].Value:='If';     Prog[3].Woker:=ParseIf;
  Prog[4].Value:='Case';   Prog[4].Woker:=ParseCase;
  Prog[5].Value:='Goto';   Prog[5].Woker:=ParseGoto;
  Prog[6].Value:='Begin';  Prog[6].Woker:=ParseCompaundStatement;
  Prog[7].Value:='End';    Prog[7].Woker:=Nop;
  Prog[8].Value:='Else';   Prog[8].Woker:=Nop;
  Prog[9].Value:='until';  Prog[9].Woker:=Nop;
  end;
var
  Woker:TParserProcedure;
  NodeAST:TNodeAST;
begin
 Init;
 Self.Anchor:=Anchor;
 Result:=ASTFabric.NodeCreate(atTStatementAST) as TStatementAST;
 ASTFabric.ActivLeaf:=Result;
 Result.Line:=(Compiler.LexAnaliser as TFileSimvler).PosRow;
// repeat
    NodeAST:=Nil;
    Woker:=ChooseOfWork(CommandCount,@Prog, CurrentSimvol());

    if @Woker=nil then
       begin
       ParseLabelingStatement(NodeAST);
       if NodeAST=Nil then
          begin
          ErrorWay(Anchor);
      //    Break;
          end;
       Result.Add(NodeAST);
       end else
       if Not IsNop(Woker) then
          begin
          Woker(NodeAST);
          Result.Add(NodeAST);
          end;
//  if SimvolsEQ(CurrentSimvol,Anchor);
 // until  IsAnyAnchor or IsNop(Woker);
  ASTFabric.LevalUp;
  if (NodeAST=Nil) then
      FreeAndNil(Result);
end;

procedure TStatementParser.ParseCase(var Node: TNodeAST);
var
  CaseStm:TCaseStmAST;
  NVarRef:TNodeAST;
  NCaseEdge:TNodeAST;
  NDefultEdge:TNodeAST;
begin
  CaseStm:=ASTFabric.NodeCreate(atTCaseStmAST) as TCaseStmAST;
  ASTFabric.ActivLeaf:=CaseStm;
    NeedSimvol(TS('Case'));
    NeedSimvol(TS('('));
    NeedVarRef(NVarRef);
    NeedSimvol(TS(')'));
    NeedSimvol(TS('of'));
    while not((CheckSimvol(TS('else')) or CheckSimvol(TS('end')))) do
      begin
      NCaseEdge:=Nil;
      ParseCaseEdge(NCaseEdge);
      CaseStm.Add(NCaseEdge);
      end;
    if CheckSimvol(TS('else')) then
       begin
       NeedSimvol(TS('Else'));
       ParseCompaundStatement(NDefultEdge);
       end;
    NeedSimvol(TS('End'));
  CaseStm.RefVar:=NVarRef as TVarDeclAST;
  CaseStm.Defult:=NDefultEdge as TStatementAST;
  ASTFabric.LevalUp;
  Node:=CaseStm;
end;

procedure TStatementParser.ParseCaseEdge(var Node: TNodeAST);
var
 CaseEdge:TCaseEdgeAST;
 NConst:TNodeAST;
 NStatement:TNodeAST;
begin
  CaseEdge:=ASTFabric.NodeCreate(atTCaseEdgeAST) as TCaseEdgeAST;
  ASTFabric.ActivLeaf:=CaseEdge;
    NeedConst(NConst);
    NeedSimvol(TS(':'));
    ParseStatement(NStatement, TS(';'));
    NeedSimvol(TS(';'));
    CaseEdge.Add(NConst);
  CaseEdge.Statement:=NStatement as TStatementAST;
  ASTFabric.LevalUp;
  Node:=CaseEdge;
end;

procedure TStatementParser.ParseCompaundRepeatStatement(var Node: TNodeAST);
var
  Statement:TStatementAST;
  CompaundStatement:TCompaundStatementAST;
begin
  CompaundStatement:=ASTFabric.NodeCreate(atTCompaundStatementAST) as TCompaundStatementAST;
  ASTFabric.ActivLeaf:=CompaundStatement;
  Anchor:=TS('until');
  while Not IsAnyAnchor do
    begin
      Statement:=Self.parse(TS(';'));
      if not CheckSimvol(TS('until')) then
         NeedSimvol(TS(';'));
      CompaundStatement.Add(Statement);
      Anchor:=TS('until');
    end;
  ASTFabric.LevalUp;
  Node:=CompaundStatement;
end;

procedure TStatementParser.ParseCompaundStatement(var Node: TNodeAST);
var
  Statement:TStatementAST;
  CompaundStatement:TCompaundStatementAST;
begin
  CompaundStatement:=ASTFabric.NodeCreate(atTCompaundStatementAST) as TCompaundStatementAST;
  ASTFabric.ActivLeaf:=CompaundStatement;
  NeedSimvol(TS('Begin'));
  Anchor:=TS('End');
  while Not IsAnyAnchor do
    begin
      Statement:=Self.parse(TS(';'));
      if not CheckSimvol(TS('End')) then
         NeedSimvol(TS(';'));
      CompaundStatement.Add(Statement);
      Anchor:=TS('End');
    end;
  NeedSimvol(TS('End'));
  ASTFabric.LevalUp;
  Node:=CompaundStatement;
end;

procedure TStatementParser.ParseFor(var Node: TNodeAST);
  function NeedTo_DownTo:TForMode;
  begin
  Result:=fmError;
    if CheckSimvol(TS('To')) then
      begin
      NeedSimvol(TS('To'));
      Result:=FmTo;
      end else
      if CheckSimvol(TS('DownTo')) then
      begin
      NeedSimvol(TS('DownTo'));
      Result:=FmDownTo;
      end else
      begin
      Result:=FmError;
      // ErrorWay  ???
      end;
    if Result=fmError then
       begin
       Compiler.Error.Add(104,'');
       end;
  end;
var
  ForStm:TForStmAST;
  NRefVar:TNodeAST;
  Expr1,Expr2:TNodeAST;
  NStatement:TNodeAST;
  Mode:TForMode;
begin
  ForStm:=ASTFabric.NodeCreate(atTForStmAST) as TForStmAST;
  ASTFabric.ActivLeaf:=ForStm;

    NeedSimvol(TS('for'));
    NeedVarRef(NRefVar);
    NeedSimvol(TS(':='));
    ExpressionParser.ParseExpression(Expr1);
    if ((Expr1 as TExpressionAST).kind <> ekSimple) then Compiler.Error.Add(144,'');
    Mode:=NeedTo_DownTo;
    ExpressionParser.ParseExpression(Expr2);
    if ((Expr2 as TExpressionAST).kind <> ekSimple) then Compiler.Error.Add(144,'');
    NeedSimvol(TS('do'));
    ParseStatement(NStatement, TS(';'));
  ForStm.RefVar:=NRefVar as TVarDeclAST;
  ForStm.Expr1:=Expr1 as TExpressionAST;
  ForStm.Expr2:=Expr2 as TExpressionAST;
  ForStm.Mode:=Mode;
  ForStm.Statement:=NStatement as TStatementAST;

  ASTFabric.LevalUp;
  Node:=ForStm;
end;


procedure TStatementParser.ParseGoto(var Node: TNodeAST);
var
   GotoStm:TGotoStmAST;
   NRefLabel:TNodeAST;
begin
  GotoStm:=ASTFabric.NodeCreate(atTGotoStmAST) as TGotoStmAST;
  ASTFabric.ActivLeaf:=GotoStm;

    NeedSimvol(TS('Goto'));
    NeedLabelRef(NRefLabel);
  GotoStm.RefLabel:=NRefLabel as TLabelDeclAST;
  ASTFabric.LevalUp;
  Node:=GotoStm;

end;

procedure TStatementParser.ParseIf(var Node: TNodeAST);
var
  IfStm:TIfStmAST;
  NRefVar:TNodeAST;
  NStatement1,NStatement2:TNodeAST;
begin
  IfStm:=ASTFabric.NodeCreate(atTIfStmAST) as TIfStmAST;
  ASTFabric.ActivLeaf:=IfStm;
    NeedSimvol(TS('If'));
    NeedSimvol(TS('('));
    NeedVarRef(NRefVar);
    NeedSimvol(TS(')'));
    NeedSimvol(TS('Then'));
    ParseStatement(NStatement1, TS(';'));
    if CheckSimvol(TS('else')) then
       begin
       NeedSimvol(TS('else'));
       ParseStatement(NStatement2, TS(';'));
       end;
  IfStm.BoolExpr:=NRefVar as TVarDeclAST;
  IfStm.TrueEdge:=NStatement1 as TStatementAST;
  IfStm.FalseEdge:=NStatement2 as TStatementAST;
  ASTFabric.LevalUp;  
  Node:=IfStm;
end;

procedure TStatementParser.ParseLabelingStatement(var Node: TNodeAST);
var
  NRefLabel:TNodeAST;
  NStatement:TNodeAST;
  LabelingStatement:TLabelingStatementAST;
begin
  LabelingStatement:=ASTFabric.NodeCreate(atTLabelingStatementAST) as TLabelingStatementAST;
  ASTFabric.ActivLeaf:=LabelingStatement;
    if SimvolsEQ(NextSimvol,TS(':')) then
       begin
         NeedLabelRef(NRefLabel);
         NeedSimvol(TS(':'));
         ParseLabelingStatement(NStatement);
         LabelingStatement.RefLabel:=NRefLabel as TLabelDeclAST;
       end else  ParseSimpleStatement(NStatement);

  LabelingStatement.Add(NStatement);
  ASTFabric.LevalUp;
  Node:=LabelingStatement;
end;

procedure TStatementParser.ParseRepeat(var Node: TNodeAST);
var
  RepeatStm:TRepeatStmAST;
  NRefVar:TNodeAST;
  NStatement:TNodeAST;
begin
  RepeatStm:=ASTFabric.NodeCreate(atTRepeatStmAST) as TRepeatStmAST;
  ASTFabric.ActivLeaf:=RepeatStm;

    NeedSimvol(TS('repeat'));
    ParseCompaundRepeatStatement(NStatement);
    NeedSimvol(TS('until'));
    NeedSimvol(TS('('));
    NeedVarRef(NRefVar);
    NeedSimvol(TS(')'));
  RepeatStm.BoolExpr:=NRefVar as TVarDeclAST;
  RepeatStm.CompaundStatement:=NStatement as TCompaundStatementAST;
  ASTFabric.LevalUp;
  Node:=RepeatStm;
end;

procedure TStatementParser.ParseSimpleStatement(var Node: TNodeAST);
begin
Node:=ExpressionParser.Parse(TS(';'));
end;

procedure TStatementParser.ParseStatement(var Node: TNodeAST; Anchor:TSimvol);
begin
Node:=Self.Parse(Anchor);
end;

procedure TStatementParser.ParseWhile(var Node: TNodeAST);
var
  WhileStm:TWhileStmAST;
  NRefVar:TNodeAST;
  NStatement:TNodeAST;
begin
  WhileStm:=ASTFabric.NodeCreate(atTWhileStmAST) as TWhileStmAST;
  ASTFabric.ActivLeaf:=WhileStm;
    NeedSimvol(TS('While'));
    NeedSimvol(TS('('));
    NeedVarRef(NRefVar);
    NeedSimvol(TS(')'));
    NeedSimvol(TS('do'));
    ParseStatement(NStatement, TS(';'));
  WhileStm.BoolExpr:=NRefVar as TVarDeclAST;
  WhileStm.Statement:=NStatement as TStatementAST;
  ASTFabric.LevalUp;
  Node:=WhileStm;
end;

end.
