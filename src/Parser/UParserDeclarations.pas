{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UParserDeclarations;

interface


uses UAST, UFSM, UASTFabric, UCompiler, USimvler, UGramAnaliser, UHohaTypes,
   UParserAsm, UParserType;

 Type
 {  TTypeDescriptor=TSimvol;

   TDeclSectionParser = class;
   TConstDescriptor=
     record
     Perent:TDeclSectionParser;
     Indent:TSimvol;
      _Type:TTypeDescriptor;
      Value:TSimvol
     end;
}

   TDeclSectionParser = class(TParser)
   private
     Compiler:TCompiler;
     Peret:TSubProgramm;
     ForeignAsmParser:TForeignAsmParser;
     TypeParser:TTypeParser;
//     ConstList:Array of TConstDescriptor;
     { private declarations }
   protected
     { protected declarations }
   public
{ TODO : ��������� ����� ������� �������� ������� }
     Anchors:TSimvolList;
     ResultExist:Boolean;
     ResultType: TTypeAST;
     constructor Create(TheCompiler:TCompiler); override;
     function Parse(Anchor:TSimvol):TDeclarationAST;
     procedure ParseConst(var Node:TNodeAST);
     procedure UsesDeclaration(var Node:TNodeAST);
     procedure TypeDeclaration(var Node:TNodeAST);
     procedure VarDeclaration(var Node:TNodeAST);
     procedure NOP(var Node:TNodeAST);
     function  IsNop(var Proc):Boolean;
     procedure AddResult(Declaration:TDeclarationAST;_Type: TTypeAST);
     procedure ConstDeclaration(var Node:TNodeAST);
     procedure LabelDeclaration(var Node:TNodeAST);
     procedure ErrorWay(Anchor:TSimvol);
     procedure ParseAsm(var Node:TNodeAST);
     procedure ParseType(var Node:TNodeAST);
     function  ParseFuncAttr:TFuncAttr;
     procedure FunctionHeading(var Node:TNodeAST);
     procedure ProcedureHeading(var Node:TNodeAST);
     procedure ParseFormalParametrs(var Node:TNodeAST);
     procedure NeedIndentList(var Node:TNodeAST);
//     procedure AddConst(Indent:TSimvol; _Type:TTypeDescriptor; Value:TSimvol);
     { public declarations }
   published
     { published declarations }
   end;

implementation


{ TDeclarationsParser }

function TDeclSectionParser.Parse(Anchor: TSimvol): TDeclarationAST;
// ������ ��������� ����� ���������
const
  CommandCount=11;
var
  Prog:array [0..CommandCount-1] of TCaseCommand;
  procedure Init;
  begin
  Prog[00].Value:='Uses';           Prog[00].Woker:=UsesDeclaration;
  Prog[01].Value:='Type';           Prog[01].Woker:=TypeDeclaration;
  Prog[02].Value:='Var';            Prog[02].Woker:=VarDeclaration;
  Prog[03].Value:='Const';          Prog[03].Woker:=ConstDeclaration;
  Prog[04].Value:='Label';          Prog[04].Woker:=LabelDeclaration;
  Prog[05].Value:='function';       Prog[05].Woker:=FunctionHeading;
  Prog[06].Value:='procedure';      Prog[06].Woker:=ProcedureHeading;
  Prog[07].Value:='asm';            Prog[07].Woker:=Nop;
  Prog[08].Value:='Implementation'; Prog[08].Woker:=Nop;
  Prog[09].Value:='Begin';          Prog[09].Woker:=Nop;
  Prog[10].Value:='End';            Prog[10].Woker:=Nop;
  end;

var
  Woker:TParserProcedure;
  NodeAST:TNodeAST;
 begin
 Init;
 Self.Anchor:=Anchor;

 Result:=ASTFabric.DeclarationCreate;
 ASTFabric.ActivLeaf:=Result;
 if ResultExist then AddResult(Result, ResultType);
  repeat
    NodeAST:=Nil;
    Woker:=ChooseOfWork(CommandCount,@Prog, CurrentSimvol());

    if @Woker=nil then
       begin
       Compiler.Error.Add(105,CurrentSimvol.Value);
       ErrorWay(Anchor);
       Break;
       end else
       if Not IsNop(Woker) then
          begin
          Woker(NodeAST);
          Result.Add(NodeAST);
          end;
//  if SimvolsEQ(CurrentSimvol,Anchor);
  until  (IsAnyAnchor) or (NodeAST=Nil);
  ASTFabric.LevalUp;
end;


procedure TDeclSectionParser.ErrorWay(Anchor:TSimvol);
begin
  repeat
     ReadSimvol;
  until SimvolInArray(CurrentSimvol,[Anchor, TS(';'), TS('.'), EOFSimvol]);
end;

procedure TDeclSectionParser.ParseType(var Node:TNodeAST);
var
  _Type:TTypeAST;
begin
_Type:=TypeParser.Parse;
Node:=_Type;
end;

procedure TDeclSectionParser.ConstDeclaration(var Node:TNodeAST);
var
  Indent:TNodeAST;
  _Const:TNodeAST;
  _Type:TNodeAST;
  ConstDecl:TConstDeclAST;
begin
  ConstDecl:=ASTFabric.NodeCreate(atTConstDeclAST) as TConstDeclAST;
  ASTFabric.ActivLeaf:=ConstDecl;
  ASTFabric.IndentKind:=ikConst;


    NeedSimvol(TS('Const'));
    NeedIndent(Indent);
    NeedSimvol(TS(':'));
    ParseType(_Type);
    NeedSimvol(TS('='));
    ParseConst(_Const);
    NeedSimvol(TS(';'));

  ASTFabric.LevalUp;
  ConstDecl.Indent:=Indent as TIndentAST;
  ConstDecl._Type:=_Type as TTypeAST;
  ConstDecl._const:=_Const as TConstAST;

  Node:=ConstDecl;
end;

procedure TDeclSectionParser.TypeDeclaration(var Node:TNodeAST);
var
  Indent:TNodeAST;
  _Type:TNodeAST;
  TypeDecl:TTypeDeclAST;
begin
  TypeDecl:=ASTFabric.NodeCreate(atTTypeDeclAST) as TTypeDeclAST;
  ASTFabric.ActivLeaf:=TypeDecl;
  ASTFabric.IndentKind:=ikType;


    NeedSimvol(TS('Type'));
    NeedIndent(Indent);
    NeedSimvol(TS('='));
    ParseType(_Type);
    NeedSimvol(TS(';'));

  ASTFabric.LevalUp;
  TypeDecl.Indent:=Indent as TIndentAST;
  TypeDecl._Type:=_Type as TTypeAST;

  Node:=TypeDecl;
end;

procedure TDeclSectionParser.UsesDeclaration(var Node:TNodeAST);
var
  Indent:TNodeAST;
  UsesDecl:TUsesDeclAST;
  UnitAST:TUnitAST;
  ParentUnit:TUnitAST;
//  LastNode:TNodeAST;
begin
  UsesDecl:=ASTFabric.NodeCreate(atTUsesDeclAST) as TUsesDeclAST;
  ASTFabric.ActivLeaf:=UsesDecl;
  ASTFabric.IndentKind:=ikVar;

    NeedSimvol(TS('Uses'));
    NeedIndent(Indent);
    NeedSimvol(TS(';'));
  ASTFabric.LevalUp;

  UsesDecl.Indent:=Indent as TIndentAST;
  UnitAST:=Compiler.getUnit(UsesDecl.Indent.Simvol.Value);
  ParentUnit:=ASTFabric.ActivLeaf.parent as TUnitAST;
  ParentUnit.Add(UnitAST);

  Node:=UsesDecl;
end;

procedure TDeclSectionParser.VarDeclaration(var Node:TNodeAST);
var
  Indent:TNodeAST;
  _Const:TNodeAST;
  _Type:TNodeAST;
  VarDecl:TVarDeclAST;
begin
  VarDecl:=ASTFabric.NodeCreate(atTVarDeclAST) as TVarDeclAST;
  ASTFabric.ActivLeaf:=VarDecl;
  ASTFabric.IndentKind:=ikVar;

    NeedSimvol(TS('Var'));
    NeedIndent(Indent);
    NeedSimvol(TS(':'));
    ParseType(_Type);
    NeedSimvol(TS(';'));
  ASTFabric.LevalUp;

  VarDecl.Indent:=Indent as TIndentAST;
  VarDecl._Type:=_Type as TTypeAST;
  VarDecl.IsResult:=False;

  Node:=VarDecl;
end;


procedure TDeclSectionParser.ParseConst(var Node: TNodeAST);
var
 _Const:TConstAST;
begin
  _Const:=ASTFabric.NodeCreate(atTConstAST) as TConstAST;
  ASTFabric.ActivLeaf:=_Const;

    _Const.Simvol:=ReadSimvol;
    if not (_Const.Simvol.Kind in [skNumber, skString]) then
       Compiler.Error.Add(100,'����� ��� ������');
  ASTFabric.LevalUp;
  Node:=_Const;
end;


procedure TDeclSectionParser.ParseFormalParametrs(var Node: TNodeAST);
  function ReadLocVarConst:TParametrMode;
  begin
    Result:=pmParam;
    ASTFabric.IndentKind:=ikVar;
    if CheckSimvol(TS('var')) then
       begin
       ReadSimvol;
       Result:=pmVar;
       ASTFabric.IndentKind:=ikVar;
       end;
    if CheckSimvol(TS('const')) then
       begin
       ReadSimvol;
       Result:=pmConst;
       ASTFabric.IndentKind:=ikConst;
       end;
  end;
var
  Mode:TParametrMode;
  Count, I:Integer;
  NStack:TNodeAST;
  _Type:TNodeAST;
  IndentList:TStackAST;
  ReverList:TStackAST;
  FormalParametrs:TFormalParametrsAST;
  VarDecl:TVarDeclAST;
begin

  FormalParametrs:=ASTFabric.NodeCreate(atTFormalParametrsAST) as TFormalParametrsAST;
    NeedSimvol(TS('('));
    while CheckSimvol(TS(')'))<>True do
      begin
        Mode:=ReadLocVarConst;

        NeedIndentList(NStack);
        NeedSimvol(TS(':'));
        ParseType(_Type);
        if CheckSimvol(TS(')'))<>True then
           NeedSimvol(TS(';'));

      IndentList:=NStack as TStackAST;
      ASTFabric.ActivLeaf:=FormalParametrs;
      Count:=IndentList.Count; // �� ��������������, ���������� ������ �� ������
      ReverList:=TStackAST.Create;
      for i:=0 to Count-1 do
        begin
        ReverList.Push(IndentList.pop);
        end;
      for i:=0 to Count-1 do
        begin
          VarDecl:=ASTFabric.NodeCreate(atTVarDeclAST) as TVarDeclAST;
          ASTFabric.ActivLeaf:=VarDecl;
          VarDecl.Indent:=ReverList.pop as TIndentAST;
          VarDecl._Type:=_Type as TTypeAST;
          VarDecl.IsResult:=False;
          VarDecl.Indent.Parent:=ASTFabric.ActivLeaf;
          VarDecl._Type.Parent:=ASTFabric.ActivLeaf;
          FormalParametrs.Add(Mode, VarDecl);
          VarDecl.Parent:=FormalParametrs.List[Length(FormalParametrs.List)-1];
          ASTFabric.LevalUp;
          ASTFabric.LevalUp; // TParametrAST;
        end;
      ReverList.Destroy;
      ASTFabric.LevalUp;
      ASTFabric.StackDestroy(IndentList);
    end;
    NeedSimvol(TS(')'));

Node:=FormalParametrs;
end;

procedure TDeclSectionParser.NOP(var Node: TNodeAST);
begin
Node:=Nil;
end;

function TDeclSectionParser.ParseFuncAttr:TFuncAttr;
begin
Result.Call:=faHohaCall;
Result.Imp:=faHoha;
 if CheckSimvol(TS('stdcall')) then
    begin
    Result.Call:=faStdcall;
    ReadSimvol;
    NeedSimvol(TS(';'));
    end;
 if CheckSimvol(TS('assemble')) then
    begin
    Result.Imp:=faAssemble;
    ReadSimvol;
    NeedSimvol(TS(';'));
    end;

end;

procedure TDeclSectionParser.FunctionHeading(var Node: TNodeAST);
var
  Indent:TNodeAST;
  _Type:TNodeAST;
  FunctionHeadin:TFuncDeclAST;
  FormalParametrs:TNodeAST;
  Attr:TFuncAttr;
begin
  FunctionHeadin:=ASTFabric.FuncDeclCreate;
    ASTFabric.IndentKind:=ikFunction;
    NeedSimvol(TS('function'));
    ASTFabric.ActivLeaf:=FunctionHeadin;
    NeedIndent(Indent);
      if CheckSimvol(TS('(')) then
         ParseFormalParametrs(FormalParametrs)
         else   FormalParametrs:=TFormalParametrsAST.Create;
      NeedSimvol(TS(':'));
      ParseType(_Type);
      NeedSimvol(TS(';'));
      Attr:=ParseFuncAttr;
    ASTFabric.LevalUp;
  FunctionHeadin.kind:=kfFunction;
  FunctionHeadin.Indent:=Indent as TIndentAST;
  FunctionHeadin.Attr:=Attr;
  FunctionHeadin.FormalParametrs:=FormalParametrs as TFormalParametrsAST;
  FunctionHeadin.result:=_Type as TTypeAST;

  Node:=FunctionHeadin;
end;

procedure TDeclSectionParser.ProcedureHeading(var Node: TNodeAST);
var
  Indent:TNodeAST;
  _Type:TNodeAST;
  FunctionHeadin:TFuncDeclAST;
  FormalParametrs:TNodeAST;
  Attr:TFuncAttr;
begin
  FunctionHeadin:=ASTFabric.FuncDeclCreate;
  ASTFabric.IndentKind:=ikFunction;

    NeedSimvol(TS('procedure'));
    ASTFabric.ActivLeaf:=FunctionHeadin;
    NeedIndent(Indent);
      if CheckSimvol(TS('(')) then
         ParseFormalParametrs(FormalParametrs)
         else   FormalParametrs:=TFormalParametrsAST.Create;
      NeedSimvol(TS(';'));
      Attr:=ParseFuncAttr;
    ASTFabric.LevalUp;

  FunctionHeadin.kind:=kfProcedure;
  FunctionHeadin.Indent:=Indent as TIndentAST;
  FunctionHeadin.Attr:=Attr;
  FunctionHeadin.FormalParametrs:=FormalParametrs as TFormalParametrsAST;
  FunctionHeadin.result:=nil;

  Node:=FunctionHeadin;

end;

procedure TDeclSectionParser.ParseAsm(var Node: TNodeAST);
begin
  Node:=ForeignAsmParser.Parse;
  NeedSimvol(TS(';'));
end;

procedure TDeclSectionParser.NeedIndentList(var Node: TNodeAST);
var
  Stack:TStackAST;
  NIndent:TNodeAST;
  Flag:Boolean;
begin
  ASTFabric.StackCreate(Stack);
  Flag:=False;
  repeat
    NeedIndent(NIndent);
    Stack.Push(NIndent);
    NIndent:=Nil;
    if CheckSimvol(TS(','))=True then
       begin
         ReadSimvol;
       end else
         Flag:=True;
  until Flag=True;
  Node:=Stack;
end;

procedure TDeclSectionParser.LabelDeclaration(var Node: TNodeAST);
var
  Indent:TNodeAST;
  LabelDecl:TLabelDeclAST;
begin
  LabelDecl:=ASTFabric.NodeCreate(atTLabelDeclAST) as TLabelDeclAST;
  ASTFabric.ActivLeaf:=LabelDecl;
  ASTFabric.IndentKind:=ikLabel;

    NeedSimvol(TS('Label'));
    NeedIndent(Indent);
    NeedSimvol(TS(';'));

  ASTFabric.LevalUp;
  LabelDecl.Indent:=Indent as TIndentAST;
  Node:=LabelDecl;
end;

function TDeclSectionParser.IsNop(var Proc): Boolean;
var Ptr:Pointer;
  Proc2:TParserProcedure;
begin
  Ptr:=Pointer(Proc);
  Proc2:=Nop;
  Result:= (Ptr=@Proc2);

end;

procedure TDeclSectionParser.AddResult(Declaration:TDeclarationAST;_Type: TTypeAST);
var
  Indent:TNodeAST;
  _Const:TNodeAST;
  VarDecl:TVarDeclAST;
begin
  VarDecl:=ASTFabric.NodeCreate(atTVarDeclAST) as TVarDeclAST;
  ASTFabric.ActivLeaf:=VarDecl;
  ASTFabric.IndentKind:=ikVar;

    Indent:=ASTFabric.IndentCreate(TS('result'));

  ASTFabric.LevalUp;

  VarDecl.Indent:=Indent as TIndentAST;
  VarDecl.Indent.Kind:=ASTFabric.IndentKind;
  VarDecl._Type:=_Type as TTypeAST;
  VarDecl.IsResult:=True;

  Declaration.Add(VarDecl);

end;

constructor TDeclSectionParser.Create(TheCompiler:TCompiler);
begin
  inherited;
  ResultExist:=False;
  ResultType:=nil;
  ForeignAsmParser:=TForeignAsmParser.Create(TheCompiler);
  TypeParser:=TTypeParser.Create(TheCompiler);
  Compiler:=TheCompiler;
end;

end.
