{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit UAST;

interface

uses USimvler;
//uses System.Generics.Collections;

type
  TNodeAST=Class
     Parent:TNodeAST;
     Childs:array of TNodeAST;
     procedure AddChild(Child:TNodeAST);
     end;

  TASTNodeType=
    (atTUnitAST,
     atTForeignAsmAST,
     atTImplimentaionAST,
     atTDeclarationAST,
     atTUsesDeclAST,
     atTTypeDeclAST,
     atTConstDeclAST,
     atTVarDeclAST,
     atTLabelDeclAST,
     atTTypeAST,
     atTConstAST,
     atTIntegerAST,
     atTRealAST,
     atTIndentAST,
     atTFuncDeclAST,
     atTFuncImpAST,
     atTTypeArrayAST,
     atTTypeRecordAST,
     atTFieldDeclAST,
     atTFormalParametrsAST,

     atTStatementAST,
     atTGotoStmAST,
     atTForStmAST,
     atTWhileStmAST,
     atTRepeatStmAST,
     atTIfStmAST,
     atTCaseStmAST,
     atTCaseEdgeAST,
     atTLabelingStatementAST,
     atTCompaundStatementAST,

     atTSimpleStatementAST,
     atTExpressionAST,
     atTFuncCallAST,
     atTTypeCastAST,
     atTVarAssignAST,
     atTFactorAST,
     atTImmConstAST
     );

  TParametrMode=(
    pmVar,
    pmConst,
    pmParam);

  TIndentKind=(
    ikUnit,
    ikFunction,
    ikVar,
    ikType,
    ikLabel,
    ikConst,
    ikUndefine);

  TTypeFamily=(
    tfUndefine,
    tfOrdinal,
    tfReal,
    tfBoolean,
    tfPointer,
    tfVoid, {tfPointerDesignator}
    tfString,
    tfStructure);

  TVarDeclKind=(
    vdkUndefine,
    vdkParam,
    vdkLocal,
    vdkGlobal
  );

  TSimvolAST= Class;
  TTypeAST=Class;
  TImmConstAST=Class;
  TConstAST=Class;
  TIndentAST=Class;
  TOperatorAST=Class;
  TDeclarationAST=class;
  TConstDeclAST=class;
  TVarDeclAST=Class;
  TTypeDeclAST=class;
  TRealAST=class;
  TIntegerAST=class;
  TStringAST=class;
  TAddrAST=class;

  TForeignAsmAST=Class(TNodeAST)
      public
      Text:TSimvlerString;
    end;

  TParametrAST=Class(TNodeAST)
    mode:TParametrMode;
    VarDecl:TVarDeclAST;
    end;

  TList_ParametrAST=array of TParametrAST;
  TFormalParametrsAST=class(TNodeAST)
    private
    function GetCount: Integer;

    public
    List:TList_ParametrAST;
    property Count:Integer read GetCount;
    procedure Add(const mode:TParametrMode; const VarDecl:TVarDeclAST);
    end;

  TIndentDeclAST=class(TNodeAST)
      Indent:TIndentAST;
     end;
  TFuncAttr= record
    Call: (faHohaCall, faStdcall);
    Imp: (faHoha, faAssemble);
    end;
  TFuncDeclAST=Class(TIndentDeclAST)
      kind:(kfFunction,kfProcedure);
      Attr:TFuncAttr;
      FormalParametrs:TFormalParametrsAST;
      result:TTypeAST;
    end;


  TSignatureAST=class;
  TExpressionAST=Class;
  TExpressionListAST=class;
  TList_TExpressionAST=array of TExpressionAST;
  TDesignatorAST=class;
  TDesignatorAST=class(TNodeAST)
    Kind:(dkSignature, dkDesignature, dkDesignatureDot, dkDeSignatureArray, dkDeSignatureFunc);
    SubDesignator:TDesignatorAST;
    {dot}
    Signature:TSignatureAST;
    {array}
    Expression:TExpressionAST;
    {Func}
    ExpressionList:TExpressionListAST;
    end;

  TSignatureAST=class(TNodeAST)
    IndentDeclRef:TIndentDeclAST;
    Designator:TDesignatorAST;
  end;

  TFactorAST=class(TNodeAST)
    Kind:(fkSignature, fkImmConst);
    Signature: TSignatureAST;
    ImmConst:TImmConstAST;
    procedure Add(var Node:TNodeAST);
    end;


  TExpressionAST=class(TNodeAST)
    kind:(ekSimple, ekUnari,ekBinary);
    Factor1:TFactorAST;
    Op:TOperatorAST;
    Factor2:TFactorAST;
    end;

  TExpressionListAST=class(TNodeAST)
    Items:TList_TExpressionAST;
    procedure Add(Expression:TNodeAST);
    end;

  TVarAssignAST=Class(TNodeAST)
      Signature:TSignatureAST;
      Expression:TExpressionAST;
    end;

  TCallParametrAST=Class(TNodeAST)
  private
    function Get_Const: TConstAST;
    function GetSimvol: TSimvolAST;
    public
      kind:(cpUndefine, cpVarDecl,cpConstDecl, cpImmConst);
      VarDecl:TVarDeclAST;
      ConstDecl:TConstDeclAST;
      ImmConst:TImmConstAST;
      function GetIndent:TIndentAST;
      function Get_Type:TTypeAST;
      procedure Add(IndentOrIMM:TNodeAST);
      property  Indent:TIndentAST read GetIndent;
      property _Type:TTypeAST read Get_Type;
      property _Const:TConstAST read Get_Const;
      property simvol:TSimvolAST read GetSimvol;
    end;

  TCallParametrsAST=Class(TNodeAST)
      Items:array of TCallParametrAST;
      procedure Add(Indent:TNodeAST);
    end;

  TFuncCallAST=Class(TNodeAST)
      RefFunc:TFuncDeclAST;
      RefResult:TVarDeclAST;
      Parametrs:TCallParametrsAST;
    end;
  TTypeCastAST=Class(TNodeAST)
      Factor:TFactorAST;
      RefNewType:TTypeDeclAST;
     end;

  TStatementAST=Class;
  TCompaundStatementAST=Class;
  TLabelDeclAST=class;

  TGotoStmAST=class(TNodeAST)
    RefLabel:TLabelDeclAST;
  end;

  THostStatementAST=class(TNodeAST)
  public
    SubStack:Integer;
    procedure Replace(var BaseStatement, NewStatement:TStatementAST); virtual;
  end;

  TLoopStmAST=class(THostStatementAST)
  public
    StartLabel:TLabelDeclAST;
    EndLabel:TLabelDeclAST;
  end;

  TForMode=(fmTo,fmDownTo, fmError);
  TForStmAST=class(TLoopStmAST)
    RefVar:TVarDeclAST;
    Mode:TForMode;
    Expr1:TExpressionAST;
    Expr2:TExpressionAST;
    Statement:TStatementAST;
    procedure Replace(var BaseStatement, NewStatement:TStatementAST); override;
  end;
  TWhileStmAST=class(TLoopStmAST)
    BoolExpr:TVarDeclAST;
    Statement:TStatementAST;
    procedure Replace(var BaseStatement, NewStatement:TStatementAST); override;
  end;
  TRepeatStmAST=class(TLoopStmAST)
    BoolExpr:TVarDeclAST;
    CompaundStatement:TCompaundStatementAST;
  end;
  TIfStmAST=class(THostStatementAST)
    BoolExpr:TVarDeclAST;
    TrueEdge:TStatementAST;
    FalseEdge:TStatementAST;
    procedure Replace(var BaseStatement, NewStatement:TStatementAST); override;
  end;
  TCaseEdgeAST=class(THostStatementAST)
    Kind:(kceUndefine,kceRef,kceImmConst);
    ConstRef:TConstDeclAST;
    ImmConst:TImmConstAST;
    Statement:TStatementAST;
    procedure Add(var Node:TNodeAST);
    procedure Replace(var BaseStatement, NewStatement:TStatementAST); override;
  end;
  TList_TCaseEdgeAST= array of TCaseEdgeAST;

  TCaseStmAST=class(THostStatementAST)
    private
    public
    Edges:TList_TCaseEdgeAST;
    RefVar:TVarDeclAST;
    Defult:TStatementAST;
    procedure Add(var Node:TNodeAST);
    procedure Replace(var BaseStatement, NewStatement:TStatementAST); override;
  end;

  TSimpleStatementAST=class(TNodeAST)
    kind:(ssUndefine, ssNil, ssFuncCall, ssTypeCast, ssVarAssign);
    FuncCall:TFuncCallAST;
    TypeCast:TTypeCastAST;
    VarAssign:TVarAssignAST;
    procedure Add(var Node:TNodeAST);
  end;

  TLabelingStatementAST=class;
  TLabelingStatementAST=class(TNodeAST)
    kind:(ksLabeling,ksSimple);
    RefLabel:TLabelDeclAST;
    LabelingStatement:TLabelingStatementAST;
    SimpleStatement:TSimpleStatementAST;
    procedure Add(var Node:TNodeAST);
  end;

  TStatementAST=class(TNodeAST)
    Line:Int64;
    kind:(ksGoto, ksFor,ksWhile,ksRepeat,ksIf, ksCase, ksLabelingStatement,ksCompaundStatement);
    _Goto:TGotoStmAST;
    _For:TForStmAST;
    _While:TWhileStmAST;
    _Repeat:TRepeatStmAST;
    _IF:TIfStmAST;
    _Case:TCaseStmAST;
    LabelingStatement:TLabelingStatementAST;
    CompaundStatement:TCompaundStatementAST;
    procedure Add(Node:TNodeAST);
    procedure Assign(AStatementAST:TStatementAST);
    end;

  TList_TStatementAST=array of TStatementAST;
  TCompaundStatementAST=Class(THostStatementAST)
     ListStatemt:TList_TStatementAST;
  private
    function GetCount: Integer;
  published
     procedure Add(Node:TNodeAST);
     function GetIndex(Statement:TStatementAST):Integer;
     procedure AddUpper(BaseStatement, NewStatement:TStatementAST);
     procedure AddLower(BaseStatement, NewStatement:TStatementAST);
     procedure Replace(var BaseStatement, NewStatement:TStatementAST); override;
     property Count:Integer read GetCount;
  end;

  TFuncImpAST=Class(TNodeAST)
    FuncDecl:TFuncDeclAST;
    LocalDec:TDeclarationAST;
    CompaundStatement:TCompaundStatementAST;
    EndLabel:TLabelDeclAST;
    AsmBlock:TForeignAsmAST;
    end;

  TVarDeclAST=class(TIndentDeclAST)
      _Type:TTypeAST;
      _const:TConstAST;
      IsResult:Boolean;
    end;

  TConstDeclAST=Class(TVarDeclAST);

  TUsesDeclAST=class(TIndentDeclAST)
    end;

  TTypeDeclAST=class(TIndentDeclAST)
      _Type:TTypeAST;
    end;

  TLabelDeclAST=class(TIndentDeclAST)
     LabelingStatement:TLabelingStatementAST;
    end;

  TFieldDeclAST=class(TIndentDeclAST)
      _Type:TTypeAST;
    end;
{ TList_TTypeDeclAST= TList<TTypeDeclAST>;
  TList_TConstDeclAST=TList<TConstDeclAST>;
  TList_TVarDeclAST=TList<TVarDeclAST>;
}
  TList_TUsesDeclAST= array of TUsesDeclAST;
  TList_TTypeDeclAST= array of TTypeDeclAST;
  TList_TConstDeclAST= array of TConstDeclAST;
  TList_TVarDeclAST=array of TVarDeclAST;
  TList_TLabelDeclAST=array of TLabelDeclAST;
  TList_TFuncDeclAST=array of TFuncDeclAST;

  TLIst_TFuncImpAST=array of TFuncImpAST;
  TList_TIndentAST=array of TIndentAST;


  TDeclarationAST=Class(TNodeAST)
      public
      ListUses: TList_TUsesDeclAST;
      ListType: TList_TTypeDeclAST;
      ListConst:TList_TConstDeclAST;
      ListVar:TList_TVarDeclAST;
      ListLabel:TList_TLabelDeclAST;
      ListFunc:TList_TFuncDeclAST;
      ForeignAsm:TForeignAsmAST;
      procedure Add(Node:TNodeAST);
    end;

 TImplimentaionAST=class(TNodeAST)
      ListFunc:TList_TFuncImpAST;
      MainBlock:TCompaundStatementAST;
      ForeignAsm:TForeignAsmAST;
      procedure Add(Node:TNodeAST);
    end;

  TUnitAST=class(TNodeAST)
      Imp:TImplimentaionAST;
      Dec:TDeclarationAST;
      procedure Add(Node:TNodeAST);
    end;

  TSimvolAST=Class(TNodeAST)
     FSimvol:TSimvol;
     property Simvol:TSimvol read FSimvol Write FSimvol;
     constructor Create; overload;
     constructor Create(Simvol:TSimvol); overload;
     end;

  TTypeSimpleAST=Class(TSimvolAST)
    end;


  TList_TFieldDeclAST=array of TFieldDeclAST;

  TTypeRecordAST=Class(TNodeAST)
      Fields:TList_TFieldDeclAST;
      procedure Add(var Node:TNodeAST);
    end;

  TTypeArrayAST=Class(TNodeAST)
      Length:Integer;
      _Type:TTypeAST;
    end;

  TTypeAST=Class(TNodeAST)
     Kind:(tkUndefine, tkImmConst, tkSimple, tkAlias, tkRecord, tkArray, tkPointer);
     ImmConst:TTypeSimpleAST;
     Simple:TTypeSimpleAST;
     AliasType:TTypeDeclAST;
     _Record:TTypeRecordAST;
     _Array:TTypeArrayAST;
     _Pointer:TTypeAST;
      procedure Add(var Node:TNodeAST);
    end;


  TConstAST=Class(TSimvolAST)
//    constructor Create(_Const:TSimvol);
    end;
  TStringAST=Class(TConstAST)
    end;
  TCharAST=Class(TConstAST)
    end;
  TNilAST=Class(TConstAST)
    constructor Create;
    end;
  TIntegerAST=Class(TConstAST)
    end;
  TRealAST=Class(TConstAST)
    end;
  TAddrAST=class(TNodeAST)
   Signature:TSignatureAST;
   end;

  TImmConstAST= class(TNodeAST)
    Kind:(immUndefine, immString, immChar, immInteger, immReal, immNil, immAddr);
    Undefine: TNodeAST;
    _String: TStringAST;
    _Char: TCharAST;
    _Integer: TIntegerAST;
    _Real: TRealAST;
    _Nil: TNilAST;
    Addr:TAddrAST;
  private
    function Get_Const: TConstAST;
    function GetSimvol: TSimvolAST;
    function Get_Type: TTypeAST;
  public
    procedure Add(Node:TNodeAST);
    property _Const:TConstAST read Get_Const;
    property simvol:TSimvolAST read GetSimvol;
    property _Type:TTypeAST read Get_Type;
  end;

  TIndentAST=Class(TSimvolAST)
    Pos:Integer;
    Kind:TIndentKind;
//    constructor Create(Indent:TSimvol);
    end;


   TOperatorAST=Class(TSimvolAST)

    end;

  TStackAST=class(TNodeAST)
    private
    FStack:array of TNodeAST;
    function GetCount: Integer;
    public
    property Count:Integer read GetCount;
    procedure Push(NodeAST:TNodeAST);
    function Pop:TNodeAST;
    end;

var
   NilFormalParamet:TParametrAST;   // SingleTon
   UndefineTypeSimple:TTypeSimpleAST; // SingleTon
   ImmTypeSimpleInteger:TTypeSimpleAST;    // SingleTon
   ImmTypeSimpleReal:TTypeSimpleAST;    // SingleTon
   ImmTypeSimpleChar:TTypeSimpleAST;    // SingleTon
   ImmTypeSimpleString:TTypeSimpleAST;    // SingleTon
   ImmTypeSimpleNil:TTypeSimpleAST;       // SingleTon
   ImmTypeSimpleAddr:TTypeSimpleAST;      // SingleTon
   ImmTypeSimpleUndefine:TTypeSimpleAST;  // SingleTon
   ImmTypeInteger:TTypeAST;    // SingleTon
   ImmTypeReal:TTypeAST;       // SingleTon
   ImmTypeChar:TTypeAST;    // SingleTon
   ImmTypeString:TTypeAST;    // SingleTon
   ImmTypeNil:TTypeAST;       // SingleTon
   ImmTypeAddr:TTypeAST;      // SingleTon
   ImmTypeUndefine:TTypeAST;  // SingleTon
implementation


{ TASTDeclaration }

procedure TDeclarationAST.Add(Node: TNodeAST);
  procedure ListConst_Add(Node:TConstDeclAST);
  begin
    Node.Parent:=Self;
    SetLength(ListConst,Length(ListConst)+1);
    ListConst[Length(ListConst)-1]:=Node;
  end;

  procedure ListUses_Add(Node:TUsesDeclAST);
  begin
    Node.Parent:=Self;
    SetLength(ListUses,Length(ListUses)+1);
    ListUses[Length(ListUses)-1]:=Node;
  end;

  procedure ListType_Add(Node:TTypeDeclAST);
  begin
    Node.Parent:=Self;
    SetLength(ListType,Length(ListType)+1);
    ListType[Length(ListType)-1]:=Node;
  end;

  procedure ListVar_Add(Node:TVarDeclAST);
  begin
    Node.Parent:=Self;
    SetLength(ListVar,Length(ListVar)+1);
    ListVar[Length(ListVar)-1]:=Node;
  end;

  procedure ListLabel_Add(Node:TLabelDeclAST);
  begin
    Node.Parent:=Self;
    SetLength(ListLabel,Length(ListLabel)+1);
    ListLabel[Length(ListLabel)-1]:=Node;
  end;

  procedure ListFunc_Add(Node:TFuncDeclAST);
  begin
    Node.Parent:=Self;
    SetLength(ListFunc,Length(ListFunc)+1);
    ListFunc[Length(ListFunc)-1]:=Node;
  end;

  procedure ForeignAsmAdd(Node:TForeignAsmAST);
  begin
    if ForeignAsm= nil then
         ForeignAsm:=Node
       else
         begin
         AddString(ForeignAsm.Text, Node.Text);
         end;
  end;
var
  DeclarationAST:TDeclarationAST;
  i:Integer;
begin
if Node is TConstDeclAST then
   ListConst_Add(Node as TConstDeclAST)
   else if Node is TVarDeclAST then
           ListVar_Add(Node as TVarDeclAST);
if Node is TTypeDeclAST then
   ListType_Add(Node as TTypeDeclAST);
if Node is TLabelDeclAST then
   ListLabel_Add(Node as TLabelDeclAST);
if Node is TFuncDeclAST then
   ListFunc_Add(Node as TFuncDeclAST);
if Node is TForeignAsmAST then
   ForeignAsmAdd(Node as TForeignAsmAST);
if Node is TUsesDeclAST then
   ListUses_Add(Node as TUsesDeclAST);

if Node is TDeclarationAST then
   begin
   DeclarationAST:=Node as TDeclarationAST;
   for i:=0 to Length(DeclarationAST.ListUses)-1 do
     ListUses_Add(DeclarationAST.ListUses[i]);

   for i:=0 to Length(DeclarationAST.ListType)-1 do
     ListType_Add(DeclarationAST.ListType[i]);

   for i:=0 to Length(DeclarationAST.ListConst)-1 do
     ListConst_Add(DeclarationAST.ListConst[i]);

   for i:=0 to Length(DeclarationAST.ListVar)-1 do
     ListVar_Add(DeclarationAST.ListVar[i]);

   for i:=0 to Length(DeclarationAST.ListLabel)-1 do
     ListLabel_Add(DeclarationAST.ListLabel[i]);

   for i:=0 to Length(DeclarationAST.ListFunc)-1 do
     ListFunc_Add(DeclarationAST.ListFunc[i]);

   ForeignAsmAdd(DeclarationAST.ForeignAsm);
   end;
end;

{ TSimvolAST }

constructor TSimvolAST.Create(Simvol: TSimvol);
begin
   inherited Create;
  FSimvol:=Simvol;
end;

constructor TSimvolAST.Create;
begin
   inherited Create;

end;

{ TFormalParametrsAST }

procedure TFormalParametrsAST.Add(const mode: TParametrMode;
  const VarDecl:TVarDeclAST);
begin
SetLength(List,Count+1);
List[Count-1]:=TParametrAST.Create;
List[Count-1].mode:=mode;
List[Count-1].VarDecl:=VarDecl;
List[Count-1].Parent:=self;
end;

function TFormalParametrsAST.GetCount: Integer;
begin
Result:=Length(List);
end;

{ TStackAST }

function TStackAST.GetCount: Integer;
begin
Result:=Length(FStack);
end;

function TStackAST.Pop: TNodeAST;
begin
if Count-1<0 then
   Result:=nil
   else
   begin
   Result:=FStack[Count-1];
   SetLength(FStack,Count-1);
   end;
end;

procedure TStackAST.Push(NodeAST: TNodeAST);
begin
if NodeAST<>Nil then
   begin
   SetLength(FStack,Count+1);
   FStack[Count-1]:=NodeAST;
   end;
end;

{ TImplimentaionAST }

procedure TImplimentaionAST.Add(Node: TNodeAST);
  procedure FuncAdd(Node:TFuncImpAST);
  begin
    Node.Parent:=Self;
    Setlength(ListFunc,Length(ListFunc)+1);
    ListFunc[Length(ListFunc)-1]:=Node;
  end;

  procedure ForeignAsmAdd(Node:TForeignAsmAST);
  begin
    if ForeignAsm= nil then
         ForeignAsm:=Node
       else
         AddString(ForeignAsm.Text, Node.Text);
  end;
var
  ImplimentaionAST:TImplimentaionAST;
  i:Integer;
begin
if Node is TFuncImpAST then
   begin
   FuncAdd(Node as TFuncImpAST);
   end;
if Node is TCompaundStatementAST then
   begin
   MainBlock:=Node as TCompaundStatementAST;
   end;
if Node is TForeignAsmAST then
   ForeignAsmAdd(Node as TForeignAsmAST);

if Node is TImplimentaionAST then
   begin
   ImplimentaionAST:=Node as TImplimentaionAST;
   for i:=0 to Length(ImplimentaionAST.ListFunc)-1 do
      FuncAdd(ImplimentaionAST.ListFunc[i]);
   ForeignAsmAdd(ImplimentaionAST.ForeignAsm);
   if ImplimentaionAST.MainBlock<>nil then
      begin
      ImplimentaionAST.MainBlock.Parent:=Self;
      self.MainBlock:=ImplimentaionAST.MainBlock;
      end;
   end;
end;

{ TStatementAST }

procedure TStatementAST.Add(Node: TNodeAST);
begin
if Node Is TGoToStmAST then
   begin
   Kind:=ksGoto;
   _GoTo:=Node as TGoToStmAST;
   end;
if Node Is TForStmAST then
   begin
   Kind:=ksFor;
   _For:=Node as TForStmAST;
   end;
if Node Is TWhileStmAST then
   begin
   Kind:=ksWhile;
   _While:=Node as TWhileStmAST;
   end;
if Node Is TRepeatStmAST then
   begin
   Kind:=ksRepeat;
   _Repeat:=Node as TRepeatStmAST;
   end;
if Node Is TIfStmAST then
   begin
   Kind:=ksIf;
   _if:=Node as TIfStmAST;
   end;
if Node Is TCaseStmAST then
   begin
   Kind:=ksCase;
   _Case:=Node as TCaseStmAST;
   end;
if Node Is TLabelingStatementAST then
   begin
   Kind:=ksLabelingStatement;
   LabelingStatement:=Node as TLabelingStatementAST;
   end;
if Node Is TCompaundStatementAST then
   begin
   Kind:=ksCompaundStatement;
   CompaundStatement:=Node as TCompaundStatementAST;
   end;
end;

procedure TStatementAST.Assign(AStatementAST: TStatementAST);
begin
Self.Line:=AStatementAST.Line;
Self.kind:=AStatementAST.kind;
Self._Goto:=AStatementAST._Goto;
Self._For:=AStatementAST._For;
Self._While:=AStatementAST._While;
Self._Repeat:=AStatementAST._Repeat;
Self._IF:=AStatementAST._IF;
Self._Case:=AStatementAST._Case;
Self.LabelingStatement:=AStatementAST.LabelingStatement;
Self.CompaundStatement:=AStatementAST.CompaundStatement;
end;

{ TCompaundStatementAST }

procedure TCompaundStatementAST.Add(Node: TNodeAST);
begin
if Node<>Nil then
   begin
   Setlength(ListStatemt,Length(ListStatemt)+1);
   ListStatemt[Length(ListStatemt)-1]:=Node as TStatementAST;
   end;
end;

procedure TCompaundStatementAST.AddLower(BaseStatement,
  NewStatement: TStatementAST);
var
  Index, i:Integer;
begin
 if Length(ListStatemt)=0 then Add(NewStatement)
  else
  begin
  Index:=GetIndex(BaseStatement)+1;
  if Index>=0 then
     begin
     SetLength(ListStatemt,Length(ListStatemt)+1);
     for i:=Length(ListStatemt)-2 downto Index do
       ListStatemt[i+1]:=ListStatemt[i];
     ListStatemt[Index]:=NewStatement;
     end;
  end;
end;

procedure TCompaundStatementAST.AddUpper(BaseStatement,
  NewStatement: TStatementAST);
var
  Index, i:Integer;
begin
 if Length(ListStatemt)=0 then Add(NewStatement)
  else
  begin
  Index:=GetIndex(BaseStatement);
  if Index>=0 then
     begin
     SetLength(ListStatemt,Length(ListStatemt)+1);
     for i:=Length(ListStatemt)-2 downto Index do
       ListStatemt[i+1]:=ListStatemt[i];
     ListStatemt[Index]:=NewStatement;
     end;
  end;
end;

function TCompaundStatementAST.GetCount: Integer;
begin
result:=Length(ListStatemt);
end;

function TCompaundStatementAST.GetIndex(Statement: TStatementAST): Integer;
var i:Integer;
begin
  result:=-1;
  for i:=0 to Length(ListStatemt)-1 do
    begin
     if ListStatemt[i]= Statement then
        begin
          result:=i;
          exit;
        end;
    end;
end;

procedure TCompaundStatementAST.Replace(var BaseStatement,
  NewStatement: TStatementAST);
var Index:Integer;
begin
  Index:=GetIndex(BaseStatement);
  ListStatemt[Index]:=NewStatement;
  NewStatement:=BaseStatement;
  BaseStatement:=ListStatemt[Index];
end;

{ TNodeAST }

procedure TNodeAST.AddChild(Child: TNodeAST);
begin
SetLength(Childs,Length(Childs)+1);
Childs[Length(Childs)]:=Child;
Child.Parent:=Self;
end;

{ TCaseStmAST }

procedure TCaseStmAST.Add(var Node: TNodeAST);
begin
if Node is TCaseEdgeAST then
   begin
   SetLength(Edges,Length(Edges)+1);
   Edges[Length(Edges)-1]:=Node as TCaseEdgeAST;
   end;
end;

procedure TCaseStmAST.Replace(var BaseStatement,
  NewStatement: TStatementAST);
var tmp:TStatementAST;
begin
 if BaseStatement=Defult then
   begin
   tmp:=Defult;
   Defult:=NewStatement;
   NewStatement:=Tmp;
   end;
end;

{ TLabelingStatementAST }

procedure TLabelingStatementAST.Add(var Node: TNodeAST);
begin
if Node is TLabelingStatementAST then
   begin
   Kind:=ksLabeling;
   LabelingStatement:=Node as TLabelingStatementAST;
   end;
if Node is TSimpleStatementAST then
   begin
   Kind:=ksSimple;
   SimpleStatement:=Node as TSimpleStatementAST;
   end;
end;

{ TCallParametrsAST }

procedure TCallParametrsAST.Add(Indent: TNodeAST);
begin
  SetLength(Items,Length(Items)+1);
  Items[Length(Items)-1]:=TCallParametrAST.Create;
  Items[Length(Items)-1].Add(Indent);
end;

{ TFactorAST }

procedure TFactorAST.Add(var Node: TNodeAST);
begin
Self.Kind:=fkImmConst;
if Node is TSignatureAST then
   begin
   Self.Kind:=fkSignature;
   Self.Signature:=Node as TSignatureAST;
   end else
   begin
   Self.Kind:=fkImmConst;
   Self.ImmConst:=Node as TImmConstAST;
   end;
end;

{ TSimpleStatementAST }

procedure TSimpleStatementAST.Add(var Node: TNodeAST);
begin
Self.Kind:=ssUndefine;
if Node is TFuncCallAST then
   begin
   Kind:=ssFuncCall;
   FuncCall:=Node as TFuncCallAST;
   end;
if Node is TTypeCastAST then
   begin
   Kind:=ssTypeCast;
   TypeCast:=Node as TTypeCastAST;
   end;
if Node is TVarAssignAST then
   begin
   Kind:=ssVarAssign;
   VarAssign:=Node as TVarAssignAST;
   end;
end;

{ TTypeRecordAST }

procedure TTypeRecordAST.Add(var Node: TNodeAST);
begin
  if Node is TFieldDeclAST then
    begin
    SetLength(Fields,Length(Fields)+1);
    Fields[Length(Fields)-1]:=Node as TFieldDeclAST;
    end;
end;

{ TCaseEdgeAST }

procedure TCaseEdgeAST.Add(var Node: TNodeAST);
begin
Kind:=kceUndefine;
if Node is TConstDeclAST then
   begin
     Kind:=kceRef;
     ConstRef:=Node as TConstDeclAST;
   end;
if Node is TImmConstAST then
   begin
     Kind:=kceImmConst;
     ImmConst:=Node as TImmConstAST;
   end;
end;

procedure TCaseEdgeAST.Replace(var BaseStatement,
  NewStatement: TStatementAST);
var tmp:TStatementAST;
begin
 if BaseStatement=self.Statement then
   begin
   tmp:=self.Statement;
   self.Statement:=NewStatement;
   NewStatement:=Tmp;
   end;
end;

{ TExpressionListAST }

procedure TExpressionListAST.Add(Expression: TNodeAST);
begin
 if (Expression is TExpressionAST) or (Expression=Nil) then
   begin
   SetLength(Items, Length(Items)+1);
   Items[Length(Items)-1]:=Expression as TExpressionAST;
   end;
end;

{ TTypeAST }

procedure TTypeAST.Add(var Node: TNodeAST);
begin
 Kind:=tkUndefine;
 if Node Is TTypeSimpleAST then
    begin
    Simple:=Node as TTypeSimpleAST ;
    Kind:=tkSimple;
    end;

 if Node Is TTypeDeclAST then
    begin
    AliasType:=Node as TTypeDeclAST ;
    Kind:=tkAlias;
    end;

 if Node Is TTypeRecordAST then
    begin
    _Record:=Node as TTypeRecordAST ;
    Kind:=tkRecord;
    end;

 if Node Is TTypeArrayAST then
    begin
    _Array:=Node as TTypeArrayAST ;
    Kind:=tkArray;
    end;
 if Node Is TTypeAST then
    begin
    _Pointer:=Node as TTypeAST;
    Kind:=tkPointer;
    end;
end;

{ TCallParametrAST }

function TCallParametrAST.GetIndent:TIndentAST;
begin
case Kind of
cpVarDecl:Result:=VarDecl.Indent;
cpConstDecl:Result:=ConstDecl.Indent;
end;
end;

function TCallParametrAST.Get_Type:TTypeAST;
begin
case Kind of
cpVarDecl:Result:=VarDecl._Type;
cpConstDecl:Result:=ConstDecl._Type;
cpImmConst:result:=ImmConst._Type;
end;
end;

procedure TCallParametrAST.Add(IndentOrIMM: TNodeAST);
begin
  if IndentOrIMM is TVarDeclAST then
    begin
    kind:=cpVarDecl;
    VarDecl:=IndentOrIMM as TVarDeclAST;
    end;
  if IndentOrIMM is TConstDeclAST then
    begin
    kind:=cpConstDecl;
    ConstDecl:=IndentOrIMM as TConstDeclAST;
    end;
  if IndentOrIMM is TImmConstAST then
    begin
    Kind:=cpImmConst;
    ImmConst:= IndentOrIMM as TImmConstAST;
    end;
end;

function TCallParametrAST.Get_Const: TConstAST;
begin
  case Kind of
  cpVarDecl:  result := VarDecl._const;
  cpConstDecl:result := ConstDecl._const;
  cpImmConst: result := ImmConst._const;
  end;
end;

function TCallParametrAST.GetSimvol: TSimvolAST;
begin
  case Kind of
  cpVarDecl:  result := VarDecl.Indent as TSimvolAST;
  cpConstDecl:result := ConstDecl.Indent as TSimvolAST;
  cpImmConst: result := ImmConst.simvol;
  end;
end;

{ TNilAST }

constructor TNilAST.Create;
begin
  inherited Create(TS('0'));
end;

{ TImmConstAST }

procedure TImmConstAST.Add(Node: TNodeAST);
var immConst:TImmConstAST;
begin
 if Node Is TImmConstAST then
    begin
    immConst:=Node as TImmConstAST;
    case immConst.Kind of
    immUndefine: Node:=immConst.Undefine;
    immChar: Node:=immConst._Char;
    immString: Node:=immConst._String;
    immInteger: Node:=immConst._Integer;
    immReal: Node:=immConst._Real;
    immNil: Node:=immConst._Nil;
    immAddr: Node:=immConst.Addr;
    end; // case;
    end;
 if Node Is TIntegerAST then
    begin
    Kind:=immInteger;
    _Integer:=Node as TIntegerAST;
    end else
 if Node Is TRealAST then
    begin
    Kind:=immReal;
    _Real:=Node as TRealAST;
    end else
 if Node Is TCharAST then
    begin
    Kind:=immChar;
    _Char:=Node as TCharAST;
    end else
 if Node Is TStringAST then
    begin
    Kind:=immString;
    _String:=Node as TStringAST;
    end else
 if Node Is TNilAST then
    begin
    Kind:=immNil;
    _Nil:=Node as TNilAST;
    end else
 if Node Is TAddrAST then
    begin
    Kind:=immAddr;
    Addr:=Node as TAddrAST;
    end else
    begin
    Kind:=immUndefine;
    Undefine:=Node;
    end;
end;

function TImmConstAST.Get_Const: TConstAST;
begin
  case kind of
  ImmInteger: result:=_Integer as TConstAST;
  ImmReal: result:=_Real as TConstAST;
  ImmChar: result:=_Char as TCharAST;
  ImmString: result:=_String as TConstAST;
  ImmNil: result:=_Nil as TConstAST;
  ImmAddr:  result:=_Integer as TConstAST;
  ImmUndefine: result:=TConstAST.Create;
  end;
end;

function TImmConstAST.GetSimvol: TSimvolAST;
begin
  case kind of
  ImmInteger: result:=_Integer as TConstAST;
  ImmReal: result:=_Real as TConstAST;
  ImmChar: result:=_Char as TConstAST;
  ImmString: result:=_String as TConstAST;
  ImmNil: result:=_Nil as TConstAST;
  ImmAddr:  result:=_Integer as TConstAST;
  ImmUndefine: result:=TConstAST.Create;
  end;
end;

function TImmConstAST.Get_Type: TTypeAST;
begin
  case kind of
  ImmInteger: result:=ImmTypeInteger;
  ImmReal: result:=ImmTypeReal;
  ImmChar: result:=ImmTypeChar;
  ImmString: result:=ImmTypeString;
  ImmNil: result:=ImmTypeNil;
  ImmAddr:  result:=ImmTypeAddr;
  ImmUndefine: result:=ImmTypeUndefine;
  end;
end;

{ TUnitAST }

procedure TUnitAST.Add(Node: TNodeAST);
var ChildUnit:TUnitAST;
begin
  if (Node is TUnitAST) then
    begin
      ChildUnit:=Node as TUnitAST;
      if (Self.Imp=nil) then
         begin
         Self.Imp:= TImplimentaionAST.Create;
         Self.Imp.Parent:=Self;
         end;
      if (Self.Dec=nil) then
         begin
         Self.Dec:= TDeclarationAST.Create;
         Self.Dec.Parent:=Self;
         end;

      self.Imp.Add(ChildUnit.Imp);
      self.Dec.Add(ChildUnit.Dec);
    end;
end;

{ THostStatementAST }

procedure THostStatementAST.Replace(var BaseStatement,
  NewStatement: TStatementAST);
begin

end;

{ TForStmAST }

procedure TForStmAST.Replace(var BaseStatement,
  NewStatement: TStatementAST);
var tmp:TStatementAST;
begin
 if BaseStatement=self.Statement then
   begin
   tmp:=self.Statement;
   self.Statement:=NewStatement;
   NewStatement:=Tmp;
   end;
end;

{ TWhileStmAST }

procedure TWhileStmAST.Replace(var BaseStatement,
  NewStatement: TStatementAST);
var tmp:TStatementAST;
begin
 if BaseStatement=self.Statement then
   begin
   tmp:=self.Statement;
   self.Statement:=NewStatement;
   NewStatement:=Tmp;
   end;
end;

{ TIfStmAST }

procedure TIfStmAST.Replace(var BaseStatement,
  NewStatement: TStatementAST);
var tmp:TStatementAST;
begin
 if BaseStatement=self.TrueEdge then
   begin
   tmp:=TrueEdge;
   TrueEdge:=NewStatement;
   NewStatement:=Tmp;
   end;
 if BaseStatement=FalseEdge then
   begin
   tmp:=FalseEdge;
   FalseEdge:=NewStatement;
   NewStatement:=Tmp;
   end;
end;

begin
  NilFormalParamet:=TParametrAST.Create;
  NilFormalParamet.mode:=pmVar;
  UndefineTypeSimple:=TTypeSimpleAST.Create(TS('Undefine'));
  ImmTypeSimpleInteger:=TTypeSimpleAST.Create(TS('Integer'));
  ImmTypeSimpleReal:=TTypeSimpleAST.Create(TS('Real'));
  ImmTypeSimpleString:=TTypeSimpleAST.Create(TS('Char'));
  ImmTypeSimpleString:=TTypeSimpleAST.Create(TS('String'));
  ImmTypeSimpleNil:=TTypeSimpleAST.Create(TS('Pointer'));
  ImmTypeSimpleAddr:=TTypeSimpleAST.Create(TS('Pointer'));
  ImmTypeSimpleUndefine:=TTypeSimpleAST.Create(TS('Undefine'));

  ImmTypeInteger:=TTypeAST.Create;
  ImmTypeInteger.Kind:=tkImmConst;
  ImmTypeInteger.ImmConst:=ImmTypeSimpleInteger;

  ImmTypeReal:=TTypeAST.Create;
  ImmTypeReal.Kind:=tkImmConst;
  ImmTypeReal.ImmConst:=ImmTypeSimpleReal;

  ImmTypeChar:=TTypeAST.Create;
  ImmTypeChar.Kind:=tkImmConst;
  ImmTypeChar.ImmConst:=ImmTypeSimpleChar;

  ImmTypeString:=TTypeAST.Create;
  ImmTypeString.Kind:=tkImmConst;
  ImmTypeString.ImmConst:=ImmTypeSimpleString;

  ImmTypeNil:=TTypeAST.Create;
  ImmTypeNil.Kind:=tkImmConst;
  ImmTypeNil.ImmConst:=ImmTypeSimpleNil;

  ImmTypeAddr:=TTypeAST.Create;
  ImmTypeAddr.Kind:=tkImmConst;
  ImmTypeAddr.ImmConst:=ImmTypeSimpleAddr;

  ImmTypeUndefine:=TTypeAST.Create;
  ImmTypeUndefine.Kind:=tkImmConst;
  ImmTypeUndefine.ImmConst:=ImmTypeSimpleUndefine;
end.
