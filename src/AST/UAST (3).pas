unit UAST;

interface

uses USimvler;
//uses System.Generics.Collections;

type
  TNodeAST=Class
     Parent:TNodeAST;
     Childs:array of TNodeAST;
     procedure AddChild(Child:TNodeAST);
     End;

  TASTNodeType=
    (atTUnitAST,
     atTImplimentaionAST,
     atTDeclarationAST,
     atTTypeDeclAST,
     atTConstDeclAST,
     atTVarDeclAST,
     atTTypeAST,
     atTConstAST,
     atTIndentAST,
     atTFuncDeclAST
     );

  TParametrMode=(
    pmVar,
    pmConst,
    pmParam);

  TIndentKind=(
    ikUnit,
    ikFunction,
    ikVar,
    ikType,
    ikLabel,
    ikConst,
    ikUndefine);

  TTypeAST=Class;
  TConstAST=Class;
  TIndentAST=Class;
  TOperatorAST=Class;
  TDeclarationAST=class;
  TVarDeclAST=Class;
  TTypeDeclAST=class;

  TParametrAST=Class(TNodeAST)
    mode:TParametrMode;
    Indent:TIndentAST;
    _Type:TTypeAST;
    end;

  TFormalParametrsAST=class(TNodeAST)
    private
    function GetCount: Integer;

    public
    List:array of TParametrAST;
    property Count:Integer read GetCount;
    procedure Add(const mode:TParametrMode; const Indent:TNodeAST; const _Type:TNodeAST);
    end;

  TIndentDeclAST=class(TNodeAST)
      Indent:TIndentAST;
     end;
  TFuncDeclAST=Class(TIndentDeclAST)
      kind:(kfFunction,kfProcedure);
      FormalParametrs:TFormalParametrsAST;
      result:TTypeAST;
    end;


  TExpressionAST=Class;
  TSignatureAST=Class;
  TDesignatorAST=class;
  TDesignatorAST=class(TNodeAST)
    Kind:(dkSignature, dkDesignature, dkDesignatureDot, dkDesignatureArray);
    Signature:TSignatureAST;
    SubDesignator:TDesignatorAST;
    Expression:TExpressionAST;
    end;

  TSignatureAST=class(TNodeAST)
    IndentDeclRef:TNodeAST;
    Designator:TDesignatorAST;
    end;

  TFactorAST=class(TNodeAST)
    Kind:(fkUndefine, fkDesignator,fkConst, fkString, fkNumber, fkNil, fkAddr);
    Node:TNodeAST;
    procedure Add(var Node:TNodeAST);
    end;


  TExpressionAST=class(TNodeAST)
    kind:(ekUnari,ekBinary);
    Factor1:TFactorAST;
    Op:TOperatorAST;
    Factor2:TFactorAST;
    end;

  TVarAssignAST=Class(TNodeAST)
      Designator:TDesignatorAST;
      Expression:TExpressionAST;
    end;
  TCallParametrsAST=Class(TNodeAST)
      Items:array of TVarDeclAST;
      procedure Add(Indent:TNodeAST);
    end;

  TFuncCallAST=Class(TNodeAST)
      RefFunc:TFuncDeclAST;
      RefResult:TVarDeclAST;
      Parametrs:TCallParametrsAST;
    end;
  TTypeCastAST=Class(TNodeAST)
      Factor:TFactorAST;
      RefNewType:TTypeDeclAST;
     end;

  TStatementAST=Class;
  TCompaundStatementAST=Class;

  TGotoSmtAST=class(TNodeAST)
    Indent:TIndentAST;
  end;

  TForMode=(fmTo,fmDownTo, fmError);
  TForSmtAST=class(TNodeAST)
    Indent:TIndentAST;
    Mode:TForMode;
    Expr1:TIndentAST;
    Expr2:TIndentAST;
    Statement:TStatementAST;
  end;
  TWhileSmtAST=class(TNodeAST)
    BoolExpr:TIndentAST;
    Statement:TStatementAST;
  end;
  TRepeatSmtAST=class(TNodeAST)
    BoolExpr:TIndentAST;
    CompaundStatement:TCompaundStatementAST;
  end;
  TIfSmtAST=class(TNodeAST)
    BoolExpr:TIndentAST;
    TrueEdge:TStatementAST;
    FalseEdge:TStatementAST;
  end;
  TCaseEdgeAST=class(TNodeAST)
    _Label:TIndentAST;
    Statement:TStatementAST;
  end;
  TList_TCaseEdgeAST= array of TCaseEdgeAST;

  TCaseSmtAST=class(TNodeAST)
    private
    FEdges:TList_TCaseEdgeAST;
    public
    Indent:TIndentAST;
    Defult:TStatementAST;
    procedure Add(var Node:TNodeAST);
  end;

  TSimpleStatementAST=class(TNodeAST)
  end;

  TLabelingStatementAST=class;
  TLabelingStatementAST=class(TNodeAST)
    _Label:TIndentAST;
    kind:(ksLabeling,ksSimple);
    LabelingStatement:TLabelingStatementAST;
    SimpleStatement:TSimpleStatementAST;
    procedure Add(var Node:TNodeAST);
  end;

  TStatementAST=class(TNodeAST)
    kind:(ksGoto, ksFor,ksWhile,ksRepeat,ksIf, ksCase, ksLabelingStatement,ksCompaundStatement);
    _Goto:TGotoSmtAST;
    _For:TForSmtAST;
    _While:TWhileSmtAST;
    _Repeat:TRepeatSmtAST;
    _IF:TIfSmtAST;
    _Case:TCaseSmtAST;
    LabelingStatement:TLabelingStatementAST;
    CompaundStatement:TCompaundStatementAST;
    procedure Add(Node:TNodeAST);
    end;

  TList_TStatementAST=array of TStatementAST;
  TCompaundStatementAST=Class(TNodeAST)
    ListStatemt:TList_TStatementAST;
    procedure Add(Node:TNodeAST);
    end;

  TFuncImpAST=Class(TNodeAST)
    FuncDecl:TFuncDeclAST;
    LocalDec:TDeclarationAST;
    CompaundStatement:TCompaundStatementAST;
    end;

  TConstDeclAST=Class(TIndentDeclAST)
      _Type:TTypeAST;
      _const:TConstAST;
    end;

  TVarDeclAST=class(TIndentDeclAST)
      _Type:TTypeAST;
      _const:TConstAST;
    end;

  TTypeDeclAST=class(TIndentDeclAST)
      _Type:TTypeAST;
    end;

  TLabelDeclAST=class(TIndentDeclAST)
    end;

{ TList_TTypeDeclAST= TList<TTypeDeclAST>;
  TList_TConstDeclAST=TList<TConstDeclAST>;
  TList_TVarDeclAST=TList<TVarDeclAST>;
}
  TList_TTypeDeclAST= array of TTypeDeclAST;
  TList_TConstDeclAST= array of TConstDeclAST;
  TList_TVarDeclAST=array of TVarDeclAST;
  TList_TLabelDeclAST=array of TLabelDeclAST;
  TList_TFuncDeclAST=array of TFuncDeclAST;

  TLIst_TFuncImpAST=array of TFuncImpAST;
  TList_TIndentAST=array of TIndentAST;

  TDeclarationAST=Class(TNodeAST)
      public
      ListType: TList_TTypeDeclAST;
      ListConst:TList_TConstDeclAST;
      ListVar:TList_TVarDeclAST;
      ListLabel:TList_TLabelDeclAST;
      ListFunc:TList_TFuncDeclAST;
      procedure Add(Node:TNodeAST);
    end;

 TImplimentaionAST=class(TNodeAST)
      ListFunc:TList_TFuncImpAST;
      MainBlock:TCompaundStatementAST;
      procedure Add(Node:TNodeAST);
    end;

  TUnitAST=class(TNodeAST)
      Imp:TImplimentaionAST;
      Dec:TDeclarationAST;
    end;

  TSimvolAST=Class(TNodeAST)
     FSimvol:TSimvol;
     property Simvol:TSimvol read FSimvol Write FSimvol;
     constructor Create; overload;
     constructor Create(Simvol:TSimvol); overload;
     end;

  TTypeAST=Class(TSimvolAST)
//    constructor Create(SimpleType:TSimvol);
    end;
  TConstAST=Class(TSimvolAST)
//    constructor Create(_Const:TSimvol);
    end;
  TStringAST=Class(TConstAST)
    end;
  TNilAST=Class(TConstAST)
    end;
  TNumberAST=Class(TConstAST)
    end;
  TIndentAST=Class(TSimvolAST)
    Pos:Integer;
    Kind:TIndentKind;
//    constructor Create(Indent:TSimvol);
    end;

  TAddrAST=class(TNodeAST)
   IndentRef:TNodeAST;
   end;

   TOperatorAST=Class(TSimvolAST)

    end;

  TStackAST=class(TNodeAST)
    private
    FStack:array of TNodeAST;
    function GetCount: Integer;
    public
    property Count:Integer read GetCount;
    procedure Push(NodeAST:TNodeAST);
    function Pop:TNodeAST;
    end;

implementation


{ TASTDeclaration }

procedure TDeclarationAST.Add(Node: TNodeAST);
  procedure ListConst_Add(Node:TConstDeclAST);
  begin
    SetLength(ListConst,Length(ListConst)+1);
    ListConst[Length(ListConst)-1]:=Node;
  end;

  procedure ListType_Add(Node:TTypeDeclAST);
  begin
    SetLength(ListType,Length(ListType)+1);
    ListType[Length(ListType)-1]:=Node;
  end;

  procedure ListVar_Add(Node:TVarDeclAST);
  begin
    SetLength(ListVar,Length(ListVar)+1);
    ListVar[Length(ListVar)-1]:=Node;
  end;

  procedure ListLabel_Add(Node:TLabelDeclAST);
  begin
    SetLength(ListLabel,Length(ListLabel)+1);
    ListLabel[Length(ListLabel)-1]:=Node;
  end;

  procedure ListFunc_Add(Node:TFuncDeclAST);
  begin
    SetLength(ListFunc,Length(ListFunc)+1);
    ListFunc[Length(ListFunc)-1]:=Node;
  end;

begin
if Node is TConstDeclAST then
   ListConst_Add(Node as TConstDeclAST);
if Node is TTypeDeclAST then
   ListType_Add(Node as TTypeDeclAST);
if Node is TVarDeclAST then
   ListVar_Add(Node as TVarDeclAST);
if Node is TLabelDeclAST then
   ListLabel_Add(Node as TLabelDeclAST);
if Node is TFuncDeclAST then
   ListFunc_Add(Node as TFuncDeclAST);

end;

{ TSimvolAST }

constructor TSimvolAST.Create(Simvol: TSimvol);
begin
   inherited Create;
  FSimvol:=Simvol;
end;

constructor TSimvolAST.Create;
begin
   inherited Create;

end;

{ TFormalParametrsAST }

procedure TFormalParametrsAST.Add(const mode: TParametrMode;
  const Indent: TNodeAST; const _Type:TNodeAST);
begin
SetLength(List,Count+1);
List[Count-1]:=TParametrAST.Create;
List[Count-1].mode:=mode;
List[Count-1].Indent:=Indent as TIndentAST;
List[Count-1]._Type:=_Type as TTypeAST;
end;

function TFormalParametrsAST.GetCount: Integer;
begin
Result:=Length(List);
end;

{ TStackAST }

function TStackAST.GetCount: Integer;
begin
Result:=Length(FStack);
end;

function TStackAST.Pop: TNodeAST;
begin
Result:=FStack[Count-1];
SetLength(FStack,Count-1);
end;

procedure TStackAST.Push(NodeAST: TNodeAST);
begin
SetLength(FStack,Count+1);
FStack[Count-1]:=NodeAST;
end;

{ TImplimentaionAST }

procedure TImplimentaionAST.Add(Node: TNodeAST);
begin
if Node Is TFuncImpAST then
   begin
   Setlength(ListFunc,Length(ListFunc)+1);
   ListFunc[Length(ListFunc)-1]:=Node as TFuncImpAST;
   end;
if Node Is TCompaundStatementAST then
   begin
   MainBlock:=Node as TCompaundStatementAST;
   end;
end;

{ TStatementAST }

procedure TStatementAST.Add(Node: TNodeAST);
begin
if Node Is TForSmtAST then
   begin
   Kind:=ksFor;
   _For:=Node as TForSmtAST;
   end;
if Node Is TWhileSmtAST then
   begin
   Kind:=ksWhile;
   _While:=Node as TWhileSmtAST;
   end;
if Node Is TRepeatSmtAST then
   begin
   Kind:=ksRepeat;
   _Repeat:=Node as TRepeatSmtAST;
   end;
if Node Is TIfSmtAST then
   begin
   Kind:=ksIf;
   _if:=Node as TIfSmtAST;
   end;
if Node Is TCaseSmtAST then
   begin
   Kind:=ksCase;
   _Case:=Node as TCaseSmtAST;
   end;
if Node Is TLabelingStatementAST then
   begin
   Kind:=ksLabelingStatement;
   LabelingStatement:=Node as TLabelingStatementAST;
   end;
if Node Is TCompaundStatementAST then
   begin
   Kind:=ksCompaundStatement;
   CompaundStatement:=Node as TCompaundStatementAST;
   end;
end;

{ TCompaundStatementAST }

procedure TCompaundStatementAST.Add(Node: TNodeAST);
begin
Setlength(ListStatemt,Length(ListStatemt)+1);
ListStatemt[Length(ListStatemt)]:=Node as TStatementAST;
end;

{ TNodeAST }

procedure TNodeAST.AddChild(Child: TNodeAST);
begin
SetLength(Childs,Length(Childs)+1);
Childs[Length(Childs)]:=Child;
Child.Parent:=Self;
end;

{ TCaseSmtAST }

procedure TCaseSmtAST.Add(var Node: TNodeAST);
begin
if Node is TCaseEdgeAST then
   begin
   SetLength(FEdges,Length(FEdges)+1);
   FEdges[Length(FEdges)-1]:=Node as TCaseEdgeAST;
   end;
end;

{ TLabelingStatementAST }

procedure TLabelingStatementAST.Add(var Node: TNodeAST);
begin
if Node is TLabelingStatementAST then
   begin
   Kind:=ksLabeling;
   LabelingStatement:=Node as TLabelingStatementAST;
   end;
if Node is TSimpleStatementAST then
   begin
   Kind:=ksSimple;
   SimpleStatement:=Node as TSimpleStatementAST;
   end;
end;

{ TCallParametrsAST }

procedure TCallParametrsAST.Add(Indent: TNodeAST);
begin
  SetLength(Items,Length(Items)+1);
  Items[Length(Items)-1]:=Indent as TVarDeclAST; 
end;

{ TFactorAST }

procedure TFactorAST.Add(var Node: TNodeAST);
begin
Self.Node:=Node;
Self.Kind:=fkUndefine;
if Node is TDesignatorAST then Self.Kind:=fkDesignator;
{???}if Node is TconstAST then Self.Kind:=fkConst;
if Node is TStringAST then Self.Kind:=fkString;
if Node is TNumberAST then Self.Kind:=fkNumber;
if Node is TNilAST then Self.Kind:=fkNil;
if Node is TAddrAST then Self.Kind:=fkAddr;
end;

end.
