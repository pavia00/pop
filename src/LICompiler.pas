{� 2017 Pavia. All Rights Reserved, by Pavia.}
unit LICompiler;

interface

type
  ILog=interface
    function GetFilePath:String;
    procedure SetFilePath(const Value: String);
    property FilePath:String Read GetFilePath Write SetFilePath;
  end;

  IConnectFace = interface
    procedure Bind(Obj:TObject);
  end;

  ICompilerWoker= interface(IConnectFace)
    procedure reset;
    procedure process;
    function LastPosRow:Integer;
    function LastPosCol:Integer;
  end;


  TCompilerWoker=class(TInterfacedObject, ICompilerWoker)
  protected
    FLastPosRow:Integer;
    FLastPosCol:Integer;
    AObj:TCompilerWoker;
  public
    procedure reset; virtual;
    procedure process; virtual;
    function LastPosRow:Integer; virtual;
    function LastPosCol:Integer; virtual;
    procedure Bind(obj:TObject);
  end;

  ICompilerError= interface
    procedure Add(Code:Integer; SubText:String);
    procedure DoDestroy;
    function GetCount:Integer;
    procedure SetCount(const Value: Integer);
    function GetCompilerWoker:TCompilerWoker;
    procedure SetCompilerWoker(const Value: TCompilerWoker);
    property Count:Integer Read GetCount Write SetCount;
    property CompilerWoker:TCompilerWoker Read GetCompilerWoker Write SetCompilerWoker;
  end;

  ILexAnaliser= interface
    function GetLine:Integer;
    function GetLinePos:Integer;
    property Line:Integer read GetLine;
    property LinePos:Integer read GetLinePos;
  end;

  IGramAnaliser= interface
  end;


  IASTFabric= interface
  end;

  ICompiler= interface
    function GetLog:ILog;
    function GetError:ICompilerError;
//    function GetSimvler:ILexAnaliser;
    property Log:ILog read GetLog;
 //   property Simvler:ILexAnaliser read GetSimvler;
    property Error:ICompilerError read GetError;
  end;



implementation

{ TCompilerWoker }

procedure TCompilerWoker.Bind(obj: TObject);
begin
  if obj is TCompilerWoker then AObj:=TCompilerWoker(obj)
   else Aobj:=Self;
end;

function TCompilerWoker.LastPosCol: Integer;
begin
  if (Aobj<>Self) and (Aobj<>nil)then Result:=Aobj.LastPosCol
    else  Result:=FLastPosCol;
end;

function TCompilerWoker.LastPosRow: Integer;
begin
  if (Aobj<>Self) and (Aobj<>nil)  then Result:=Aobj.LastPosRow
    else  Result:=FLastPosRow;
end;

procedure TCompilerWoker.process;
begin
  if (Aobj<>Self) and (Aobj<>nil) then AObj.process;
end;

procedure TCompilerWoker.reset;
begin
  if (Aobj<>Self) and (Aobj<>nil) then AObj.reset;

end;

end.
